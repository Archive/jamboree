# translation of jamboree.HEAD.po to Arabic
# Arabic translations for PACKAGE package.
# Copyright (C) 2006 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2006.
# Khaled Hosny <khaledhosny@eglug.org>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: jamboree.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-11-17 21:14+0000\n"
"PO-Revision-Date: 2006-11-18 13:05+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <support@eglug.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n\n"
"X-Poedit-Language: Arabic\n"
"X-Generator: KBabel 1.11.4\n"

#: ../data/glade/jamboree.glade.h:1
#, fuzzy
msgid " "
msgstr " "

#: ../data/glade/jamboree.glade.h:2
#, fuzzy
msgid "    "
msgstr "    "

#: ../data/glade/jamboree.glade.h:3
#, fuzzy
msgid "<b>Saving the song:</b>"
msgstr "جاري حفظ الصورة"

#: ../data/glade/jamboree.glade.h:4
#, fuzzy
msgid "<b>Searching the folder:</b>"
msgstr "افتح مجلد %s"

#: ../data/glade/jamboree.glade.h:5
#, fuzzy
msgid "<b>Visible Columns</b>"
msgstr "%s أعمدة مرئية"

#: ../data/glade/jamboree.glade.h:6
msgid "<span weight=\"bold\" size=\"larger\">Adding songs to the music library</span>"
msgstr ""

#: ../data/glade/jamboree.glade.h:7
msgid "<span weight=\"bold\" size=\"larger\">Saving song properties</span>"
msgstr ""

#: ../data/glade/jamboree.glade.h:8
msgid "Ar_tist"
msgstr ""

#: ../data/glade/jamboree.glade.h:9
#, fuzzy
msgid "Choose Visible Columns"
msgstr "عدد خلايا اﻷعمدة"

#: ../data/glade/jamboree.glade.h:10
msgid "Jamboree"
msgstr ""

#: ../data/glade/jamboree.glade.h:11
#, fuzzy
msgid "Progress"
msgstr "تقدُّم"

#: ../data/glade/jamboree.glade.h:12
#, fuzzy
msgid "Toggle random playing"
msgstr "لعب اللوحة %dx%d"

#: ../data/glade/jamboree.glade.h:13
#, fuzzy
msgid "Toggle repeated playing"
msgstr "لعب اللوحة %dx%d"

#: ../data/glade/jamboree.glade.h:14
#, fuzzy
msgid "Track _number"
msgstr "الرّقم. فنّان المقطوعة - عنوان المقطوعة"

#: ../data/glade/jamboree.glade.h:15
#, fuzzy
msgid "_Album"
msgstr "الألبوم:"

#: ../data/glade/jamboree.glade.h:16
#, fuzzy
msgid "_Duration"
msgstr "<b>المدة:</b>"

#: ../data/glade/jamboree.glade.h:17
#, fuzzy
msgid "_Genre"
msgstr "ال_نوع:"

#: ../data/glade/jamboree.glade.h:18
#, fuzzy
msgid "_Last played"
msgstr "الاسم الأخير"

#: ../data/glade/jamboree.glade.h:19
msgid "_Playcount"
msgstr ""

#: ../data/glade/jamboree.glade.h:20
#, fuzzy
msgid "_Rating"
msgstr "ترتيب ISO للسرعة"

#: ../data/glade/jamboree.glade.h:21
#, fuzzy
msgid "_Search:"
msgstr "_بحث:"

#: ../data/glade/jamboree.glade.h:22
#, fuzzy
msgid "_Year"
msgstr "السّنة"

#: ../data/glade/smart-playlist-dialog.glade.h:1
#, fuzzy
msgid "*"
msgstr "*"

#: ../data/glade/smart-playlist-dialog.glade.h:2
#: ../src/smart-playlist-dialog.c:187
#, fuzzy
msgid "GB"
msgstr "%.1f جيجابايت"

#: ../data/glade/smart-playlist-dialog.glade.h:3
#, fuzzy
msgid "Limit to"
msgstr "مسجل ل %s"

#: ../data/glade/smart-playlist-dialog.glade.h:4
#: ../src/smart-playlist-dialog.c:186
#, fuzzy
msgid "MB"
msgstr "مب"

#: ../data/glade/smart-playlist-dialog.glade.h:5
#, fuzzy
msgid "Match"
msgstr "<b>مطابقة</b>"

#: ../data/glade/smart-playlist-dialog.glade.h:6
#, fuzzy
msgid "Playlist name:"
msgstr "اسم الموصول"

#: ../data/glade/smart-playlist-dialog.glade.h:7
#: ../src/smart-playlist-dialog.c:1039
#, fuzzy
msgid "Smart Playlist"
msgstr "حفظ قائمة التّشغيل"

#: ../data/glade/smart-playlist-dialog.glade.h:8
#: ../src/smart-playlist-dialog.c:192
#, fuzzy
msgid "all"
msgstr "الكل"

#: ../data/glade/smart-playlist-dialog.glade.h:9
#: ../src/smart-playlist-dialog.c:191
#, fuzzy
msgid "any"
msgstr "أيّ"

#: ../data/glade/smart-playlist-dialog.glade.h:10
#: ../src/smart-playlist-dialog.c:196
#, fuzzy
msgid "date added"
msgstr "سي-في-إس-مضاف"

#. FIXME: i18n, need to separate those from the ones above
#: ../data/glade/smart-playlist-dialog.glade.h:11
#: ../src/smart-playlist-dialog.c:162
#: ../src/smart-playlist-dialog.c:172
#: ../src/smart-playlist-dialog.c:185
#, fuzzy
msgid "hours"
msgstr "ساعات"

#: ../data/glade/smart-playlist-dialog.glade.h:12
#: ../src/smart-playlist-dialog.c:198
#, fuzzy
msgid "least recently played"
msgstr "إحصائيّات اللّعب الملعوبة"

#: ../data/glade/smart-playlist-dialog.glade.h:13
#: ../src/smart-playlist-dialog.c:161
#: ../src/smart-playlist-dialog.c:171
#: ../src/smart-playlist-dialog.c:184
#, fuzzy
msgid "minutes"
msgstr "دقائق"

#: ../data/glade/smart-playlist-dialog.glade.h:14
#: ../src/smart-playlist-dialog.c:197
#, fuzzy
msgid "most recently played"
msgstr "إحصائيّات اللّعب الملعوبة"

#: ../data/glade/smart-playlist-dialog.glade.h:15
#: ../src/smart-playlist-dialog.c:200
#, fuzzy
msgid "my rating"
msgstr "calendar:MY"

#: ../data/glade/smart-playlist-dialog.glade.h:16
#, fuzzy
msgid "of the following conditions:"
msgstr "ال_حالة الحالية"

#: ../data/glade/smart-playlist-dialog.glade.h:17
#: ../src/smart-playlist-dialog.c:202
#, fuzzy
msgid "quality"
msgstr "جودة الصورة"

#: ../data/glade/smart-playlist-dialog.glade.h:18
#: ../src/smart-playlist-dialog.c:203
#, fuzzy
msgid "random"
msgstr "عشوائي"

#: ../data/glade/smart-playlist-dialog.glade.h:19
#, fuzzy
msgid "selected by"
msgstr "المطورون"

#: ../data/glade/smart-playlist-dialog.glade.h:20
#: ../src/smart-playlist-dialog.c:183
msgid "songs"
msgstr ""

#: ../data/glade/smart-playlist-dialog.glade.h:21
#: ../src/smart-playlist-dialog.c:204
#, fuzzy
msgid "year"
msgstr "السّنة"

#: ../jamboree.desktop.in.h:1
#, fuzzy
msgid "Jamboree Music Player"
msgstr "ترقّب اللاعب القيم"

#: ../jamboree.desktop.in.h:2
#, fuzzy
msgid "Listen to your music"
msgstr "خلع موسيقاك من أقراصك المدمجة"

#: ../jamboree.schemas.in.h:1
#, fuzzy
msgid "Active song view"
msgstr "عرض الفيديو الافتراضي"

#: ../jamboree.schemas.in.h:2
msgid "Default folder to add music from"
msgstr ""

#: ../jamboree.schemas.in.h:3
#, fuzzy
msgid "Height of main window"
msgstr "ارتفاع النّافذة الرّئيسيّة."

#: ../jamboree.schemas.in.h:4
#, fuzzy
msgid "Horizontal position of main window"
msgstr "موضع النافذة الرئيسية على الشاشة"

#: ../jamboree.schemas.in.h:5
#, fuzzy
msgid "Position of the browse pane"
msgstr "إظهار لوح \"المعاينة\""

#: ../jamboree.schemas.in.h:6
#, fuzzy
msgid "Position of the playlist pane"
msgstr "إظهار لوح \"المعاينة\""

#: ../jamboree.schemas.in.h:7
#, fuzzy
msgid "Repeat the playlist"
msgstr "حفظ قائمة التّشغيل"

#: ../jamboree.schemas.in.h:8
#, fuzzy
msgid "Show album column"
msgstr "اظهر جدول العنوان فقط"

#: ../jamboree.schemas.in.h:9
#, fuzzy
msgid "Show artist column"
msgstr "اظهر جدول العنوان فقط"

#: ../jamboree.schemas.in.h:10
#, fuzzy
msgid "Show genre column"
msgstr "اظهر جدول العنوان فقط"

#: ../jamboree.schemas.in.h:11
#, fuzzy
msgid "Show last played column"
msgstr "اظهر جدول العنوان فقط"

#: ../jamboree.schemas.in.h:12
#, fuzzy
msgid "Show play count column"
msgstr "اظهر جدول العنوان فقط"

#: ../jamboree.schemas.in.h:13
#, fuzzy
msgid "Show time column"
msgstr "اظهار وقت آ_خر تعديل"

#: ../jamboree.schemas.in.h:14
#, fuzzy
msgid "Show track number column"
msgstr "الرّقم. فنّان المقطوعة - عنوان المقطوعة"

#: ../jamboree.schemas.in.h:15
#, fuzzy
msgid "Show year column"
msgstr "اظهر جدول العنوان فقط"

#: ../jamboree.schemas.in.h:16
#, fuzzy
msgid "Shuffle the playlist"
msgstr "حفظ قائمة التّشغيل"

#: ../jamboree.schemas.in.h:17
#, fuzzy
msgid "Vertical position of main window"
msgstr "موضع النافذة الرئيسية على الشاشة"

#: ../jamboree.schemas.in.h:18
#, fuzzy
msgid "Volume setting"
msgstr "<b>تعيينات المؤشر</b>"

#: ../jamboree.schemas.in.h:19
msgid "Whether to display Jamboree with a minimal interface"
msgstr ""

#: ../jamboree.schemas.in.h:20
#, fuzzy
msgid "Width of main window"
msgstr "عرض النّافذة الرّئيسيّة."

#: ../src/main-window.c:300
#, fuzzy
msgid "_File"
msgstr "_ملف"

#: ../src/main-window.c:301
#, fuzzy
msgid "_View"
msgstr "_اعرض"

#. Actions menu
#: ../src/main-window.c:302
#: ../src/main-window.c:329
#, fuzzy
msgid "_Play"
msgstr "العب"

#: ../src/main-window.c:303
#, fuzzy
msgid "_Help"
msgstr "م_ساعدة"

#. File menu
#: ../src/main-window.c:306
#, fuzzy
msgid "_Add Folder"
msgstr "إضافة مجلّد"

#: ../src/main-window.c:309
#, fuzzy
msgid "_New Playlist"
msgstr "حفظ قائمة التّشغيل"

#: ../src/main-window.c:312
#, fuzzy
msgid "New _Smart Playlist"
msgstr "اختر نوع القائمة"

#: ../src/main-window.c:315
#, fuzzy
msgid "_Edit Smart Playlist"
msgstr "اختر نوع القائمة"

#. View menu
#: ../src/main-window.c:322
#, fuzzy
msgid "_Visible Columns"
msgstr "%s أعمدة مرئية"

#: ../src/main-window.c:325
msgid "_Show other songs by this artist"
msgstr ""

#: ../src/main-window.c:329
#, fuzzy
msgid "Start"
msgstr "إبدأ"

#: ../src/main-window.c:332
#, fuzzy
msgid "_Stop"
msgstr "تو_قف"

#: ../src/main-window.c:332
#, fuzzy
msgid "Stop"
msgstr "توقّف"

#: ../src/main-window.c:335
#, fuzzy
msgid "_Previous"
msgstr "_السابق"

#: ../src/main-window.c:335
#, fuzzy
msgid "Previous song"
msgstr "الصّ_ورة السّابقة"

#: ../src/main-window.c:338
#, fuzzy
msgid "_Next"
msgstr "ال_تالي"

#: ../src/main-window.c:338
#, fuzzy
msgid "Next song"
msgstr "الصّورة ال_تّالية"

#: ../src/main-window.c:341
#, fuzzy
msgid "_Reset Playcount"
msgstr "إعا_دة ضبط الخطوط"

#: ../src/main-window.c:350
#, fuzzy
msgid "_Random"
msgstr "عشوائي"

#: ../src/main-window.c:350
#: ../src/main-window.c:3305
#, fuzzy
msgid "Random"
msgstr "عشوائي"

#: ../src/main-window.c:352
#, fuzzy
msgid "_Repeat"
msgstr "إعادة"

#: ../src/main-window.c:352
#: ../src/main-window.c:3315
#, fuzzy
msgid "Repeat"
msgstr "إعادة"

#: ../src/main-window.c:609
#, c-format
msgid "%d song"
msgid_plural "%d songs"
msgstr[0] ""
msgstr[1] ""

#: ../src/main-window.c:651
#, fuzzy
msgid "All (... genres)"
msgstr "نسخ ال_كل"

#: ../src/main-window.c:686
#, fuzzy
msgid "All (... albums)"
msgstr "نسخ ال_كل"

#: ../src/main-window.c:720
#, fuzzy
msgid "All (... artists)"
msgstr "فناني جنوم"

#: ../src/main-window.c:930
#: ../src/main.c:140
#, fuzzy
msgid "Pause"
msgstr "ايقاف مؤقّت"

#: ../src/main-window.c:937
#, fuzzy
msgid "Play"
msgstr "العب"

#: ../src/main-window.c:1293
#: ../src/main-window.c:1296
#: ../src/main-window.c:3478
#: ../src/main-window.c:3508
#, fuzzy
msgid "Unknown"
msgstr "مجهول"

#: ../src/main-window.c:1298
#, fuzzy, c-format
msgid "%s by %s"
msgstr "المطورون"

#: ../src/main-window.c:1307
#, c-format
msgid "%s - %s"
msgstr ""

#: ../src/main-window.c:1326
#, fuzzy
msgid "0:00"
msgstr "00/00/00"

#: ../src/main-window.c:1804
#, fuzzy
msgid "Show Song Information"
msgstr "عرض خيارات معلومات النّصوص"

#: ../src/main-window.c:2342
#: ../src/main-window.c:2356
#, fuzzy
msgid "New playlist"
msgstr "حفظ قائمة التّشغيل"

#: ../src/main-window.c:2392
#, fuzzy
msgid "Add Music Folder"
msgstr "إضافة مجلّد إلى المحفوظة"

#: ../src/main-window.c:2502
msgid "Yes, there is a Swedish conspiracy"
msgstr ""

#: ../src/main-window.c:2503
#, fuzzy
msgid "A music player for GNOME"
msgstr "عازف أقراص مدمجة لجنوم"

#: ../src/main-window.c:2504
#, fuzzy
msgid "translator-credits"
msgstr "Yousef Raffah يوسف رفه (yousef@raffah.com)"

#: ../src/main-window.c:2603
#: ../src/tag-reader.c:91
#, fuzzy
msgid "Invalid Unicode"
msgstr " (يونيكود غير صحيح)"

#: ../src/main-window.c:2702
#, fuzzy, c-format
msgid "All (%d genre)"
msgid_plural "All (%d genres)"
msgstr[0] "نسخ ال_كل"
msgstr[1] ""

#: ../src/main-window.c:2711
#, fuzzy, c-format
msgid "All (%d artist)"
msgid_plural "All (%d artists)"
msgstr[0] "فنان مجهول"
msgstr[1] ""

#: ../src/main-window.c:2720
#, fuzzy, c-format
msgid "All (%d album)"
msgid_plural "All (%d albums)"
msgstr[0] "ألبوم مجهول"
msgstr[1] ""

#: ../src/main-window.c:3169
#: ../src/smart-playlist-dialog.c:122
#: ../src/source-view.c:210
#, fuzzy
msgid "Artist"
msgstr "الفنان"

#: ../src/main-window.c:3213
#: ../src/smart-playlist-dialog.c:127
#: ../src/source-view.c:211
#, fuzzy
msgid "Genre"
msgstr "ال_نوع:"

#: ../src/main-window.c:3259
#: ../src/smart-playlist-dialog.c:121
#: ../src/source-view.c:209
#, fuzzy
msgid "Album"
msgstr "الألبوم:"

#: ../src/main-window.c:3481
#: ../src/main-window.c:3511
#, fuzzy, c-format
msgid "Could not play the song \"%s\""
msgstr "لم يتمكن طوطم من عزف '%s'."

#: ../src/main-window.c:3525
#, fuzzy
msgid "_Try again"
msgstr "حاول مرة أخرى"

#: ../src/main.c:131
#, fuzzy
msgid "Print version"
msgstr "صورة للطباعة"

#: ../src/main.c:134
#, fuzzy
msgid "Alternative song database file"
msgstr "ملف الإعدادات الإفتراضية البديل"

#: ../src/main.c:134
#, fuzzy
msgid "FILE"
msgstr "الملف"

#: ../src/main.c:137
#, fuzzy
msgid "Start playing"
msgstr "بدأ التشغيل حالا"

#: ../src/main.c:143
msgid "Toggle play and pause mode"
msgstr ""

#: ../src/main.c:146
#, fuzzy
msgid "Stop playing"
msgstr "عرض فيلم"

#: ../src/main.c:149
#, fuzzy
msgid "Jump to the next song"
msgstr "إذهب إلى الصفحة التالية"

#: ../src/main.c:152
#, fuzzy
msgid "Jump to the previous song"
msgstr "إذهب إلى الصفحة السابقة"

#: ../src/main.c:155
msgid "Start without a visible window"
msgstr ""

#: ../src/main.c:158
#, fuzzy
msgid "Toggle visibility of the window"
msgstr "تحويل نافذة القاموس"

#: ../src/main.c:161
#, fuzzy
msgid "Show all songs"
msgstr "أظهر كل الأعمال"

#: ../src/main.c:164
msgid "Select the currently playing artist"
msgstr ""

#: ../src/main.c:167
msgid "Select the currently playing album"
msgstr ""

#: ../src/main.c:170
msgid "Quit any running instance"
msgstr ""

#: ../src/main.c:293
#, fuzzy
msgid "Could not read song database"
msgstr "لم يمكن قراءة التّحيّة من %s: %s"

#: ../src/main.c:294
msgid "Jamboree is most likely already running."
msgstr ""

#: ../src/player.c:226
#: ../src/player.c:618
#, fuzzy
msgid "The audio device is busy."
msgstr "المستخدم المحلي مشغول"

#: ../src/player.c:593
msgid "Internal error, please check your GStreamer installation."
msgstr ""

#. { N_("Bit rate"),      VARIABLE_BIT_RATE,      CONSTANT_TYPE_INT, },
#. { N_("Comment"),       VARIABLE_COMMENT,       CONSTANT_TYPE_STRING },
#: ../src/smart-playlist-dialog.c:125
#, fuzzy
msgid "Date Added"
msgstr "سي-في-إس-مضاف"

#: ../src/smart-playlist-dialog.c:126
#, fuzzy
msgid "Date Modified"
msgstr "تاريخ التعديل"

#. { N_("Kind"),          VARIABLE_KIND,          CONSTANT_TYPE_STRING },
#: ../src/smart-playlist-dialog.c:129
#: ../src/source-view.c:216
#, fuzzy
msgid "Last Played"
msgstr "الاسم الأخير"

#. { N_("My Rating"),     VARIABLE_RATING,        CONSTANT_TYPE_INT },
#: ../src/smart-playlist-dialog.c:131
#: ../src/source-view.c:215
#, fuzzy
msgid "Play Count"
msgstr "عد الرموز "

#. { N_("Sample Rate"),   VARIABLE_SAMPLE_RATE,   CONSTANT_TYPE_INT },
#. { N_("Size"),          VARIABLE_SIZE,          CONSTANT_TYPE_SIZE },
#: ../src/smart-playlist-dialog.c:134
#, fuzzy
msgid "Song Name"
msgstr "اسم الموصول"

#. { N_("Time"),          VARIABLE_TIME,          CONSTANT_TYPE_TIME },
#: ../src/smart-playlist-dialog.c:136
#, fuzzy
msgid "Track Number"
msgstr "الرّقم. فنّان المقطوعة - عنوان المقطوعة"

#: ../src/smart-playlist-dialog.c:137
#: ../src/source-view.c:213
#, fuzzy
msgid "Year"
msgstr "السّنة"

#: ../src/smart-playlist-dialog.c:141
#: ../src/smart-playlist-dialog.c:149
#, fuzzy
msgid "is"
msgstr "هو"

#: ../src/smart-playlist-dialog.c:142
#: ../src/smart-playlist-dialog.c:150
#, fuzzy
msgid "is not"
msgstr "ليس"

#: ../src/smart-playlist-dialog.c:143
#, fuzzy
msgid "is greater than"
msgstr "أكبر من"

#: ../src/smart-playlist-dialog.c:144
#, fuzzy
msgid "is less than"
msgstr "أصغر من"

#: ../src/smart-playlist-dialog.c:145
#, fuzzy
msgid "is in the range"
msgstr "%s غير مثبت في المسار."

#: ../src/smart-playlist-dialog.c:151
#, fuzzy
msgid "contains"
msgstr "يحتوي"

#: ../src/smart-playlist-dialog.c:152
#, fuzzy
msgid "does not contain"
msgstr "لا يحتوي"

#: ../src/smart-playlist-dialog.c:156
#, fuzzy
msgid "is in the last"
msgstr "%s غير مثبت في المسار."

#: ../src/smart-playlist-dialog.c:157
#, fuzzy
msgid "is not in the last"
msgstr "%s غير مثبت في المسار."

#: ../src/smart-playlist-dialog.c:163
#, fuzzy
msgid "days"
msgstr "الأيّام"

#: ../src/smart-playlist-dialog.c:164
#, fuzzy
msgid "weeks"
msgstr "أسابيع"

#: ../src/smart-playlist-dialog.c:165
#, fuzzy
msgid "months"
msgstr "أشهر"

#: ../src/smart-playlist-dialog.c:170
#, fuzzy
msgid "seconds"
msgstr "ثوان"

#: ../src/smart-playlist-dialog.c:177
#, fuzzy
msgid "kb"
msgstr "الحجم (ك.بايت)"

#: ../src/smart-playlist-dialog.c:178
#, fuzzy
msgid "Mb"
msgstr "مب"

#: ../src/smart-playlist-dialog.c:179
#, fuzzy
msgid "Gb"
msgstr "%.1f جيجابايت"

#: ../src/smart-playlist-dialog.c:199
#, fuzzy
msgid "last played"
msgstr "الاسم الأخير"

#: ../src/smart-playlist-dialog.c:201
#, fuzzy
msgid "play count"
msgstr "عد الرموز "

#: ../src/smart-playlist-dialog.c:695
#, fuzzy
msgid "to"
msgstr "إلى:"

#: ../src/source-database.c:202
#, fuzzy
msgid "All songs"
msgstr "نسخ ال_كل"

#. Cached data from the file.
#: ../src/source-database.c:371
#, fuzzy
msgid "Invalid title"
msgstr "عنوان ال_صفحة"

#: ../src/source-database.c:377
#, fuzzy
msgid "Invalid artist name"
msgstr "اسم صورة غير صحيح: \"%s\"."

#: ../src/source-database.c:383
#, fuzzy
msgid "Invalid album name"
msgstr "اسم صورة غير صحيح: \"%s\"."

#: ../src/source-database.c:389
#, fuzzy
msgid "Invalid genre name"
msgstr "اسم صورة غير صحيح: \"%s\"."

#. i18n: the # sign is used as a number sign here.
#: ../src/source-view.c:206
msgid "#"
msgstr ""

#: ../src/source-view.c:207
#, fuzzy
msgid "Title"
msgstr "العنوان"

#: ../src/source-view.c:212
#, fuzzy
msgid "Duration"
msgstr "المدّة"

#: ../src/source-view.c:214
#, fuzzy
msgid "Rating"
msgstr "ترتيب ISO للسرعة"

#: ../src/source-view.c:547
msgid "%Y-%m-%d"
msgstr ""

#: ../src/sources-view.c:432
#, fuzzy
msgid "_Rename"
msgstr "ا_عادة التسمية"

#: ../src/sources-view.c:437
#, fuzzy
msgid "_Edit smart playlist"
msgstr "اختر نوع القائمة"

#: ../src/sources-view.c:591
msgid "Playlists"
msgstr ""

#: ../src/sources-xml.c:664
#, fuzzy
msgid "Recently added"
msgstr "سي-في-إس-مضاف"

#: ../src/sources-xml.c:665
#, fuzzy
msgid "Recently played"
msgstr "إحصائيّات اللّعب الملعوبة"

#: ../src/sources-xml.c:666
#, fuzzy
msgid "Never played"
msgstr "لا تتقبل _أبدا"

#: ../src/sources-xml.c:667
#, fuzzy
msgid "Most played"
msgstr "إحصائيّات اللّعب الملعوبة"

#: ../src/sources-xml.c:668
msgid "Favorites"
msgstr ""

#: ../src/string-utils.c:255
#, fuzzy, c-format
msgid "%d second"
msgid_plural "%d seconds"
msgstr[0] "%d ثانية واحدة"
msgstr[1] ""

#: ../src/string-utils.c:260
#, fuzzy, c-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] "%d دقيقة"
msgstr[1] ""

#: ../src/string-utils.c:265
#, fuzzy, c-format
msgid "%.1f hour"
msgid_plural "%.1f hours"
msgstr[0] "_24 ساعة"
msgstr[1] ""

#: ../src/string-utils.c:270
#, fuzzy, c-format
msgid "%.1f day"
msgid_plural "%.1f days"
msgstr[0] "آخر يوم"
msgstr[1] ""

#: ../src/string-utils.c:274
#, fuzzy, c-format
msgid "%.1f week"
msgid_plural "%.1f weeks"
msgstr[0] "مشهد الأسبوع"
msgstr[1] ""

#: ../src/tag-reader.c:264
#, fuzzy
msgid "Unknown artist"
msgstr "فنان مجهول"

#: ../src/tag-reader.c:268
#, fuzzy
msgid "Unknown album"
msgstr "ألبوم مجهول"

#: ../src/volume-button.c:291
#, fuzzy
msgid "+"
msgstr "+"

#: ../src/volume-button.c:295
#, fuzzy
msgid "-"
msgstr "-"

