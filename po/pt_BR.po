# Brazilian Portuguese translation of jamboree.
# Copyright (C) 2004 Free Software Foundation, Inc.
# This file is distributed under the same license as the jamboree package.
# Everson Santos Araujo <nobios@por.com.br>, 2004.
# Raphael Higino <raphaelh@uai.com.br>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: jamboree\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-04-02 13:06-0300\n"
"PO-Revision-Date: 2005-04-02 13:16-0300\n"
"Last-Translator: Raphael Higino <raphaelh@uai.com.br>\n"
"Language-Team: Brazilian Portuguese <gnome-l10n-br@listas.cipsga.org.br>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../data/glade/jamboree.glade.h:1
msgid " "
msgstr " "

#: ../data/glade/jamboree.glade.h:2
msgid "    "
msgstr "    "

#: ../data/glade/jamboree.glade.h:3
msgid "<b>Saving the song:</b>"
msgstr "<b>Salvando a música:</b>"

#: ../data/glade/jamboree.glade.h:4
msgid "<b>Searching the folder:</b>"
msgstr "<b>Procurando na pasta:</b>"

#: ../data/glade/jamboree.glade.h:5
msgid "<b>Visible Columns</b>"
msgstr "<b>Colunas Visíveis</b>"

#: ../data/glade/jamboree.glade.h:6
msgid ""
"<span weight=\"bold\" size=\"larger\">Adding songs to the music library</"
"span>"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Adicionando músicas à biblioteca de "
"músicas</span>"

#: ../data/glade/jamboree.glade.h:7
msgid "<span weight=\"bold\" size=\"larger\">Saving song properties</span>"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Salvando as propriedades da música</"
"span>"

#: ../data/glade/jamboree.glade.h:8
msgid "Ar_tist"
msgstr "Ar_tista"

#: ../data/glade/jamboree.glade.h:9
msgid "Choose Visible Columns"
msgstr "Escolha as Colunas Visíveis"

#: ../data/glade/jamboree.glade.h:10
msgid "Jamboree"
msgstr "Jamboree"

#: ../data/glade/jamboree.glade.h:11
msgid "Toggle random playing"
msgstr "Alternar reprodução aleatória"

#: ../data/glade/jamboree.glade.h:12
msgid "Toggle repeated playing"
msgstr "Alternar reprodução repetida"

#: ../data/glade/jamboree.glade.h:13
msgid "Track _number"
msgstr "_Número da faixa"

#: ../data/glade/jamboree.glade.h:14
msgid "_Album"
msgstr "Ál_bum"

#: ../data/glade/jamboree.glade.h:15
msgid "_Duration"
msgstr "_Duração"

#: ../data/glade/jamboree.glade.h:16
msgid "_Genre"
msgstr "_Gênero"

#: ../data/glade/jamboree.glade.h:17
msgid "_Last played"
msgstr "Ú_ltima reprodução"

#: ../data/glade/jamboree.glade.h:18
msgid "_Playcount"
msgstr "Número de _reproduções"

#: ../data/glade/jamboree.glade.h:19
msgid "_Rating"
msgstr "_Classificação"

#: ../data/glade/jamboree.glade.h:20
msgid "_Search:"
msgstr "_Procurar:"

#: ../data/glade/jamboree.glade.h:21
msgid "_Year"
msgstr "_Ano"

#: ../data/glade/smart-playlist-dialog.glade.h:1
msgid "*"
msgstr "*"

#: ../data/glade/smart-playlist-dialog.glade.h:2
#: ../src/smart-playlist-dialog.c:186
msgid "GB"
msgstr "GB"

#: ../data/glade/smart-playlist-dialog.glade.h:3
msgid "Limit to"
msgstr "Limitar a"

#: ../data/glade/smart-playlist-dialog.glade.h:4
#: ../src/smart-playlist-dialog.c:185
msgid "MB"
msgstr "MB"

#: ../data/glade/smart-playlist-dialog.glade.h:5
msgid "Match"
msgstr "Coincidir"

#: ../data/glade/smart-playlist-dialog.glade.h:6
msgid "Playlist name:"
msgstr "Nome da lista de reprodução:"

#: ../data/glade/smart-playlist-dialog.glade.h:7
#: ../src/smart-playlist-dialog.c:1038
msgid "Smart Playlist"
msgstr "Lista de Reprodução Inteligente"

#: ../data/glade/smart-playlist-dialog.glade.h:8
#: ../src/smart-playlist-dialog.c:191
msgid "all"
msgstr "todas"

#: ../data/glade/smart-playlist-dialog.glade.h:9
#: ../src/smart-playlist-dialog.c:190
msgid "any"
msgstr "qualquer"

#: ../data/glade/smart-playlist-dialog.glade.h:10
#: ../src/smart-playlist-dialog.c:195
msgid "date added"
msgstr "data de inclusão"

#. FIXME: i18n, need to separate those from the ones above
#: ../data/glade/smart-playlist-dialog.glade.h:11
#: ../src/smart-playlist-dialog.c:161 ../src/smart-playlist-dialog.c:171
#: ../src/smart-playlist-dialog.c:184
msgid "hours"
msgstr "horas"

#: ../data/glade/smart-playlist-dialog.glade.h:12
#: ../src/smart-playlist-dialog.c:197
msgid "least recently played"
msgstr "menos recentemente reproduzida"

#: ../data/glade/smart-playlist-dialog.glade.h:13
#: ../src/smart-playlist-dialog.c:160 ../src/smart-playlist-dialog.c:170
#: ../src/smart-playlist-dialog.c:183
msgid "minutes"
msgstr "minutos"

#: ../data/glade/smart-playlist-dialog.glade.h:14
#: ../src/smart-playlist-dialog.c:196
msgid "most recently played"
msgstr "mais recentemente reproduzida"

#: ../data/glade/smart-playlist-dialog.glade.h:15
#: ../src/smart-playlist-dialog.c:199
msgid "my rating"
msgstr "minha classificação"

#: ../data/glade/smart-playlist-dialog.glade.h:16
msgid "of the following conditions:"
msgstr "nas seguintes condições:"

#: ../data/glade/smart-playlist-dialog.glade.h:17
#: ../src/smart-playlist-dialog.c:201
msgid "quality"
msgstr "qualidade"

#: ../data/glade/smart-playlist-dialog.glade.h:18
#: ../src/smart-playlist-dialog.c:202
msgid "random"
msgstr "aleatória"

#: ../data/glade/smart-playlist-dialog.glade.h:19
msgid "selected by"
msgstr "selecionada por"

#: ../data/glade/smart-playlist-dialog.glade.h:20
#: ../src/smart-playlist-dialog.c:182
msgid "songs"
msgstr "músicas"

#: ../data/glade/smart-playlist-dialog.glade.h:21
#: ../src/smart-playlist-dialog.c:203
msgid "year"
msgstr "ano"

#: ../jamboree.desktop.in.h:1
msgid "Jamboree Music Player"
msgstr "Jamboree - Reprodutor de Músicas"

#: ../jamboree.desktop.in.h:2
msgid "Listen to your music"
msgstr "Ouça suas músicas"

#: ../jamboree.schemas.in.h:1
msgid "Active song view"
msgstr "Visão da música ativa"

#: ../jamboree.schemas.in.h:2
msgid "Default folder to add music from"
msgstr "Pasta padrão para inclusão de músicas"

#: ../jamboree.schemas.in.h:3
msgid "Height of main window"
msgstr "Altura da janela principal"

#: ../jamboree.schemas.in.h:4
msgid "Horizontal position of main window"
msgstr "Posição horizontal da janela principal"

#: ../jamboree.schemas.in.h:5
msgid "Position of the browse pane"
msgstr "Posição do painel de navegação"

#: ../jamboree.schemas.in.h:6
msgid "Position of the playlist pane"
msgstr "Posição do painel da lista de reprodução"

#: ../jamboree.schemas.in.h:7
msgid "Repeat the playlist"
msgstr "Repetir a lista de reprodução"

#: ../jamboree.schemas.in.h:8
msgid "Show album column"
msgstr "Mostra a coluna álbum"

#: ../jamboree.schemas.in.h:9
msgid "Show artist column"
msgstr "Mostra a coluna artista"

#: ../jamboree.schemas.in.h:10
msgid "Show genre column"
msgstr "Mostra a coluna gênero"

#: ../jamboree.schemas.in.h:11
msgid "Show last played column"
msgstr "Mostra a coluna última reprodução"

#: ../jamboree.schemas.in.h:12
msgid "Show play count column"
msgstr "Mostra a coluna contagem de reproduções"

#: ../jamboree.schemas.in.h:13
msgid "Show time column"
msgstr "Mostra a coluna duração"

#: ../jamboree.schemas.in.h:14
msgid "Show track number column"
msgstr "Mostra a coluna número da faixa"

#: ../jamboree.schemas.in.h:15
msgid "Show year column"
msgstr "Mostra a coluna ano"

#: ../jamboree.schemas.in.h:16
msgid "Shuffle the playlist"
msgstr "Embaralha a lista de reprodução"

#: ../jamboree.schemas.in.h:17
msgid "Vertical position of main window"
msgstr "Posição vertical da janela principal"

#: ../jamboree.schemas.in.h:18
msgid "Volume setting"
msgstr "Configuração de volume"

#: ../jamboree.schemas.in.h:19
msgid "Whether to display Jamboree with a minimal interface"
msgstr "Se o Jamboree deve ser exibido com uma interface mínima"

#: ../jamboree.schemas.in.h:20
msgid "Width of main window"
msgstr "Largura da janela principal"

#: ../src/main-window.c:276
msgid "_File"
msgstr "_Arquivo"

#: ../src/main-window.c:277
msgid "_View"
msgstr "_Ver"

#. Actions menu
#: ../src/main-window.c:278 ../src/main-window.c:305
msgid "_Play"
msgstr "_Reproduzir"

#: ../src/main-window.c:279
msgid "_Help"
msgstr "Aj_uda"

#. File menu
#: ../src/main-window.c:282
msgid "_Add Folder"
msgstr "_Adicionar Pasta"

#: ../src/main-window.c:285
msgid "_New Playlist"
msgstr "_Nova Lista de Reprodução"

#: ../src/main-window.c:288
msgid "New _Smart Playlist"
msgstr "Li_sta de Reprodução Inteligente"

#: ../src/main-window.c:291
msgid "_Edit Smart Playlist"
msgstr "_Editar Lista de Reprodução Inteligente"

#. View menu
#: ../src/main-window.c:298
msgid "_Visible Columns"
msgstr "Colunas _Visíveis"

#: ../src/main-window.c:301
msgid "_Show other songs by this artist"
msgstr "_Mostrar outras músicas desse artista"

#: ../src/main-window.c:305
msgid "Start"
msgstr "Iniciar"

#: ../src/main-window.c:308
msgid "_Stop"
msgstr "_Parar"

#: ../src/main-window.c:308
msgid "Stop"
msgstr "Parar"

#: ../src/main-window.c:311
msgid "_Previous"
msgstr "_Anterior"

#: ../src/main-window.c:311
msgid "Previous song"
msgstr "Música anterior"

#: ../src/main-window.c:314
msgid "_Next"
msgstr "_Próxima"

#: ../src/main-window.c:314
msgid "Next song"
msgstr "Próxima música"

#: ../src/main-window.c:317
msgid "_Reset Playcount"
msgstr "_Zerar número de reproduções"

#: ../src/main-window.c:326
msgid "_Random"
msgstr "_Aleatória"

#: ../src/main-window.c:328
msgid "_Repeat"
msgstr "_Repetir"

#: ../src/main-window.c:579
#, c-format
msgid "%d song"
msgid_plural "%d songs"
msgstr[0] "%d música"
msgstr[1] "%d músicas"

#: ../src/main-window.c:620
msgid "All (... genres)"
msgstr "Todos (... gêneros)"

#: ../src/main-window.c:655
msgid "All (... albums)"
msgstr "Todos (... álbuns)"

#: ../src/main-window.c:689
msgid "All (... artists)"
msgstr "Todos (... artistas)"

#: ../src/main-window.c:889 ../src/main.c:140
msgid "Pause"
msgstr "Pausar"

#: ../src/main-window.c:896
msgid "Play"
msgstr "Reproduzir"

#: ../src/main-window.c:972 ../src/main-window.c:975 ../src/main-window.c:3050
#: ../src/main-window.c:3080
msgid "Unknown"
msgstr "Desconhecida"

#: ../src/main-window.c:977
#, c-format
msgid "%s by %s"
msgstr "%s de %s"

#: ../src/main-window.c:1001
msgid "0:00"
msgstr "0:00"

#: ../src/main-window.c:1477
msgid "Show Song Information"
msgstr "Mostrar Informações da Música"

#: ../src/main-window.c:1998 ../src/main-window.c:2012
msgid "New playlist"
msgstr "Nova lista de reprodução"

#: ../src/main-window.c:2048
msgid "Add Music Folder"
msgstr "Adicionar Pasta de Música"

# Por favor, não altere essa tradução.
# Eu sei que ela não está "correta", mas é uma
# brincadeira que eu faço sempre, e é até mais
# significativa que essa coisa da Conspiração Sueca
#: ../src/main-window.c:2158
msgid "Yes, there is a Swedish conspiracy"
msgstr "Sim, existe o São Geraldo 14"

#: ../src/main-window.c:2159
msgid "A music player for GNOME"
msgstr "Um reprodutor de músicas para o GNOME"

#: ../src/main-window.c:2160
msgid "translator-credits"
msgstr "Raphael Higino <raphaelh@uai.com.br>"

#: ../src/main-window.c:2255 ../src/tag-reader.c:95
msgid "Invalid Unicode"
msgstr "Unicode Inválido"

#: ../src/main-window.c:2331
#, c-format
msgid "All (%d genre)"
msgid_plural "All (%d genres)"
msgstr[0] "Todos (%d gênero)"
msgstr[1] "Todos (%d gêneros)"

#: ../src/main-window.c:2340
#, c-format
msgid "All (%d artist)"
msgid_plural "All (%d artists)"
msgstr[0] "Todos (%d artista)"
msgstr[1] "Todos (%d artistas)"

#: ../src/main-window.c:2349
#, c-format
msgid "All (%d album)"
msgid_plural "All (%d albums)"
msgstr[0] "Todos (%d álbum)"
msgstr[1] "Todos (%d álbuns)"

#: ../src/main-window.c:2794 ../src/smart-playlist-dialog.c:121
#: ../src/source-view.c:209
msgid "Artist"
msgstr "Artista"

#: ../src/main-window.c:2835 ../src/smart-playlist-dialog.c:126
#: ../src/source-view.c:210
msgid "Genre"
msgstr "Gênero"

#: ../src/main-window.c:2881 ../src/smart-playlist-dialog.c:120
#: ../src/source-view.c:208
msgid "Album"
msgstr "Álbum"

#: ../src/main-window.c:3053 ../src/main-window.c:3083
#, c-format
msgid "Could not play the song \"%s\""
msgstr "Não foi possível reproduzir a música \"%s\""

#: ../src/main-window.c:3097
msgid "_Try again"
msgstr "_Tentar novamente"

#: ../src/main.c:131
msgid "Print version"
msgstr "Imprimir versão"

#: ../src/main.c:134
msgid "Alternative song database file"
msgstr "Arquivo de banco de dados de músicas alternativo"

#: ../src/main.c:134
msgid "FILE"
msgstr "ARQUIVO"

#: ../src/main.c:137
msgid "Start playing"
msgstr "Inicia a reprodução"

#: ../src/main.c:143
msgid "Toggle play and pause mode"
msgstr "Alterna o modo de reprodução e de pausa"

#: ../src/main.c:146
msgid "Stop playing"
msgstr "Pára de reproduzir"

#: ../src/main.c:149
msgid "Jump to the next song"
msgstr "Pula para a próxima música"

#: ../src/main.c:152
msgid "Jump to the previous song"
msgstr "Pula para a música anterior"

#: ../src/main.c:155
msgid "Start without a visible window"
msgstr "Inicia sem uma janela vísivel"

#: ../src/main.c:158
msgid "Toggle visibility of the window"
msgstr "Alterna a visibilidade da janela"

#: ../src/main.c:161
msgid "Show all songs"
msgstr "Mostrar todas as músicas"

#: ../src/main.c:164
msgid "Select the currently playing artist"
msgstr "Seleciona o artista atualmente sendo reproduzido"

#: ../src/main.c:167
msgid "Select the currently playing album"
msgstr "Seleciona o álbum atualmente sendo reproduzido"

#: ../src/main.c:170
msgid "Quit any running instance"
msgstr "Sai de qualquer instância em execução"

#: ../src/main.c:295
msgid "Could not read song database"
msgstr "Não foi possível ler o banco de dados de músicas"

#: ../src/main.c:296
msgid "Jamboree is most likely already running."
msgstr "O Jamboree provavelmente já está executando."

#: ../src/player.c:268 ../src/player.c:651
msgid "The audio device is busy."
msgstr "O dispositivo de audio está ocupado."

#: ../src/player.c:614
msgid "Internal error, please check your GStreamer installation."
msgstr "Erro interno, por favor verifique sua instalação do GStreamer."

#: ../src/player.c:642
msgid "Unrecognized music format."
msgstr "Formato de música desconhecido."

#. { N_("Bit rate"),      VARIABLE_BIT_RATE,      CONSTANT_TYPE_INT, },
#. { N_("Comment"),       VARIABLE_COMMENT,       CONSTANT_TYPE_STRING },
#: ../src/smart-playlist-dialog.c:124
msgid "Date Added"
msgstr "Data de Inclusão"

#: ../src/smart-playlist-dialog.c:125
msgid "Date Modified"
msgstr "Data de Alteração"

#. { N_("Kind"),          VARIABLE_KIND,          CONSTANT_TYPE_STRING },
#: ../src/smart-playlist-dialog.c:128 ../src/source-view.c:215
msgid "Last Played"
msgstr "Última Reprodução"

#. { N_("My Rating"),     VARIABLE_RATING,        CONSTANT_TYPE_INT },
#: ../src/smart-playlist-dialog.c:130 ../src/source-view.c:214
msgid "Play Count"
msgstr "Contagem de Reproduções"

#. { N_("Sample Rate"),   VARIABLE_SAMPLE_RATE,   CONSTANT_TYPE_INT },
#. { N_("Size"),          VARIABLE_SIZE,          CONSTANT_TYPE_SIZE },
#: ../src/smart-playlist-dialog.c:133
msgid "Song Name"
msgstr "Nome da Música"

#. { N_("Time"),          VARIABLE_TIME,          CONSTANT_TYPE_TIME },
#: ../src/smart-playlist-dialog.c:135
msgid "Track Number"
msgstr "Número da Faixa"

#: ../src/smart-playlist-dialog.c:136 ../src/source-view.c:212
msgid "Year"
msgstr "Ano"

#: ../src/smart-playlist-dialog.c:140 ../src/smart-playlist-dialog.c:148
msgid "is"
msgstr "é"

#: ../src/smart-playlist-dialog.c:141 ../src/smart-playlist-dialog.c:149
msgid "is not"
msgstr "não é"

#: ../src/smart-playlist-dialog.c:142
msgid "is greater than"
msgstr "é maior que"

#: ../src/smart-playlist-dialog.c:143
msgid "is less than"
msgstr "é menor que"

#: ../src/smart-playlist-dialog.c:144
msgid "is in the range"
msgstr "está no intervalo"

#: ../src/smart-playlist-dialog.c:150
msgid "contains"
msgstr "contém"

#: ../src/smart-playlist-dialog.c:151
msgid "does not contain"
msgstr "não contém"

#: ../src/smart-playlist-dialog.c:155
msgid "is in the last"
msgstr "está no último(s)"

#: ../src/smart-playlist-dialog.c:156
msgid "is not in the last"
msgstr "não está no último(s)"

#: ../src/smart-playlist-dialog.c:162
msgid "days"
msgstr "dias"

#: ../src/smart-playlist-dialog.c:163
msgid "weeks"
msgstr "semanas"

#: ../src/smart-playlist-dialog.c:164
msgid "months"
msgstr "meses"

#: ../src/smart-playlist-dialog.c:169
msgid "seconds"
msgstr "segundos"

#: ../src/smart-playlist-dialog.c:176
msgid "kb"
msgstr "kb"

#: ../src/smart-playlist-dialog.c:177
msgid "Mb"
msgstr "Mb"

#: ../src/smart-playlist-dialog.c:178
msgid "Gb"
msgstr "Gb"

#: ../src/smart-playlist-dialog.c:198
msgid "last played"
msgstr "última reprodução"

#: ../src/smart-playlist-dialog.c:200
msgid "play count"
msgstr "contagem de reproduções"

#: ../src/smart-playlist-dialog.c:694
msgid "to"
msgstr "para"

#: ../src/source-database.c:201
msgid "All songs"
msgstr "Todas as músicas"

#. Cached data from the file.
#: ../src/source-database.c:370
msgid "Invalid title"
msgstr "Título inválido"

#: ../src/source-database.c:376
msgid "Invalid artist name"
msgstr "Nome de artista inválido"

#: ../src/source-database.c:382
msgid "Invalid album name"
msgstr "Nome de álbum inválido"

#: ../src/source-database.c:388
msgid "Invalid genre name"
msgstr "Nome de gênero inválido"

#. i18n: the # sign is used as a number sign here.
#: ../src/source-view.c:205
msgid "#"
msgstr "nº"

#: ../src/source-view.c:206
msgid "Title"
msgstr "Título"

#: ../src/source-view.c:211
msgid "Duration"
msgstr "Duração"

#: ../src/source-view.c:213
msgid "Rating"
msgstr "Classificação"

#: ../src/source-view.c:546
msgid "%Y-%m-%d"
msgstr "%Y-%m-%d"

#: ../src/sources-view.c:432
msgid "_Rename"
msgstr "_Renomear"

#: ../src/sources-view.c:437
msgid "_Edit smart playlist"
msgstr "_Editar lista de reprodução inteligente"

#: ../src/sources-view.c:591
msgid "Playlists"
msgstr "Listas de Reprodução"

#: ../src/string-utils.c:255
#, c-format
msgid "%d second"
msgid_plural "%d seconds"
msgstr[0] "%d segundo"
msgstr[1] "%d segundos"

#: ../src/string-utils.c:260
#, c-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] "%d minuto"
msgstr[1] "%d minutos"

#: ../src/string-utils.c:265
#, c-format
msgid "%.1f hour"
msgid_plural "%.1f hours"
msgstr[0] "%.1f hora"
msgstr[1] "%.1f horas"

#: ../src/string-utils.c:270
#, c-format
msgid "%.1f day"
msgid_plural "%.1f days"
msgstr[0] "%.1f dia"
msgstr[1] "%.1f dias"

#: ../src/string-utils.c:274
#, c-format
msgid "%.1f week"
msgid_plural "%.1f weeks"
msgstr[0] "%.1f semana"
msgstr[1] "%.1f semanas"

#: ../src/tag-reader.c:306
msgid "Unknown artist"
msgstr "Artista desconhecido"

#: ../src/tag-reader.c:310
msgid "Unknown album"
msgstr "Álbum desconhecido"

#: ../src/volume-button.c:291
msgid "+"
msgstr "+"

#: ../src/volume-button.c:295
msgid "-"
msgstr "-"

#~ msgid "Next"
#~ msgstr "Próxima"

#~ msgid "of"
#~ msgstr "de"

#~ msgid "_Actions"
#~ msgstr "Açõ_es"

#~ msgid "_Pause"
#~ msgstr "_Pausar"

#~ msgid "Find artist as you type"
#~ msgstr "Localiza o artista ao digitar"

#~ msgid "Find album as you type"
#~ msgstr "Localiza o álbum ao digitar"

#~ msgid "Artist: \"%s\""
#~ msgstr "Artista: \"%s\""

#~ msgid "Album: \"%s\""
#~ msgstr "Álbum: \"%s\""

#~ msgid "Artist not found: \"%s\""
#~ msgstr "Artista não encontrado: \"%s\""

#~ msgid "Album not found: \"%s\""
#~ msgstr "Álbum não encontrado: \"%s\""

#~ msgid "Random"
#~ msgstr "Aleatória"

#~ msgid "Volume"
#~ msgstr "Volume"

#~ msgid "Multiple songs selected"
#~ msgstr "Múltiplas músicas selecionadas"

#~ msgid "Go to the next song in the playlist"
#~ msgstr "Vai para a próxima música na lista de reprodução"

#~ msgid "Go to the previous song in the playlist"
#~ msgstr "Vai para a música anterior na lista de reprodução"

#~ msgid "Play the current song"
#~ msgstr "Reproduz a música atual"

#~ msgid "Height of main window in mini mode"
#~ msgstr "Altura da janela principal em modo mini"

#~ msgid "Width of main window in mini mode"
#~ msgstr "Largura da janela principal em modo mini"

#~ msgid "All (1 artist)"
#~ msgstr "Todas (1 artista)"

#~ msgid "All (1 album)"
#~ msgstr "Todas (1 álbum)"

#~ msgid "Start in mini display mode"
#~ msgstr "Iniciar em modo de mini tela"

#~ msgid "Specify a command to pass to an already running instance"
#~ msgstr ""
#~ "Especifique um comando para passar para uma instância já em execução"

#~ msgid ""
#~ "play|stop|prev|next|push-mute|pop-mute|current-song|quit|show-window|hide-"
#~ "window"
#~ msgstr ""
#~ "play|stop|prev|next|push-mute|pop-mute|current-song|quit|show-window|hide-"
#~ "window"

#~ msgid ""
#~ "Run \"%s --help\" to see a full list of available command line options.\n"
#~ msgstr ""
#~ "Execute \"%s --help\" para ver uma lista de todas as opções de linha de "
#~ "comando disponíveis.\n"

#~ msgid "Recently Played"
#~ msgstr "Reproduzidas Recentemente"

#~ msgid "Most Played"
#~ msgstr "Mais Reproduzidas"

#~ msgid "Length"
#~ msgstr "Duração"

#~ msgid "A_lbum"
#~ msgstr "Á_lbum"

#~ msgid "_Artist"
#~ msgstr "_Artista"

#~ msgid "_Length"
#~ msgstr "_Tamanho"

#~ msgid "Unnamed Playlist"
#~ msgstr "Lista de Reprodução Sem Nome"

#~ msgid "_Shuffle"
#~ msgstr "_Embaralhar"

#~ msgid "_Small Display"
#~ msgstr "_Tela Pequena"

#~ msgid "Bit rate"
#~ msgstr "Taxa de bits"

#~ msgid "Comment"
#~ msgstr "Comentário"

#~ msgid "Kind"
#~ msgstr "Tipo"

#~ msgid "My Rating"
#~ msgstr "Minha Classificação"

#~ msgid "Sample Rate"
#~ msgstr "Taxa de Amostragem"

#~ msgid "Size"
#~ msgstr "Tamanho"

#~ msgid "Time"
#~ msgstr "Duração"

#~ msgid "0 songs"
#~ msgstr "0 músicas"

#~ msgid "Gstreamer options"
#~ msgstr "Opções do Gstreamer"

#~ msgid "Don't write changes to the music database"
#~ msgstr "Não gravar as alterações no banco de dados de músicas"

#~ msgid "1 byte"
#~ msgstr "1 byte"

#~ msgid "%u bytes"
#~ msgstr "%u bytes"

#~ msgid "%.1f K"
#~ msgstr "%.1f K"

#~ msgid "%.1f MB"
#~ msgstr "%.1f MB"

#~ msgid "%.1f GB"
#~ msgstr "%.1f GB"

#~ msgid "%d h %d min and %d sec"
#~ msgstr "%d h %d min e %d seg"

#~ msgid "%d min %d sec"
#~ msgstr "%d min %d seg"

#~ msgid "%d sec"
#~ msgstr "%d seg"

#~ msgid "1 sec"
#~ msgstr "1 seg"

#~ msgid "1 second"
#~ msgstr "1 segundo"

#~ msgid "Failed to set up an audio driver; check your installation"
#~ msgstr "Falha ao configurar um driver de aúdio; verifique sua instalação"

#~ msgid "No plugin available for \"%s\", check your installation."
#~ msgstr "Nenhum plugin disponível para \"%s\", verifique sua instalação."

#~ msgid "Failed playing \"%s\", check your installation."
#~ msgstr "Falha ao reproduzir \"%s\", verifique sua instalação."

#~ msgid "Internal error, check your installation."
#~ msgstr "Erro interno, verifique sua instalação."

#~ msgid "Could not play \"%s\", check your installation."
#~ msgstr "Não foi possível resproduzir \"%s\", verifique sua instalação."

#~ msgid "     "
#~ msgstr "     "

#~ msgid "Error while sending message to Jamboree.\n"
#~ msgstr "Erro ao enviar mensagem para o Jamboree.\n"

#~ msgid "_Edit Song Columns"
#~ msgstr "_Editar colunas de música..."
