/*
 * Copyright (C) 2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SOURCE_PROXY_H__
#define __SOURCE_PROXY_H__

#include "source.h"

#define TYPE_SOURCE_PROXY            (source_proxy_get_type ())
#define SOURCE_PROXY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SOURCE_PROXY, SourceProxy))
#define SOURCE_PROXY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SOURCE_PROXY, SourceProxyClass))
#define IS_SOURCE_PROXY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SOURCE_PROXY))
#define IS_SOURCE_PROXY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SOURCE_PROXY))
#define SOURCE_PROXY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SOURCE_PROXY, SourceProxyClass))


typedef struct _SourceProxy      SourceProxy;
typedef struct _SourceProxyClass SourceProxyClass;
typedef struct _SourceProxyPriv  SourceProxyPriv;

typedef enum {
	SOURCE_PROXY_TYPE_REGULAR,
	SOURCE_PROXY_TYPE_SMART
} SourceProxyType;

struct _SourceProxy {
	Source           parent;
	SourceProxyPriv *priv;
};

struct _SourceProxyClass {
	SourceClass parent_class;
};

GType source_proxy_get_type   (void) G_GNUC_CONST;
void  source_proxy_set_source (SourceProxy *proxy,
			       Source      *source);


#endif /* __SOURCE_PROXY_H__ */
