/*
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __jamboree_marshal_MARSHAL_H__
#define __jamboree_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:VOID (jamboree-marshal.list:1) */
#define jamboree_marshal_VOID__VOID	g_cclosure_marshal_VOID__VOID

/* VOID:BOXED (jamboree-marshal.list:2) */
#define jamboree_marshal_VOID__BOXED	g_cclosure_marshal_VOID__BOXED

/* VOID:BOXED,INT (jamboree-marshal.list:3) */
extern void jamboree_marshal_VOID__BOXED_INT (GClosure     *closure,
                                              GValue       *return_value,
                                              guint         n_param_values,
                                              const GValue *param_values,
                                              gpointer      invocation_hint,
                                              gpointer      marshal_data);

/* VOID:INT (jamboree-marshal.list:4) */
#define jamboree_marshal_VOID__INT	g_cclosure_marshal_VOID__INT

/* VOID:POINTER (jamboree-marshal.list:5) */
#define jamboree_marshal_VOID__POINTER	g_cclosure_marshal_VOID__POINTER

G_END_DECLS

#endif /* __jamboree_marshal_MARSHAL_H__ */

