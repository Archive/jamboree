/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __STRING_UTILS_H__
#define __STRING_UTILS_H__

typedef struct _SharedString SharedString;
typedef struct _FormattedInt FormattedInt;

void          shared_string_init                 (void);
void          shared_string_shutdown             (void);
SharedString *shared_string_ref                  (SharedString *string);
void          shared_string_unref                (SharedString *string);
SharedString *shared_string_add                  (char         *str);
SharedString *shared_string_add_const            (const char   *str);
const char *  shared_string_get_str              (SharedString *string);
const char *  shared_string_get_collated         (SharedString *string);
const char *  shared_string_get_folded           (SharedString *string);


char *string_utils_create_collate_key   (const char *p);
char *string_utils_format_duration      (int         seconds);
char *string_utils_format_duration_long (int         seconds);


#endif /*__STRING_UTILS_H__ */
