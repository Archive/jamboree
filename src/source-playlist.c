/*
 * Copyright (C) 2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <glib/gi18n.h>
#include "source-playlist.h"
#include "song-private.h"


static void             source_playlist_class_init    (SourcePlaylistClass *klass);
static void             source_playlist_init          (SourcePlaylist      *source);
static void             playlist_finalize             (GObject             *object);
static gboolean         playlist_get_is_editable      (Source              *source);
static const GdkPixbuf *playlist_get_pixbuf           (Source              *source);
static GList *          playlist_get_songs_filtered   (Source              *source);
static char *           playlist_to_xml               (Source              *source);
static gboolean         playlist_remove_song          (Source              *source,
						       Song                *song);


#define SOURCE_PLAYLIST_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_SOURCE_PLAYLIST, SourcePlaylistPriv))
G_DEFINE_TYPE (SourcePlaylist, source_playlist, TYPE_SOURCE_PROXY);

struct _SourcePlaylistPriv {
	GdkPixbuf       *pixbuf;
	char            *name;

	int              id;
	Expr            *expr;
};

static int next_id = 0;


static void
source_playlist_class_init (SourcePlaylistClass *klass)
{
	GObjectClass *object_class;
	SourceClass  *source_class;

	object_class = (GObjectClass*) klass;
	object_class->finalize = playlist_finalize;

	source_class = (SourceClass*) klass;

	source_class->get_is_editable = playlist_get_is_editable;
	source_class->get_pixbuf = playlist_get_pixbuf;
	source_class->get_songs_filtered = playlist_get_songs_filtered;
	source_class->to_xml = playlist_to_xml;
	source_class->remove_song = playlist_remove_song;

	g_type_class_add_private (klass, sizeof (SourcePlaylistPriv));
}

static void
source_playlist_init (SourcePlaylist *playlist)
{
	SourcePlaylistPriv *priv;

	priv = SOURCE_PLAYLIST_GET_PRIVATE (playlist);
	playlist->priv = priv;
}

static void
playlist_finalize (GObject *object)
{
	SourcePlaylist     *playlist;
	SourcePlaylistPriv *priv;
	
	playlist = SOURCE_PLAYLIST (object);
	priv = playlist->priv;

	g_object_unref (priv->pixbuf);

	if (G_OBJECT_CLASS (source_playlist_parent_class)->finalize) {
		G_OBJECT_CLASS (source_playlist_parent_class)->finalize (object);
	}
}

Source *
source_playlist_new (Source     *source,
		     const char *name,
		     int         id)
{
	SourcePlaylist     *playlist;
	SourcePlaylistPriv *priv;

	playlist = g_object_new (TYPE_SOURCE_PLAYLIST, NULL);

	priv = playlist->priv;

	source_proxy_set_source (SOURCE_PROXY (playlist), source);

	if (id == -1) {
		/* Get the next free id. */
		id = ++next_id;
	} else {
		next_id = id;
	}
	
	source_set_name (SOURCE (playlist), name);
	source_playlist_set_id (playlist, id);
	
	return SOURCE (playlist);
}

static gboolean
playlist_get_is_editable (Source *source)
{
	return TRUE;
}

static const GdkPixbuf *
playlist_get_pixbuf (Source *source)
{
	SourcePlaylist     *playlist;
	SourcePlaylistPriv *priv;
	const char         *filename;

	playlist = SOURCE_PLAYLIST (source);
	priv = playlist->priv;

	if (!priv->pixbuf) {
		filename = DATADIR "/jamboree/jamboree-playlist-regular.png";
		priv->pixbuf = gdk_pixbuf_new_from_file (filename, NULL);
	}
	
	return priv->pixbuf;
}

void
source_playlist_set_id (SourcePlaylist *playlist, int id)
{
	SourcePlaylistPriv *priv;
	Expr               *expr, *v, *c;

	priv = playlist->priv;
	
	priv->id = id;

	v = expr_new_variable (VARIABLE_PLAYLISTS);
	c = expr_new_constant (constant_int_new (id));
	
	expr = expr_new_binary (EXPR_OP_HAS_PLAYLIST, v, c);

	if (priv->expr) {
		expr_free (priv->expr);
	}
	
	priv->expr = expr;
}

int
source_playlist_get_id (SourcePlaylist *playlist)
{
	return playlist->priv->id;
}

static GList *
playlist_get_songs_filtered (Source *source)
{
	SourcePlaylist     *playlist;
	SourcePlaylistPriv *priv;
	GList              *songs, *l;
	GList              *filtered;
	Song               *song;
	Expr               *browse_expr;
	Expr               *search_expr;

	playlist = SOURCE_PLAYLIST (source);
	priv = playlist->priv;
	
	songs = source_get_songs (source);

	browse_expr = source_get_browse_expr (source);
	search_expr = source_get_search_expr (source);

	filtered = NULL;
	for (l = songs; l; l = l->next) {
		song = l->data;

		if ((!priv->expr || expr_evaluate (priv->expr, song)) &&
		    (!browse_expr || expr_evaluate (browse_expr, song)) &&
		    (!search_expr || expr_evaluate (search_expr, song))) {
			filtered = g_list_prepend (filtered, song);
		}
	}
	
	return g_list_reverse (filtered);
}

static char *
playlist_to_xml (Source *source)
{
	SourcePlaylist     *playlist;
	SourcePlaylistPriv *priv;
	char               *tmp, *str;

	playlist = SOURCE_PLAYLIST (source);
	priv = playlist->priv;

	tmp = g_markup_escape_text (source_get_name (source), -1);

	str = g_strdup_printf ("  <regular-playlist id='%d' name='%s'/>\n",
			       priv->id,
			       tmp);

	g_free (tmp);

	return str;
}

static gboolean
playlist_remove_song (Source *source,
		      Song   *song)
{
	SourcePlaylist     *playlist;
	SourcePlaylistPriv *priv;

	playlist = SOURCE_PLAYLIST (source);
	
	priv = playlist->priv;

	if (!_song_remove_playlist (song, priv->id)) {
		return FALSE;
	}

 	/* FIXME: add when we ref the songs here, song_unref (song); */
  
	return TRUE;
}

