/* -*- mode: C; c-file-style: "gnu" -*- */
/*
 * Copyright (C) 2003 Richard Hult <richard@imendio.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __DBUS_H__
#define __DBUS_H__

#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>

#define JAMBOREE_DBUS_SERVICE "com.imendio.jamboree"

#define JAMBOREE_PLAYER_INTERFACE "com.imendio.jamboree.Player"
#define JAMBOREE_PLAYER_OBJECT "/com/imendio/jamboree/Player"

/* Methods */
#define JAMBOREE_PLAYER_PLAY  "Play"
#define JAMBOREE_PLAYER_PAUSE "Pause"
#define JAMBOREE_PLAYER_STOP  "Stop"
#define JAMBOREE_PLAYER_PREV  "Prev"
#define JAMBOREE_PLAYER_NEXT  "Next"
#define JAMBOREE_PLAYER_QUIT  "Quit"

#define JAMBOREE_PLAYER_PLAY_SONG  "PlaySong"

#define JAMBOREE_PLAYER_GET_CURRENT_SONG      "get_CurrentSong"
#define JAMBOREE_PLAYER_GET_ALL_SONGS         "get_AllSongs"
#define JAMBOREE_PLAYER_GET_WINDOW_VISIBILITY "get_WindowVisibility"
#define JAMBOREE_PLAYER_SET_WINDOW_VISIBILITY "set_WindowVisibility"
#define JAMBOREE_PLAYER_GET_RANDOM            "get_Random"
#define JAMBOREE_PLAYER_SET_RANDOM            "set_Random"
#define JAMBOREE_PLAYER_GET_REPEAT            "get_Repeat"
#define JAMBOREE_PLAYER_SET_REPEAT            "set_Repeat"

/* Signals */
#define JAMBOREE_PLAYER_PLAYING "Playing"
#define JAMBOREE_PLAYER_STATE   "State"



void         jamboree_dbus_init_handler               (void);
gboolean     jamboree_dbus_init_service               (void);
gboolean     jamboree_dbus_send_remote_cmd            (const gchar  *msg,
						       gboolean     *invalid_arg);
DBusMessage *jamboree_dbus_send_remote_cmd_with_reply (const gchar  *msg);
Song        *jamboree_dbus_message_to_song            (DBusMessage  *message);
void         jamboree_dbus_emit_playing_song          (Song         *song);
void         jamboree_dbus_emit_state                 (PlayingState  state);

#endif /* __DBUS_H__ */
