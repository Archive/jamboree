/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SONG_H__
#define __SONG_H__

#include <time.h>
#include <gtk/gtk.h> /* only need sort type... */
#include <glib-object.h>
#include "types.h"

#define TYPE_SONG (song_get_type ())

typedef struct _Song Song;

typedef enum {
	SONG_TAG_INVALID  = 0,
	SONG_TAG_TITLE    = 1 << 0,
	SONG_TAG_ALBUM    = 1 << 1,
	SONG_TAG_ARTIST   = 1 << 2,
	SONG_TAG_GENRE    = 1 << 3,
	SONG_TAG_DURATION = 1 << 4,
	SONG_TAG_YEAR     = 1 << 5,

	SONG_TAG_ALL = SONG_TAG_TITLE | SONG_TAG_ALBUM | SONG_TAG_ARTIST | SONG_TAG_GENRE | SONG_TAG_DURATION | SONG_TAG_YEAR
} SongTag;

GType       song_get_type            (void) G_GNUC_CONST;
Song *      song_ref                 (Song        *song);
void        song_unref               (Song        *song);
Song *      song_new                 (const char  *path);
const char *song_get_path            (Song        *song);
const char *song_get_title           (Song        *song);
const char *song_get_album           (Song        *song);
const char *song_get_artist          (Song        *song);
const char *song_get_genre           (Song        *song);
const char *song_get_title_folded    (Song        *song);
const char *song_get_album_folded    (Song        *song);
const char *song_get_artist_folded   (Song        *song);
const char *song_get_genre_folded    (Song        *song);
const char *song_get_title_collated  (Song        *song);
const char *song_get_album_collated  (Song        *song);
const char *song_get_artist_collated (Song        *song);
const char *song_get_genre_collated  (Song        *song);
int         song_get_track           (Song        *song);
int         song_get_year            (Song        *song);
size_t      song_get_size            (Song        *song);
int         song_get_duration        (Song        *song);
int         song_get_bitrate         (Song        *song);
int         song_get_samplerate      (Song        *song);
int         song_get_playcount       (Song        *song);
time_t      song_get_time_played     (Song        *song);
time_t      song_get_time_added      (Song        *song);
time_t      song_get_time_modified   (Song        *song);
short       song_get_rating          (Song        *song);
GList *     song_get_playlists       (Song        *song);
void        _song_print              (Song        *song);
int         song_get_random          (Song        *song);
void        _song_update_random      (Song        *song);
int         song_compare_func        (Song        *song_a,
				      Song        *song_b,
				      SongSortId   sort_id,
				      GtkSortType  sort_type);

#endif /*__SONG_H__ */
