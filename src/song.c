/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include "song-private.h"
#include "string-utils.h"
#include "types.h"

#define d(x) x

struct _Song {
	guint         ref_count;

	guint         is_queued : 1;

	/* FIXME: Is there a better way? */
	int           random;
	
	char         *path;

	char         *title;
	char         *title_folded;
	char         *title_collated;

	SharedString *album;
	SharedString *artist;
	SharedString *genre;

	GList        *playlists;
	
	int           duration;

	int           track;
	int           year;
	size_t        size;
	int           bitrate;
	int           samplerate;
	int           playcount;
	time_t        time_played;
	time_t        time_added;
	time_t        time_modified;
	short         rating;
};

Song *
song_ref (Song *song)
{
	song->ref_count++;

	return song;
}

void
song_unref (Song *song)
{
	song->ref_count--;

	if (song->ref_count > 0) {
		return;
	}

	g_free (song->title);
	g_free (song->title_folded);
	g_free (song->title_collated);

	shared_string_unref (song->album);
	shared_string_unref (song->artist);
	shared_string_unref (song->genre);

	g_free (song);
}

Song *
song_new (const char *path)
{
	Song *song;

	song = g_new0 (Song, 1);

	song->ref_count = 1;
	song->path = g_strdup (path);

	return song;
}

gboolean
_song_set_path (Song       *song,
		const char *str)
{
	g_return_val_if_fail (str != NULL, FALSE);

	if (!song->path || strcmp (song->path, str) != 0) {
		g_free (song->path);
		song->path = g_strdup (str);
		return TRUE;
	}

	return FALSE;
}

const char *
song_get_path (Song *song)
{
	return song->path;
}


/*
 * All the _song_ setters take ownership of the indata (except const variants).
 */

gboolean
_song_set_title (Song *song, char *str)
{
	if (!song->title || strcmp (song->title, str) != 0) {
		g_free (song->title);
		g_free (song->title_folded);
		g_free (song->title_collated);
		
		song->title = str;
		song->title_folded = NULL;
		song->title_collated = NULL;

		return TRUE;
	}

	g_free (str);
	return FALSE;
}

static gboolean
change_shared_string (SharedString **str, char *val)
{
	gboolean changed;
	
	if (!*str || strcmp (shared_string_get_str (*str), val) != 0) {
		changed = TRUE;
	} else {
		changed = FALSE;
	}

	if (*str) {
		shared_string_unref (*str);
	}

	*str = shared_string_add (val);

	return changed;
}

static gboolean
change_shared_string_const (SharedString **str, const char *val)
{
	gboolean changed;
	
	if (!*str || strcmp (shared_string_get_str (*str), val) != 0) {
		changed = TRUE;
	} else {
		changed = FALSE;
	}

	if (*str) {
		shared_string_unref (*str);
	}

	*str = shared_string_add_const (val);

	return changed;
}

gboolean
_song_set_album (Song *song, char *str)
{
	return change_shared_string (&song->album, str);
}

gboolean
_song_set_album_const (Song *song, const char *str)
{
	return change_shared_string_const (&song->album, str);
}

gboolean
_song_set_artist (Song *song, char *str)
{
	return change_shared_string (&song->artist, str);
}

gboolean
_song_set_artist_const (Song *song, const char *str)
{
	return change_shared_string_const (&song->artist, str);
}

gboolean
_song_set_genre (Song *song, char *str)
{
	return change_shared_string (&song->genre, str);
}

gboolean
_song_set_genre_const (Song *song, const char *str)
{
	return change_shared_string_const (&song->genre, str);
}

gboolean
_song_set_duration (Song *song, int duration)
{
	if (song->duration != duration) {
		song->duration = duration;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_playcount (Song *song, int playcount)
{
	if (song->playcount != playcount) {
		song->playcount = playcount;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_time_played (Song *song, time_t t)
{
	if (song->time_played != t) {
		song->time_played = t;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_time_added (Song *song, time_t t)
{
	if (song->time_added != t) {
		song->time_added = t;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_time_modified (Song *song, time_t t)
{
	if (song->time_modified != t) {
		song->time_modified = t;
		return TRUE;
	}
	
	return FALSE;
}

gboolean
_song_set_year (Song *song, int year)
{
	if (song->year != year) {
		song->year = year;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_bitrate (Song *song, int bitrate)
{
	if (song->bitrate != bitrate) {
		song->bitrate = bitrate;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_samplerate (Song *song, int samplerate)
{
	if (song->samplerate != samplerate) {
		song->samplerate = samplerate;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_track (Song *song, int track)
{
	if (song->track != track) {
		song->track = track;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_rating (Song *song, short rating)
{
	if (song->rating != rating) {
		song->rating = rating;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_size (Song *song, size_t size)
{
	if (song->size != size) {
		song->size = size;
		return TRUE;
	}

	return FALSE;
}

gboolean
_song_set_playlists (Song *song, GList *playlists)
{
	g_list_free (song->playlists);
	song->playlists = playlists;

	return TRUE;
}

const char *
song_get_title (Song *song)
{
	return song->title;
}

const char *
song_get_album (Song *song)
{
	return shared_string_get_str (song->album);
}

const char *
song_get_artist (Song *song)
{
	return shared_string_get_str (song->artist);
}

const char *
song_get_genre (Song *song)
{
	return shared_string_get_str (song->genre);
}

const char *
song_get_title_folded (Song *song)
{
	if (G_UNLIKELY (!song->title_folded)) {
		song->title_folded =  g_utf8_casefold (song->title, -1);
	}

	return song->title_folded;
}

const char *
song_get_album_folded (Song *song)
{
	return shared_string_get_folded (song->album);
}

const char *
song_get_artist_folded (Song *song)
{
	return shared_string_get_folded (song->artist);
}

const char *
song_get_genre_folded (Song *song)
{
	return shared_string_get_folded (song->genre);
}

const char *
song_get_title_collated (Song *song)
{
	if (G_UNLIKELY (!song->title_collated)) {
		song->title_collated = string_utils_create_collate_key (song->title);
	}

	return song->title_collated;
}

const char *
song_get_album_collated (Song *song)
{
	return shared_string_get_collated (song->album);
}

const char *
song_get_artist_collated (Song *song)
{
	return shared_string_get_collated (song->artist);
}

const char *
song_get_genre_collated (Song *song)
{
	return shared_string_get_collated (song->genre);
}

int
song_get_track (Song *song)
{
	return song->track;
}

int
song_get_year (Song *song)
{
	return song->year;
}

size_t
song_get_size (Song *song)
{
	return song->size;
}

int
song_get_duration (Song *song)
{
	return song->duration;
}

int
song_get_bitrate (Song *song)
{
	return song->bitrate;
}

int
song_get_samplerate (Song *song)
{
	return song->samplerate;
}

int
song_get_playcount (Song *song)
{
	return song->playcount;
}

time_t
song_get_time_played (Song *song)
{
	return song->time_played;
}

time_t
song_get_time_added (Song *song)
{
	return song->time_added;
}

time_t
song_get_time_modified (Song *song)
{
	return song->time_modified;
}

short
song_get_rating (Song *song)
{
	return song->rating;
}

GList *
song_get_playlists (Song *song)
{
	return song->playlists;
}

void
_song_print (Song *song)
{
	g_print ("%s, %s, %s\n",
		 song->title,
		 shared_string_get_str (song->album),
		 shared_string_get_str (song->artist));
}

/* Note: temporary hack, need to solve in a nicer way. */
int
song_get_random (Song *song)
{
	return song->random;
}

void
_song_update_random (Song *song)
{
	song->random = g_random_int ();
}

gboolean
_song_remove_playlist (Song *song, int id)
{
	GList *l;

	for (l = song->playlists; l; l = l->next) {
		if (GPOINTER_TO_INT (l->data) == id) {
			song->playlists = g_list_delete_link (song->playlists, l);
			return TRUE;
		}
	}

	return FALSE;
}

gboolean
_song_add_playlist (Song *song, int id)
{
	GList *l;

	for (l = song->playlists; l; l = l->next) {
		if (GPOINTER_TO_INT (l->data) == id) {
			return FALSE;
		}
	}

	song->playlists = g_list_prepend (song->playlists, GINT_TO_POINTER (id));
	
	return TRUE;
}

GType
song_get_type (void)
{
	static GType type = 0;
  
	if (type == 0) {
		type = g_boxed_type_register_static ("Song",
						     (GBoxedCopyFunc) song_ref,
						     (GBoxedFreeFunc) song_unref);
	}
	
	return type;
}

/* Sorting routines */

static int
compare_int (int a, int b)
{
	if (a < b) {
		return -1;
	}
	else if (a > b) {
		return 1;
	} else {
		return 0;
	}
}

static int
sort_artist_album_track (Song *song_a, Song *song_b)
{
	int ret;

	ret = strcmp (song_get_artist_collated (song_a),
		      song_get_artist_collated (song_b));
	if (ret == 0) {
		ret = strcmp (song_get_album_collated (song_a),
			      song_get_album_collated (song_b));
		if (ret == 0) {
			ret = compare_int (song_get_track (song_a), song_get_track (song_b));
		}
	}

	return ret;
}

static int
sort_album_track (Song *song_a, Song *song_b)
{
	int ret;

	ret = strcmp (song_get_album_collated (song_a),
		      song_get_album_collated (song_b));
	if (ret == 0) {
		ret = compare_int (song_get_track (song_a), song_get_track (song_b));
	}
	
	return ret;
}

int
song_compare_func (Song        *song_a,
		   Song        *song_b,
		   SongSortId   sort_id,
		   GtkSortType  sort_type)
{
	int ret, second = -1;

	switch (sort_id) {
	case SONG_SORT_ID_DEFAULT:
	case SONG_SORT_ID_ARTIST:
		ret = sort_artist_album_track (song_a, song_b);
		second = 0;
		break;
    
	case SONG_SORT_ID_TITLE:
		ret = strcmp (song_get_title_collated (song_a),
			      song_get_title_collated (song_b));
		break;

	case SONG_SORT_ID_TIME:
		ret = compare_int (song_get_duration (song_a), song_get_duration (song_b));
		break;

	case SONG_SORT_ID_ALBUM:
		ret = sort_album_track (song_a, song_b);
		break;

	case SONG_SORT_ID_GENRE:
		ret = strcasecmp (song_get_genre (song_a), song_get_genre (song_b));
		break;
    
	case SONG_SORT_ID_YEAR:
		ret = compare_int (song_get_year (song_a), song_get_year (song_b));
		break;
    
	case SONG_SORT_ID_TRACK:
		ret = compare_int (song_get_track (song_a), song_get_track (song_b));
		break;
    
	case SONG_SORT_ID_RATING:
		ret = compare_int (song_get_rating (song_a), song_get_rating (song_b));
		break;

	case SONG_SORT_ID_PLAYCOUNT:
		ret = compare_int (song_get_playcount (song_a), song_get_playcount (song_b));
		break;

	default:
		ret = 0;
		break;
	}

	if (ret == 0 && second == -1) {
		ret = sort_artist_album_track (song_a, song_b);
	}
	
	if (sort_type == GTK_SORT_DESCENDING) {
		ret = -ret;
	}

	return ret;
}
