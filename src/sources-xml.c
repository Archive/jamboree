/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <glib.h>
#include <glib/gi18n.h>
#include "sources-xml.h"
#include "source-proxy.h"
#include "source-playlist.h"
#include "source-smart-playlist.h"


#define INITIAL_PLAYLISTS \
"<playlists version='1'>" \
"  <smart-playlist name='%s' match='any'>" \
"    <expr type='greater'>" \
"      <variable type='date-added'/>" \
"      <constant type='date' value='7' unit='days'/>" \
"    </expr>" \
"  </smart-playlist>" \
"  <smart-playlist name='%s' match='any'>" \
"    <expr type='greater'>" \
"      <variable type='last-played'/>" \
"      <constant type='date' value='7' unit='days'/>" \
"    </expr>" \
"  </smart-playlist>" \
"  <smart-playlist name='%s' match='any'>" \
"    <expr type='equal'>" \
"      <variable type='play-count'/>" \
"      <constant type='int' value='0'/>" \
"    </expr>" \
"  </smart-playlist>" \
"  <smart-playlist name='%s' match='any'>" \
"    <expr type='greater'>" \
"      <variable type='play-count'/>" \
"      <constant type='int' value='0'/>" \
"    </expr>" \
"    <limit type='songs' value='100' selection='play-count'/>" \
"  </smart-playlist>" \
"  <regular-playlist id='1' name='%s'/>" \
"</playlists>"


enum {
	NO_ERROR,
	PARSE_ERROR,
};

typedef enum {
	STATE_START,
	STATE_PLAYLISTS,
	STATE_REGULAR_PLAYLIST,
	STATE_SMART_PLAYLIST,
	STATE_EXPR,
	STATE_VARIABLE,
	STATE_CONSTANT,
	STATE_LIMIT
} State;

typedef struct {
	GMarkupParseContext *context;
	GMarkupParser       *parser;

	Limiter             *limiter;
	
	Source              *main_source;
	Source              *source;

	ExprOp               expr_op;
	Expr                *variable_expr;
	Expr                *constant_expr;

	GList               *exprs;
	ExprOp               match_type;
	
	GList               *sources;
	GList               *state_stack;
} ParserData;

static GQuark error_quark; 


static void   start_element_handler (GMarkupParseContext  *context,
				     const char           *node_name,
				     const char          **attribute_names,
				     const char          **attribute_values,
				     gpointer              user_data,
				     GError              **error);
static void   end_element_handler   (GMarkupParseContext  *context,
				     const char           *node_name,
				     gpointer              user_data,
				     GError              **error);
static void   error_handler         (GMarkupParseContext  *context,
				     GError               *error,
				     gpointer              user_data);
static GList *playlists_from_xml    (const char           *str,
				     Source               *main_source);


static void
push_state (ParserData *data, State state)
{
	data->state_stack = g_list_prepend (data->state_stack, GINT_TO_POINTER (state));
}      

static State
pop_state (ParserData *data)
{
	State state;
  
	state = GPOINTER_TO_INT (data->state_stack->data);
	data->state_stack = g_list_delete_link (data->state_stack, data->state_stack);

	return state;
}      

static State
peek_state (ParserData *data)
{
	return GPOINTER_TO_INT (data->state_stack->data);
}      

static void
set_error (GError              **err,
           GMarkupParseContext  *context,
           int                   error_code,
           const char           *format,
           ...)
{
	int line, ch;
	va_list   args;
	char     *str;
  
	g_markup_parse_context_get_position (context, &line, &ch);
  
	va_start (args, format);
	str = g_strdup_vprintf (format, args);
	va_end (args);

	g_set_error (err, error_quark, error_code, "Line %d character %d: %s", line, ch, str);
  
	g_free (str);
}

static const char *
get_attribute_value (const char  *name,
		     const char **names,
		     const char **values)
{
	int i = 0;

	while (names[i]) {
		if (strcmp (name, names[i]) == 0) {
			return values[i];
		}
		i++;
	}

	return NULL;
}

static const char *
get_attribute_value_required (GMarkupParseContext  *context,
			      const char           *tag,
			      const char           *name,
			      const char          **names,
			      const char          **values,
			      GError              **error)
{
	const char *value;

	value = get_attribute_value (name, names, values);
	if (!value) {
		set_error (error, context, PARSE_ERROR,
			   "%s must have '%s' attribute",
			   tag, name);
	}

	return value;
}

static void
start_element_handler (GMarkupParseContext  *context,
                       const char           *element,
                       const char          **attribute_names,
                       const char          **attribute_values,
                       gpointer              user_data,
                       GError              **error)
{
	ParserData   *data;
	State         state;
	const char   *tmp, *tmp2;
	ConstantType  constant_type;
	Constant     *constant;
	Variable      variable;
	const char   *name;
	int           id;
	ExprOp        op;

	data = user_data;
	state = peek_state (data);

	switch (state) {
	case STATE_START:
		if (strcmp (element, "playlists") != 0) {
			set_error (error, context, PARSE_ERROR,
				   "Outermost element must be a <playlists> not <%s>",
				   element);
			return;
		}
		
		push_state (data, STATE_PLAYLISTS);
		break;
		
	case STATE_PLAYLISTS:
		if (strcmp (element, "regular-playlist") == 0) {
			name = get_attribute_value_required (context, "<regular-playlist>", "name",
							     attribute_names, attribute_values,
							     error);
			if (!name) {
				return;
			}
			
			tmp = get_attribute_value_required (context, "<regular-playlist>", "id",
							    attribute_names, attribute_values,
							    error);
			if (!tmp) {
				return;
			}
			
			id = atoi (tmp);
			data->source = source_playlist_new (data->main_source,
							    name,
							    id);
			
			push_state (data, STATE_REGULAR_PLAYLIST);
		}
		else if (strcmp (element, "smart-playlist") == 0) {
			name = get_attribute_value_required (context, "<smart-playlist>", "name",
							     attribute_names, attribute_values,
							     error);
			if (!name) {
				return;
			}
			
			tmp = get_attribute_value_required (context, "<smart-playlist>", "match",
							    attribute_names, attribute_values,
							    error);
			if (!tmp) {
				return;
			}

			if (strcmp (tmp, "any") == 0) {
				data->match_type = EXPR_OP_OR;
			}
			else if (strcmp (tmp, "all") == 0) {
				data->match_type = EXPR_OP_AND;
			} else {
				set_error (error, context, PARSE_ERROR,
					   "unknown match type: %s", tmp);
				return;
			}
			
			data->source = source_smart_playlist_new (data->main_source,
								  name);
			
			push_state (data, STATE_SMART_PLAYLIST);
		} else {
			set_error (error, context, PARSE_ERROR,
				   "<regular-playlist> or <smart-playlist> tag expected, not <%s>",
				   element);
			return;
		}
		
		break;

	case STATE_REGULAR_PLAYLIST:
		break;

	case STATE_SMART_PLAYLIST:
		if (strcmp (element, "expr") == 0) {
			if (data->expr_op != EXPR_OP_NONE) {
				set_error (error, context, PARSE_ERROR,
					   "recursive exprs are not allowed");
				return;
			}
			
			tmp = get_attribute_value_required (context, "<expr>", "type",
							    attribute_names, attribute_values,
							    error);
			if (!tmp) {
				return;
			}
			
			op = operator_from_string (tmp);
			switch (op) {
			case EXPR_BINARY_OPS:
				/* OK. */
				break;
			default:
				set_error (error, context, PARSE_ERROR,
					   "invalid 'type' attribute '%s', can only have binary ops",
					   tmp);
				break;
			}

			data->expr_op = op;
			push_state (data, STATE_EXPR);
		}
		else if (strcmp (element, "limit") == 0) {
			LimiterVariable  variable;
			LimiterUnit      unit;
			long             value;
			
			tmp = get_attribute_value_required (context, "<limit>", "type",
							    attribute_names, attribute_values,
							    error);
			if (!tmp) {
				return;
			}
			unit = limiter_unit_from_string (tmp);
			
			tmp = get_attribute_value_required (context, "<limit>", "value",
							    attribute_names, attribute_values,
							    error);
			if (!tmp) {
				return;
			}
			value = atol (tmp);
			
			tmp = get_attribute_value_required (context, "<limit>", "selection",
							    attribute_names, attribute_values,
							    error);
			if (!tmp) {
				return;
			}
			variable = limiter_variable_from_string (tmp);
			
			data->limiter = limiter_new (variable, unit, value);
			
			push_state (data, STATE_LIMIT);
		} else {
			set_error (error, context, PARSE_ERROR,
				   "Element <%s> is not allowed inside a <query> element",
				   element);
			return;
		}

		break;
      
	case STATE_EXPR:
		if (strcmp (element, "constant") == 0) {
			if (data->constant_expr) {
				set_error (error, context, PARSE_ERROR,
					   "can only have one constant expression");
				return;
			}
			
			tmp = get_attribute_value_required (context, "<constant>", "type",
							    attribute_names, attribute_values,
							    error);
			if (!tmp) {
				return;
			}
	  
			constant_type = constant_type_from_string (tmp);
			if (constant_type == CONSTANT_TYPE_NONE) {
					set_error (error, context, PARSE_ERROR,
						   "unknown constant type: %s", tmp);
					return;
			}

			if (constant_type == CONSTANT_TYPE_DATE) {
				tmp = get_attribute_value ("value", attribute_names, attribute_values);
				if (!tmp) {
					set_error (error, context, PARSE_ERROR,
						   "<constant> tag of type date must have 'value' attribute");
					return;
				}
	      
				tmp2 = get_attribute_value ("unit", attribute_names, attribute_values);
				if (!tmp2) {
					set_error (error, context, PARSE_ERROR,
						   "<constant> tag of type date must have 'unit' attribute");
					return;
				}
	      
			}
			else if (constant_type == CONSTANT_TYPE_RANGE) {
				tmp = get_attribute_value ("start", attribute_names, attribute_values);
				if (!tmp) {
					set_error (error, context, PARSE_ERROR,
						   "<constant> tag of type range must have 'start' attribute");
					return;
				}
	      
				tmp2 = get_attribute_value ("end", attribute_names, attribute_values);
				if (!tmp2) {
					set_error (error, context, PARSE_ERROR,
						   "<constant> tag of type range must have 'end' attribute");
					return;
				}
			} else {
				tmp = get_attribute_value ("value", attribute_names, attribute_values);
				if (!tmp) {
					set_error (error, context, PARSE_ERROR,
						   "<constant> tag must have 'value' attribute");
					return;
				}
				tmp2 = NULL;
			}

			switch (constant_type) {
			case CONSTANT_TYPE_STRING:
				constant = constant_string_new (tmp);
				break;
			case CONSTANT_TYPE_INT:
			case CONSTANT_TYPE_SIZE:
				constant = constant_int_new (atoi (tmp));
				break;	      
			case CONSTANT_TYPE_DATE:
				constant = constant_date_new (atol (tmp), unit_from_string (tmp2));
				break;
			case CONSTANT_TYPE_RANGE:
				constant = constant_range_new (atoi (tmp), atoi (tmp2));
				break;
			default:
				constant = NULL;
				g_assert_not_reached ();
			}
	  
			data->constant_expr = expr_new_constant (constant);
			push_state (data, STATE_CONSTANT);
		}
		else if (strcmp (element, "variable") == 0) {
			if (data->variable_expr) {
				set_error (error, context, PARSE_ERROR,
					   "can only have one variable expression");
				return;
			}
			
			tmp = get_attribute_value ("type", attribute_names, attribute_values);
			if (!tmp) {
				set_error (error, context, PARSE_ERROR,
					   "<variable> tag must have 'type' attribute");
				return;
			}
	  
			variable = variable_from_string (tmp);
			if (variable == VARIABLE_NONE) {
				set_error (error, context, PARSE_ERROR,
					   "unknown variable: %s", tmp);
				return;
			}
			
			data->variable_expr = expr_new_variable (variable);
			push_state (data, STATE_VARIABLE);
		} else {
			set_error (error, context, PARSE_ERROR,
				   "Element <%s> is not allowed inside a <expr> element",
				   element);
			return;
		}
		
		break;
      
	case STATE_CONSTANT:
		break;
       
	case STATE_VARIABLE:
		break;

	case STATE_LIMIT:
		break;
	}
}

static void
end_element_handler (GMarkupParseContext  *context,
                     const char           *element,
                     gpointer              user_data,
                     GError              **error)
{
	ParserData *data;
	Expr       *expr;
	State       state;

	data = user_data;
	state = pop_state (data);

	switch (state) {
	case STATE_START:
		break;

	case STATE_PLAYLISTS:
		break;

	case STATE_REGULAR_PLAYLIST:
		data->sources = g_list_append (data->sources, data->source);
		data->source = NULL;
		break;

	case STATE_SMART_PLAYLIST:
		if (data->exprs) {
			source_smart_playlist_set_exprs (SOURCE_SMART_PLAYLIST (data->source),
							 data->exprs);
			data->exprs = NULL;
			
			source_smart_playlist_set_op (SOURCE_SMART_PLAYLIST (data->source),
						      data->match_type);
		}
		
		data->sources = g_list_append (data->sources, data->source);
		data->source = NULL;
		break;

	case STATE_EXPR:
		expr = expr_new_binary (data->expr_op, data->variable_expr, data->constant_expr);
		data->exprs = g_list_append (data->exprs, expr);

		data->expr_op = EXPR_OP_NONE;
		data->variable_expr = NULL;
		data->constant_expr = NULL;
		break;

	case STATE_CONSTANT:
		break;

	case STATE_VARIABLE:
		break;

	case STATE_LIMIT:
		if (data->limiter) {
			source_smart_playlist_set_limiter (SOURCE_SMART_PLAYLIST (data->source),
							   data->limiter);
		}
		
		data->limiter = NULL;
		break;
	}

}

static void
error_handler (GMarkupParseContext *context,
	       GError              *error,
	       gpointer             user_data)
{
	g_warning ("%s", error->message);
}

static GList *
playlists_from_xml (const char *str, Source *main_source)
{
	ParserData data;
	static     gboolean inited = FALSE;
  
	g_return_val_if_fail (str != NULL, NULL);

	if (!inited) {
		error_quark = g_quark_from_static_string ("playlist-xml-error-quark");
		inited = TRUE;
	}
  
	memset (&data, 0, sizeof (data));
  
	push_state (&data, STATE_START);
  
	data.parser = g_new0 (GMarkupParser, 1);

	data.parser->start_element = start_element_handler;
	data.parser->end_element = end_element_handler;
	data.parser->error = error_handler;

	data.context = g_markup_parse_context_new (data.parser, 0, &data, NULL);
	data.main_source = main_source;
  
	if (!g_markup_parse_context_parse (data.context, str, -1, NULL)) {
		data.sources = NULL;
	}
  
	g_markup_parse_context_free (data.context);
	g_free (data.parser);

	return data.sources;
}

void
sources_xml_save (GList *sources)
{
	char   *filename;
	char   *str;
	FILE   *file;
	GList  *l;
	Source *source;

  
	filename = g_build_filename (g_get_home_dir (),
				     ".gnome2",
				     "jamboree",
				     "playlists.xml",
				     NULL);
	file = fopen (filename, "w");
	g_free (filename);

	if (!file) {
		return;
	}
	
	fprintf (file, "<playlists version='1'>\n");
  
	for (l = sources; l; l = l->next) {
		source = l->data;

		str = source_to_xml (source);
		if (!str) {
			continue;
		}

		fprintf (file, str);
		g_free (str);
	}
  
	fprintf (file, "</playlists>\n");
	fclose (file);
}

GList *
sources_xml_load (Source *main_source)
{
	char  *filename;
	char  *str;
	GList *playlists = NULL;

	filename = g_build_filename (g_get_home_dir (),
				     ".gnome2",
				     "jamboree",
				     "playlists.xml",
				     NULL);

	if (!g_file_get_contents (filename, &str, NULL, NULL)) {
		str = g_strdup_printf (INITIAL_PLAYLISTS,
				       _("Recently added"),
				       _("Recently played"),
				       _("Never played"),
				       _("Most played"),
				       _("Favorites"));
	}

	playlists = playlists_from_xml (str, main_source);

	g_free (str);
	g_free (filename);
  
	return playlists;
}



