/*
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <gtk/gtk.h>
#include "stock-icons.h"

static GtkStockItem stock_items[] = {
	{ JAMBOREE_STOCK_PLAY,    "Play" },     
	{ JAMBOREE_STOCK_STOP,    "Stop" },
	{ JAMBOREE_STOCK_NEXT,    "Next" },
	{ JAMBOREE_STOCK_PREV,    "Prev" },
	{ JAMBOREE_STOCK_PAUSE,   "Pause" },    
	{ JAMBOREE_STOCK_RANDOM,  "Random" },
	
	{ JAMBOREE_STOCK_VOLUME_ZERO,   NULL } ,
	{ JAMBOREE_STOCK_VOLUME_MIN,    NULL },
	{ JAMBOREE_STOCK_VOLUME_MEDIUM, NULL },
	{ JAMBOREE_STOCK_VOLUME_MAX,    NULL }
};

void
stock_icons_register (void)
{
	GtkIconFactory *icon_factory;
	GtkIconSet     *icon_set;
	GdkPixbuf      *pixbuf;
	int             i;
	char           *filename;
       
	gtk_stock_add (stock_items, G_N_ELEMENTS (stock_items));
	
	icon_factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (icon_factory);
	
	for (i = 0; i < G_N_ELEMENTS (stock_items); i++) {
		filename = g_strdup_printf (DATADIR "/jamboree/%s.png", stock_items[i].stock_id);
		pixbuf = gdk_pixbuf_new_from_file (filename, NULL);
		g_free (filename);
		
		icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);
	       
		gtk_icon_factory_add (icon_factory,
				      stock_items[i].stock_id,
				      icon_set);
		
		g_object_unref (pixbuf);
	}
}
