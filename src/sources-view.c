/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdio.h>
#include <string.h>
#include <gconf/gconf.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "sources-view.h"
#include "sources-xml.h"
#include "source-proxy.h"
#include "source-playlist.h"
#include "source-smart-playlist.h"
#include "source-database.h"
#include "smart-playlist-dialog.h"
#include "utils.h"

/* FIXME: remove */
#include "song-private.h"

enum {
	COL_SOURCE,
	NUM_COLS
};


typedef struct {
	Source      *source;
	const char  *name;
	GtkTreeIter  iter;
	gboolean     found;
} FindSourceData;

static const GtkTargetEntry tree_target_types[] = {
	{ "x-special/jamboree-song-list", GTK_TARGET_SAME_APP, 0 }
};

static void save_sources            (GtkTreeView *view);
static void restore_selected_source (GtkTreeView *view);
static void save_selected_source    (GtkTreeView *view);


static gboolean
find_source_foreach (GtkTreeModel *model,
		     GtkTreePath  *path,
		     GtkTreeIter  *iter,
		     gpointer      user_data)
{
	FindSourceData *data;
	Source         *source;

	data = user_data;

	gtk_tree_model_get (model, iter,
			    COL_SOURCE, &source,
			    -1);

	if (source == data->source) {
		data->found = TRUE;
		data->iter = *iter;
		return TRUE;
	}
	
	return FALSE;
}

static gboolean
find_source_by_name_foreach (GtkTreeModel *model,
			     GtkTreePath  *path,
			     GtkTreeIter  *iter,
			     gpointer      user_data)
{
	FindSourceData *data;
	Source         *source;

	data = user_data;

	gtk_tree_model_get (model, iter,
			    COL_SOURCE, &source,
			    -1);
	
	if (strcmp (source_get_name (source), data->name) == 0) {
		data->found = TRUE;
		data->iter = *iter;
		return TRUE;
	}
	
	return FALSE;
}

static gboolean
tree_destroy_foreach (GtkTreeModel *model,
		      GtkTreePath  *path,
		      GtkTreeIter  *iter,
		      gpointer      user_data)
{
	Source *source;

	gtk_tree_model_get (model, iter,
			    COL_SOURCE, &source,
			    -1);

	g_object_unref (source);

	return FALSE;
}

static void
tree_destroy_cb (GtkWidget *widget, gpointer user_data)
{
	GtkTreeModel *model;
	
	save_selected_source (GTK_TREE_VIEW (widget));
	save_sources (GTK_TREE_VIEW (widget));
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));
	
	gtk_tree_model_foreach (model, tree_destroy_foreach, NULL);
}

static void
tree_name_func (GtkTreeViewColumn *tree_column,
		GtkCellRenderer   *cell,
		GtkTreeModel      *tree_model,
		GtkTreeIter       *iter,
		gpointer           data)
{
	Source  *source;
	
	gtk_tree_model_get (tree_model, iter,
			    COL_SOURCE, &source,
			    -1);

	g_object_set (cell,
		      "text", source_get_name (source),
		      "editable", source_get_is_editable (source),
		      NULL);
}

static void
tree_pixbuf_func (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer   *cell,
		  GtkTreeModel      *tree_model,
		  GtkTreeIter       *iter,
		  gpointer           data)
{
	Source        *source;
	const GdkPixbuf *pixbuf;
	
	gtk_tree_model_get (tree_model, iter,
			    COL_SOURCE, &source,
			    -1);
  
	pixbuf = source_get_pixbuf (source);
  
	g_object_set (cell, "pixbuf", pixbuf, NULL);
}

static gboolean
tree_drag_motion_cb (GtkWidget      *widget,
		     GdkDragContext *context,
		     int             x,
		     int             y,
		     guint           time,
		     gpointer        data)
{
	GtkTreeModel *model;
	GtkTreePath  *path;
	GtkTreeIter   iter;
	gboolean      found;
	Source       *source;
	
	g_signal_stop_emission_by_name (widget, "drag_motion");
  
	found = gtk_tree_view_get_dest_row_at_pos (GTK_TREE_VIEW (widget), x, y, &path, NULL);
	if (found) {
		model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));
		
		if (gtk_tree_model_get_iter (model, &iter, path)) {
			gtk_tree_model_get (model, &iter,
					    COL_SOURCE, &source,
					    -1);

			if (IS_SOURCE_PLAYLIST (source)) {
				gtk_tree_view_set_drag_dest_row (GTK_TREE_VIEW (widget),
								 path,
								 GTK_TREE_VIEW_DROP_INTO_OR_AFTER);
				gdk_drag_status (context, context->suggested_action, time);
				
				gtk_tree_path_free (path);
				return TRUE;
			}
		}
		
		gtk_tree_path_free (path);
	}
	
	gdk_drag_status (context, 0, time);  

	return TRUE;
}

static void
tree_drag_data_received_cb (GtkWidget        *widget,
			    GdkDragContext   *context,
			    int               x,
			    int               y,
			    GtkSelectionData *data,
			    guint             info,
			    guint             time,
			    gpointer          user_data)
{
	gboolean      found;
	GtkTreePath  *path;
	GtkTreeModel *model;
	GtkTreeIter   iter;
	Source       *source;
        int           id;
  
	found = gtk_tree_view_get_dest_row_at_pos (GTK_TREE_VIEW (widget), x, y, &path, NULL);
	if (!found) {
		return;
	}
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));
  
	if (gtk_tree_model_get_iter (model, &iter, path)) {
		gtk_tree_model_get (model, &iter,
				    COL_SOURCE, &source,
				    -1);
	} else {
		gtk_tree_path_free (path);
		return;
	}

	gtk_tree_path_free (path);

	if (!IS_SOURCE_PLAYLIST (source)) {
		return;
	}

        id = source_playlist_get_id (SOURCE_PLAYLIST (source));

        if (data->length >= 0 && info == 0) {
		int       len, i;
		Song     *song;
                gpointer *songs;

                songs = (gpointer *) data->data;

                len = data->length / sizeof (gpointer);
                for (i = 0; i < len; i++) {
                        song = songs[i];
                        
                        if (_song_add_playlist (song, id)) {
                                source_update_song (source, song);
                        }
                }
                
		gtk_drag_finish (context, TRUE, FALSE, time);
	} else {
		/*g_message ("Don't know how to handle format %d", data->format);*/
		gtk_drag_finish (context, FALSE, FALSE, time);      
	}

	g_signal_stop_emission_by_name (widget, "drag_data_received");
}

static void
name_edited_cb (GtkCellRendererText *cellrenderertext,
		char                *path_string,
		char                *str,
		GtkTreeView         *view)
{
	GtkTreeModel *model;
	GtkTreePath  *path;
	GtkTreeIter   iter;
	Source       *source;

	model = gtk_tree_view_get_model (view);
	
	path = gtk_tree_path_new_from_string (path_string);
	gtk_tree_model_get_iter (model, &iter, path);

	gtk_tree_model_get (model, &iter,
			    COL_SOURCE, &source,
			    -1);

	source_set_name (source, str);

	gtk_tree_path_free (path);

	save_sources (view);
}
	
static void
context_remove_cb (GtkWidget   *menuitem,
		   GtkTreeView *view)
{
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkTreeModel     *model;
	GList            *l;
	Source           *source;
	Song             *song;
  
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	if (!gtk_tree_selection_get_selected (selection, &model, &iter)) {
		return;
	}

	source = sources_view_get_selected (view);
	if (!source) {
		return;
	}
	
	gtk_list_store_remove (GTK_LIST_STORE (model), &iter);

	if (IS_SOURCE_PLAYLIST (source)) {
		for (l = source_get_songs (source); l; l = l->next) {
			song = l->data;
			
			if (_song_remove_playlist (song, source_playlist_get_id (SOURCE_PLAYLIST (source)))) {
				source_update_song (source, song);
			}
		}
	}

	save_sources (view);
}

static void
context_rename_cb (GtkWidget   *menuitem,
		   GtkTreeView *view)
{
	GtkTreeSelection  *selection;
	GtkTreeModel      *model;
	GtkTreeIter        iter;
	GtkTreePath       *path;
	GtkTreeViewColumn *column;
  
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	if (!gtk_tree_selection_get_selected (selection, &model, &iter)) {
		return;
	}

	column = gtk_tree_view_get_column (view, 0);
	
	path = gtk_tree_model_get_path (model, &iter);
	gtk_tree_view_set_cursor (view, path, column, TRUE);
	gtk_tree_path_free (path);
}

static void
context_edit_smart_playlist_cb (GtkWidget   *menuitem,
				GtkTreeView *view)
{
	Source *source;
  
	source = sources_view_get_selected (view);
	if (!source) {
		return;
	}

	if (smart_playlist_dialog_run (NULL, SOURCE_SMART_PLAYLIST (source))) {
		save_sources (view);
	}
}

static GtkWidget *
append_context_item (GtkWidget *menu,
		     char      *label,
		     char      *stock,
		     gpointer   function,
		     gpointer   data)
{
	GtkWidget *item;

	g_return_val_if_fail (label != NULL || stock != NULL, NULL);
	
	if (stock) {
		item = gtk_image_menu_item_new_from_stock (stock, NULL);
	} else {
		item = gtk_image_menu_item_new_with_mnemonic (label);
	}
	
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	gtk_widget_show (item);
	
	if (function) {
		g_signal_connect (item,
				  "activate", G_CALLBACK (function),
				  data);
	}

	return item;
}

static GtkWidget *
build_context_menu (GtkWidget *view)
{
	GtkWidget *menu;
	Source    *source;
	gboolean   added;

	menu = gtk_menu_new ();

	source = sources_view_get_selected (GTK_TREE_VIEW (view));

	added = FALSE;
	
	if (source_get_is_editable (source)) {
		added = TRUE;
		append_context_item (menu, NULL, GTK_STOCK_REMOVE, context_remove_cb, view);
		append_context_item (menu, _("_Rename"), NULL, context_rename_cb, view);
	}

	if (IS_SOURCE_SMART_PLAYLIST (source)) {
		added = TRUE;
		append_context_item (menu, _("_Edit smart playlist"), NULL, context_edit_smart_playlist_cb, view);
	}

	if (added) {
		return menu;
	}

	gtk_widget_destroy (menu);
	
	return NULL;
}

static void
tree_popup_menu (GtkWidget      *view,
		 GdkEventButton *event)
{
	int        button;
	guint32    timestamp;
	GtkWidget *menu;
	
	if (event) {
		button = event->button;
		timestamp = event->time;
	} else {
		button = 0;
		timestamp = GDK_CURRENT_TIME;
	}

	menu = build_context_menu (view);
	if (menu) {
		gtk_menu_popup (GTK_MENU (menu),
				NULL, NULL, NULL, NULL,
				button, timestamp);
	}
}

static gboolean
tree_button_press_event_cb (GtkWidget      *view,
			    GdkEventButton *event,
			    gpointer        data)
{
	GtkTreePath      *path;
	GtkTreeModel     *model;
	GtkTreeIter       iter;
	GtkTreeSelection *selection;
	
	if (event->button != 3) {
		return FALSE;
	}
  
	if (!gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW (view),
					    event->x,
					    event->y,
					    &path,
					    NULL, NULL, NULL)) {
		return FALSE;
	}

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));
	gtk_tree_model_get_iter (model, &iter, path);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	if (!gtk_tree_selection_iter_is_selected (selection, &iter)) {
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &iter);
	}
	
	tree_popup_menu (view, event);
	
	return TRUE;
}

static gboolean
tree_popup_menu_cb (GtkWidget *view, gpointer user_data)
{
	tree_popup_menu (view, NULL);

	return TRUE;
}

static void
selection_changed_cb (GtkTreeSelection *selection, GtkTreeView *tree)
{
	Source *source, *last_source;

	/* Hack to get the tree selection to only emit when we have real
	 * changes. Makes the code using this much simpler.
	 */
	
	last_source = g_object_get_data (G_OBJECT (tree), "last_selected");	

	source = sources_view_get_selected (tree);

	if (last_source == source) {
		g_signal_stop_emission_by_name (selection, "changed");
		return;
	}

	g_object_set_data (G_OBJECT (tree), "last_selected", source);	
}

void
sources_view_setup (GtkTreeView *tree, Source *source)
{
	GtkTreeSelection  *selection;
	GtkTreeModel      *model;
	GtkTreeViewColumn *column;
	GtkCellRenderer   *cell;
	GList             *sources, *l;

	selection = gtk_tree_view_get_selection (tree);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);

	g_signal_connect (selection,
			  "changed",
			  G_CALLBACK (selection_changed_cb),
			  tree);			  
	
	model = GTK_TREE_MODEL (gtk_list_store_new (NUM_COLS, G_TYPE_POINTER));

	gtk_tree_view_set_model (tree, model);
	
	g_signal_connect (tree,
			  "destroy",
			  G_CALLBACK (tree_destroy_cb),
			  NULL);

	g_signal_connect (tree,
			  "drag_motion",
			  G_CALLBACK (tree_drag_motion_cb),
			  NULL);

	gtk_drag_dest_set (GTK_WIDGET (tree),
			   GTK_DEST_DEFAULT_ALL ,
			   tree_target_types,
			   G_N_ELEMENTS (tree_target_types),
			   GDK_ACTION_COPY);
	
	g_signal_connect (tree,
			  "drag_data_received",
			  G_CALLBACK (tree_drag_data_received_cb),
			  NULL);

	g_signal_connect (tree,
			  "button_press_event",
			  G_CALLBACK (tree_button_press_event_cb),
			  NULL);

	g_signal_connect (tree,
			  "popup_menu",
			  G_CALLBACK (tree_popup_menu_cb),
			  NULL);

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, _("Playlists"));

	cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, cell,
						 tree_pixbuf_func,
						 NULL,
						 NULL);
  
	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, cell, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, cell,
						 tree_name_func,
						 NULL,
						 NULL);

	gtk_tree_view_append_column (tree, column);

	g_signal_connect (cell,
			  "edited",
			  G_CALLBACK (name_edited_cb),
			  tree);
	
	/* Hardcode the library playlist. */
	sources_view_append (tree, source);
	g_object_ref (source);

	/* Read playlists. */
	sources = sources_xml_load (source);
	for (l = sources; l; l = l->next) {
		sources_view_append (tree, l->data);
	}

	restore_selected_source (tree);
}

Source *
sources_view_get_selected (GtkTreeView *tree)
{
	GtkTreeSelection *selection;
	GtkTreeModel     *model;
	GtkTreeIter       iter;
	Source           *source;
	
	selection = gtk_tree_view_get_selection (tree);
	
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		gtk_tree_model_get (model, &iter,
				    COL_SOURCE, &source,
				    -1);
		return source;
	}
	
	return NULL;
}

void
sources_view_set_selected (GtkTreeView *tree, Source *source)
{
	GtkTreeModel     *model;
	FindSourceData    data;
	GtkTreeSelection *selection;
	
	model = gtk_tree_view_get_model (tree);

	data.found = FALSE;
	data.source = source;
	
	gtk_tree_model_foreach (model,
				find_source_foreach,
				&data);

	if (data.found) {
		selection = gtk_tree_view_get_selection (tree);

		/* FIXME: Rework this by making this file implement a subclass
		 * of the tree view instead, with a signal that is emitted when
		 * the selected source needs updating. Unselect/select for now.
		 */
		gtk_tree_selection_unselect_iter (selection, &data.iter);
		gtk_tree_selection_select_iter (selection, &data.iter);
	}
}

static void
songs_changed_cb (Source *source, GtkTreeView *tree)
{
	Source *selected;

	selected = sources_view_get_selected (tree);

	if (source == selected) {
		
		g_object_set_data (G_OBJECT (tree), "last_selected", NULL);	

		sources_view_set_selected (tree, source);
	}
}

static void
view_append_internal (GtkTreeView *tree, Source *source, GtkTreeIter *iter)
{
	GtkTreeModel *model;
	GtkTreeIter   iter2;

	model = gtk_tree_view_get_model (tree);

	if (!iter) {
		iter = &iter2;
	}
	
	gtk_list_store_append (GTK_LIST_STORE (model), iter);
	gtk_list_store_set (GTK_LIST_STORE (model), iter,
			    COL_SOURCE, source,
			    -1);

	g_signal_connect (source,
			  "songs_changed",
			  G_CALLBACK (songs_changed_cb),
			  tree);
}

void
sources_view_append (GtkTreeView *tree, Source *source)
{
	view_append_internal (tree, source, NULL);
}

void
sources_view_append_and_edit (GtkTreeView *tree, Source *source)
{
	GtkTreeModel      *model;
	GtkTreeIter        iter;
	GtkTreePath       *path;
	GtkTreeViewColumn *col;

	view_append_internal (tree, source, &iter);

	model = gtk_tree_view_get_model (tree);
	path = gtk_tree_model_get_path (model, &iter);

	col = gtk_tree_view_get_column (tree, 0);
	gtk_tree_view_set_cursor (tree, path, col, TRUE);

	gtk_tree_path_free (path);
}

void
sources_view_remove (GtkTreeView *tree, Source *source)
{
	GtkTreeModel   *model;
	FindSourceData  data;

	model = gtk_tree_view_get_model (tree);

	data.found = FALSE;
	data.source = source;
	
	gtk_tree_model_foreach (model,
				find_source_foreach,
				&data);

	if (data.found) {
		g_signal_handlers_disconnect_by_func (source,
						      songs_changed_cb,
						      tree);
		
		gtk_list_store_remove (GTK_LIST_STORE (model), &data.iter);
	}
}

static gboolean
save_foreach (GtkTreeModel *model,
	      GtkTreePath  *path,
	      GtkTreeIter  *iter,
	      gpointer      data)
{
	GList  **list;
	Source  *source;

	list = data;

	gtk_tree_model_get (model, iter,
			    COL_SOURCE, &source,
			    -1);
	
	*list = g_list_prepend (*list, source);
	
	return FALSE;
}

static void
save_sources (GtkTreeView *view)
{
	GtkTreeModel *model;
	GList        *sources;

	sources = NULL;
	
	model = gtk_tree_view_get_model (view);
	gtk_tree_model_foreach (model, save_foreach, &sources);

	sources = g_list_reverse (sources);

	sources_xml_save (sources);

	g_list_free (sources);
}

static void
save_selected_source (GtkTreeView *tree)
{
	Source     *source;
	const char *str = "";
  
	source = sources_view_get_selected (tree);
	if (source) {
		if (!IS_SOURCE_DATABASE (source)) {
			str = source_get_name (source);
		}
	}

	gconf_client_set_string (gconf_client, "/apps/jamboree/active_view", str, NULL);
}

static void
restore_selected_source (GtkTreeView *tree)
{
	char             *active_view;
	GtkTreeModel     *model;
	FindSourceData    data;
	GtkTreeSelection *selection;

	selection = gtk_tree_view_get_selection (tree);
	model = gtk_tree_view_get_model (tree);

	active_view = gconf_client_get_string (gconf_client, "/apps/jamboree/active_view", NULL);
	if (!active_view || !active_view[0]) {
		gtk_tree_model_get_iter_first (model, &data.iter);
		gtk_tree_selection_select_iter (selection, &data.iter);
		return;
	}

	data.found = FALSE;
	data.name = active_view;
	
	gtk_tree_model_foreach (model,
				find_source_by_name_foreach,
				&data);

	if (data.found) {
		gtk_tree_selection_select_iter (selection, &data.iter);
	}

	g_free (active_view);
}
