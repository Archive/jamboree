/*
 * Copyright (C) 2003-2004 Imendio HB
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "expr.h"

static void expr_clear (Expr *expr);

static time_t now = 0;

const char *
operator_to_string (ExprOp op)
{
	switch (op) {
	case EXPR_OP_NONE:
		return "none";
	case EXPR_OP_FAKE:
		return "fake";
	case EXPR_OP_CONTAINS:
		return "contains";
	case EXPR_OP_DOES_NOT_CONTAIN:
		return "does-not-contain";
	case EXPR_OP_STARTS_WITH:
		return "starts-with";
	case EXPR_OP_ENDS_WITH:
		return "ends-with";
	case EXPR_OP_EQ:
		return "equal";
	case EXPR_OP_NE:
		return "not-equal";
	case EXPR_OP_GE:
		return "greater-or-equal";
	case EXPR_OP_GT:
		return "greater";
	case EXPR_OP_LE:
		return "less-or-equal";
	case EXPR_OP_LT:
		return "less";
	case EXPR_OP_AND:
		return "and";
	case EXPR_OP_OR:
		return "or";
	case EXPR_OP_CONSTANT:
		return "constant";
	case EXPR_OP_VARIABLE:
		return "variable";
	case EXPR_OP_HAS_PLAYLIST:
		return "has-playlist";
	case EXPR_OP_RANGE:
		return "in-range";
	default:
		g_warning ("Invalid op.");
		return "invalid-op";
	}
}

ExprOp
operator_from_string (const char *str)
{
	ExprOp op;
  
	if (strcmp (str, "fake") == 0) {
		op = EXPR_OP_FAKE;
	}
	else if (strcmp (str, "contains") == 0) {
		op = EXPR_OP_CONTAINS;
	}
	else if (strcmp (str, "does-not-contain") == 0) {
		op = EXPR_OP_DOES_NOT_CONTAIN;
	}
	else if (strcmp (str, "starts-with") == 0) {
		op = EXPR_OP_STARTS_WITH;
	}
	else if (strcmp (str, "ends-with") == 0) {
		op = EXPR_OP_ENDS_WITH;
	}
	else if (strcmp (str, "equal") == 0) {
		op = EXPR_OP_EQ;
	}
	else if (strcmp (str, "not-equal") == 0) {
		op = EXPR_OP_NE;
	}
	else if (strcmp (str, "greater-or-equal") == 0) {
		op = EXPR_OP_GE;
	}
	else if (strcmp (str, "greater") == 0) {
		op = EXPR_OP_GT;
	}
	else if (strcmp (str, "less-or-equal") == 0) {
		op = EXPR_OP_LE;
	}
	else if (strcmp (str, "less") == 0) {
		op = EXPR_OP_LT;
	}
	else if (strcmp (str, "and") == 0) {
		op = EXPR_OP_AND;
	}
	else if (strcmp (str, "or") == 0) {
		op = EXPR_OP_OR;
	}
	else if (strcmp (str, "constant") == 0) {
		op = EXPR_OP_CONSTANT;
	}
	else if (strcmp (str, "variable") == 0) {
		op = EXPR_OP_VARIABLE;
	}
	else if (strcmp (str, "in-range") == 0) {
		op = EXPR_OP_RANGE;
	} else {
		op = EXPR_OP_NONE;
	}

	return op;
}

const char *
constant_type_to_string (ConstantType type)
{
	switch (type) {
	case CONSTANT_TYPE_NONE:
		return "none";
	case CONSTANT_TYPE_STRING:
		return "string";
	case CONSTANT_TYPE_INT:
		return "int";
	case CONSTANT_TYPE_DATE:
		return "date";
	case CONSTANT_TYPE_SIZE:
		return "size";
	case CONSTANT_TYPE_RANGE:
		return "range";
      
	default:
		g_warning ("Invalid type.");
		return "invalid-type";
	}
}

ConstantType
constant_type_from_string (const char *str)
{
	ConstantType type;
  
	if (strcmp (str, "string") == 0) {
		type = CONSTANT_TYPE_STRING;
	}
	else if (strcmp (str, "int") == 0) {
		type = CONSTANT_TYPE_INT;
	}
	else if (strcmp (str, "date") == 0) {
		type = CONSTANT_TYPE_DATE;
	}
	else if (strcmp (str, "size") == 0) {
		type = CONSTANT_TYPE_SIZE;
	}
	else if (strcmp (str, "range") == 0) {
		type = CONSTANT_TYPE_RANGE;
	} else {
		type = CONSTANT_TYPE_NONE;
	}

	return type;
}

const char *
variable_to_string (Variable variable)
{
	switch (variable) {
	case VARIABLE_TITLE:
		return "title";
	case VARIABLE_ARTIST:
		return "artist";
	case VARIABLE_ALBUM:
		return "album";
	case VARIABLE_TRACK:
		return "track-number";
	case VARIABLE_RATING:
		return "rating";
	case VARIABLE_YEAR:
		return "year";
	case VARIABLE_GENRE:
		return "genre";
	case VARIABLE_PLAYCOUNT:
		return "play-count";
	case VARIABLE_LENGTH:
		return "length";
	case VARIABLE_LAST_PLAYED:
		return "last-played";
	case VARIABLE_BIT_RATE:
		return "bit-rate";
	case VARIABLE_SAMPLE_RATE:
		return "sample-rate";      
	case VARIABLE_SIZE:
		return "size";
	case VARIABLE_DATE_ADDED:
		return "date-added";
	case VARIABLE_DATE_MODIFIED:
		return "date-modified";      
	case VARIABLE_PLAYLISTS:
		return "playlists";
	case VARIABLE_NONE:
		return "none";

	default:
		return "invalid-variable";
	}
}

Variable
variable_from_string (const char *str)
{
	Variable variable;
  
	if (strcmp (str, "title") == 0) {
		variable = VARIABLE_TITLE;
	}
	else if (strcmp (str, "artist") == 0) {
		variable = VARIABLE_ARTIST;
	}
	else if (strcmp (str, "album") == 0) {
		variable = VARIABLE_ALBUM;
	}
	else if (strcmp (str, "year") == 0) {
		variable = VARIABLE_YEAR;
	}
	else if (strcmp (str, "genre") == 0) {
		variable = VARIABLE_GENRE;
	}
	else if (strcmp (str, "rating") == 0) {
		variable = VARIABLE_RATING;
	}
	else if (strcmp (str, "track-number") == 0) {
		variable = VARIABLE_TRACK;
	}
	else if (strcmp (str, "play-count") == 0) {
		variable = VARIABLE_PLAYCOUNT;
	}
	else if (strcmp (str, "last-played") == 0) {
		variable = VARIABLE_LAST_PLAYED;
	}
	else if (strcmp (str, "length") == 0) {
		variable = VARIABLE_LENGTH;
	}
	else if (strcmp (str, "bit-rate") == 0) {
		variable = VARIABLE_BIT_RATE;
	}
	else if (strcmp (str, "size") == 0) {
		variable = VARIABLE_SIZE;
	}
	else if (strcmp (str, "sample-rate") == 0) {
		variable = VARIABLE_SAMPLE_RATE;
	}
	else if (strcmp (str, "comment") == 0) {
		variable = VARIABLE_COMMENT;
	}
	else if (strcmp (str, "date-modified") == 0) {
		variable = VARIABLE_DATE_MODIFIED;
	}
	else if (strcmp (str, "date-added") == 0) {
		variable = VARIABLE_DATE_ADDED;
	}
	else if (strcmp (str, "kind") == 0) {
		variable = VARIABLE_KIND;
	} else {
		variable = VARIABLE_NONE;
	}
	
	return variable;
}

static gboolean
is_binary (ExprOp op)
{
	switch (op) {
	case EXPR_OP_NONE:
	case EXPR_OP_CONSTANT:
	case EXPR_OP_VARIABLE:
	case EXPR_UNARY_OPS:
		return FALSE;

	case EXPR_BINARY_OPS:
		return TRUE;

	default:
		g_assert_not_reached ();
		break;
	}

	return FALSE;
}

static gboolean
is_unary (ExprOp op)
{
	switch (op) {
	case EXPR_OP_NONE:
	case EXPR_OP_CONSTANT:
	case EXPR_OP_VARIABLE:
	case EXPR_BINARY_OPS:
		return FALSE;

	case EXPR_UNARY_OPS:
		return TRUE;

	default:
		g_assert_not_reached ();
		break;
	}

	return FALSE;
}

Expr *
expr_new (ExprOp op)
{
	Expr *ret;

	g_return_val_if_fail (op != EXPR_OP_NONE, NULL);

	ret = g_new0 (Expr, 1);

	ret->op = op;

	return ret;
}

Expr *
expr_new_simple (const char *title,
		 const char *artist,
		 const char *album)
{
	Expr *v, *c;
	Expr *e, *tmp;
	
	e = NULL;
	
	if (title) {
		v = expr_new_variable (VARIABLE_TITLE);
		c = expr_new_constant (constant_string_new (title));
		
		e = expr_new_binary (EXPR_OP_CONTAINS, v, c);
	}
	
	if (artist) {
		v = expr_new_variable (VARIABLE_ARTIST);
		c = expr_new_constant (constant_string_new (artist));
		
		tmp = expr_new_binary (EXPR_OP_CONTAINS, v, c);
		
		if (e) {
			e = expr_new_binary (EXPR_OP_OR, e, tmp);
		} else {
			e = tmp;
		}
	}
	
	if (album) {
		v = expr_new_variable (VARIABLE_ALBUM);
		c = expr_new_constant (constant_string_new (album));
		
		tmp = expr_new_binary (EXPR_OP_CONTAINS, v, c);
		
		if (e) {
			e = expr_new_binary (EXPR_OP_OR, e, tmp);
		} else {
			e = tmp;
		}
	}
	
	return e;
}

static Constant *
constant_copy (Constant *source)
{
	Constant *ret;

	ret = g_new0 (Constant, 1);

	ret->type = source->type;

	switch (source->type) {
	case CONSTANT_TYPE_STRING:
		ret->v.str = g_strdup (source->v.str);
		break;
	case CONSTANT_TYPE_INT:
	case CONSTANT_TYPE_DATE:
	case CONSTANT_TYPE_SIZE:
	case CONSTANT_TYPE_RANGE:
		ret->v = source->v;
		break;

	default:
		g_assert_not_reached ();
	}

	return ret;
}

static Expr *
expr_copy_helper (Expr *source)
{
	Expr *dest;

	dest = expr_new (source->op);
  
	switch (source->op) {
	case EXPR_UNARY_OPS:
		expr_set_unary_op (dest, source->op,
				   expr_copy_helper (source->v.unary.expr));
		break;
      
	case EXPR_BINARY_OPS:
		expr_set_binary_op (dest, source->op,
				    expr_copy_helper (source->v.binary.expr_1),
				    expr_copy_helper (source->v.binary.expr_2));
		break;

	case EXPR_OP_CONSTANT:
		expr_set_constant_op (dest, constant_copy (source->v.constant));
		break;

	case EXPR_OP_VARIABLE:
		expr_set_variable_op (dest, source->v.variable);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	return dest;
}

Expr *
expr_copy (Expr *expr)
{
	g_return_val_if_fail (expr != NULL, NULL);

	return expr_copy_helper (expr);
}

Expr *
expr_new_unary (ExprOp op, Expr *expr)
{
	Expr *ret;

	g_return_val_if_fail (expr != NULL, NULL);
	g_return_val_if_fail (is_unary (op), NULL);

	ret = g_new0 (Expr, 1);

	ret->op = op;
	ret->v.unary.expr = expr;

	return ret;
}

Expr *
expr_new_binary (ExprOp op, Expr *expr_1, Expr *expr_2)
{
	Expr *ret;

	g_return_val_if_fail (expr_1 != NULL, NULL);
	g_return_val_if_fail (expr_2 != NULL, NULL);
	g_return_val_if_fail (is_binary (op), NULL);

	ret = g_new0 (Expr, 1);
  
	ret->op = op;

	ret->v.binary.expr_1 = expr_1;
	ret->v.binary.expr_2 = expr_2;

	return ret;
}

Expr *
expr_new_constant (Constant *constant)
{
	Expr *ret;

	ret = g_new0 (Expr, 1);

	ret->op = EXPR_OP_CONSTANT;
	ret->v.constant = constant;

	return ret;
}

Expr *
expr_new_variable (Variable variable)
{
	Expr *ret;

	ret = g_new0 (Expr, 1);

	ret->op = EXPR_OP_VARIABLE;
	ret->v.variable = variable;

	return ret;
}

void
expr_set_binary_op (Expr   *expr,
		    ExprOp  op,
		    Expr   *expr_1,
		    Expr   *expr_2)
{
	g_return_if_fail (expr != NULL);
	g_return_if_fail (expr_1 != NULL);
	g_return_if_fail (expr_2 != NULL);
	g_return_if_fail (is_binary (op));
  
	expr_clear (expr);
  
	expr->op = op;
	expr->v.binary.expr_1 = expr_1;
	expr->v.binary.expr_2 = expr_2;
}

void
expr_set_unary_op (Expr   *expr,
		   ExprOp  op,
		   Expr   *expr_1)
{
	g_return_if_fail (expr != NULL);
	g_return_if_fail (expr_1 != NULL);
	g_return_if_fail (is_unary (op));

	expr_clear (expr);
  
	expr->op = op;
	expr->v.unary.expr = expr_1;
}

void
expr_set_constant_op (Expr     *expr,
		      Constant *constant)
{
	g_return_if_fail (expr != NULL);
	g_return_if_fail (constant != NULL);

	expr_clear (expr);
  
	expr->op = EXPR_OP_CONSTANT;
	expr->v.constant = constant;
}

void
expr_set_variable_op (Expr     *expr,
		      Variable  variable)
{
	g_return_if_fail (expr != NULL);
	g_return_if_fail (variable != VARIABLE_NONE);

	expr_clear (expr);
  
	expr->op = EXPR_OP_VARIABLE;
	expr->v.variable = variable;
}

static void
expr_clear (Expr *expr)
{
	if (!expr) {
		return;
	}
	
	switch (expr->op) {
	case EXPR_UNARY_OPS:
		expr_free (expr->v.unary.expr);
		break;
      
	case EXPR_BINARY_OPS:
		expr_free (expr->v.binary.expr_1);
		expr_free (expr->v.binary.expr_2);
		break;

	case EXPR_OP_CONSTANT:
		constant_free (expr->v.constant);
		break;

	case EXPR_OP_VARIABLE:
		break;

	default:
		g_assert_not_reached ();
		break;
	}
}

void
expr_free (Expr *expr)
{
	if (!expr) {
		return;
	}
	
	expr_clear (expr);
	g_free (expr);
}

Constant *
constant_string_new (const char *str)
{
	Constant *ret;

	ret = g_new0 (Constant, 1);

	ret->type = CONSTANT_TYPE_STRING;
	ret->v.str = g_strdup (str);

	return ret;
}

Constant *
constant_int_new (int i)
{
	Constant *ret;

	ret = g_new0 (Constant, 1);

	ret->type = CONSTANT_TYPE_INT;
	ret->v.i = i;

	return ret;
}

Constant *
constant_date_new (time_t date, Unit unit)
{
	Constant *ret;

	ret = g_new0 (Constant, 1);

	ret->type = CONSTANT_TYPE_DATE;
	ret->v.date = date;
	ret->unit = unit;

	return ret;
}

Constant *
constant_range_new (int start, int end)
{
	Constant *ret;

	ret = g_new0 (Constant, 1);

	ret->type = CONSTANT_TYPE_RANGE;
	ret->v.range.start = start;
	ret->v.range.end = end;
  
	return ret;
}

void
constant_free (Constant *constant)
{
	if (!constant)
		return;

	switch (constant->type) {
	case CONSTANT_TYPE_STRING:
		g_free (constant->v.str);
		break;

	case CONSTANT_TYPE_INT:
	case CONSTANT_TYPE_DATE:
	case CONSTANT_TYPE_SIZE:
	case CONSTANT_TYPE_RANGE:
	case CONSTANT_TYPE_NONE:
		break;
	}

	g_free (constant);
}

static const char *
constant_get_string_value_const (Constant *constant)
{
	switch (constant->type) {
	case CONSTANT_TYPE_STRING:
		return constant->v.str;

	case CONSTANT_TYPE_RANGE:
	case CONSTANT_TYPE_NONE:
	case CONSTANT_TYPE_DATE:
	case CONSTANT_TYPE_SIZE:
	case CONSTANT_TYPE_INT:
		g_warning ("Trying to get const string value for constant type '%s'.",
			   constant_type_to_string (constant->type));
		return "";
	}

	return "";
}

char *
constant_get_string_value (Constant *constant)
{
	switch (constant->type) {
	case CONSTANT_TYPE_STRING:
		return g_strdup (constant->v.str);
	case CONSTANT_TYPE_SIZE:
	case CONSTANT_TYPE_INT:
		return g_strdup_printf ("%d", constant->v.i);
	case CONSTANT_TYPE_DATE:
		return g_strdup_printf ("%ld", (long) constant->v.date);
	case CONSTANT_TYPE_RANGE:
	case CONSTANT_TYPE_NONE:
		g_warning ("Trying to get string value for constant type '%s'.",
			   constant_type_to_string (constant->type));
		return 0;
	}

	return NULL;
}

void
constant_set_unit (Constant *constant,
                   Unit      unit)
{
	g_assert (constant->type == CONSTANT_TYPE_DATE);

	constant->unit = unit;
}

Unit
constant_get_unit (Constant *constant)
{
	g_assert (constant->type == CONSTANT_TYPE_DATE);
  
	return constant->unit;
}

const char *
unit_to_string (Unit unit)
{
	switch (unit) {
	case UNIT_MINUTES:
		return "minutes";
	case UNIT_HOURS:
		return "hours";
	case UNIT_DAYS:
		return "days";
	case UNIT_WEEKS:
		return "weeks";
	case UNIT_MONTHS:
		return "months";
	case UNIT_YEARS:
		return "years";
	default:
		g_warning ("Can't get string for unit %d.", unit);
		return NULL;
	}
}

Unit
unit_from_string (const char * str)
{
	if (strcmp (str, "minutes") == 0) {
		return UNIT_MINUTES;
	}
	else if (strcmp (str, "hours") == 0) {
		return UNIT_HOURS;
	}
	else if (strcmp (str, "days") == 0) {
		return UNIT_DAYS;
	}
	else if (strcmp (str, "weeks") == 0) {
		return UNIT_WEEKS;
	}
	else if (strcmp (str, "months") == 0) {
		return UNIT_MONTHS;
	}
	else if (strcmp (str, "years") == 0) {
		return UNIT_YEARS;
	} else {
		g_assert_not_reached ();
		return -1;
	}
}

static const char *
variable_get_string_value_const (Variable variable, Song *song)
{
	switch (variable) {
	case VARIABLE_TITLE:
		return song_get_title (song);
	case VARIABLE_ARTIST:
		return song_get_artist (song);
	case VARIABLE_ALBUM:
		return song_get_album (song);
	case VARIABLE_GENRE:
		return song_get_genre (song);
      
	case VARIABLE_YEAR:
	case VARIABLE_TRACK:
	case VARIABLE_SIZE:
	case VARIABLE_BIT_RATE:
	case VARIABLE_SAMPLE_RATE:
	case VARIABLE_LENGTH:
	case VARIABLE_PLAYCOUNT:
	case VARIABLE_LAST_PLAYED:
	case VARIABLE_RATING:
	default:
		g_warning ("Can't get string for variable %s.", variable_to_string (variable));
		return "";
	}
}

#if 0
static char *
variable_get_string_value (Variable variable, Song *song)
{
	const char *tmp;
  
	switch (variable) {
	case VARIABLE_TITLE:
		return g_strdup (song->title);
	case VARIABLE_ARTIST:
		return g_strdup (song->artist);
	case VARIABLE_ALBUM:
		return g_strdup (song->album);
	case VARIABLE_YEAR:
		return g_strdup_printf ("%d", song->year);
	case VARIABLE_TRACK_NUMBER:
		return g_strdup_printf ("%d", song->track_number);
	case VARIABLE_SIZE:
		return g_strdup_printf ("%lld", song->filesize);
	case VARIABLE_BIT_RATE:
		return g_strdup_printf ("%d", song->bitrate);
	case VARIABLE_SAMPLE_RATE:
		return g_strdup_printf ("%d", song->samplerate);
	case VARIABLE_LENGTH:
		return g_strdup_printf ("%d", song->length);
	case VARIABLE_PLAYCOUNT:
		return g_strdup_printf ("%d", song->playcount);
	case VARIABLE_LAST_PLAYED:
		return g_strdup_printf ("%d", (int) song->last_played);
	case VARIABLE_RATING:
		return g_strdup_printf ("%d", song->rating);
	case VARIABLE_GENRE:
		return g_strdup (song->genre);
      
	case VARIABLE_NONE:
		g_warning ("Trying to get string value for type VARIABLE_NONE.");
		return NULL;
      
	default:
		g_warning ("Can't get string for variable %s.", variable_to_string (variable));
		return NULL;
	}
}
#endif

int
constant_get_int_value (Constant *constant)
{
	switch (constant->type) {
	case CONSTANT_TYPE_STRING:
		return atoi (constant->v.str);
	case CONSTANT_TYPE_SIZE:
	case CONSTANT_TYPE_INT:
		return constant->v.i;
	case CONSTANT_TYPE_DATE:
		return constant->v.date;

	case CONSTANT_TYPE_RANGE:
	case CONSTANT_TYPE_NONE:
		g_warning ("Trying to get int value for constant type '%s'.",
			   constant_type_to_string (constant->type));
		return 0;
	}

	return 0;
}

void
constant_get_range_values (Constant *constant,
			   int      *start,
			   int      *end)
{
	g_assert (constant->type == CONSTANT_TYPE_RANGE);

	*start = constant->v.range.start;
	*end = constant->v.range.end;
}

static int
variable_get_int_value (Variable variable, Song *song)
{
	switch (variable) {
	case VARIABLE_TITLE:
	case VARIABLE_ARTIST:
	case VARIABLE_ALBUM:
		g_warning ("Can't get int for type %s.", variable_to_string (variable));
		return 0;
	case VARIABLE_TRACK:
		return song_get_track (song);
	case VARIABLE_YEAR:
		return song_get_year (song);
	case VARIABLE_SIZE:
		return song_get_size (song);
	case VARIABLE_BIT_RATE:
		return song_get_bitrate (song);
	case VARIABLE_SAMPLE_RATE:
		return song_get_samplerate (song);
	case VARIABLE_LENGTH:
		return song_get_duration (song);
	case VARIABLE_PLAYCOUNT:
		return song_get_playcount (song);
	case VARIABLE_LAST_PLAYED:
		return song_get_time_played (song);
	case VARIABLE_DATE_ADDED:
		return song_get_time_added (song);
	case VARIABLE_DATE_MODIFIED:
		return song_get_time_modified (song);
	case VARIABLE_RATING:
		return song_get_rating (song);
	case VARIABLE_NONE:
		g_warning ("Trying to get int value for VARIABLE_NONE.");
		return 0;
      
	default:
		g_warning ("Can't get int for variable %s.", variable_to_string (variable));
		return 0;
	}
}

static gboolean
get_now (gpointer data)
{
	now = time (NULL);
	return TRUE;
}

time_t
constant_get_date_value (Constant *constant)
{
	int list[] = { 60, 3600, 86400, 86400*7, 86400*30, 86400*365 };
	int unit = constant_get_unit (constant);

	if (G_UNLIKELY (now == 0)) {
		g_timeout_add (60*1000, get_now, NULL);
		get_now (NULL);
	}
  
	return now - (time_t) constant_get_int_value (constant) * list[unit];
}

#define variable_get_date_value(v,s) ((time_t) variable_get_int_value (v, s))

/*
 * Expression evaluation.
 */

static gboolean
boolean_compare_with_op (gboolean v1, gboolean v2, ExprOp op)
{
	switch (op) {
	case EXPR_OP_EQ:
		return v1 == v2;
	case EXPR_OP_NE:
		return v1 != v2;
	case EXPR_OP_LT:
		return v1 < v2;
	case EXPR_OP_GT:
		return v1 > v2;
	case EXPR_OP_LE:
		return v1 <= v2;
	case EXPR_OP_GE:
		return v1 >= v2;
	default:
		g_warning ("Can't compare booleans when op is %s.", operator_to_string (op));
		return FALSE;
	}
}

static gboolean
int_compare_with_op (int v1, int v2, ExprOp op)
{
	switch (op)
	{
	case EXPR_OP_EQ:
		return v1 == v2;
	case EXPR_OP_NE:
		return v1 != v2;
	case EXPR_OP_LT:
		return v1 < v2;
	case EXPR_OP_GT:
		return v1 > v2;
	case EXPR_OP_LE:
		return v1 <= v2;
	case EXPR_OP_GE:
		return v1 >= v2;
	default:
		g_warning ("Can't compare ints when op is %s.", operator_to_string (op));
		return FALSE;
	}
}

static gboolean
string_compare_with_op (const char *str1, const char *str2, ExprOp op)
{
	char     *tmp1, *tmp2;
	gboolean  ret;

	/* Be paranoid. */
	if (str1 == NULL || str2 == NULL) {
		return FALSE;
	}
	
	switch (op) {
	case EXPR_OP_EQ:
		return strcasecmp (str1, str2) == 0;
	case EXPR_OP_NE:
		return strcasecmp (str1, str2) != 0;
	case EXPR_OP_LT:
		return strcasecmp (str1, str2) < 0;
	case EXPR_OP_GT:
		return strcasecmp (str1, str2) > 0;
	case EXPR_OP_LE:
		return strcasecmp (str1, str2) <= 0;
	case EXPR_OP_GE:
		return strcasecmp (str1, str2) >= 0;

	case EXPR_OP_CONTAINS:
		tmp1 = g_utf8_casefold (str1, -1);
		tmp2 = g_utf8_casefold (str2, -1);

		ret = strstr (tmp1, tmp2) != NULL;

		g_free (tmp1);
		g_free (tmp2);

		return ret;
      
	case EXPR_OP_DOES_NOT_CONTAIN:
		tmp1 = g_utf8_casefold (str1, -1);
		tmp2 = g_utf8_casefold (str2, -1);

		ret = strstr (tmp1, tmp2) == NULL;

		g_free (tmp1);
		g_free (tmp2);

		return ret;

	default:
		g_warning ("Can't compare strings when op is %s.", operator_to_string (op));
		return FALSE;
	}
}

#define IS_CONSTANT_OR_VARIABLE(expr) (expr->op == EXPR_OP_CONSTANT || expr->op == EXPR_OP_VARIABLE)

static gboolean
evaluate_comparison (Expr *e1, Expr *e2, ExprOp op, Song *song)
{
	Constant    *constant;
	Variable    variable;
	const char *str1;
	const char *str2;
	int         i1, i2;
	int         start, end;
	gboolean    ret;

	/* The case where e1 or e2 are subexpressions, treat both of them as such. */
	if (!IS_CONSTANT_OR_VARIABLE (e1) || !IS_CONSTANT_OR_VARIABLE (e2)) {
		return boolean_compare_with_op (expr_evaluate (e1, song),
						expr_evaluate (e2, song), op);
	}
  
	if (e1->op == EXPR_OP_CONSTANT && e2->op == EXPR_OP_VARIABLE) {
		constant = e1->v.constant;
		variable = e2->v.variable;
	}
	else if (e1->op == EXPR_OP_VARIABLE && e2->op == EXPR_OP_CONSTANT) {
		variable = e1->v.variable;
		constant = e2->v.constant;
	} else {
		g_warning ("Binary ops must have a constant and a variable operand.");
		return FALSE;
	}

	/* Special case playlist handling... a bit ugly. */
	if (op == EXPR_OP_HAS_PLAYLIST) {
		GList *l;
		int    i;

		i = constant_get_int_value (constant);
      
		for (l = song_get_playlists (song); l; l = l->next) {
			if (GPOINTER_TO_INT (l->data) == i) {
				return TRUE;
			}
		}
      
		return FALSE;
	}
  
	switch (constant->type) {
	case CONSTANT_TYPE_STRING:
		str1 = variable_get_string_value_const (variable, song);
		str2 = constant_get_string_value_const (constant);
      
		ret = string_compare_with_op (str1, str2, op);
      
		return ret;

	case CONSTANT_TYPE_INT:
		i1 = variable_get_int_value (variable, song);
		i2 = constant_get_int_value (constant);
      
		return int_compare_with_op (i1, i2, op);

	case CONSTANT_TYPE_DATE:
		i1 = variable_get_date_value (variable, song);
		i2 = constant_get_date_value (constant);

		return int_compare_with_op (i1, i2, op);
      
	case CONSTANT_TYPE_RANGE:
		i1 = variable_get_date_value (variable, song);
		constant_get_range_values (constant, &start, &end);

		return i1 >= start && i1 <= end;
      
	case CONSTANT_TYPE_SIZE:
		g_warning ("Size not implemented yet.");
		return FALSE;

	case CONSTANT_TYPE_NONE:
		return FALSE;
	}
  
	return FALSE;
}

static gboolean
evaluate_binary_op (Expr *expr, Song *song)
{
	Expr     *e1, *e2;
	gboolean  v1, v2;

	/*g_assert (is_binary (expr->op));*/
  
	e1 = expr->v.binary.expr_1;
	e2 = expr->v.binary.expr_2;

	switch (expr->op) {
	case EXPR_OP_AND:
		/* Be lazy. */
		v1 = expr_evaluate (e1, song);
		if (!v1) {
			return FALSE;
		}
		
		v2 = expr_evaluate (e2, song);
		return v1 && v2;
      
	case EXPR_OP_OR:
		/* Be lazy. */
		v1 = expr_evaluate (e1, song);
		if (v1) {
			return TRUE;
		}
		
		v2 = expr_evaluate (e2, song);
		return v1 || v2;
      
	case EXPR_OP_EQ:
	case EXPR_OP_NE:
	case EXPR_OP_LT:
	case EXPR_OP_GT:
	case EXPR_OP_LE:
	case EXPR_OP_GE:
	case EXPR_OP_CONTAINS:
	case EXPR_OP_DOES_NOT_CONTAIN:
	case EXPR_OP_HAS_PLAYLIST:
	case EXPR_OP_RANGE:
		return evaluate_comparison (e1, e2, expr->op, song);
      
	default:
		g_warning ("Unimplemented op '%s'.", operator_to_string (expr->op));
		break;
	}

	return FALSE;
}

gboolean
expr_evaluate (Expr *expr, Song *song)
{
	g_return_val_if_fail (expr != NULL, FALSE);
	g_return_val_if_fail (song != NULL, FALSE);
  
	switch (expr->op)
	{
	case EXPR_UNARY_OPS:
		g_warning ("Eval of unary ops unimplemented.");
		return FALSE;
      
	case EXPR_BINARY_OPS:
		return evaluate_binary_op (expr, song);

	case EXPR_OP_CONSTANT:
		return TRUE;
      
	case EXPR_OP_VARIABLE:
		return TRUE;

	case EXPR_OP_NONE:
		g_warning ("Got EXPR_OP_NONE.");
		return FALSE;
      
	default:
		g_assert_not_reached ();
		break;
	}

	return FALSE;
}

gboolean
expr_equal (Expr *e1, Expr *e2)
{
	g_return_val_if_fail (e1 != NULL, FALSE);
	g_return_val_if_fail (e2 != NULL, FALSE);

	if (e1->op != e2->op) {
		return FALSE;
	}
  
	switch (e1->op) {
	case EXPR_UNARY_OPS:
		g_warning ("Unary ops unimplemented.");
		return FALSE;
      
	case EXPR_BINARY_OPS:
		return (expr_equal (e1->v.binary.expr_1, e2->v.binary.expr_1) &&
			expr_equal (e1->v.binary.expr_2, e2->v.binary.expr_2));
      
	case EXPR_OP_CONSTANT:
		if (e1->v.constant->type != e2->v.constant->type) {
			return FALSE;
		}

		if (e1->v.constant->unit != e2->v.constant->unit) {
			return FALSE;
		}

		switch (e1->v.constant->type) {
		case CONSTANT_TYPE_INT:
		case CONSTANT_TYPE_SIZE:
			return e1->v.constant->v.i == e2->v.constant->v.i;

		case CONSTANT_TYPE_STRING:
			return strcmp (e1->v.constant->v.str, e2->v.constant->v.str) == 0;

		case CONSTANT_TYPE_DATE:
			return e1->v.constant->v.date == e2->v.constant->v.date;

		case CONSTANT_TYPE_RANGE:
			return (e1->v.constant->v.range.start == e2->v.constant->v.range.start &&
				e1->v.constant->v.range.end == e2->v.constant->v.range.end);
	
		case CONSTANT_TYPE_NONE:
		default:	  
			g_warning ("Unhandled constant type.");
			return FALSE;
		}
		return FALSE;
     
	case EXPR_OP_VARIABLE:
		return (e1->v.variable == e2->v.variable);
      
	case EXPR_OP_NONE:
		g_warning ("Got EXPR_OP_NONE.");
		return FALSE;
      
	default:
		g_assert_not_reached ();
		break;
	}

	return FALSE;
}

/*
 * Output
 */

static void
string_append_padding (GString *str, int depth)
{
	char *tmp;
	int   len;

	len = depth * 2;
  
	tmp = g_new0 (char, len + 1);
	memset (tmp, ' ', len);

	g_string_append (str, tmp);

	g_free (tmp);
}

static void
output_constant (GString *str, Constant *constant)
{
	char *tmp;
	
	g_string_append_printf (str, "<constant type='%s' ", constant_type_to_string (constant->type));

	switch (constant->type) {
	case CONSTANT_TYPE_INT:
		g_string_append_printf (str, "value='%d'/>\n", constant_get_int_value (constant));
		break;

	case CONSTANT_TYPE_STRING:
		tmp = g_markup_escape_text (constant_get_string_value (constant), -1);
		g_string_append_printf (str, "value='%s'/>\n",  tmp);
		g_free (tmp);
		break;

	case CONSTANT_TYPE_DATE:
		g_string_append_printf (str, "value='%d' unit='%s'/>\n",
					constant_get_int_value (constant),
					unit_to_string (constant_get_unit (constant)));
		break;

	case CONSTANT_TYPE_RANGE: {
		int start, end;

		constant_get_range_values (constant, &start, &end);
	
		g_string_append_printf (str, "start='%d' end='%d'/>\n", start, end);
		break;
	}
	
	default:
		g_warning ("Type not implemented.");
		break;
	}
}

static void
output_expr (GString *str, Expr *expr, int depth)
{
	string_append_padding (str, depth);
  
	switch (expr->op) {
	case EXPR_UNARY_OPS:
		g_string_append (str, "<unary>");
		output_expr (str, expr->v.unary.expr, depth + 1);
		g_string_append (str, "</unary>");
		break;
      
	case EXPR_BINARY_OPS:
		g_string_append_printf (str, "<expr type='%s'>\n",
					operator_to_string (expr->op));
      
		output_expr (str, expr->v.binary.expr_1, depth + 1);
		output_expr (str, expr->v.binary.expr_2, depth + 1);
      
		string_append_padding (str, depth);
		g_string_append (str, "</expr>\n");
		break;

	case EXPR_OP_CONSTANT:
		output_constant (str, expr->v.constant);
		break;

	case EXPR_OP_VARIABLE:
		g_string_append_printf (str, "<variable type='%s'/>\n",
					variable_to_string (expr->v.variable));
		break;

	case EXPR_OP_NONE:
		g_string_append (str, "<no-op>");
		break;
      
	default:
		g_assert_not_reached ();
		break;
	}
}

char *
expr_to_xml (Expr *expr)
{
	GString *string;

	string = g_string_new (NULL);

	output_expr (string, expr, 2);

	return g_string_free (string, FALSE);
}
