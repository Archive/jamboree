/*
 * Copyright (C) 2003-2004 Imendio HB
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include <glib/gi18n.h>
#include <gdk/gdkx.h>
#include <gtk/gtkbbox.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkvbox.h>
#include "utils.h"


static GdkFilterReturn event_filter (GdkXEvent *xevent,
				     GdkEvent  *event,
				     gpointer   user_data);

static KeyCode         grab_key     (const char *keystring);

typedef struct {
	char    *name;
	XF86Key  key;
	KeyCode  code;
} KeyEntry;

static KeyEntry keys[] = {
	{ "XF86AudioNext", XF86AUDIO_NEXT, 0 },
	{ "XF86AudioPrev", XF86AUDIO_PREV, 0 },
	{ "XF86AudioPlay", XF86AUDIO_PLAY, 0 },
	{ "XF86AudioStop", XF86AUDIO_STOP, 0 },
	{ "XF86AudioPause", XF86AUDIO_PAUSE, 0 },
	{ NULL, 0, 0 }
};

typedef struct {
	KeyGrabFunction function;
	gpointer user_data;
} Helper;

GtkWidget *
hig_dialog_new (GtkWindow      *parent,
		GtkDialogFlags  flags,
		GtkMessageType  type,
		GtkButtonsType  buttons,
		const char      *header,
		const char      *messagefmt,
		...)
{
	GtkWidget *dialog;
	GtkWidget *dialog_vbox;
	GtkWidget *dialog_action_area;
	GtkWidget *hbox;
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *button;
	GtkWidget *image;
	char      *title;
	va_list    args;
	char      *msg;
	char      *hdr;

	if (messagefmt && messagefmt[0]) {
		va_start (args, messagefmt);
		msg = g_strdup_vprintf (messagefmt, args);
		va_end (args);
	} else { 
		msg = NULL;
	}
	
	if (header) {
		hdr = g_markup_escape_text (header, -1);
	} else { 
		hdr = NULL;
	}
	
	dialog = gtk_dialog_new ();
	
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_window_set_title (GTK_WINDOW (dialog), "");
  
	dialog_vbox = GTK_DIALOG (dialog)->vbox;
	gtk_box_set_spacing (GTK_BOX (dialog_vbox), 12);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (dialog_vbox), hbox, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
	gtk_widget_show (hbox);

	switch (type) {
	case GTK_MESSAGE_ERROR:
		image = gtk_image_new_from_stock ("gtk-dialog-error", GTK_ICON_SIZE_DIALOG);
		break;
      
	case GTK_MESSAGE_QUESTION:
		image = gtk_image_new_from_stock ("gtk-dialog-question", GTK_ICON_SIZE_DIALOG);
		break;
      
	case GTK_MESSAGE_INFO:
		image = gtk_image_new_from_stock ("gtk-dialog-info", GTK_ICON_SIZE_DIALOG);
		break;
      
	case GTK_MESSAGE_WARNING:
		image = gtk_image_new_from_stock ("gtk-dialog-warning", GTK_ICON_SIZE_DIALOG);
		break;
      
	default:
		image = NULL;
		g_assert_not_reached ();
	}
  
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_widget_show (image);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show (vbox);

	if (hdr) {
		title = g_strconcat ("<span weight='bold' size='larger'>", hdr, "</span>\n", NULL);
		label = gtk_label_new (title);  
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
		gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
		gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_widget_show (label);
		g_free (title);
	}
  
	if (msg && msg[0]) {
		label = gtk_label_new (msg);
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
		gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_widget_show (label);
	}
  
	dialog_action_area = GTK_DIALOG (dialog)->action_area;
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area), GTK_BUTTONBOX_END);
  
	switch (buttons) {		
	case GTK_BUTTONS_NONE:
		break;
      
	case GTK_BUTTONS_OK:
		button = gtk_button_new_from_stock (GTK_STOCK_OK);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_OK);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
      
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
		break;
      
	case GTK_BUTTONS_CLOSE:
		button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CLOSE);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
		
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);
		break;
		
	case GTK_BUTTONS_CANCEL:
		button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CANCEL);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
		
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CANCEL);
		break;
		
	case GTK_BUTTONS_YES_NO:
		button = gtk_button_new_from_stock (GTK_STOCK_NO);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_NO);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);

		button = gtk_button_new_from_stock (GTK_STOCK_YES);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_YES);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);

		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_YES);
		break;
		
		
	case GTK_BUTTONS_OK_CANCEL:
		button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
		gtk_widget_show (button);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CANCEL);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);

		button = gtk_button_new_from_stock (GTK_STOCK_OK);
		gtk_widget_show (button);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_OK);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);

		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	if (parent != NULL) {
		gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	}
	
	if (flags & GTK_DIALOG_MODAL) {
		gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	}
	
	if (flags & GTK_DIALOG_DESTROY_WITH_PARENT) {
		gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog), TRUE);
	}
	
	g_free (msg);
	g_free (hdr);
  
	return dialog;
}

static GdkFilterReturn
event_filter (GdkXEvent *xevent,
	      GdkEvent  *event,
	      gpointer   user_data)
{
	XKeyEvent *keyevent = (XKeyEvent *) xevent;
	int        i;
	Helper    *helper = (Helper*) user_data;
  
	if (((XEvent *) xevent)->type != KeyPress) {
		return GDK_FILTER_CONTINUE;
	}
  
	for (i = 0; i < XF86AUDIO_MAX; i++) {
		if (keys[i].code == keyevent->keycode) {
			break;
		}
	}

	switch (keys[i].key) {
	case XF86AUDIO_PLAY:
	case XF86AUDIO_STOP:
	case XF86AUDIO_PREV:
	case XF86AUDIO_NEXT:
	case XF86AUDIO_PAUSE:
		helper->function (keys[i].key, helper->user_data);
		break;

	default:
		return GDK_FILTER_CONTINUE;
	}

	return GDK_FILTER_REMOVE;
}

static KeyCode
grab_key (const char *keystring)
{
	KeySym  sym;
	KeyCode code;
  
	sym = XStringToKeysym (keystring);
	if (sym == NoSymbol) {
		return 0;
	}
  
	code = XKeysymToKeycode (GDK_DISPLAY (), sym);
	if (code == 0) {
		return 0;
	}
  
	gdk_error_trap_push ();

	XGrabKey (GDK_DISPLAY (), code,
		  0,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);
	XGrabKey (GDK_DISPLAY (), code,
		  Mod2Mask,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);
	XGrabKey (GDK_DISPLAY (), code,
		  Mod5Mask,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);
	XGrabKey (GDK_DISPLAY (), code,
		  LockMask,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);
	XGrabKey (GDK_DISPLAY (), code,
		  Mod2Mask | LockMask,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);
	XGrabKey (GDK_DISPLAY (), code,
		  Mod5Mask | LockMask,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);
	XGrabKey (GDK_DISPLAY (), code,
		  Mod2Mask | Mod5Mask,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);
	XGrabKey (GDK_DISPLAY (), code,
		  Mod2Mask | Mod5Mask | LockMask,
		  GDK_ROOT_WINDOW (), True,
		  GrabModeAsync, GrabModeAsync);

	gdk_flush ();

	if (gdk_error_trap_pop ()) {
		/* Silently fail... */
		return 0;
	}

	return code;
}

void
grab_keys (KeyGrabFunction function,
	   gpointer        user_data)
{
	int      i;
	KeyCode  code;
	Helper  *helper;

	for (i = 0; keys[i].name; i++) {
		code = grab_key (keys[i].name);
		if (code) {
			keys[i].code = code;
		}
	}

	helper = g_new (Helper, 1);
	helper->function = function;
	helper->user_data = user_data;
  
	gdk_window_add_filter (gdk_get_default_root_window (),
			       event_filter,
			       helper);
}

static void
hack_selection_list_free (GList *list)
{
	g_list_foreach (list, (GFunc) gtk_tree_row_reference_free, NULL);
	g_list_free (list);
}

static gboolean
hack_selection_lists_equal (GList *old_ref_list, GList *new_path_list)
{
	int          len;
	GList       *ol, *nl;
	int          matches;
	GtkTreePath *old_path;

	len = g_list_length (old_ref_list);
	
	/* Different number of selected items => selection changed. */
	if (len != g_list_length (new_path_list)) {
		return FALSE;
	}

	matches = 0;
	for (ol = old_ref_list; ol; ol = ol->next) {
		for (nl = new_path_list; nl; nl = nl->next) {
			old_path = gtk_tree_row_reference_get_path (ol->data);

			if (gtk_tree_path_compare (old_path, nl->data) == 0) {
				matches++;

				gtk_tree_path_free (old_path);				
				break;
			}
			
			gtk_tree_path_free (old_path);
		}
	}
	
	if (matches != len) {
		return FALSE;
	}

	return TRUE;
}
	
static void
hack_selection_changed_cb (GtkTreeSelection *selection, GtkTreeView *tree)
{
	GList               *old_ref_list, *new_ref_list;
	GList               *new_path_list, *l;
	GtkTreeModel        *model;
	GtkTreeRowReference *ref;

	new_path_list = gtk_tree_selection_get_selected_rows (selection, &model);
	old_ref_list = g_object_get_data (G_OBJECT (tree), "hack_row_ref_list");

	if (hack_selection_lists_equal (old_ref_list, new_path_list)) {
		/* Don't emit the signal if we have the same selection. */
		g_signal_stop_emission_by_name (selection, "changed");

		g_list_foreach (new_path_list, (GFunc) gtk_tree_path_free, NULL);
		g_list_free (new_path_list);

		return;
	}

	/* Selection did change, get the new ref list. */
	new_ref_list = NULL;
	for (l = new_path_list; l; l = l->next) {
		ref = gtk_tree_row_reference_new (model, l->data);
		new_ref_list = g_list_prepend (new_ref_list, ref);

		gtk_tree_path_free (l->data);
	}
	g_list_free (new_path_list);
	
	g_object_set_data (G_OBJECT (tree), "hack_row_ref_list", new_ref_list);

	hack_selection_list_free (old_ref_list);
}

/* Hack to get the tree selection to only emit when we have real
 * changes. Makes the code using the treeview much simpler.
 */
void
hack_tree_view_setup_selection (GtkTreeView *tree)
{
	GtkTreeSelection *selection;

	selection = gtk_tree_view_get_selection (tree);
	
	g_signal_connect (selection,
			  "changed",
			  G_CALLBACK (hack_selection_changed_cb),
			  tree);
}
