/*
 * Copyright (C) 2003-2004 Imendio HB
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SOURCE_DATABASE_H__
#define __SOURCE_DATABASE_H__

#include <glib-object.h>
#include <gdbm.h>
#include "source.h"

#define TYPE_SOURCE_DATABASE            (source_database_get_type ())
#define SOURCE_DATABASE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SOURCE_DATABASE, SourceDatabase))
#define SOURCE_DATABASE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SOURCE_DATABASE, SourceDatabaseClass))
#define IS_SOURCE_DATABASE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SOURCE_DATABASE))
#define IS_SOURCE_DATABASE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SOURCE_DATABASE))
#define SOURCE_DATABASE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SOURCE_DATABASE, SourceDatabaseClass))


typedef struct _SourceDatabase      SourceDatabase;
typedef struct _SourceDatabaseClass SourceDatabaseClass;
typedef struct _SourceDatabasePriv  SourceDatabasePriv;


typedef gboolean (*SourceDatabaseAddProgressFunc) (SourceDatabase *, const char *, guint, guint, gpointer);

struct _SourceDatabase {
	Source              parent;
	SourceDatabasePriv *priv;
};

struct _SourceDatabaseClass {
	SourceClass parent_class;
};

GType    source_database_get_type        (void) G_GNUC_CONST;
Source * source_database_new             (const gchar                   *filename);
gboolean source_database_add_dir         (SourceDatabase                *db,
					  const char                    *path,
					  SourceDatabaseAddProgressFunc  progress_callback,
					  gpointer                       user_data);
void     source_database_update_song     (SourceDatabase                *db,
					  Song                          *song);
gboolean source_database_remove_song     (SourceDatabase                *db,
					  Song                          *song);
void     source_database_reset_playcount (SourceDatabase                *database);


#endif /* __SOURCE_DATABASE_H__ */
