/*
 * Copyright (C) 2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib/gi18n.h>
#include <glade/glade.h>
#include <gtk/gtk.h>
#include "song-properties.h"
/*#include "tag-writer.h"*/
#include "main-window.h"

typedef struct {
	Song      *song;

	GtkWidget *title_entry;
	GtkWidget *album_entry;
	GtkWidget *artist_entry;
	GtkWidget *genre_combo;
	GtkWidget *track_entry;
	GtkWidget *tracks_entry;
	GtkWidget *disc_entry;
	GtkWidget *discs_entry;
	GtkWidget *year_entry;
} SingleData;

typedef struct {
	GList     *songs;

	GtkWidget *album_entry;
	GtkWidget *artist_entry;
	GtkWidget *genre_combo;
	GtkWidget *tracks_entry;
	GtkWidget *disc_entry;
	GtkWidget *discs_entry;
	GtkWidget *year_entry;

	GtkWidget *album_checkbutton;
	GtkWidget *artist_checkbutton;
	GtkWidget *genre_checkbutton;
	GtkWidget *tracks_checkbutton;
	GtkWidget *disc_checkbutton;
	
	GtkWidget *year_checkbutton;
} MultipleData;

typedef struct {
	GtkWidget    *window;

	Source       *source;

	gboolean      is_single;
	
	SingleData    single;
	MultipleData  multiple;
} PropertiesData;


static void properties_reset_songs            (PropertiesData *data);
static void properties_update_source_single   (PropertiesData *data);
static void properties_update_source_multiple (PropertiesData *data);
static void properties_set_multiple_songs     (PropertiesData *data,
					       GList          *songs);
static void properties_set_single_song        (PropertiesData *data,
					       Song           *song);
static void properties_set_songs              (PropertiesData *data,
					       GList          *songs);


/* Hard-coded list of genres that always are present in the combobox, in
 * addition to the ones in the database.
 */
static char *genres[] = {
	"Pop",
	"Rock",
	"Heavy Metal",
	"Jazz",
	"Classical",
	"Blues"
};

static int num_genres = G_N_ELEMENTS (genres);

static gboolean
entry_get_int (GtkEntry *entry, int *i)
{
	const char *str;
	char       *endptr;
	
	str = gtk_entry_get_text (entry);
	*i = strtol (str, &endptr, 10);
	if (endptr != str) {
		return TRUE;
	}

	return FALSE;
}

static void
entry_set_int (GtkEntry *entry, int i)
{
	char *tmp;

	if (i > 0) {
		tmp = g_strdup_printf ("%d", i);
		gtk_entry_set_text (entry, tmp);
		g_free (tmp);
	} else {
		gtk_entry_set_text (entry, "");
	}
}

static void
properties_window_destroy_cb (GtkWidget      *window,
			      PropertiesData *data)
{
	properties_reset_songs (data);
		
	g_object_unref (data->source);
	
	g_free (data);
}

static void
properties_cancel_clicked_cb (GtkWidget      *button,
			      PropertiesData *data)
{
	gtk_widget_destroy (data->window);
}

static void
properties_back_clicked_cb (GtkWidget      *button,
			    PropertiesData *data)
{
	MainWindow *window;
	Song       *song;

	g_return_if_fail (data->is_single);
	g_return_if_fail (data->single.song);
	
	properties_update_source_single (data);

	window = main_window_get ();

	song = main_window_get_prev_song (window, data->single.song);
	if (song) {
		properties_set_single_song (data, song);
	}
}

static void
properties_forward_clicked_cb (GtkWidget          *button,
			       PropertiesData *data)
{
	MainWindow *window;
	Song       *song;

	g_return_if_fail (data->is_single);
	g_return_if_fail (data->single.song);

	properties_update_source_single (data);

	window = main_window_get ();

	song = main_window_get_next_song (window, data->single.song);
	if (song) {
		properties_set_single_song (data, song);
	}
}

static void
properties_ok_clicked_cb (GtkWidget      *button,
			  PropertiesData *data)
{
	gtk_widget_hide (data->window);

	if (data->is_single) {
		properties_update_source_single (data);
	} else {
		properties_update_source_multiple (data);
	}
		
	gtk_widget_destroy (data->window);
}

static void
properties_update_source_single (PropertiesData *data)
{
	Song       *song;
	const char *str;
	gboolean    changed;
	int         year;

	g_return_if_fail (data->is_single);
	
	song = data->single.song;
	
	changed = FALSE;
	
	str = gtk_entry_get_text (GTK_ENTRY (data->single.title_entry));
	changed |= source_song_set_title (data->source, song, str);
	
	str = gtk_entry_get_text (GTK_ENTRY (data->single.album_entry));
	changed |= source_song_set_album (data->source, song, str);
	
	str = gtk_entry_get_text (GTK_ENTRY (data->single.artist_entry));
	changed |= source_song_set_artist (data->source, song, str);

	str = gtk_entry_get_text (GTK_ENTRY (GTK_BIN (data->single.genre_combo)->child));
	changed |= source_song_set_genre (data->source, song, str);
	
	if (entry_get_int (GTK_ENTRY (data->single.year_entry), &year)) {
		changed |= source_song_set_year (data->source, song, year);
	}

	// FIXME: add disc, track etc
	
	source_update_song (data->source, song);

	if (changed) {
		g_print ("FIXME: save to disk\n");
	}
}

static void
properties_update_source_multiple (PropertiesData *data)
{
	GList      *songs, *l;
	Song       *song;
	const char *str;
	gboolean    changed;
	int         year;

	g_return_if_fail (!data->is_single);
	
	songs = data->multiple.songs;

	for (l = songs; l; l = l->next) {
		changed = FALSE;

		song = l->data;

		// FIXME: check if the checkbuttons
		
		str = gtk_entry_get_text (GTK_ENTRY (data->multiple.album_entry));
		changed |= source_song_set_album (data->source, song, str);
	
		str = gtk_entry_get_text (GTK_ENTRY (data->multiple.artist_entry));
		changed |= source_song_set_artist (data->source, song, str);

		str = gtk_entry_get_text (GTK_ENTRY (GTK_BIN (data->multiple.genre_combo)->child));
		changed |= source_song_set_genre (data->source, song, str);
	
		if (entry_get_int (GTK_ENTRY (data->multiple.year_entry), &year)) {
			changed |= source_song_set_year (data->source, song, year);
		}

		// FIXME: add disc, track etc
	
		source_update_song (data->source, song);

		if (changed) {
			g_print ("FIXME: save to disk\n");
		}
	}
}

static void
properties_reset_songs (PropertiesData *data)
{
	if (data->is_single) {
		if (data->single.song) {
			song_unref (data->single.song);
			data->single.song = NULL;
		}
	} else {
		g_list_foreach (data->multiple.songs, (GFunc) song_unref, NULL);
		g_list_free (data->multiple.songs);
		data->multiple.songs = NULL;
	}
}

static void
properties_set_multiple_songs (PropertiesData *data, GList *songs)
{
	Song *song;

	g_return_if_fail (!data->is_single);
	
	properties_reset_songs (data);

	data->multiple.songs = g_list_copy (songs);
	g_list_foreach (data->multiple.songs, (GFunc) song_ref, NULL);	

	/* FIXME: choose sensible values from the songs. Just use the first for
	 * now.
	 */
	song = songs->data;
	
	gtk_entry_set_text (GTK_ENTRY (data->multiple.artist_entry), song_get_artist (song));
	gtk_entry_set_text (GTK_ENTRY (data->multiple.album_entry), song_get_album (song));
	gtk_entry_set_text (GTK_ENTRY (GTK_BIN (data->multiple.genre_combo)->child), song_get_genre (song));
	//entry_set_int (GTK_ENTRY (data->multiple.tracks_entry), song_get_tracks (song));
	//entry_set_int (GTK_ENTRY (data->multiple.disc_entry), song_get_disc (song));
	//entry_set_int (GTK_ENTRY (data->multiple.discs_entry), song_get_discs (song));
	entry_set_int (GTK_ENTRY (data->multiple.year_entry), song_get_year (song));

	/* FIXME: How should this be setup? */
	
	/*gtk_widget_set_sensitive (data->multiple.artist_entry, FALSE);
	gtk_widget_set_sensitive (data->multiple.album_entry, FALSE);
	gtk_widget_set_sensitive (data->multiple.genre_combo, FALSE);
	gtk_widget_set_sensitive (data->multiple.tracks_entry, FALSE);
	gtk_widget_set_sensitive (data->multiple.discs_entry, FALSE);
	gtk_widget_set_sensitive (data->multiple.disc_entry, FALSE);
	gtk_widget_set_sensitive (data->multiple.year_entry, FALSE);
	*/

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (data->multiple.artist_checkbutton), TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (data->multiple.album_checkbutton), TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (data->multiple.genre_checkbutton), TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (data->multiple.disc_checkbutton), TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (data->multiple.tracks_checkbutton), TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (data->multiple.year_checkbutton), TRUE);
}

static void
properties_set_single_song (PropertiesData *data, Song *song)
{
	g_return_if_fail (data->is_single);

	properties_reset_songs (data);
	
	data->single.song = song_ref (song);

	gtk_entry_set_text (GTK_ENTRY (data->single.title_entry), song_get_title (song));
	gtk_entry_set_text (GTK_ENTRY (data->single.artist_entry), song_get_artist (song));
	gtk_entry_set_text (GTK_ENTRY (data->single.album_entry), song_get_album (song));
	gtk_entry_set_text (GTK_ENTRY (GTK_BIN (data->single.genre_combo)->child), song_get_genre (song));
	entry_set_int (GTK_ENTRY (data->single.track_entry), song_get_track (song));
	//entry_set_int (GTK_ENTRY (data->single.tracks_entry), song_get_tracks (song));
	//entry_set_int (GTK_ENTRY (data->single.disc_entry), song_get_disc (song));
	//entry_set_int (GTK_ENTRY (data->single.discs_entry), song_get_discs (song));
	entry_set_int (GTK_ENTRY (data->single.year_entry), song_get_year (song));
}

static void
properties_set_songs (PropertiesData *data, GList *songs)
{
	if (data->is_single) {
		properties_set_single_song (data, songs->data);
	} else {
		properties_set_multiple_songs (data, songs);
	}
}

static void
add_to_store_if_unique (GtkListStore *store, GHashTable *hash, char *str)
{
	GtkTreeIter iter;

	if (!g_hash_table_lookup (hash, str)) {
		g_hash_table_insert (hash, str, str);
			
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
				    0, str,
				    -1);
	}
}

static void
setup_completion (GtkWidget *entry, GtkListStore *store)
{
	GtkEntryCompletion *completion;
	
	completion = gtk_entry_completion_new ();

	gtk_entry_completion_set_text_column (completion, 0);
	gtk_entry_completion_set_model (completion, GTK_TREE_MODEL (store));
	gtk_entry_set_completion (GTK_ENTRY (entry), completion);

	g_object_unref (completion);
	g_object_unref (store);
}

static void
setup_combo_box (GtkWidget *entry, GtkListStore *store)
{
	/* Sort the list. */
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (store),
					      0, GTK_SORT_ASCENDING);
	
	gtk_combo_box_set_model (GTK_COMBO_BOX (entry), GTK_TREE_MODEL (store));
	g_object_unref (store);
}

static void
properties_create_single_ui (PropertiesData *data, GladeXML *glade)
{
	GtkSizeGroup *group;
	GList        *list, *l;
	GtkWidget    *w;

	g_return_if_fail (data->is_single);
	
	data->window = glade_xml_get_widget (glade, "song_info_single_dialog");

	/* Setup the size group for the "Details" tab. */
	group = gtk_size_group_new (GTK_SIZE_GROUP_VERTICAL);
	list = glade_xml_get_widget_prefix (glade, "details_group");
	
	for (l = list; l; l = l->next) {
		gtk_size_group_add_widget (group, l->data);
	}

	gtk_size_group_add_widget (group, glade_xml_get_widget (glade, "location_entry"));
	gtk_size_group_add_widget (group, glade_xml_get_widget (glade, "playcount_reset_button"));
	gtk_size_group_add_widget (group, glade_xml_get_widget (glade, "last_played_reset_button"));
	
	g_list_free (list);
	g_object_unref (group);
	
	w = glade_xml_get_widget (glade, "back_button");
	g_signal_connect (w, 
			  "clicked",
			  G_CALLBACK (properties_back_clicked_cb),
			  data);

	w = glade_xml_get_widget (glade, "forward_button");
	g_signal_connect (w, 
			  "clicked",
			  G_CALLBACK (properties_forward_clicked_cb),
			  data);
  
	data->single.title_entry = glade_xml_get_widget (glade, "title_entry");
	data->single.artist_entry = glade_xml_get_widget (glade, "artist_entry");
	data->single.album_entry = glade_xml_get_widget (glade, "album_entry");
	data->single.genre_combo = glade_xml_get_widget (glade, "genre_comboboxentry");
	data->single.track_entry = glade_xml_get_widget (glade, "track_entry");
	data->single.tracks_entry = glade_xml_get_widget (glade, "tracks_entry");
	data->single.disc_entry = glade_xml_get_widget (glade, "disc_entry");
	data->single.discs_entry = glade_xml_get_widget (glade, "discs_entry");
	data->single.year_entry = glade_xml_get_widget (glade, "year_entry");
}

static void
checkbutton_toggled_cb (GtkToggleButton *button, GtkWidget *widget)
{
	gtk_widget_set_sensitive (widget,
				  gtk_toggle_button_get_active (button));
}

static void
properties_create_multiple_ui (PropertiesData *data, GladeXML *glade)
{
	g_return_if_fail (!data->is_single);

	data->window = glade_xml_get_widget (glade, "song_info_multiple_dialog");
	
	data->multiple.artist_entry = glade_xml_get_widget (glade, "artist_entry");
	data->multiple.album_entry = glade_xml_get_widget (glade, "album_entry");
	data->multiple.genre_combo = glade_xml_get_widget (glade, "genre_comboboxentry");
	data->multiple.tracks_entry = glade_xml_get_widget (glade, "tracks_entry");
	data->multiple.disc_entry = glade_xml_get_widget (glade, "disc_entry");
	data->multiple.discs_entry = glade_xml_get_widget (glade, "discs_entry");
	data->multiple.year_entry = glade_xml_get_widget (glade, "year_entry");

	data->multiple.artist_checkbutton = glade_xml_get_widget (glade, "artist_checkbutton");
	data->multiple.album_checkbutton = glade_xml_get_widget (glade, "album_checkbutton");
	data->multiple.genre_checkbutton = glade_xml_get_widget (glade, "genre_checkbutton");
	data->multiple.disc_checkbutton = glade_xml_get_widget (glade, "disc_checkbutton");
	data->multiple.tracks_checkbutton = glade_xml_get_widget (glade, "tracks_checkbutton");
	data->multiple.year_checkbutton = glade_xml_get_widget (glade, "year_checkbutton");

	g_signal_connect (data->multiple.artist_checkbutton,
			  "toggled",
			  G_CALLBACK (checkbutton_toggled_cb),
			  data->multiple.artist_entry);

	g_signal_connect (data->multiple.album_checkbutton,
			  "toggled",
			  G_CALLBACK (checkbutton_toggled_cb),
			  data->multiple.album_entry);

	g_signal_connect (data->multiple.genre_checkbutton,
			  "toggled",
			  G_CALLBACK (checkbutton_toggled_cb),
			  data->multiple.genre_combo);

	g_signal_connect (data->multiple.tracks_checkbutton,
			  "toggled",
			  G_CALLBACK (checkbutton_toggled_cb),
			  data->multiple.tracks_entry);

	g_signal_connect (data->multiple.disc_checkbutton,
			  "toggled",
			  G_CALLBACK (checkbutton_toggled_cb),
			  data->multiple.disc_entry);

	g_signal_connect (data->multiple.disc_checkbutton,
			  "toggled",
			  G_CALLBACK (checkbutton_toggled_cb),
			  data->multiple.discs_entry);

	g_signal_connect (data->multiple.year_checkbutton,
			  "toggled",
			  G_CALLBACK (checkbutton_toggled_cb),
			  data->multiple.year_entry);
}

void
song_properties_show (GtkWindow *parent,
		      GList     *songs,
		      Source    *source)
{
	static PropertiesData *data = NULL;
	gboolean               is_single;
	GladeXML              *glade;
	GtkWidget             *w;
	GHashTable            *artist_hash;
	GHashTable            *album_hash;
	GHashTable            *genre_hash;
	GtkListStore          *artist_store;
	GtkListStore          *album_store;
	GtkListStore          *genre_store;
	GList                 *all_songs, *a;
	char                  *str;
	int                    i;

	g_return_if_fail (songs != NULL);
	g_return_if_fail (source != NULL);
	
	if (g_list_length (songs) == 1) {
		is_single = TRUE;
	} else {
		is_single = FALSE;
	}
	
	if (data) {
		/* Check if we have the right kind of dialog setup already. */
		if (data->is_single != is_single) {
			gtk_widget_destroy (data->window);
			data = NULL;
		}
	}

	if (data) {
		properties_set_songs (data, songs);
		gtk_window_present (GTK_WINDOW (data->window));
		return;
	}

	data = g_new0 (PropertiesData, 1);
	
	data->source = g_object_ref (source);
	data->is_single = is_single;

	if (is_single) {
		glade = glade_xml_new (DATADIR "/jamboree/song-properties.glade",
				       "song_info_single_dialog", NULL);
		properties_create_single_ui (data, glade);
	} else {
		glade = glade_xml_new (DATADIR "/jamboree/song-properties.glade",
				       "song_info_multiple_dialog", NULL);
		properties_create_multiple_ui (data, glade);
	}		
	
	gtk_window_set_transient_for (GTK_WINDOW (data->window), parent);

	g_signal_connect (data->window,
			  "destroy",
			  G_CALLBACK (properties_window_destroy_cb),
			  data);
	
	w = glade_xml_get_widget (glade, "cancel_button");
	g_signal_connect (w, 
			  "clicked",
			  G_CALLBACK (properties_cancel_clicked_cb),
			  data);
  
	w = glade_xml_get_widget (glade, "ok_button");
	g_signal_connect (w, 
			  "clicked",
			  G_CALLBACK (properties_ok_clicked_cb),
			  data);
  
	g_object_add_weak_pointer (G_OBJECT (data->window), (gpointer) &data);
	
	properties_set_songs (data, songs);
	
	artist_hash = g_hash_table_new (g_str_hash, g_str_equal);
	album_hash = g_hash_table_new (g_str_hash, g_str_equal);
	genre_hash = g_hash_table_new (g_str_hash, g_str_equal);
	
	artist_store = gtk_list_store_new (1, G_TYPE_STRING);
	album_store = gtk_list_store_new (1, G_TYPE_STRING);
	genre_store = gtk_list_store_new (1, G_TYPE_STRING);

	/* Setup entry completion. */
	all_songs = source_get_songs (source);

	for (a = all_songs; a; a = a->next) {
		str = (char *) song_get_artist (a->data);
		add_to_store_if_unique (artist_store, artist_hash, str);
		
		str = (char *) song_get_album (a->data);
		add_to_store_if_unique (album_store, album_hash, str);
		
		str = (char *) song_get_genre (a->data);
		add_to_store_if_unique (genre_store, genre_hash, str);
	}

	for (i = 0; i < num_genres; i++) {
		add_to_store_if_unique (genre_store, genre_hash, genres[i]);
	}

	g_hash_table_destroy (artist_hash);
	g_hash_table_destroy (album_hash);
	g_hash_table_destroy (genre_hash);

	if (is_single) {
		setup_completion (data->single.artist_entry, artist_store);
		setup_completion (data->single.album_entry, album_store);
		setup_combo_box (data->single.genre_combo, genre_store);
	} else {
		setup_completion (data->multiple.artist_entry, artist_store);
		setup_completion (data->multiple.album_entry, album_store);
		setup_combo_box (data->multiple.genre_combo, genre_store);
	}		
	
	gtk_widget_show (data->window);
}



#if 0
typedef struct {
	GtkWidget  *dialog;
	GtkWidget  *progress;
	GtkWidget  *label;

	gboolean    cancel;
	guint       timeout_id;
} WriteTagsData;

static gboolean
write_tags_progress_show_cb (WriteTagsData *data)
{
	gtk_widget_show_all (data->dialog);

	data->timeout_id = 0;
  
	return FALSE;
}

static void
write_tags_response_cb (GtkWidget     *dialog,
			int            response,
			WriteTagsData *data)
{
	data->cancel = TRUE;
	data->dialog = NULL;

	gtk_widget_destroy (dialog);
}

static TagWriter *writer;

static void
test_save_foo (GtkWidget *window, GList *songs)
{
	GladeXML      *glade;
	WriteTagsData  data;
	GList         *l;
	Song          *song;
	GError        *error = NULL;

	if (!writer) {
		writer = tag_writer_new ();
	}
	
	data.cancel = FALSE;

	glade = glade_xml_new (DATADIR "/jamboree/jamboree.glade", "write_tags_dialog", NULL);

	data.dialog = glade_xml_get_widget (glade, "write_tags_dialog");
	data.label = glade_xml_get_widget (glade, "song_label");

	g_signal_connect (data.dialog,
			  "response",
			  G_CALLBACK (write_tags_response_cb),
			  &data);
  
	data.timeout_id = g_timeout_add (2000, (GSourceFunc) write_tags_progress_show_cb, &data);

	for (l = songs; l; l = l->next) {
		song = l->data;

		gtk_label_set_text (GTK_LABEL (data.label), song_get_title (song));

		if (!tag_writer_write_song (writer, song, &error)) {
			g_print ("ERROR: %s\n", error->message);
			g_error_free (error);
		}

		while (gtk_events_pending ()) {
			gtk_main_iteration ();
		}

		if (data.cancel) {
			g_print ("Cancel\n");
			break;
		}
	}
	
	if (data.timeout_id) {
		g_source_remove (data.timeout_id);
	}
	
	if (data.dialog) {
		gtk_widget_destroy (data.dialog);
	}
	
	g_object_unref (glade);
}
#endif




