/*
 * Copyright (C) 2004-2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <glib/gi18n.h>
#include <gtk/gtkmain.h>
#include <gconf/gconf-client.h>
#include <gst/gst.h>
#include "main-window.h"
#include "source-database.h"
#include "stock-icons.h"
#include "utils.h"
#include "string-utils.h"
#include "player.h"
#include "bacon-message-connection.h"

#ifdef HAVE_DBUS
#include "dbus.h"
#endif

GConfClient *gconf_client;

static void
ensure_dir (void)
{
	char *dir;
	
	dir = g_build_filename (g_get_home_dir (), ".gnome2", NULL);
	
	if (!g_file_test (dir, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
	}
	
	g_free (dir);
	
	dir = g_build_filename (g_get_home_dir (), ".gnome2", "jamboree", NULL);
	
	if (!g_file_test (dir, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
	}
	
	g_free (dir);
}

static void
bacon_func (const char *message,
	    gpointer    user_data)
{
	MainWindow *window;

	window = user_data;

	if (strcmp (message, "toggle-play") == 0) {
		main_window_handle_play (window, TRUE);
	}
	else if (strcmp (message, "play") == 0) {
		main_window_handle_play (window, FALSE);
	}
	else if (strcmp (message, "pause") == 0) {
		main_window_handle_pause (window);
	}
	else if (strcmp (message, "stop") == 0) {
		main_window_handle_stop (window);
		player_stop ();
	}
	else if (strcmp (message, "prev") == 0) {
		main_window_handle_prev (window);
	}
	else if (strcmp (message, "next") == 0) {
		main_window_handle_next (window);
	}
	else if (strcmp (message, "toggle-hidden") == 0) {
		main_window_toggle_visibility (window);
	}
	else if (strcmp (message, "select-all") == 0) {
		main_window_select_all (window);
	}
	else if (strcmp (message, "select-playing-artist") == 0) {
		main_window_select_playing_artist (window);
	}
	else if (strcmp (message, "select-playing-album") == 0) {
		main_window_select_playing_album (window);
	}
	else if (strcmp (message, "show") == 0) {
		main_window_present (window);
	}
	else if (strcmp (message, "quit") == 0) {
		gtk_main_quit ();
	} else {
		g_warning ("Unhandled cmd: %s\n", message);
	}
}

static gboolean  option_version;
static char     *option_db;
static gboolean  option_play;
static gboolean  option_pause;
static gboolean  option_toggle_play;
static gboolean  option_stop;
static gboolean  option_prev;
static gboolean  option_next;
static gboolean  option_hidden;
static gboolean  option_toggle_hidden;
static gboolean  option_select_all;
static gboolean  option_select_playing_artist;
static gboolean  option_select_playing_album;
static gboolean  option_quit;

static GOptionEntry entries[] = {
	{ "version", 'v', 0, G_OPTION_ARG_NONE, &option_version,
	  N_("Print version"), NULL
	},
	{ "db", '\0', 0, G_OPTION_ARG_FILENAME, &option_db,
	  N_("Alternative song database file"), N_("FILE"),
	},
	{ "play", '\0', 0, G_OPTION_ARG_NONE, &option_play,
	  N_("Start playing"), NULL
	},
	{ "pause", '\0', 0, G_OPTION_ARG_NONE, &option_pause,
	  N_("Pause"), NULL
	},
	{ "toggle-play", 't', 0, G_OPTION_ARG_NONE, &option_toggle_play,
	  N_("Toggle play and pause mode"), NULL
	},
	{ "stop", 's', 0, G_OPTION_ARG_NONE, &option_stop,
	  N_("Stop playing"), NULL
	},
	{ "next", 'n', 0, G_OPTION_ARG_NONE, &option_next,
	  N_("Jump to the next song"), NULL
	},
	{ "previous", 'p', 0, G_OPTION_ARG_NONE, &option_prev,
	  N_("Jump to the previous song"), NULL
	},
	{ "hidden", 'h', 0, G_OPTION_ARG_NONE, &option_hidden,
	  N_("Start without a visible window"), NULL
	},
	{ "toggle-hidden", '\0', 0, G_OPTION_ARG_NONE, &option_toggle_hidden,
	  N_("Toggle visibility of the window"), NULL
	},
	{ "select-all", '\0', 0, G_OPTION_ARG_NONE, &option_select_all,
	  N_("Show all songs"), NULL
	},
	{ "select-playing-artist", '\0', 0, G_OPTION_ARG_NONE, &option_select_playing_artist,
	  N_("Select the currently playing artist"), NULL
	},
	{ "select-playing-album", '\0', 0, G_OPTION_ARG_NONE, &option_select_playing_album,
	  N_("Select the currently playing album"), NULL
	},
	{ "quit", 'q', 0, G_OPTION_ARG_NONE, &option_quit,
	  N_("Quit any running instance"), NULL
	},
		
	{ NULL }
};

int
main (int argc, char **argv)
{
	GError                 *error = NULL;
	BaconMessageConnection *conn = NULL;
	gboolean                is_server;
	Source                 *database;
	GtkWidget              *window;
	GOptionContext         *ctx;
	char                   *filename;
 	
	bindtextdomain (GETTEXT_PACKAGE, JAMBOREE_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	ctx = g_option_context_new ("jamboree");
	g_option_context_add_main_entries (ctx, entries, GETTEXT_PACKAGE);
	g_option_context_add_group (ctx, gtk_get_option_group (TRUE));
	g_option_context_add_group (ctx, gst_init_get_option_group ());
	if (!g_option_context_parse (ctx, &argc, &argv, &error)) {
		g_printerr ("Option error: %s\n", error->message);
		return 1;
	}
	
	g_set_application_name ("Jamboree");

	if (option_version) {
		g_print ("Jamboree " VERSION "\n");
		exit (1);
	}

	/* If the database != the regular one, don't setup a server or try to
	 * contact one.
	 */
	if (!option_db) {
 		conn = bacon_message_connection_new ("jamboree");
		is_server = bacon_message_connection_get_is_server (conn);

		/* First, handle the client case (toggle, stop, next, prev). */
		if (!is_server) {
			if (option_play) {
				bacon_message_connection_send (conn, "play");
			}
			else if (option_pause) {
				bacon_message_connection_send (conn, "pause");
			}
			else if (option_toggle_play) {
				bacon_message_connection_send (conn, "toggle-play");
			}
			else if (option_stop) {
				bacon_message_connection_send (conn, "stop");
			}
			else if (option_prev) {
				bacon_message_connection_send (conn, "prev");
			}
			else if (option_next) {
				bacon_message_connection_send (conn, "next");
			}
			else if (option_toggle_hidden) {
				bacon_message_connection_send (conn, "toggle-hidden");
			}
			else if (option_select_all) {
				bacon_message_connection_send (conn, "select-all");
			}
			else if (option_select_playing_artist) {
				bacon_message_connection_send (conn, "select-playing-artist");
			}
			else if (option_select_playing_album) {
				bacon_message_connection_send (conn, "select-playing-album");
			}
			else if (option_quit) {
				bacon_message_connection_send (conn, "quit");
			}
			else if (option_hidden) {
				/* This does not make sense in client mode. */
			} else {
				bacon_message_connection_send (conn, "show");
			}
			
			gdk_notify_startup_complete ();
			
			return 0;
		} else {
			/* These do not make sense in server mode. */
			if (option_toggle_hidden || option_quit || option_pause ||
			    option_prev || option_next || option_stop || option_select_all ||
			    option_select_playing_artist || option_select_playing_album) {
				gdk_notify_startup_complete ();
				return 0;
			}
		}
	}
		
	shared_string_init ();

	g_random_set_seed (time (NULL));

	ensure_dir ();
	
	if (option_db != NULL) {
		filename = g_strdup (option_db);
	} else {
		filename = g_build_filename (g_get_home_dir (),
					     ".gnome2", "jamboree", "songs2.db",
					     NULL);
	}

	database = source_database_new (filename);
	if (!database) {
		GtkWidget *dialog;
		
		gdk_notify_startup_complete ();
		
		dialog = hig_dialog_new (NULL,
					 0,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_OK,
					 _("Could not read song database"),
					 _("Jamboree is most likely already running."));
		
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		
		exit (1);
	}
  
	g_free (filename);

#ifdef HAVE_DBUS
	jamboree_dbus_init_service ();
#endif
  
	gconf_client = gconf_client_get_default ();
	gconf_client_add_dir (gconf_client, GCONF_PATH,
			      GCONF_CLIENT_PRELOAD_ONELEVEL,
			      NULL);
	
	stock_icons_register ();
	window = main_window_new (database);

	g_object_unref (database);

	if (conn) {
		bacon_message_connection_set_callback (conn, bacon_func, window);
	}
	
	if (!option_hidden) {
		gtk_widget_show (window);
	}

	g_signal_connect (window,
			  "destroy", G_CALLBACK (gtk_main_quit),
			  NULL);
	
	gtk_main ();

	if (conn) {
		bacon_message_connection_free (conn);
	}
	
	shared_string_shutdown ();

	g_object_unref (gconf_client);

	return 0;
}
