/*
 * Copyright (C) 2003-2004 Imendio HB
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SOURCE_VIEW_H__
#define __SOURCE_VIEW_H__

#include <gtk/gtktreeview.h>
#include "source-model.h"

#define TYPE_SOURCE_VIEW            (source_view_get_type ())
#define SOURCE_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SOURCE_VIEW, SourceView))
#define SOURCE_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SOURCE_VIEW, SourceViewClass))
#define IS_SOURCE_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SOURCE_VIEW))
#define IS_SOURCE_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SOURCE_VIEW))
#define SOURCE_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SOURCE_VIEW, SourceViewClass))

typedef struct _SourceView      SourceView;
typedef struct _SourceViewClass SourceViewClass;
typedef struct _SourceViewPriv  SourceViewPriv;

struct _SourceView {
	GtkTreeView     parent;
	SourceViewPriv *priv;
};

struct _SourceViewClass {
	GtkTreeViewClass parent_class;
};

GType      source_view_get_type             (void) G_GNUC_CONST;
GtkWidget *source_view_new                  (void);
void       source_view_column_chooser       (SourceView *view,
					     GtkWindow  *parent);
void       source_view_setup_columns        (SourceView *view);
void       source_view_store_columns_widths (SourceView *view);


#endif /* __SOURCE_VIEW_H__ */
