/*
 * Copyright (C) 2003-2004 Richard Hult <richard@imendio.com>
 * Copyright (C) 2003      Anders Carlsson <andersca@gnome.org>
 * Copyright (C) 2003      Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <glib/gi18n.h>
#include "song-private.h"
#include "source.h"
#include "source-database.h"
#include "tag-reader.h"

#define VERSION_KEY "jamboree-version"
#define VERSION_KEY_LENGTH (16)
#define VERSION_NR 2


#define _ALIGN_VALUE(this, boundary) \
  (( ((unsigned long)(this)) + (((unsigned long)(boundary)) -1)) & (~(((unsigned long)(boundary))-1)))
	
#define _ALIGN_ADDRESS(this, boundary) \
  ((guchar *)_ALIGN_VALUE(this, boundary))


static void             source_database_class_init    (SourceDatabaseClass *klass);
static void             source_database_init          (SourceDatabase      *db);
static void             database_finalize             (GObject             *object);
static gboolean         database_update_song          (Source              *source,
						       Song                *song);
static GList *          database_get_songs            (Source              *list);
static const char *     database_get_name             (Source              *list);
static gboolean         database_get_is_editable      (Source              *list);
static const GdkPixbuf *database_get_pixbuf           (Source              *list);
static GList *          database_get_songs            (Source              *list);
static guchar *         database_pack_song            (Song                *song,
						       int                 *len);
static Song *           database_unpack_song          (guchar              *p,
						       int                  len);
static void             database_read_songs           (SourceDatabase      *database);
static gboolean         database_add_song             (SourceDatabase      *database,
						       Song                *song,
						       gboolean             force);
static gboolean         database_song_exists          (SourceDatabase      *db,
						       const char          *filename);
static gboolean         database_remove_song          (Source              *source,
						       Song                *song);
static int              database_get_version          (SourceDatabase      *database);
static void             database_set_version          (SourceDatabase      *database,
						       int                  version);
static void             database_rebuild              (SourceDatabase      *database,
						       int                  version_nr);
static gboolean         database_get_metadata         (SourceDatabase      *database,
						       const char          *path,
						       time_t              *time_modified,
						       time_t              *time_added);

#define SOURCE_DATABASE_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_SOURCE_DATABASE, SourceDatabasePriv))

G_DEFINE_TYPE (SourceDatabase, source_database, TYPE_SOURCE);

struct _SourceDatabasePriv {
	GDBM_FILE  handle;
	GList     *songs;
	GdkPixbuf *pixbuf;
};

static void
source_database_class_init (SourceDatabaseClass *klass)
{
	GObjectClass *object_class;
	SourceClass  *source_class;

	object_class = (GObjectClass*) klass;
	object_class->finalize = database_finalize;

	source_class = (SourceClass*) klass;

	source_class->update_song = database_update_song;
	source_class->get_songs = database_get_songs;
	source_class->get_pixbuf = database_get_pixbuf;
	source_class->get_name = database_get_name;
	source_class->get_is_editable = database_get_is_editable;
	source_class->remove_song = database_remove_song;

	g_type_class_add_private (klass, sizeof (SourceDatabasePriv));
}

static void
source_database_init (SourceDatabase *database)
{
	SourceDatabasePriv *priv;

	priv = SOURCE_DATABASE_GET_PRIVATE (database);
	database->priv = priv;
	
	priv->pixbuf = gdk_pixbuf_new_from_file (
		DATADIR "/jamboree/jamboree-playlist-library.png", NULL);

}

static void
database_finalize (GObject *object)
{
	SourceDatabase     *database;
	SourceDatabasePriv *priv;

	database = SOURCE_DATABASE (object);
	priv = database->priv;

	g_list_foreach (priv->songs, (GFunc) song_unref, NULL);
	g_list_free (priv->songs);

	g_object_unref (priv->pixbuf);
	
	if (priv->handle) {
		gdbm_close (priv->handle);
	}

	if (G_OBJECT_CLASS (source_database_parent_class)->finalize) {
		G_OBJECT_CLASS (source_database_parent_class)->finalize (object);
	}
}

static gboolean
database_update_song (Source *source,
		      Song   *song)
{
	SourceDatabase     *database;
	SourceDatabasePriv *priv;
	int                 ret;
	datum               key, data;
	guchar             *p;
	int                 len;

	database = SOURCE_DATABASE (source);
	priv = database->priv;
	
	/* Just ignore if the song isn't in the database. */
	if (!database_song_exists (database, song_get_path (song))) {
		return FALSE;
	}

	/* FIXME: Should we really change this or should it just be changed when
	 * we change tags?
	 */
	/*_song_set_time_modified (song, time (NULL));*/

	p = database_pack_song (song, &len);

	key.dptr = (char *) song_get_path (song);
	key.dsize = strlen (key.dptr);
  
	data.dptr = (char *) p;
	data.dsize = len;

	ret = gdbm_store (priv->handle, key, data, GDBM_REPLACE);

	g_free (p);
	
	return (ret == 0);
}

static GList *
database_get_songs (Source *list)
{
	SourceDatabase     *database;
	SourceDatabasePriv *priv;

	database = SOURCE_DATABASE (list);
	priv = database->priv;

	return priv->songs;
}

static const char *
database_get_name (Source *list)
{
	return _("All songs");
}

static gboolean
database_get_is_editable (Source *list)
{
	return FALSE;
}

static const GdkPixbuf *
database_get_pixbuf (Source *list)
{
	SourceDatabase     *database;
	SourceDatabasePriv *priv;

	database = SOURCE_DATABASE (list);
	priv = database->priv;

	return priv->pixbuf;
}

/* Note: str is set to NULL for empty strings. */
static guchar *
unpack_string (guchar *p, char **str, guchar *endp)
{
	int len;
  
	p = _ALIGN_ADDRESS (p, 4);
  
	len = *(int *) p;

	p += 4;

	/* Try to catch corrupt database :/ */
	if (p + len > endp) {
		g_warning ("String length seems to be corrupt.");
		if (*str) {
			*str = NULL;
		}
		
		return endp + 1;
	}
      
	if (len == 0) {
		if (str) {
			*str = NULL;
		}
		
		return p;      
	}
  
	if (str != NULL) {
		*str = g_malloc (len + 1);

		memcpy (*str, p, len);
		(*str)[len] = 0;
	}
  
	return p + len + 1;
}

static guchar *
unpack_int (guchar *p, int *val)
{
	p = _ALIGN_ADDRESS (p, 4);

	if (val) {
		*val = *(int *) p;
	}
	
	p += 4;
  
	return p;
}

static guchar *
unpack_uint64 (guchar *p, guint64 *val)
{
	p = _ALIGN_ADDRESS (p, 8);

	if (val) {
		*val = *(guint64 *) p;
	}
	
	p += 8;

	return p;
}

static guchar *
unpack_playlists (guchar *p, GList **playlists, guchar *endp)
{
	int len, i;
	int id;

	*playlists = NULL;
	
	p = unpack_int (p, &len);

	if (len == 0) {
		return p;
	}

	for (i = 0; i < len; i++) {
		p = unpack_int (p, &id);
		*playlists = g_list_append (*playlists, GINT_TO_POINTER (id));
	}

	return p;
}

static guchar *
unpack_string_with_fallback (guchar *p, guchar *end, const char *fallback, char **str)
{
	p = unpack_string (p, str, end);
	if (p > end) {
		g_free (*str);
		return p;
	}

	if (*str == NULL) {
		*str = g_strdup (fallback);
	}

	return p;
}

static Song *
database_unpack_song (guchar *p, int len)
{
	guchar  *end;
	Song    *song;
	char    *str;
	int      i;
	guint64  ui64;
	GList   *playlists;
  
	end = p + len;
  
	song = song_new (NULL);

	/* Data that is not available in the file. We store this first so that
	 * we can restore it when rebuilding the database even if we change the
	 * order/type/amount of cached data later.
	 */
	
	p = unpack_int (p, &i);
	_song_set_time_added (song, i);

	p = unpack_int (p, &i);
	_song_set_time_modified (song, i);

	p = unpack_int (p, &i);
	_song_set_rating (song, i);

	p = unpack_int (p, &i);
	_song_set_playcount (song, i);

	p = unpack_int (p, &i);
	_song_set_time_played (song, i);

	p = unpack_playlists (p, &playlists, end);
	if (p > end) {
		goto fail;
	}
	_song_set_playlists (song, playlists);

	/* Cached data from the file. */

	p = unpack_string_with_fallback (p, end, _("Invalid title"), &str);
	if (p > end) {
		goto fail;
	}
	_song_set_title (song, str);
  
	p = unpack_string_with_fallback (p, end, _("Invalid artist name"), &str);
	if (p > end) {
		goto fail;
	}
	_song_set_artist (song, str);
  
	p = unpack_string_with_fallback (p, end, _("Invalid album name"), &str);
	if (p > end) {
		goto fail;
	}
	_song_set_album (song, str);

	p = unpack_string_with_fallback (p, end, _("Invalid genre name"), &str);
	if (p > end) {
		goto fail;
	}
	_song_set_genre (song, str);

	p = unpack_int (p, &i);
	_song_set_year (song, i);

	p = unpack_int (p, &i);
	_song_set_duration (song, i);

	p = unpack_int (p, &i);
	_song_set_bitrate (song, i);

	p = unpack_int (p, &i);
	_song_set_samplerate (song, i);

	p = unpack_int (p, &i);
	_song_set_track (song, i);

	p = unpack_uint64 (p, &ui64);
	_song_set_size (song, ui64);

	return song;

 fail:
	song_unref (song);
	return NULL;
}

static void
string_align (GString *string, int boundary)
{
	guchar *p;
	int     padding;
	int     i;

	p = (guchar *) string->str + string->len;

	padding = _ALIGN_ADDRESS (p, boundary) - p;

	for (i = 0; i < padding; i++) {
		g_string_append_c (string, 0);
	}
}

static void
pack_int (GString *string, int val)
{
	string_align (string, 4);

	g_string_append_len (string, (char *) &val, 4);
}

static void
pack_uint64 (GString *string, guint64 val)
{
	string_align (string, 8);

	g_string_append_len (string, (char *) &val, 8);
}

static void
pack_string (GString *string, const char *str)
{
	int len;
  
	if (str) {
		len = strlen (str);
	} else {
		len = 0;
	}
	
	pack_int (string, len);

	if (len > 0) {
		g_string_append (string, str);
		g_string_append_c (string, 0);
	}
}

static void
pack_playlists (GString *string, GList *playlists)
{
	int    len;
	GList *l;

	len = g_list_length (playlists);

	pack_int (string, len);

	if (len == 0) {
		return;
	}

	for (l = playlists; l; l = l->next) {
		pack_int (string, GPOINTER_TO_INT (l->data));
	}
}

static guchar *
database_pack_song (Song *song, int *len)
{
	GString *string;

	string = g_string_new ("");

	/* Data that is not available in the file. */
	
	pack_int (string, song_get_time_added (song));
	pack_int (string, song_get_time_modified (song));
	pack_int (string, song_get_rating (song));
	pack_int (string, song_get_playcount(song));
	pack_int (string, song_get_time_played (song));
	pack_playlists (string, song_get_playlists (song));

	/* Cached data. */

	pack_string (string, song_get_title (song));
	pack_string (string, song_get_artist (song));
	pack_string (string, song_get_album (song));

	pack_string (string, song_get_genre (song));
  
	pack_int (string, song_get_year (song));
	pack_int (string, song_get_duration (song));
	pack_int (string, song_get_bitrate (song));
	pack_int (string, song_get_samplerate (song));
	pack_int (string, song_get_track (song));

	pack_uint64 (string, song_get_size (song));

	if (len) {
		*len = string->len;
	}
	
	return (guchar *) g_string_free (string, FALSE);
}

static void
read_songs (SourceDatabase *database)
{
	SourceDatabasePriv *priv;
	datum               key, next_key, data;
	Song               *song;
	char               *tmp;

	priv = database->priv;
	
	key = gdbm_firstkey (priv->handle);
	while (key.dptr) {
		next_key = gdbm_nextkey (priv->handle, key);
      
		if (((char *) key.dptr)[0] == VERSION_KEY[0] &&
		    strncmp (key.dptr, VERSION_KEY, VERSION_KEY_LENGTH) == 0) {
			free (key.dptr);
			key = next_key;
			continue;
		}
      
		data = gdbm_fetch (priv->handle, key);

		if (!data.dptr) {
			free (key.dptr);
			key = next_key;
			continue;
		}

		song = database_unpack_song ((guchar *) data.dptr, data.dsize);
		if (song != NULL) {
			tmp = g_strndup (key.dptr, key.dsize);
			_song_set_path (song, tmp);
			priv->songs = g_list_prepend (priv->songs, song);
			g_free (tmp);
		} else {
			tmp = g_strndup (key.dptr, key.dsize);
			g_warning ("Corrupt database, skipping song %s\n", tmp);
			g_free (tmp);
		}
      
		free (key.dptr);
		free (data.dptr);

		key = next_key;
	}
}

Source *
source_database_new (const char *filename)
{
	SourceDatabasePriv *priv;
	SourceDatabase     *database;
	int                 fd;

	database = g_object_new (TYPE_SOURCE_DATABASE, NULL);
	priv = database->priv;
	
	priv->handle = gdbm_open ((char*) filename, 4096, GDBM_WRCREAT | GDBM_SYNC, 04644, NULL);
	if (!priv->handle) {
		goto fail;
	}
	
	/* Prevent esd from keeping the fd open when it forks, therefore messing
	 * with the file locking.
	 */
	fd = gdbm_fdesc (priv->handle);
	fcntl (fd, F_SETFD, FD_CLOEXEC);

	/* FIXME: Don't do it like this... ask the user. */
	if (database_get_version (database) < VERSION_NR) {
		database_rebuild (database, VERSION_NR);
	} else {
		database_read_songs (database);
	}
	
	return SOURCE (database);
  
 fail:
  
	g_object_unref (database);
	return NULL;
}

static void
database_read_songs (SourceDatabase *database)
{
	SourceDatabasePriv *priv;

	priv = database->priv;
	
	g_list_foreach (priv->songs, (GFunc) song_unref, NULL);
	g_list_free (priv->songs);
	priv->songs = NULL;
  
	read_songs (database);
}

static gboolean
database_add_song (SourceDatabase* database, Song *song, gboolean force)
{
	SourceDatabasePriv *priv;
	datum               key, data;
	int                 ret;
	guchar             *p;
	int                 len;
	gboolean            exists;

	priv = database->priv;
	
	key.dptr = (char *) song_get_path (song);
	key.dsize = strlen (key.dptr);

	p = database_pack_song (song, &len);

	data.dptr = (char *) p;
	data.dsize = len;

	exists = database_song_exists (database, key.dptr);
	if (exists) {
		/* FIXME: remove the old song and add the new... */
	}
	
	ret = gdbm_store (priv->handle, key, data, force ? GDBM_REPLACE : GDBM_INSERT);
	if (ret == 0) {
		priv->songs = g_list_prepend (priv->songs, song);
	}
	
	g_free (p);

	return ret == 0;
}

typedef struct {
	SourceDatabase                *database;
	SourceDatabaseAddProgressFunc  progress_func;
	gpointer                       progress_data;
} AddDirData;

static void
reader_song_read_func (Song       *song,
		       gpointer    user_data)
{
	AddDirData *data;

	data = user_data;

	song_ref (song);
	database_add_song (data->database, song, TRUE);
}

static gboolean
reader_song_found_func (const char *path,
			gpointer    user_data)
{
	AddDirData  *data;
	time_t       time_modified;
	struct stat  buf;
	
	data = user_data;

	if (!database_get_metadata (data->database,
				    path,
				    &time_modified,
				    NULL)) {
		/* We don't have the song, add it. */
		return TRUE;
	}

	/* Don't update songs for now. */
	return FALSE;
	
	/* Update the song if it's changed. */
       	if (stat (path, &buf) != 0) {
		return TRUE;
	}
	
	if (time_modified >= buf.st_mtime) {
		return FALSE;
	}

	return TRUE;
}

static gboolean
reader_progress_func (const char *path,
		      gint        files,
		      gint        completed,
		      gpointer    user_data)
{
	AddDirData *data;

	data = user_data;

	if (!data->progress_func (data->database, path, files, completed, data->progress_data)) {
		return FALSE;
	}

	return TRUE;
}

gboolean
source_database_add_dir (SourceDatabase                *database,
			 const char                    *path,
			 SourceDatabaseAddProgressFunc  progress_func,
			 gpointer                       user_data)
{
	SourceDatabasePriv *priv;
	AddDirData          data;

	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (progress_func != NULL, FALSE);

	priv = database->priv;

	data.database = database;
	data.progress_func = progress_func;
	data.progress_data = user_data;
	
	return tag_reader_read_path (path,
				     reader_song_found_func,
				     reader_song_read_func,
				     reader_progress_func,
				     &data);
}

static gboolean
database_song_exists (SourceDatabase *database, const char *filename)
{
	SourceDatabasePriv *priv;
	datum               key;

	priv = database->priv;
	
	key.dptr = (char *) filename;
	key.dsize = strlen (key.dptr);
  
	return gdbm_exists (priv->handle, key);
}

static gboolean
database_remove_song (Source *source,
		      Song   *song)
{
	SourceDatabase     *database;
	SourceDatabasePriv *priv;
	datum               key;
	int                 ret;

	database = SOURCE_DATABASE (source);
	
	priv = database->priv;
	
	key.dptr = (char *) song_get_path (song);
	key.dsize = strlen (key.dptr);
	
	ret = gdbm_delete (priv->handle, key);

	priv->songs = g_list_remove (priv->songs, song);

	song_unref (song);
  
	return TRUE;
}

static int
database_get_version (SourceDatabase *database)
{
	SourceDatabasePriv *priv;
	datum               key, data;
	int                 ret;

	priv = database->priv;
	
	key.dptr = VERSION_KEY;
	key.dsize = strlen (key.dptr);
  
	data = gdbm_fetch (priv->handle, key);
	if (data.dptr == NULL) {
		return -1;
	}
  
	unpack_int ((guchar *) data.dptr, &ret);

	free (data.dptr);
  
	return ret;
}

static void
database_set_version (SourceDatabase *database, int version)
{
	SourceDatabasePriv *priv;
	GString            *string;
	datum               key, data;
	int                 ret;
  
	priv = database->priv;
	
	string = g_string_new (NULL);

	pack_int (string, version);
  
	key.dptr = VERSION_KEY;
	key.dsize = strlen (key.dptr);
    
	data.dptr = string->str;
	data.dsize = string->len;

	ret = gdbm_store (priv->handle, key, data, GDBM_REPLACE);
	if (ret) {
		g_warning ("Could not update database version");
	}
			 
	g_string_free (string, TRUE);
}

static void
rebuild_song_read_func (Song       *song,
			gpointer    user_data)
{
	SourceDatabase *database;

	database = user_data;

	song_ref (song);
	database_add_song (database, song, TRUE);
}

static void
database_rebuild (SourceDatabase *database, int version_nr)
{
	SourceDatabasePriv *priv;
	datum               key, next_key;
	GList              *songs = NULL;
	GList              *l;

	priv = database->priv;
	
	key = gdbm_firstkey (priv->handle);
	while (key.dptr) {
		next_key = gdbm_nextkey (priv->handle, key);

		if (((char *) key.dptr)[0] == VERSION_KEY[0] &&
		    strncmp (key.dptr, VERSION_KEY, VERSION_KEY_LENGTH) == 0) {
			free (key.dptr);
			key = next_key;
			continue;
		}

		songs = g_list_prepend (songs, g_strndup (key.dptr, key.dsize));

		free (key.dptr);
		key = next_key;
	}

	for (l = songs; l; l = l->next) {
		tag_reader_read_path (l->data,
				      NULL, /* found_song */
				      rebuild_song_read_func,
				      NULL,
				      database);
	}
	
	g_list_foreach (songs, (GFunc) g_free, NULL);
	g_list_free (songs);

	database_set_version (database, VERSION_NR);
}

static gboolean
database_get_metadata (SourceDatabase *database,
		       const char     *path,
		       time_t         *time_modified,
		       time_t         *time_added)
{
	SourceDatabasePriv *priv;
	datum               key, data;
	guchar             *p;
	int                 i;

	priv = database->priv;
	
	key.dptr = (char *) path;
	key.dsize = strlen (key.dptr);
  
	data = gdbm_fetch (priv->handle, key);
	if (!data.dptr) {
		return FALSE;
	}

	p = (guchar *) data.dptr;

	p = unpack_int (p, &i);
	if (time_added) {
		*time_added = i;
	}

	p = unpack_int (p, &i);
	if (time_modified) {
		*time_modified = i;
	}

	free (data.dptr);

	return TRUE;
}

void
source_database_reset_playcount (SourceDatabase *database)
{
	SourceDatabasePriv *priv;
	GList              *l;
	Song               *song;
	int                 playcount;
	time_t              time_played;
	
	priv = database->priv;

	for (l = priv->songs; l; l = l->next) {
		song = l->data;

		playcount = song_get_playcount (song);
		time_played = song_get_time_played (song);

		if (playcount != 0 || time_played != 0) {
			_song_set_playcount (song, 0);
			_song_set_time_played (song, 0);

			source_song_changed (SOURCE (database), song, SONG_TAG_ALL);
			source_update_song (SOURCE (database), song);
		}
	}
}


/*

add ()
	path = song_get_path (song);
	
	if (g_hash_table_lookup (list->hash, path)) {
		return FALSE;
	}

	song_ref (song);
	
	g_hash_table_insert (list->hash, (char *) path, song);

	source_added (list, song);

	return TRUE;
*/


/*

remove ()
	const char *path;

	path = song_get_path (song);
	
	if (g_hash_table_remove (list->hash, path)) {
		return FALSE;
	}

	source_removed (list, song);
	song_unref (song);
	
	return TRUE;
*/

