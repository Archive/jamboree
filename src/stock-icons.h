/*
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __STOCK_ICONS_H__
#define __STOCK_ICONS_H__

#define JAMBOREE_STOCK_PLAY          "jamboree-play"
#define JAMBOREE_STOCK_STOP          "jamboree-stop"
#define JAMBOREE_STOCK_NEXT          "jamboree-next"
#define JAMBOREE_STOCK_PREV          "jamboree-prev"
#define JAMBOREE_STOCK_PAUSE         "jamboree-pause"
#define JAMBOREE_STOCK_RANDOM        "jamboree-random"

#define JAMBOREE_STOCK_VOLUME_ZERO   "jamboree-volume-zero"
#define JAMBOREE_STOCK_VOLUME_MIN    "jamboree-volume-min"
#define JAMBOREE_STOCK_VOLUME_MEDIUM "jamboree-volume-medium"
#define JAMBOREE_STOCK_VOLUME_MAX    "jamboree-volume-max"

void stock_icons_register (void);

#endif /* __STOCK_ICONS_H__ */
