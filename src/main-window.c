/*
 * Copyright (C) 2003-2005 Imendio AB
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 * Copyright (C) 2003 Anders Carlsson <andersca@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <math.h>
#include <time.h>
#include <glib/gi18n.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <gdk/gdkkeysyms.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <gconf/gconf-client.h>

#include "main-window.h"
#include "source-view.h"
#include "smart-playlist-dialog.h"
#include "player.h"
#include "cursors.h"
#include "stock-icons.h"
#include "utils.h"
#include "string-utils.h"
#include "sources-view.h"
#include "song-properties.h"
#include "song-private.h"
#include "source-database.h"
#include "source-proxy.h"
#include "source-playlist.h"
#include "source-smart-playlist.h"
#include "sources-xml.h"
#include "volume-button.h"


#ifdef HAVE_DBUS
#include "dbus.h"
#endif


static void         main_window_finalize                   (GObject           *object);
static void         setup_gui                              (MainWindow        *window);
static gboolean     delete_event_cb                        (GtkWidget         *window,
							    gpointer           data);
static void 	    volume_changed_cb                      (GtkWidget         *button, 	 
							    int                volume, 	 
							    MainWindow        *window);
static void         genre_selection_changed_cb             (GtkTreeSelection  *selection,
							    MainWindow        *window);

static void         artist_selection_changed_cb            (GtkTreeSelection  *selection,
							    MainWindow        *window);
static void         artist_row_activated_cb                (GtkTreeView       *treeview,
							    GtkTreePath       *path,
							    GtkTreeViewColumn *col,
							    MainWindow        *window);
static void         album_selection_changed_cb             (GtkTreeSelection  *selection,
							    MainWindow        *window);
static void         album_row_activated_cb                 (GtkTreeView       *treeview,
							    GtkTreePath       *path,
							    GtkTreeViewColumn *col,
							    MainWindow        *window);
static void         seek_scale_value_changed_cb            (GtkWidget         *widget,
							    MainWindow        *window);
static gboolean     seek_scale_button_press_cb             (GtkWidget         *widget,
							    GdkEventButton    *event,
							    MainWindow        *window);
static gboolean     seek_scale_button_release_cb           (GtkWidget         *widget,
							    GdkEventButton    *event,
							    MainWindow        *window);
static gboolean     seek_scale_motion_notify_cb            (GtkWidget         *widget,
							    GdkEventMotion    *event,
							    MainWindow        *window);
static void         search_entry_activate_cb               (GtkEntry          *entry,
							    MainWindow        *window);
static void         update_playing_info                    (MainWindow        *window);
static void         player_eos_cb                          (Player            *player,
							    Song              *song,
							    MainWindow        *window);
static void         player_error_cb                        (Player            *player,
							    GError            *error,
							    MainWindow        *window);
static void         player_tick_cb                         (Player            *player,
							    long               secs,
							    MainWindow        *window);
static void         player_state_changed_cb                (Player            *player,
							    PlayingState       state,
							    MainWindow        *window);
static void         random_toggled_cb                      (GtkToggleAction   *action,
							    MainWindow        *window);
static void         repeat_toggled_cb                      (GtkToggleAction   *action,
							    MainWindow        *window);
static void         update_edit_smart_playlist_sensitivity (MainWindow        *window);
static void         sources_view_selection_changed_cb      (GtkTreeSelection  *selection,
							    MainWindow        *window);
static void         new_playlist_cb                        (GtkAction         *action,
							    MainWindow        *window);
static void         new_smart_playlist_cb                  (GtkAction         *action,
							    MainWindow        *window);
static void         edit_smart_playlist_cb                 (GtkAction         *action,
							    MainWindow        *window);
static void         add_folder_cb                          (GtkAction         *action,
							    MainWindow        *window);
static void         quit_cb                                (GtkAction         *action,
							    MainWindow        *window);
static void         play_cb                                (GtkAction         *action,
							    MainWindow        *window);
static void         stop_cb                                (GtkAction         *action,
							    MainWindow        *window);
static void         prev_cb                                (GtkAction         *action,
							    MainWindow        *window);
static void         next_cb                                (GtkAction         *action,
							    MainWindow        *window);
static void         reset_playcount_cb                     (GtkAction         *action,
							    MainWindow        *window);
static void         song_columns_cb                        (GtkAction         *action,
							    MainWindow        *window);
static void         same_artist_cb                         (GtkAction         *action,
							    MainWindow        *window);
static void         about_cb                               (GtkAction         *action,
							    MainWindow        *window);
static void         add_paths                              (MainWindow        *window,
							    GList             *paths);
static void         handle_play_error                      (MainWindow        *window,
							    Song              *song,
							    GError            *error);
static void         handle_generic_error                   (MainWindow        *window,
							    Song              *song,
							    GError            *error);
static void         handle_resource_busy_error             (MainWindow        *window,
							    Song              *song,
							    GError            *error);
static void         save_ui_state                          (MainWindow        *window);
static void         update_artist_album_count              (MainWindow        *window);
static void         select_artist                          (MainWindow        *window,
							    const char        *folded_artist);
static void         select_album                           (MainWindow        *window,
							    const char        *folded_album);
static void         set_browse_mode                        (MainWindow        *window,
							    gboolean           browse);
static void         perform_search                         (MainWindow        *window,
							    gboolean           play_first);
static void         update_songs                           (MainWindow        *window,
							    gboolean           update_genres,
							    gboolean           update_artists,
							    gboolean           update_albums);
static void         set_cursor_watch                       (MainWindow        *window);
static void         set_cursor_default                     (MainWindow        *window);
static void         song_changed                           (MainWindow        *window,
							    Song              *song);
static void         rescale_cover_image                    (MainWindow        *window);
static void         setup_root_window                      (MainWindow        *main_window);



struct _MainWindowPriv {
	Source         *database;
	GList          *playlists;

	GtkTooltips    *tooltips;
	
	GtkUIManager   *manager;
	GtkActionGroup *action_group;
  
	gboolean        seeking;
	guint           seeking_idle_id;
  
	gboolean        browse_mode;

	GHashTable     *genre_hash;
	GHashTable     *artist_hash;
	GHashTable     *album_hash;

	guint           search_timeout_id;

	GtkListStore   *genre_store;
	GtkListStore   *artist_store;
	GtkListStore   *album_store;
	SourceModel    *song_model; 

	GtkWidget      *search_entry;
	GtkWidget      *search_box;

	GtkTreeView    *sources_view;
	GtkTreeView    *genre_tree;
	GtkTreeView    *artist_tree;
	GtkTreeView    *album_tree;
	SourceView     *song_tree;

	GtkWidget      *browse_paned;
	GtkWidget      *browse_hbox;
	GtkWidget      *playlist_paned;
  
	GtkWidget      *info_label;
	GtkWidget      *playing_label;
	GtkWidget      *playing_song_label;
	GtkWidget      *playing_info_box;

	GdkPixbuf      *cover_pixbuf;
	GtkWidget      *cover_eventbox;
	GtkWidget      *cover_frame;
	GtkWidget      *cover_image;
	GtkWidget      *cover_popup;

	guint           rescale_cover_timeout_id;
	
	GtkWidget      *tick_label;
	GtkWidget      *length_label;

	GtkWidget      *seek_scale;
	GtkWidget      *volume_button;
  
	GtkWidget      *top_vbox;
	GtkWidget      *ui_vbox;

	GdkCursor      *watch_arrow_cursor;
  
	/* FIXME: remove! move to model */
	int             total_num;
	guint           total_length;
	guint64         total_size;

	guint           fill_songs_idle_id;
	GList          *fill_songs_list;

        gboolean        initial_browse_done;
};


#define FIRST_BURST       1500
#define BURST             1500


static MainWindow *glob_window;


enum {
	GENRE_COL_NAME,
	GENRE_COL_KEY,
	GENRE_COL_ALL,
	GENRE_NUM_COLS
};
enum {
	ARTIST_COL_NAME,
	ARTIST_COL_KEY,
	ARTIST_COL_ALL,
	ARTIST_NUM_COLS
};

enum {
	ALBUM_COL_NAME,
	ALBUM_COL_KEY,
	ALBUM_COL_ALL,
	ALBUM_NUM_COLS
};

enum {
	TARGET_STRING,
	TARGET_URI_LIST,
	TARGET_ICON_LIST,
};

enum {
	ACTION_PREV = 1,
	ACTION_NEXT,
	ACTION_EDIT
};

static const GtkTargetEntry songs_target_types[] = {
	{ "STRING",     0, TARGET_STRING },
	{ "text/plain", 0, TARGET_STRING },
	{ "text/uri-list", 0, TARGET_URI_LIST },
	{ "x-special/gnome-icon-list", 0, TARGET_ICON_LIST }
};

static GtkActionEntry actions[] = {
	{ "FileMenu", NULL, N_("_File") },
	{ "ViewMenu", NULL, N_("_View") },
	{ "PlayMenu", NULL, N_("_Play") },
	{ "HelpMenu", NULL, N_("_Help") },
  
	/* File menu */
	{ "AddFolder", GTK_STOCK_ADD, N_("_Add Folder"), NULL, NULL,
	  G_CALLBACK (add_folder_cb) },
  
	{ "NewPlaylist", GTK_STOCK_NEW, N_("_New Playlist"), "<control>N", NULL,
	  G_CALLBACK (new_playlist_cb) },

	{ "NewSmartPlaylist", GTK_STOCK_NEW, N_("New _Smart Playlist"), "<control>M", NULL,
	  G_CALLBACK (new_smart_playlist_cb) },

	{ "EditSmartPlaylist", NULL, N_("_Edit Smart Playlist"), NULL, NULL,
	  G_CALLBACK (edit_smart_playlist_cb) },

	{ "Quit", GTK_STOCK_QUIT, NULL, "<control>Q", NULL,
	  G_CALLBACK (quit_cb) },

	/* View menu */
	{ "VisibleColumns", NULL, N_("_Visible Columns"), NULL, NULL,
	  G_CALLBACK (song_columns_cb) },

	{ "ShowSameArtist", NULL, N_("_Show other songs by this artist"), NULL, NULL,
	  G_CALLBACK (same_artist_cb) },

	/* Actions menu */
	{ "Play", JAMBOREE_STOCK_PLAY, N_("_Play"), "<control>space", N_("Start"),
	  G_CALLBACK (play_cb) },

	{ "Stop", JAMBOREE_STOCK_STOP, N_("_Stop"), NULL, N_("Stop"),
	  G_CALLBACK (stop_cb) },

	{ "Prev", JAMBOREE_STOCK_PREV, N_("_Previous"), NULL, N_("Previous song"),
	  G_CALLBACK (prev_cb) },

	{ "Next", JAMBOREE_STOCK_NEXT, N_("_Next"), NULL, N_("Next song"),
	  G_CALLBACK (next_cb) },

	{ "ResetPlaycount", NULL, N_("_Reset Playcount"), NULL, NULL,
	  G_CALLBACK (reset_playcount_cb) },

	/* Help menu */
	{ "About", GTK_STOCK_ABOUT, NULL, NULL, NULL,
	  G_CALLBACK (about_cb) }
};

static GtkToggleActionEntry toggle_actions[] = {
	{ "Random", JAMBOREE_STOCK_RANDOM, N_("_Random"), NULL, N_("Random"),
	  G_CALLBACK (random_toggled_cb) },
	{ "Repeat", JAMBOREE_STOCK_RANDOM, N_("_Repeat"), NULL, N_("Repeat"),
	  G_CALLBACK (repeat_toggled_cb) }
};

static const char *ui = ""
"<ui>"
"  <menubar>"
"    <menu action='FileMenu'>"
"      <menuitem action='AddFolder'/>"
"      <separator name='sep1'/>"
"      <menuitem action='NewPlaylist'/>"
"      <menuitem action='NewSmartPlaylist'/>"
"      <menuitem action='EditSmartPlaylist'/>"
"      <separator name='sep2'/>"
"      <menuitem action='Quit'/>"
"    </menu>"
"    <menu action='PlayMenu'>"
"      <menuitem action='Play'/>"
"      <menuitem action='Stop'/>"
"      <menuitem action='Prev'/>"
"      <menuitem action='Next'/>"
"      <separator name='sep3'/>"
"      <menuitem action='Random'/>"
"      <menuitem action='Repeat'/>"
"      <separator name='sep4'/>"
"      <menuitem action='ResetPlaycount'/>"
"    </menu>"
"    <menu action='ViewMenu'>"
"      <menuitem action='VisibleColumns'/>"
"      <separator name='sep5'/>"
"      <menuitem action='ShowSameArtist'/>"
"    </menu>"
"    <menu action='HelpMenu'>"
"      <menuitem action='About'/>"
"    </menu>"
"  </menubar>"
""
"  <toolbar name='Toolbar'>"
"    <toolitem  action='Prev'/>"
"    <toolitem  action='Stop'/>"
"    <toolitem  action='Play'/>"
"    <toolitem  action='Next'/>"
"  </toolbar>"
"</ui>";


#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_MAIN_WINDOW, MainWindowPriv))

G_DEFINE_TYPE (MainWindow, main_window, GTK_TYPE_WINDOW);


static void
main_window_class_init (MainWindowClass *klass)
{
	GObjectClass *object_class;

	object_class = (GObjectClass*) klass;
	object_class->finalize = main_window_finalize;

	g_type_class_add_private (klass, sizeof (MainWindowPriv));
}

static void
grab_key_cb (XF86Key key, gpointer user_data)
{
	MainWindow *window;

	window = user_data;
  
	switch (key) {
	case XF86AUDIO_PLAY:
		main_window_handle_play (window, TRUE);
		break;
	case XF86AUDIO_PAUSE:
		main_window_handle_pause (window);
		break;
	case XF86AUDIO_STOP:
		main_window_handle_stop (window);
		break;
	case XF86AUDIO_PREV:
		main_window_handle_prev (window);
		break;
	case XF86AUDIO_NEXT:
		main_window_handle_next (window);
		break;
      
	default:
		break;
	}
}

static void
manager_add_widget_cb (GtkUIManager *manager,
		       GtkWidget    *widget,
		       MainWindow   *window)
{
	MainWindowPriv *priv;
	
	priv = window->priv;

	gtk_box_pack_start (GTK_BOX (priv->ui_vbox), widget, FALSE, FALSE, 0);
	gtk_widget_show (widget);
}

static void
main_window_init (MainWindow *window)
{
	window->priv = GET_PRIV (window);

	setup_root_window (window);

	/* Init the player (a bit ugly). */
	player_get ();
}

static void
main_window_finalize (GObject *object)
{
	MainWindow     *window;
	MainWindowPriv *priv;

	window = MAIN_WINDOW (object);
	priv = window->priv;

	player_shutdown ();
	g_object_unref (priv->database);

	if (priv->watch_arrow_cursor) {
		gdk_cursor_unref (priv->watch_arrow_cursor);
	}
	
	if (priv->rescale_cover_timeout_id) {
		g_source_remove (priv->rescale_cover_timeout_id);
	}
	
	g_hash_table_destroy (priv->artist_hash);
	g_hash_table_destroy (priv->album_hash);

	G_OBJECT_CLASS (main_window_parent_class)->finalize (object);
}

static gboolean
delete_event_cb (GtkWidget *window,
		 gpointer   data)
{
	save_ui_state (MAIN_WINDOW (window));
  
	return FALSE;
}

/* Used to keep track of how many songs correspond to a certain artist or
   album.
*/
typedef struct {
	int        count;
	GtkTreeIter iter;
} SongCount;

static void
add_song (MainWindow *window, Song *song)
{
	MainWindowPriv *priv;
	GtkTreeIter     iter;
	const char     *str;
	SongCount      *count;

	priv = window->priv;
 
	priv->total_num++;
	priv->total_size += song_get_size (song);
	priv->total_length += song_get_duration (song);
  
	source_model_add (priv->song_model, song);

	if (!priv->browse_mode) {
		return;
	}

	str = song_get_genre (song);

	count = g_hash_table_lookup (priv->genre_hash, str); 
	if (!count) {
		count = g_new0 (SongCount, 1);
		count->count = 1;
      
		g_hash_table_insert (priv->genre_hash, g_strdup (str), count);
      
		gtk_list_store_append (priv->genre_store, &iter);
		gtk_list_store_set (priv->genre_store, &iter,
				    GENRE_COL_NAME, song_get_genre (song),
				    GENRE_COL_KEY, song_get_genre_collated (song),
				    -1);

		count->iter = iter;
	} else {
		count->count++;
	}

	str = song_get_artist_folded (song);

	count = g_hash_table_lookup (priv->artist_hash, str); 
	if (!count) {
		count = g_new0 (SongCount, 1);
		count->count = 1;
      
		g_hash_table_insert (priv->artist_hash, g_strdup (str), count);
      
		gtk_list_store_append (priv->artist_store, &iter);
		gtk_list_store_set (priv->artist_store, &iter,
				    ARTIST_COL_NAME, song_get_artist (song),
				    ARTIST_COL_KEY, song_get_artist_collated (song),
				    -1);

		count->iter = iter;
	} else {
		count->count++;
	}
	
	str = song_get_album_folded (song);

	count = g_hash_table_lookup (priv->album_hash, str);
	if (!count) {
		count = g_new0 (SongCount, 1);
		count->count = 1;

		g_hash_table_insert (priv->album_hash, g_strdup (str), count);
      
		gtk_list_store_append (priv->album_store, &iter);
		gtk_list_store_set (priv->album_store, &iter,
				    ALBUM_COL_NAME, song_get_album (song),
				    ALBUM_COL_KEY, song_get_album_collated (song),
				    -1);

		count->iter = iter;
	} else {
		count->count++;
	}
}

static void
foreach_reset_count_cb (gpointer key, gpointer data, gpointer user_data)
{
	SongCount *count = data;

	count->count = 0;
}

static void
update_songs_info (MainWindow *window)
{
	MainWindowPriv *priv;
	GString        *str;
	char           *tmp;

	priv = window->priv;
  
	str = g_string_new (NULL);
	g_string_append_printf (str, ngettext ("%d song", "%d songs", priv->total_num), priv->total_num);
	g_string_append (str, ", ");

	tmp = string_utils_format_duration_long (priv->total_length);
	g_string_append (str, tmp);
	g_free (tmp);
  
	gtk_label_set_text (GTK_LABEL (priv->info_label), str->str);
	g_string_free (str, TRUE);

	gtk_widget_queue_draw (priv->info_label);
}

static gboolean
foreach_remove_cb (gpointer key, gpointer data, gpointer user_data)
{
	return TRUE;
}

static void
reset_genre_view (MainWindow *window)
{
	MainWindowPriv   *priv;
	GtkTreeView      *view;
	GtkListStore     *model;
	GtkTreeSelection *selection;
	GtkTreeIter       iter;

	priv = window->priv;

	g_hash_table_foreach_remove (priv->genre_hash, foreach_remove_cb, NULL);

	view = GTK_TREE_VIEW (priv->genre_tree);
	model = GTK_LIST_STORE (gtk_tree_view_get_model (view));
  
	selection = gtk_tree_view_get_selection (view);
	g_signal_handlers_block_by_func (selection, genre_selection_changed_cb, window);
  
	gtk_list_store_clear (model);

	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
			    GENRE_COL_NAME, _("All (... genres)"),
			    GENRE_COL_ALL, TRUE,
			    -1);

	gtk_tree_selection_select_iter (selection, &iter);
  
	g_signal_handlers_unblock_by_func (selection,
					   genre_selection_changed_cb,
					   window);
}

static void
reset_album_view (MainWindow *window)
{
	MainWindowPriv   *priv;
	GtkTreeView      *view;
	GtkListStore     *model;
	GtkTreeSelection *selection;
	GtkTreeIter       iter;

	priv = window->priv;

	g_hash_table_foreach_remove (priv->album_hash, foreach_remove_cb, NULL);

	view = GTK_TREE_VIEW (priv->album_tree);
	model = GTK_LIST_STORE (gtk_tree_view_get_model (view));
  
	selection = gtk_tree_view_get_selection (view);

	g_signal_handlers_block_by_func (selection, album_selection_changed_cb, window);
  
	gtk_list_store_clear (model);

	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
			    ALBUM_COL_NAME, _("All (... albums)"),
			    ALBUM_COL_ALL, TRUE,
			    -1);

	gtk_tree_selection_select_iter  (selection, &iter);
		
	g_signal_handlers_unblock_by_func (selection,
					   album_selection_changed_cb,
					   window);
}

static void
reset_artist_view (MainWindow *window)
{
	MainWindowPriv   *priv;
	GtkTreeView      *view;
	GtkListStore     *model;
	GtkTreeSelection *selection;
	GtkTreeIter       iter;

	priv = window->priv;

	g_hash_table_foreach_remove (priv->artist_hash, foreach_remove_cb, NULL);

	view = GTK_TREE_VIEW (priv->artist_tree);
	model = GTK_LIST_STORE (gtk_tree_view_get_model (view));
  
	selection = gtk_tree_view_get_selection (view);
	g_signal_handlers_block_by_func (selection, artist_selection_changed_cb, window);
  
	gtk_list_store_clear (model);

	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
			    ARTIST_COL_NAME, _("All (... artists)"),
			    ARTIST_COL_ALL, TRUE,
			    -1);

	gtk_tree_selection_select_iter  (selection, &iter);
  
	g_signal_handlers_unblock_by_func (selection,
					   artist_selection_changed_cb,
					   window);
}

static void
select_all (MainWindow *window)
{
	MainWindowPriv   *priv;
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkTreePath      *path;

	priv = window->priv;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->album_tree));
	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->album_store), &iter);

	if (!(gtk_tree_selection_count_selected_rows (selection) == 1 &&
	      gtk_tree_selection_iter_is_selected (selection, &iter))) {
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &iter);
	}
  
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->artist_tree));
	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->artist_store), &iter);
  
	if (!(gtk_tree_selection_count_selected_rows (selection) == 1 &&
	      gtk_tree_selection_iter_is_selected (selection, &iter))) {
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &iter);
	}

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->artist_store), &iter);
	gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (priv->artist_tree),
				      path, NULL,
				      TRUE, 0, 0);

	gtk_tree_path_free (path);
}

static void
select_artist (MainWindow *window, const char *folded_artist)
{
	MainWindowPriv   *priv;
	SongCount        *count;
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GtkTreePath      *path;

	priv = window->priv;

	count = g_hash_table_lookup (priv->artist_hash, folded_artist);
  
	if (!count) {
		return;
	}
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->genre_tree));
	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->genre_store), &iter);

	if (!(gtk_tree_selection_count_selected_rows (selection) == 1 &&
	      gtk_tree_selection_iter_is_selected (selection, &iter))) {
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &iter);
	}

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->album_tree));
	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->album_store), &iter);

	if (!(gtk_tree_selection_count_selected_rows (selection) == 1 &&
	      gtk_tree_selection_iter_is_selected (selection, &iter))) {
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &iter);
	}
  
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->artist_tree));
  
	if (!(gtk_tree_selection_count_selected_rows (selection) == 1 &&
	      gtk_tree_selection_iter_is_selected (selection, &count->iter))) {
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &count->iter);
	}

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->artist_store), &count->iter);
	gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (priv->artist_tree),
				      path, NULL,
				      TRUE, 0, 0);

	gtk_tree_path_free (path);
}

static void
select_album (MainWindow *window, const char *folded_album)
{
	MainWindowPriv   *priv;
	SongCount        *count;
	GtkTreeSelection *selection;
	GtkTreePath      *path;

	priv = window->priv;

	count = g_hash_table_lookup (priv->album_hash, folded_album);
  
	if (!count) {
		return;
	}
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->album_tree));
  
	if (!(gtk_tree_selection_count_selected_rows (selection) == 1 &&
	      gtk_tree_selection_iter_is_selected (selection, &count->iter))) {
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &count->iter);
	}

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->album_store), &count->iter);
	gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (priv->album_tree),
				      path, NULL,
				      TRUE, 0, 0);
	gtk_tree_path_free (path);
}

static void
genre_row_activated_cb (GtkTreeView       *treeview,
			GtkTreePath       *path,
			GtkTreeViewColumn *col,
			MainWindow        *window)
{
	player_stop ();
	main_window_handle_play (window, FALSE);
}

static void
genre_selection_changed_cb (GtkTreeSelection *selection,
			    MainWindow       *window)
{
	if (gtk_tree_selection_count_selected_rows (selection) == 0) {
		return;
	}
	
	gtk_entry_set_text (GTK_ENTRY (window->priv->search_entry), "");
	update_songs (window, TRUE, TRUE, FALSE);
}


static void
artist_selection_changed_cb (GtkTreeSelection *selection,
			     MainWindow       *window)
{
	if (gtk_tree_selection_count_selected_rows (selection) == 0) {
		return;
	}
	
	gtk_entry_set_text (GTK_ENTRY (window->priv->search_entry), "");
	update_songs (window, FALSE, TRUE, FALSE);
}

static void
artist_row_activated_cb (GtkTreeView       *treeview,
			 GtkTreePath       *path,
			 GtkTreeViewColumn *col,
			 MainWindow        *window)
{
	player_stop ();
	main_window_handle_play (window, FALSE);
}

static void
album_selection_changed_cb (GtkTreeSelection *selection,
			    MainWindow       *window)
{
	if (gtk_tree_selection_count_selected_rows (selection) == 0) {
		return;
	}

	gtk_entry_set_text (GTK_ENTRY (window->priv->search_entry), "");
	update_songs (window, FALSE, FALSE, FALSE);
}

static void
album_row_activated_cb (GtkTreeView       *treeview,
			GtkTreePath       *path,
			GtkTreeViewColumn *col,
			MainWindow        *window)
{
	player_stop ();
	main_window_handle_play (window, FALSE);
}

static void 	 
update_play_icon (MainWindow   *window,
		  PlayingState  state) 	 
{ 	 
	MainWindowPriv *priv; 	 
	const char     *icon_name; 	 
	const char     *tooltip;
	GtkAction      *action;
	
	priv = window->priv; 	 
	
	switch (state) { 	 
	case PLAYING_STATE_PLAYING: 	 
		icon_name = JAMBOREE_STOCK_PAUSE;
		tooltip = _("Pause"); 	 
		break; 	 
		
	case PLAYING_STATE_PAUSED: 	 
	case PLAYING_STATE_STOPPED: 	 
	default: 	 
		icon_name = JAMBOREE_STOCK_PLAY;
		tooltip = _("Play"); 	 
		break; 	 
	} 	 

	action = gtk_action_group_get_action (priv->action_group, "Play");
	
	g_object_set (action,
		      "stock-id", icon_name,
		      "tooltip", tooltip,
		      NULL);
}

static gboolean
rescale_cover_timeout_cb (MainWindow *window)
{
	MainWindowPriv *priv;

	priv = window->priv;

	rescale_cover_image (window);
	
	priv->rescale_cover_timeout_id = 0;
	
	return FALSE;
}

static void
playlist_paned_notify_position_cb (GtkWidget  *widget,
				   GParamSpec *pspec,
				   MainWindow *window)
{
	MainWindowPriv *priv;

	priv = window->priv;
	
	if (priv->rescale_cover_timeout_id) {
		return;
	}
	
	priv->rescale_cover_timeout_id = g_timeout_add (25,
							(GSourceFunc) rescale_cover_timeout_cb,
							window);
}

static void
rescale_cover_image (MainWindow *window)
{
 	MainWindowPriv *priv;
	GdkPixbuf      *pixbuf;
	gint            width;

	priv = window->priv;

	if (!priv->cover_pixbuf) {
		gtk_widget_hide (priv->cover_frame);
		return;
	}

	width = gtk_paned_get_position (GTK_PANED (priv->playlist_paned));

	/* FIXME: Width * width is not right here... */
	
	pixbuf = gdk_pixbuf_scale_simple (priv->cover_pixbuf,
					  width, width,
					  GDK_INTERP_HYPER);

	g_signal_handlers_block_by_func (priv->playlist_paned,
					 playlist_paned_notify_position_cb,
					 window);
	
	gtk_image_set_from_pixbuf (GTK_IMAGE (priv->cover_image), pixbuf);
	g_object_unref (pixbuf);

	gtk_widget_show (priv->cover_frame);
	
	g_signal_handlers_unblock_by_func (priv->playlist_paned,
					   playlist_paned_notify_position_cb,
					   window);
}

static void
update_cover_image (MainWindow *window,
		    Song       *song)
{
 	MainWindowPriv *priv;
	gchar          *path;
	GDir           *dir;
	const gchar    *name;

	priv = window->priv;

	if (!song) {
		gtk_widget_hide (priv->cover_frame);
		return;
	}
	
	path = g_path_get_dirname (song_get_path (song));

	dir = g_dir_open (path, 0, NULL);
	if (!dir) {
		g_free (path);
		gtk_widget_hide (priv->cover_frame);
		return;
	}

	if (priv->cover_pixbuf) {
		g_object_unref (priv->cover_pixbuf);
		priv->cover_pixbuf = NULL;
	}
	
	while ((name = g_dir_read_name (dir))) {
		if (g_str_has_suffix (name, ".jpg") ||
		    g_str_has_suffix (name, ".gif") ||
		    g_str_has_suffix (name, ".png") ||
		    g_str_has_suffix (name, ".jpeg")) {
			gchar *filename;

			filename = g_build_filename (path, name, NULL);

			priv->cover_pixbuf = gdk_pixbuf_new_from_file (filename,
								       NULL);
			g_free (filename);

			if (priv->cover_pixbuf) {
				break;
			}
		}
	}

	g_dir_close (dir);
	g_free (path);

	rescale_cover_image (window);
}

static GdkFilterReturn
root_window_filter_func (GdkXEvent  *gdkxevent,
			 GdkEvent   *event,
			 gpointer    data)
{
	XEvent         *xevent = gdkxevent;
	Atom            atom;
	MainWindow     *window;
	MainWindowPriv *priv;

	window = data;
	priv = window->priv;
	
	switch (xevent->type) {
	case PropertyNotify:
		atom = gdk_x11_get_xatom_by_name ("_NET_CURRENT_DESKTOP");
		if (xevent->xproperty.atom == atom) {
			if (priv->cover_popup) {
				gtk_widget_destroy (priv->cover_popup);
				priv->cover_popup = NULL;
			}
		}		
		break;
	}
	
	return GDK_FILTER_CONTINUE;
}

static void
setup_root_window (MainWindow *main_window)
{
	Window     window;
	GdkWindow *gdkwindow;
	gint       mask;

	mask = PropertyChangeMask;
	
	window = GDK_ROOT_WINDOW ();
	gdkwindow = gdk_xid_table_lookup (window);
	
	gdk_error_trap_push ();
	if (gdkwindow) {
		XWindowAttributes attrs;
		XGetWindowAttributes (gdk_display, window, &attrs);
		mask |= attrs.your_event_mask;
	}
	
	XSelectInput (gdk_display, window, mask);
	
	gdk_error_trap_pop ();
	
	gdk_window_add_filter (NULL, root_window_filter_func, main_window);
}

#define MAX_WIDTH  400
#define MAX_HEIGHT 400

static gboolean
cover_button_press_event_cb (GtkWidget      *widget,
			     GdkEventButton *event,
			     MainWindow     *window)
{
	MainWindowPriv *priv;
	GtkWidget      *popup;
	GtkWidget      *image;
	gint            x, y;
	gint            popup_width, popup_height;
	gint            width, height;
	GdkPixbuf      *pixbuf;

	priv = window->priv;

	if (priv->cover_popup) {
		gtk_widget_destroy (priv->cover_popup);
		priv->cover_popup = NULL;
	}
	
	if (event->button != 1 || event->type != GDK_BUTTON_PRESS) {
		return FALSE;
	}

	popup_width = gdk_pixbuf_get_width (priv->cover_pixbuf);
	popup_height = gdk_pixbuf_get_height (priv->cover_pixbuf);

	width = priv->cover_frame->allocation.width;
	height = priv->cover_frame->allocation.height;

	/* Don't show a popup if the popup is smaller then the currently scaled
	 * image.
	 */
	if (popup_height < height || popup_width < width) {
		return FALSE;
	}
	if (height > MAX_WIDTH || width > MAX_HEIGHT) {
		return FALSE;
	}

	/* Arbitrary size limit... */
	if (popup_width > MAX_WIDTH || popup_height > MAX_HEIGHT) {
		pixbuf = gdk_pixbuf_scale_simple (priv->cover_pixbuf,
						  MAX_WIDTH, MAX_HEIGHT,
						  GDK_INTERP_HYPER);
		popup_width = MAX_WIDTH;
		popup_height = MAX_HEIGHT;
	} else {
		pixbuf = g_object_ref (priv->cover_pixbuf);
	}
	
	popup = gtk_window_new (GTK_WINDOW_POPUP);

	image = gtk_image_new ();
	gtk_container_add (GTK_CONTAINER (popup), image);

	gtk_image_set_from_pixbuf (GTK_IMAGE (image), pixbuf);
	g_object_unref (pixbuf);
	
	gdk_window_get_origin (priv->cover_image->window, &x, &y);

	x = x - (popup_width - width) / 2;
	y = y - (popup_height - height) / 2;
	
	gtk_window_move (GTK_WINDOW (popup), x, y);

	priv->cover_popup = popup;
	
	gtk_widget_show_all (popup);

	//gtk_grab_add (priv->cover_eventbox);
	
	return FALSE;
}

static gboolean
cover_button_release_event_cb (GtkWidget      *widget,
			       GdkEventButton *event,
			       MainWindow     *window)
{
	MainWindowPriv *priv;
	
	priv = window->priv;
	
	if (event->button != 1 || event->type != GDK_BUTTON_RELEASE) {
		return FALSE;
	}

	if (!priv->cover_popup) {
		return FALSE;
	}
	
	//gtk_grab_remove (priv->cover_eventbox);
	
	gtk_widget_destroy (priv->cover_popup);
	priv->cover_popup = NULL;
		
	return FALSE;
}

static void
update_playing_info (MainWindow *window)
{
	MainWindowPriv *priv;
	Song           *song;
	PlayingState    state;
	GtkTreeIter     iter;
	GtkTreePath    *path;
	char           *str;
	char           *tmp_title, *tmp_artist;
	char           *markup_title, *markup_artist;
	int             sec;
  
	priv = window->priv;

	state = player_get_state ();
	switch (state) {
	case PLAYING_STATE_PAUSED:
		song = player_get_song ();
		break;

	case PLAYING_STATE_PLAYING:
		song = player_get_song ();
		break;

	case PLAYING_STATE_STOPPED:
	default:
		song = NULL;
		break;
	}

	update_play_icon (window, state);
	
	if (!song) {
		song = source_model_get_current (priv->song_model);
	}
	
	if (!song){
		song = source_model_first (priv->song_model);
	}
	
	if (!song) {
		gtk_window_set_title (GTK_WINDOW (window), "Jamboree");
		gtk_widget_hide (priv->playing_info_box);
		return;
	}
  
	if (source_model_song_get_iter (priv->song_model, song, &iter)) {
		path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->song_model), &iter);

		/* FIXME: Should only do this when we change state for a song. */
		gtk_tree_model_row_changed (GTK_TREE_MODEL (priv->song_model), path, &iter);

		gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (priv->song_tree),
					      path, NULL,
					      TRUE, 0.2, 0);
      
		gtk_tree_path_free (path);
	}

	tmp_title = g_markup_escape_text (song_get_title (song), -1);
	tmp_artist = g_markup_escape_text (song_get_artist (song), -1);

	markup_title = g_strdup_printf ("<b>%s</b>",
					tmp_title[0] ? tmp_title : _("Unknown"));

	markup_artist = g_strdup_printf ("<b>%s</b>",
					 tmp_artist[0] ? tmp_artist : _("Unknown"));
  
	str = g_strdup_printf (_("%s by %s"), markup_title, markup_artist);

	gtk_label_set_markup (GTK_LABEL (priv->playing_song_label), str);
	g_free (tmp_title);
	g_free (tmp_artist);
	g_free (markup_title);
	g_free (markup_artist);
	g_free (str);

	str = g_strdup_printf (_("%s - %s"), song_get_artist (song), song_get_title (song));
	gtk_window_set_title (GTK_WINDOW (window), str);
	g_free (str);

	update_cover_image (window, song);
	
	sec = song_get_duration (song);

	if (sec > 0) {
		gtk_widget_set_sensitive (priv->seek_scale, TRUE);
	} else {
		gtk_widget_set_sensitive (priv->seek_scale, FALSE);
	}
  
	str = string_utils_format_duration (sec);
	gtk_label_set_text (GTK_LABEL (priv->length_label), str);
	g_free (str);

	if (state == PLAYING_STATE_STOPPED) {
		gtk_label_set_text (GTK_LABEL (priv->tick_label), _("0:00"));
		gtk_range_set_value (GTK_RANGE (priv->seek_scale), 0);
	}
  
	g_object_set (gtk_action_group_get_action (priv->action_group, "Next"),
		      "sensitive",
		      source_model_has_next (priv->song_model),
		      NULL);

	g_object_set (gtk_action_group_get_action (priv->action_group, "Prev"),
		      "sensitive",
		      state == PLAYING_STATE_PLAYING || source_model_has_prev (priv->song_model),
		      NULL);

	g_object_set (gtk_action_group_get_action (priv->action_group, "Stop"),
		      "sensitive",
		      state != PLAYING_STATE_STOPPED,
		      NULL);
	
	gtk_widget_show (priv->playing_info_box);
}

static gboolean
ensure_current_song (SourceModel *model, Song *song)
{
	/* If a song is being played, and we switch the view, then switch back,
	 * continue after the playing one instead of starting from the top.
	 */
	if (!source_model_song_get_iter (model, song, NULL))
		return FALSE;

	if (source_model_get_current (model) != song)
		source_model_set_current (model, song);

	return TRUE;
}

static void
player_eos_cb (Player     *player,
	       Song       *song,
	       MainWindow *window)
{
	MainWindowPriv *priv;
	Source         *source;
	Song           *next = NULL;
  
	priv = window->priv;
	
	if (song) {
		_song_set_playcount (song, song_get_playcount (song) + 1);
		_song_set_time_played (song, time (NULL));

		/* FIXME: The song should have a pointer to its source, and we
		 * should use that instead.
		 */
		source = sources_view_get_selected (priv->sources_view);
		source_song_changed (source, song, SONG_TAG_ALL);
		source_update_song (source, song);
		
#if 0
		if (song->length == 0) {
			song->length = player_tell ();
		}
#endif

		if (IS_SOURCE_SMART_PLAYLIST (source)) {
			if (!source_smart_playlist_song_evaluates (SOURCE_SMART_PLAYLIST (source), song)) {
				if (ensure_current_song (priv->song_model, song)) {
					next = source_model_next (priv->song_model);
				}
				
				if (source_model_remove (priv->song_model, song)) {
					/* The list model should probably keep track of this instead. */
					priv->total_num--;
					priv->total_size -= song_get_size (song);
					priv->total_length -= song_get_duration (song);
					
					update_songs_info (window);
				}
				
				source_model_set_current (priv->song_model, next);
			} else {
				source_model_add (priv->song_model, song);
			}
		}
		
		if (next) {
			song = next;
		} else {
			ensure_current_song (priv->song_model, song);
			song = source_model_next (priv->song_model);
		}

		if (!song) {
			/* Reset the play list. */
			source_model_reshuffle (priv->song_model);
			source_model_set_current (priv->song_model, NULL);

			if (main_window_get_repeat (window)) {
				song = source_model_next (priv->song_model);
			}
		}
		
		if (song) { 
			if (player_play_song (song, NULL)) {
#ifdef HAVE_DBUS
				jamboree_dbus_emit_playing_song (song);
#endif
			}
		}
	}
  
	update_playing_info (window);
}

static void
set_elapsed_time_label (MainWindow *window, int sec)
{
	MainWindowPriv *priv;
	char           *str;
  
	priv = window->priv;

	str = string_utils_format_duration (sec);
	gtk_label_set_text (GTK_LABEL (priv->tick_label), str);
	g_free (str);
}

static void
set_elapsed_time_scale (MainWindow *window, int sec)
{
	MainWindowPriv *priv;
	Song *song;
	double value;
  
	priv = window->priv;

	song = player_get_song ();
	if (song) {
		if (sec == 0 || song_get_duration (song) == 0)
			value = 0;
		else
			value = sec * 100.0 / song_get_duration (song);
      
		g_signal_handlers_block_by_func (priv->seek_scale, seek_scale_value_changed_cb, window);
		gtk_range_set_value (GTK_RANGE (priv->seek_scale), value);
		g_signal_handlers_unblock_by_func (priv->seek_scale, seek_scale_value_changed_cb, window);
	}
}

static void
player_tick_cb (Player     *player,
		long        sec,
		MainWindow *window)
{
	if (!window->priv->seeking) {
		set_elapsed_time_scale (window, sec);
		set_elapsed_time_label (window, sec);
	}
}

static void
player_error_cb (Player     *player,
		 GError     *error,
		 MainWindow *window)
{
	update_playing_info (window);

	handle_play_error (window,
			   player_get_song (),
			   error);
}

static void
player_state_changed_cb (Player      *player,
			 PlayingState  state,
			 MainWindow  *window)
{
	update_playing_info (window);

#ifdef HAVE_DBUS
        jamboree_dbus_emit_state (state);
#endif
}

static void
random_toggled_cb (GtkToggleAction *action,
		   MainWindow      *window)
{
	MainWindowPriv *priv;
	gboolean active;

	priv = window->priv;
  
	active = gtk_toggle_action_get_active (action);
  
	source_model_set_random (priv->song_model, active);

	gconf_client_set_bool (gconf_client,
			       "/apps/jamboree/control/shuffle",
			       active,
			       NULL);

	if (active && player_get_state () == PLAYING_STATE_STOPPED) {
		main_window_handle_next (window);
	}
}

static void
repeat_toggled_cb (GtkToggleAction *action,
		   MainWindow      *window)
{
	MainWindowPriv *priv;
	gboolean active;

	priv = window->priv;
  
	active = gtk_toggle_action_get_active (action);
  
	gconf_client_set_bool (gconf_client,
			       "/apps/jamboree/control/repeat",
			       active,
			       NULL);
}

static void
song_tree_row_activated_cb (GtkTreeView       *tree,
			    GtkTreePath       *path,
			    GtkTreeViewColumn *column,
			    MainWindow        *window)
{
	MainWindowPriv *priv;
	GtkTreeIter     iter;
	Song           *song;

	priv = window->priv;

	gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->song_model), &iter, path);
	song = source_model_get_song (priv->song_model, &iter);

	player_stop ();
  
	source_model_set_current (priv->song_model, song);
	main_window_handle_play (window, FALSE);
}

static GtkWidget *
append_context_item (GtkWidget  *menu,
		     const char *stock_item,
		     const char *label,
		     gpointer    function,
		     gpointer    data)
{
	GtkWidget *menuitem;

	if (stock_item) {
		menuitem = gtk_image_menu_item_new_from_stock (stock_item, NULL);
	}
	else if (label) {
		menuitem = gtk_menu_item_new_with_label (label);
	} else {
		menuitem = NULL;
		g_assert_not_reached ();
	}

	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	if (function) {
		g_signal_connect (menuitem,
				  "activate",
				  G_CALLBACK (function),
				  data);
	}
	
	return menuitem;
}

static void
context_play_cb (GtkWidget       *menu_item,
		 MainWindow      *window)
{
	MainWindowPriv   *priv;
	GtkTreeSelection *selection;
	GtkTreeIter       iter;
	GList            *rows;
	Song             *song;

	priv = window->priv;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->song_tree));
	rows = gtk_tree_selection_get_selected_rows (selection, NULL);
	if (!rows) {
		return;
	}
	
	/* Play the first selected song. */
	gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->song_model), &iter, rows->data);
	song = source_model_get_song (SOURCE_MODEL (priv->song_model), &iter);
  
	if (song != player_get_song ()) {
		player_play_song (song, NULL);
	} else {
		player_play (NULL);
	}
}

static void
context_pause_cb (GtkWidget       *menu_item,
		  MainWindow      *window)
{
	player_pause ();
}

static void
context_properties_cb (GtkWidget  *menu_item,
		       MainWindow *window)
{
	MainWindowPriv   *priv;
	GtkTreeSelection *selection;
	GList            *rows, *songs = NULL, *l;
	GtkTreeIter       iter;
	Song             *song;
	
	priv = window->priv;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->song_tree));
	rows = gtk_tree_selection_get_selected_rows (selection, NULL);
	for (l = rows; l; l = l->next) {
		gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->song_model), &iter, l->data);
		song = source_model_get_song (SOURCE_MODEL (priv->song_model), &iter);

		songs = g_list_prepend (songs, song);
	}

	songs = g_list_reverse (songs);
	
	if (songs) {
		song_properties_show (GTK_WINDOW (window), songs, priv->database);
	}

	g_list_foreach (rows, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (rows);
	g_list_free (songs);
}

static void
context_remove_cb (GtkWidget  *menu_item,
		   MainWindow *window)
{
	MainWindowPriv   *priv;
	GtkTreeSelection *selection;
	GList            *rows, *songs = NULL, *l;
	GtkTreeIter       iter;
	Source           *source;
	Song             *song;
	gboolean          count_changed = FALSE;

	priv = window->priv;
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->song_tree));
	rows = gtk_tree_selection_get_selected_rows (selection, NULL);
	for (l = rows; l; l = l->next) {
		gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->song_model), &iter, l->data);
		song = source_model_get_song (SOURCE_MODEL (priv->song_model), &iter);

		songs = g_list_prepend (songs, song);
	}

	source = sources_view_get_selected (priv->sources_view);

	for (l = songs; l; l = l->next) {
		SongCount *count;
      
		song = l->data;

		/* Stop if we're playing a removed song. */
		if (player_get_song () == song) {
			player_stop ();
		}

		/* Update artist list. */
		count = g_hash_table_lookup (priv->artist_hash, song_get_artist_folded (song));
		if (count) {
			count->count--;

			if (count->count == 0) {
				gtk_list_store_remove (priv->artist_store, &count->iter);

				g_hash_table_remove (priv->artist_hash,
						     song_get_artist_folded (song));
	      
				count_changed = TRUE;
			}
		}
      
		/* Update album list. */
		count = g_hash_table_lookup (priv->album_hash, song_get_album_folded (song));
		if (count) {
			count->count--;

			if (count->count == 0) {
				gtk_list_store_remove (priv->album_store, &count->iter);

				g_hash_table_remove (priv->album_hash,
						     song_get_album_folded (song));
	      
				count_changed = TRUE;
			}
		}

		/* FIXME: remove from the treeview in a signal callback. */
		if (source_remove_song (source, song)) {
			source_model_remove (SOURCE_MODEL (priv->song_model), song);
		}
	}
  
	if (count_changed) {
		update_artist_album_count (window);
	}
	
	g_list_foreach (rows, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (rows);
	g_list_free (songs);
}

static void 	 
volume_changed_cb (GtkWidget  *button, 	 
		   int         volume, 	 
		   MainWindow *window) 	 
{ 	 
	player_set_volume (volume); 	 
}

static gboolean
song_tree_row_button_press_event_cb (GtkWidget      *widget,
				     GdkEventButton *event,
				     MainWindow     *window)
{
	MainWindowPriv   *priv;
	GtkWidget        *menu;
	GtkTreePath      *path;
	GtkTreeIter       iter;
	gboolean          has_cell;
	Song             *song;
	GtkTreeSelection *selection;

	if (event->button != 3)
		return FALSE;

	priv = window->priv;
  
	has_cell = gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW (priv->song_tree),
						  event->x,
						  event->y,
						  &path,
						  NULL, NULL, NULL);
	if (!has_cell)
		return FALSE;
  
	gtk_tree_model_get_iter (GTK_TREE_MODEL (priv->song_model), &iter, path);
	song = source_model_get_song (SOURCE_MODEL (priv->song_model), &iter);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->song_tree));
	if (!gtk_tree_selection_iter_is_selected (selection, &iter))
	{
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_iter (selection, &iter);
	}
	menu = gtk_menu_new ();

	if (player_is_playing (song)) {
		append_context_item (menu, JAMBOREE_STOCK_PAUSE, NULL, context_pause_cb, window);
	} else {
		append_context_item (menu, JAMBOREE_STOCK_PLAY, NULL, context_play_cb, window);    
	}
	
	append_context_item (menu, GTK_STOCK_REMOVE, NULL, context_remove_cb, window);
	append_context_item (menu, NULL, _("Show Song Information"), context_properties_cb, window);

	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 3, event->time);
  
	return TRUE;
}
				     
static void
song_tree_drag_data_received_cb (GtkWidget        *widget,
				 GdkDragContext   *context,
				 int               x,
				 int               y,
				 GtkSelectionData *data,
				 guint             info,
				 guint             time,
				 MainWindow       *window)
{
	MainWindowPriv *priv;

	priv = window->priv;
  
	if (data->length >= 0 && data->format == 8) {
		const char *ptr = (const char *) data->data;
		char        url[1024];
		char       *path;
		GList      *paths = NULL, *l;
      
		while (sscanf (ptr, "%s\r\n", (char *) &url) > 0) {
			path = gnome_vfs_get_local_path_from_uri (url);
			paths = g_list_append (paths, (char *) path);
	  
			ptr += strlen (url) + 2;
		} 
      
		gtk_drag_finish (context, TRUE, FALSE, time);

		add_paths (window, paths);

		for (l = paths; l; l = l->next) {
			g_free (l->data);
		}
		g_list_free (paths);
	} else {
		g_message ("Don't know how to handle format %d", data->format);
		gtk_drag_finish (context, FALSE, FALSE, time);      
	}

	g_signal_stop_emission_by_name (widget, "drag_data_received");
}

static int
genre_sort_func (GtkTreeModel *model,
		  GtkTreeIter  *a,
		  GtkTreeIter  *b,
		  gpointer      user_data)
{
	char     *val1, *val2;
	gboolean  all1, all2;
	int       ret;
  
	gtk_tree_model_get (model, a,
			    GENRE_COL_KEY, &val1,
			    GENRE_COL_ALL, &all1,
			    -1);

	gtk_tree_model_get (model, b,
			    GENRE_COL_KEY, &val2,
			    GENRE_COL_ALL, &all2,
			    -1);

	if (all1) {
		ret = -1;
	}
	else if (all2) {
		ret = 1;
	} else {
		ret = strcasecmp (val1, val2);
	}
	
	g_free (val1);
	g_free (val2);

	return ret;
}

static int
album_sort_func (GtkTreeModel *model,
		 GtkTreeIter  *a,
		 GtkTreeIter  *b,
		 gpointer      user_data)
{
	char     *val1, *val2;
	gboolean  all1, all2;
	int       ret;
  
	gtk_tree_model_get (model, a,
			    ARTIST_COL_KEY, &val1,
			    ARTIST_COL_ALL, &all1,
			    -1);

	gtk_tree_model_get (model, b,
			    ARTIST_COL_KEY, &val2,
			    ARTIST_COL_ALL, &all2,
			    -1);

	if (all1) {
		ret = -1;
	}
	else if (all2) {
		ret = 1;
	} else {
		ret = strcasecmp (val1, val2);
	}
	
	g_free (val1);
	g_free (val2);

	return ret;
}

static int
artist_sort_func (GtkTreeModel *model,
		  GtkTreeIter  *a,
		  GtkTreeIter  *b,
		  gpointer      user_data)
{
	char     *val1, *val2;
	gboolean  all1, all2;
	int       ret;
  
	gtk_tree_model_get (model, a,
			    ARTIST_COL_KEY, &val1,
			    ARTIST_COL_ALL, &all1,
			    -1);

	gtk_tree_model_get (model, b,
			    ARTIST_COL_KEY, &val2,
			    ARTIST_COL_ALL, &all2,
			    -1);

	if (all1) {
		ret = -1;
	}
	else if (all2) {
		ret = 1;
	} else {
		ret = strcasecmp (val1, val2);
	}
	
	g_free (val1);
	g_free (val2);

	return ret;
}

static void
genre_data_func (GtkTreeViewColumn *tree_column,
		 GtkCellRenderer   *cell,
		 GtkTreeModel      *tree_model,
		 GtkTreeIter       *iter,
		 gpointer           data)
{
	char *genre;
	
	gtk_tree_model_get (tree_model, iter,
			    GENRE_COL_NAME, &genre,
			    -1);

	g_object_set (cell, "text", genre, NULL);

	g_free (genre);
}

static void
artist_data_func (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer   *cell,
		  GtkTreeModel      *tree_model,
		  GtkTreeIter       *iter,
		  gpointer           data)
{
	char *artist;
	
	gtk_tree_model_get (tree_model, iter,
			    ARTIST_COL_NAME, &artist,
			    -1);

	g_object_set (cell, "text", artist, NULL);

	g_free (artist);
}

static gboolean
seeking_done_cb (MainWindow *window)
{
	window->priv->seeking = FALSE;
	window->priv->seeking_idle_id = 0;

	return FALSE;
}

static void
seek_scale_value_changed_cb (GtkWidget  *widget,
			     MainWindow *window)
{
	MainWindowPriv *priv = window->priv;
	GtkRange       *range = GTK_RANGE (widget);
	Song           *song;
	double          value;
	int             offset;
  
	if (priv->seeking_idle_id) {
		return;
	}
  
	priv->seeking = TRUE;

	value = gtk_range_get_value (range);

	song = player_get_song ();
	if (!song) {
		return;
	}

	offset = value * (song_get_duration (song) - 0.5) / 100.0;
  
	player_seek (offset);

	priv->seeking_idle_id = g_idle_add ((GSourceFunc)seeking_done_cb, window);
}

static gboolean
seek_scale_button_press_cb (GtkWidget      *widget,
			    GdkEventButton *event,
			    MainWindow     *window)
{
	MainWindowPriv *priv = window->priv;

	if (priv->seeking_idle_id) {
		return FALSE;
	}
    
	priv->seeking = TRUE;

	return FALSE;
}

static gboolean
seek_scale_button_release_cb (GtkWidget      *widget,
			      GdkEventButton *event,
			      MainWindow     *window)
{
	MainWindowPriv *priv;
	GtkRange       *range;
	Song           *song;
	GtkAdjustment  *adj;
	double          value;
	int             offset;

	priv = window->priv;

	if (priv->seeking_idle_id) {
		return FALSE;
	}
  
	range = GTK_RANGE (widget);
	
	song = player_get_song ();
	if (!song) {
		return FALSE;
	}
	
	adj = gtk_range_get_adjustment (range);
	value = gtk_adjustment_get_value (adj);

	offset = value * (song_get_duration (song) - 0.5) / 100.0;

	if (player_get_state () != PLAYING_STATE_STOPPED && value >= 99) {
		/* Don't go around the trouble of seeking if it's just silence
		 * left.
		 */
		gtk_range_set_value (GTK_RANGE (widget), 0);

		/* Make sure the current song is redrawn when we stop playing
		 * it.
		 */
		song_changed (window, song);
		
		main_window_handle_next (window);
		return FALSE;
	}
	
	player_seek (offset);

	priv->seeking_idle_id = g_idle_add ((GSourceFunc) seeking_done_cb, window);

	return FALSE;
}

static gboolean
seek_scale_motion_notify_cb (GtkWidget      *widget,
			     GdkEventMotion *event,
			     MainWindow     *window)
{
	MainWindowPriv *priv;
	GtkRange       *range;
	Song           *song;
	GtkAdjustment  *adj;
	int             sec;
	double          value;

	priv = window->priv;
	
	if (!priv->seeking) {
		return FALSE;
	}

	range = GTK_RANGE (widget);

	song = player_get_song ();
	if (!song) {
		return FALSE;
	}
	
	adj = gtk_range_get_adjustment (range);
	value = gtk_adjustment_get_value (adj);

	sec = value * song_get_duration (song) / 100.0;
	set_elapsed_time_label (window, sec);
  
	return FALSE;
}

static gboolean
search_entry_timeout_cb (MainWindow *window)
{
	MainWindowPriv *priv;
    
	priv = window->priv;
  
	priv->search_timeout_id = 0;

	perform_search (window, FALSE);

	return FALSE;
}

static void
search_entry_activate_cb (GtkEntry   *entry,
			  MainWindow *window)
{
	MainWindowPriv *priv;

	priv = window->priv;
  
	if (priv->search_timeout_id) {
		g_source_remove (priv->search_timeout_id);
		priv->search_timeout_id = 0;
	}

	perform_search (window, TRUE);
}

static gboolean
search_entry_changed_cb (GtkEntry   *entry,
			 MainWindow *window)
{
	MainWindowPriv *priv;

	priv = window->priv;

	if (priv->search_timeout_id) {
		g_source_remove (priv->search_timeout_id);
	}
	
	priv->search_timeout_id = g_timeout_add (250,
						 (GSourceFunc) search_entry_timeout_cb,
						 window);

	return FALSE;
}

static gboolean
initial_browse_idle_cb (MainWindow *window)
{
        if (!window->priv->initial_browse_done) {
                update_songs (window, TRUE, TRUE, TRUE);
                window->priv->initial_browse_done = TRUE;
        }
						
	return FALSE;
}

static void
set_browse_mode (MainWindow *window, gboolean browse)
{
	MainWindowPriv *priv;
	GtkAction *action;

	priv = window->priv;

	priv->browse_mode = browse;

	action = gtk_action_group_get_action (priv->action_group, "ShowSameArtist");
	g_object_set (action, "sensitive", browse, NULL);
}

static void
song_changed_cb (Source *source, Song *song, int tags, MainWindow *window)
{
	MainWindowPriv *priv;

	priv = window->priv;

	if (source != sources_view_get_selected (priv->sources_view)) {
		return;
	}
	
	song_changed (window, song);
}

static void
song_changed (MainWindow *window,
	      Song       *song)
{
	MainWindowPriv *priv;
	GtkTreeIter     iter;
	GtkTreePath    *path;

	priv = window->priv;
	
	if (!source_model_song_get_iter (priv->song_model, song, &iter)) {
		return;
	}

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->song_model), &iter);
	
	gtk_tree_model_row_changed (GTK_TREE_MODEL (priv->song_model),
				    path,
				    &iter);

	gtk_tree_path_free (path);
}

static gboolean
window_focus_cb (GtkWidget        *widget,
		 GtkDirectionType  dir,
		 MainWindow       *window)
{
	if (dir == GTK_DIR_DOWN) {
		gtk_widget_child_focus (GTK_WIDGET (window->priv->song_tree), dir);
	}

	return FALSE;
}

GtkWidget *
main_window_new (Source *database)
{
	MainWindow     *window;
	MainWindowPriv *priv;

	g_return_val_if_fail (IS_SOURCE_DATABASE (database), NULL);
	g_assert (glob_window == NULL);

	window = g_object_new (TYPE_MAIN_WINDOW, NULL);

	priv = window->priv;

	priv->database = g_object_ref (database);

	g_signal_connect (database,
			  "song_changed",
			  G_CALLBACK (song_changed_cb),
			  window);

	g_signal_connect (window,
			  "focus",
			  G_CALLBACK (window_focus_cb),
			  window);

	setup_gui (window);
	
	g_idle_add ((GSourceFunc) initial_browse_idle_cb, window);

	glob_window = window;
  
	return GTK_WIDGET (window);
}

static void
update_edit_smart_playlist_sensitivity (MainWindow *window)
{
	MainWindowPriv *priv;
	Source         *source;
	GtkAction      *action;

	priv = window->priv;

	action = gtk_action_group_get_action (priv->action_group, "EditSmartPlaylist");

	source = sources_view_get_selected (priv->sources_view);
	if (!source || !IS_SOURCE_SMART_PLAYLIST (source)) {
		g_object_set (action, "sensitive", FALSE, NULL);
		return;
	}

	g_object_set (action, "sensitive", TRUE, NULL);
}

static void
sources_view_selection_changed_cb (GtkTreeSelection *selection,
				   MainWindow       *window)
{
	MainWindowPriv *priv;
	Source         *list;

	priv = window->priv;

	update_edit_smart_playlist_sensitivity (window);

	list = sources_view_get_selected (priv->sources_view);
	if (!list) {
		return;
	}

	gtk_entry_set_text (GTK_ENTRY (priv->search_entry), "");
	update_songs (window, TRUE, TRUE, TRUE);
}

static void
new_playlist_cb (GtkAction *action, MainWindow *window)
{
	MainWindowPriv *priv;
	Source         *source;
  
	priv = window->priv;

	source = source_playlist_new (priv->database,
				      _("New playlist"),
				      -1);

	sources_view_append_and_edit (priv->sources_view, source);
}

static void
new_smart_playlist_cb (GtkAction *action, MainWindow *window)
{
	MainWindowPriv *priv;
	Source         *source;

	priv = window->priv;

	source = source_smart_playlist_new (priv->database, _("New playlist"));

	if (!smart_playlist_dialog_run (GTK_WINDOW (window), SOURCE_SMART_PLAYLIST (source))) {
		g_object_unref (source);
		return;
	}

	sources_view_append (priv->sources_view, source);
}

static void
edit_smart_playlist_cb (GtkAction *action, MainWindow *window)
{
	MainWindowPriv *priv;
	Source         *source;
  
	priv = window->priv;

	source = sources_view_get_selected (priv->sources_view);
	if (!source) {
		return;
	}

	if (smart_playlist_dialog_run (GTK_WINDOW (window), SOURCE_SMART_PLAYLIST (source))) {
		/* FIXME: remove when we get a changed signal instead. */
		update_songs (window, TRUE, TRUE, TRUE);
	}
}

static void
add_folder_cb (GtkAction *action, MainWindow *window)
{
	GtkWidget  *dialog;
	const char *dir;
	char       *start_dir;

	dialog = gtk_file_chooser_dialog_new (_("Add Music Folder"),
					      GTK_WINDOW (window),
					      GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
					      NULL);

	start_dir = gconf_client_get_string (gconf_client,
					     GCONF_PATH "/default_add_folder",
					     NULL);
	if (start_dir) {
		if (strcmp (start_dir, "~") == 0) {
			gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), g_get_home_dir ()); 
		} else {
			if (g_file_test (start_dir, G_FILE_TEST_IS_DIR)) {
				gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), start_dir);
			}
		}
		
		g_free (start_dir);
	}
	
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		dir = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		
		gtk_widget_hide (dialog);
		
		if (dir) {
			GList *list;
	  
			gconf_client_set_string (gconf_client,
						 GCONF_PATH "/default_add_folder",
						 dir,
						 NULL);

			list = g_list_append (NULL, (char *) dir);
			add_paths (window, list);
			g_list_free (list);
		}
	}
  
	gtk_widget_destroy (dialog);
}

static void
quit_cb (GtkAction *action, MainWindow *window)
{
	save_ui_state (window);

	gtk_widget_destroy (GTK_WIDGET (window));
}

static void
play_cb (GtkAction *action, MainWindow *window)
{
	main_window_handle_play (window, TRUE);
}

static void
stop_cb (GtkAction *action, MainWindow *window)
{
	main_window_handle_stop (window);
}

static void
prev_cb (GtkAction *action, MainWindow *window)
{
	main_window_handle_prev (window);
}

static void
next_cb (GtkAction *action, MainWindow *window)
{
	main_window_handle_next (window);
}

static void
reset_playcount_cb (GtkAction *action, MainWindow *window)
{
	/* Could virtualize this, but we only have one real source now. */
	source_database_reset_playcount (SOURCE_DATABASE (window->priv->database));
}

static void
song_columns_cb (GtkAction *action, MainWindow *window)
{
	source_view_column_chooser (window->priv->song_tree, GTK_WINDOW (window));
}

static void
same_artist_cb (GtkAction *action, MainWindow *window)
{
	Song *song;

	song = main_window_get_current_song (window);
	if (song) {
		select_artist (window, song_get_artist_folded (song));
	}
}

static void 
about_cb (GtkAction *action, MainWindow *window)
{
	static GtkWidget *about = NULL;
	const char       *authors[] = {
		"Richard Hult <richard@imendio.com>",
		"Anders Carlsson <andersca@gnome.org>",
		"Johan Dahlin <johan@gnome.org>",
		NULL
	};
	const gchar      *copyright = _("Yes, there is a Swedish conspiracy");
	const gchar      *comments = _("A music player for GNOME");
	const gchar      *translator_credits = _("translator-credits");
	GdkPixbuf        *pixbuf;

	if (about) {
		gtk_window_present (GTK_WINDOW (about));
		return;
	}

	pixbuf = gdk_pixbuf_new_from_file (DATADIR "/pixmaps/jamboree.png", NULL);
  	
	about = g_object_new (GTK_TYPE_ABOUT_DIALOG,
			      "name", "Jamboree",
			      "version", VERSION,
			      "copyright", copyright,
			      "comments", comments,
			      /*"website", "http://www....",*/
			      "authors", authors,
			      /*"documenters", documenters,*/
			      "translator_credits", translator_credits,
			      "logo", pixbuf,
			      NULL);

	if (pixbuf) {
		g_object_unref (pixbuf);
	}
	
	gtk_window_set_destroy_with_parent (GTK_WINDOW (about), TRUE);

	g_signal_connect (about, "response", G_CALLBACK (gtk_widget_destroy), NULL);
	g_signal_connect (about, "destroy", G_CALLBACK (gtk_widget_destroyed), &about);

	gtk_window_set_transient_for (GTK_WINDOW (about),
				      GTK_WINDOW (window));
	gtk_window_present (GTK_WINDOW (about));
}

typedef struct {
	MainWindow *window;
	GtkWidget  *dialog;
	GtkWidget  *progress;
	GtkWidget  *label;
	GtkWidget  *bar;

	gboolean    cancel;
	guint       timeout_id;
	char       *dirname;
} ProgressData;

static gboolean
add_progress_show_cb (ProgressData *data)
{
	gtk_widget_show_all (data->dialog);

	data->timeout_id = 0;
  
	return FALSE;
}

static void
add_progress_response_cb (GtkWidget    *dialog,
			  gint          response,
			  ProgressData *data)
{
	data->cancel = TRUE;
	data->dialog = NULL;

	gtk_widget_destroy (dialog);
}

static gboolean
add_progress_callback (SourceDatabase *db,
		       const char     *path,
		       gint            files,
		       gint            completed,
		       ProgressData   *data)
{
	gdouble  percentage;
	gchar   *dirname;
	gchar   *tmp;

	while (gtk_events_pending ()) {
		gtk_main_iteration ();
	}
	 
	if (data->cancel) {
		return FALSE;
	}

	tmp = g_path_get_dirname (path);
	dirname = g_path_get_basename (tmp);
	g_free (tmp);
  
	if (!data->dirname || strcmp (dirname, data->dirname) != 0) {
		g_free (data->dirname);
		data->dirname = g_strdup (dirname);
      
		if (!g_utf8_validate (dirname, -1, NULL)) {
			tmp = g_filename_to_utf8 (dirname, -1, NULL, NULL, NULL);
			if (!tmp) {
				tmp = g_strdup (_("Invalid Unicode"));
			}
			
			g_free (dirname);
			dirname = tmp;
		}
	  
		gtk_label_set_text (GTK_LABEL (data->label), dirname);
	}

	percentage = (gdouble) completed / (gdouble) files;
	percentage = CLAMP (percentage, 0.0, 1.0);
	gtk_progress_bar_update (GTK_PROGRESS_BAR (data->bar), percentage);

	g_free (dirname);
  
	return TRUE;
}

static void
add_paths (MainWindow *window, GList *paths)
{
	MainWindowPriv *priv;
	GladeXML       *glade;
	ProgressData    data;
	gboolean        ret;
  
	priv = window->priv;

	data.window = window;
	data.cancel = FALSE;
	data.dirname = NULL;

	glade = glade_xml_new (DATADIR "/jamboree/jamboree.glade", "progress_dialog", NULL);

	data.dialog = glade_xml_get_widget (glade, "progress_dialog");
	data.label = glade_xml_get_widget (glade, "folder_label");
	data.bar = glade_xml_get_widget (glade, "bar");

	gtk_window_set_transient_for (GTK_WINDOW (data.dialog), GTK_WINDOW (window));

	gtk_label_set_ellipsize (GTK_LABEL (data.label), PANGO_ELLIPSIZE_END);
	
	g_signal_connect (data.dialog,
			  "response",
			  G_CALLBACK (add_progress_response_cb),
			  &data);
  
	data.timeout_id = g_timeout_add (1500, (GSourceFunc) add_progress_show_cb, &data);

	set_cursor_watch (window);

	/* Don't make it possible to add while adding... */
	g_object_set (gtk_action_group_get_action (priv->action_group, "AddFolder"),
		      "sensitive", FALSE, NULL);
	
	/* Make sure the filechooser goes away directly. */
	while (gtk_events_pending ()) {
		gtk_main_iteration ();
	}
	
	ret = source_database_add_dir (SOURCE_DATABASE (priv->database),
				       paths->data,
				       (SourceDatabaseAddProgressFunc) add_progress_callback,
				       &data);

	g_object_set (gtk_action_group_get_action (priv->action_group, "AddFolder"),
		      "sensitive", TRUE, NULL);
	
	set_cursor_default (window);
	
	if (data.timeout_id) {
		g_source_remove (data.timeout_id);
	}
	
	if (data.dialog) {
		gtk_widget_destroy (data.dialog);
	}
	
	g_free (data.dirname);

	g_object_unref (glade);

	if (ret) {
		source_songs_changed (priv->database);
	}
}

static void
update_artist_album_count (MainWindow *window)
{
	MainWindowPriv *priv;
	int i;
	char *str;
	GtkTreeIter iter;

	priv = window->priv;

	i = gtk_tree_model_iter_n_children (GTK_TREE_MODEL (priv->genre_store), NULL) - 1;
	str = g_strdup_printf (ngettext ("All (%d genre)", "All (%d genres)", i), i);
	if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->genre_store), &iter)) {
		gtk_list_store_set (priv->genre_store, &iter,
				    GENRE_COL_NAME, str,
				    -1);
	}	
	g_free (str);

	i = gtk_tree_model_iter_n_children (GTK_TREE_MODEL (priv->artist_store), NULL) - 1;
	str = g_strdup_printf (ngettext ("All (%d artist)", "All (%d artists)", i), i);
	if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->artist_store), &iter)) {
		gtk_list_store_set (priv->artist_store, &iter,
				    ARTIST_COL_NAME, str,
				    -1);
	}	
	g_free (str);

	i = gtk_tree_model_iter_n_children (GTK_TREE_MODEL (priv->album_store), NULL) - 1;
	str = g_strdup_printf (ngettext ("All (%d album)", "All (%d albums)", i), i);
	if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->album_store), &iter)) {
		gtk_list_store_set (priv->album_store, &iter,
				    ALBUM_COL_NAME, str,
				    -1);
	}
	g_free (str);

	update_songs_info (window);
}

void
main_window_handle_next (MainWindow *window)
{
	MainWindowPriv *priv;
	Song           *song;
	PlayingState    state;
  
	priv = window->priv;
  
	song = source_model_next (priv->song_model);

	if (!song) {
		song = source_model_first (priv->song_model);
	}

	state = player_get_state ();
	switch (state) {
	case PLAYING_STATE_PLAYING:
		player_set_song (song, NULL);
		break;
	case PLAYING_STATE_PAUSED:
		/* Stop the player so play will start the new song. */
		player_stop ();
		break;
	case PLAYING_STATE_STOPPED:
		break;
	}
     
	update_playing_info (window);

#ifdef HAVE_DBUS
	if (song && player_get_state () == PLAYING_STATE_PLAYING) {
		jamboree_dbus_emit_playing_song (song);
	}
#endif
}

void
main_window_handle_prev (MainWindow *window)
{
	MainWindowPriv *priv;
	Song           *song;
	int             sec;
  	PlayingState    state;

	priv = window->priv;

	if (player_get_state () == PLAYING_STATE_PLAYING) {
		sec = player_tell ();

		if (sec > 3) { 
			player_stop ();
			player_play (NULL);
			update_playing_info (window);
			return;
		}
	}
  
	song = source_model_prev (priv->song_model);

	if (!song) {
		song = source_model_first (priv->song_model);
	}

	state = player_get_state ();
	switch (state) {
	case PLAYING_STATE_PLAYING:
		player_set_song (song, NULL);
		break;
	case PLAYING_STATE_PAUSED:
		/* Stop the player so play will start the new song. */
		player_stop ();
		break;
	case PLAYING_STATE_STOPPED:
		break;
	}
	
	update_playing_info (window);

#ifdef HAVE_DBUS
	if (song && player_get_state () == PLAYING_STATE_PLAYING) {
		jamboree_dbus_emit_playing_song (song);
	}
#endif
}

static gboolean
handle_stop_idle_func (gpointer data)
{
	player_stop ();
	return FALSE;
}

void
main_window_handle_stop (MainWindow *window)
{
	/* Do this in an idle since it looks bad when gstreamer is really
	 * slow.
	 */
	g_idle_add (handle_stop_idle_func, NULL);

	/* Hack again... make the action insensitive before we get the state
	 * changed signal, otherwise it looks bad.
	 */
	g_object_set (gtk_action_group_get_action (window->priv->action_group, "Stop"),
		      "sensitive", FALSE, NULL);
}

static gboolean
handle_pause_idle_func (gpointer data)
{
	player_pause ();
	update_playing_info (data);

	return FALSE;
}

void
main_window_handle_play (MainWindow *window, gboolean toggle)
{
	MainWindowPriv *priv;
	Song           *song;
	GError         *error = NULL;

	priv = window->priv;

        /* Hack for now to make sure that we have a song selected when using the
         * dbus interface.
         */
        if (!priv->initial_browse_done) {
                initial_browse_idle_cb (window);
        }
        
	switch (player_get_state ()) {
	case PLAYING_STATE_PLAYING:
		if (!toggle) {
			return;
		}

		g_idle_add (handle_pause_idle_func, window);
		break;

	case PLAYING_STATE_PAUSED:
		if (!player_play (&error)) {
			handle_play_error (window, player_get_song (), error);
			g_error_free (error);
			return;
		}
		update_playing_info (window);
		break;
      
	case PLAYING_STATE_STOPPED:
		song = source_model_get_current (priv->song_model);
		if (!song) {
			song = source_model_first (priv->song_model);
		}
		
		if (player_play_song (song, &error)) {
#ifdef HAVE_DBUS
			if (song) {
				jamboree_dbus_emit_playing_song (song);
			}
#endif
		} else {
			handle_play_error (window, song, error);
			g_error_free (error);
			return;
		}
		update_playing_info (window);
		break;
	}
}

void
main_window_handle_play_song (MainWindow *window, Song *song)
{
	MainWindowPriv *priv;
  
	priv = window->priv;

	if (player_play_song (song, NULL)) {
#ifdef HAVE_DBUS
		jamboree_dbus_emit_playing_song (song);
#endif
	}
	
	update_playing_info (window);
}

void
main_window_handle_pause (MainWindow *window)
{
	MainWindowPriv *priv;
  
	priv = window->priv;

	switch (player_get_state ()) {
	case PLAYING_STATE_PLAYING:
		player_pause ();
		break;

	case PLAYING_STATE_PAUSED:
	case PLAYING_STATE_STOPPED:
		return;
	}
  
	update_playing_info (window);
}

/* Note: window and player should just be singletons. */
MainWindow *
main_window_get (void)
{
	return glob_window;
}

Song *
main_window_get_current_song (MainWindow *window)
{
	MainWindowPriv *priv;
	PlayingState    state;
	Song           *song = NULL;

	priv = window->priv;
	
	state = player_get_state ();
	switch (state) {
	case PLAYING_STATE_PAUSED:
		song = player_get_song ();
		break;
	case PLAYING_STATE_PLAYING:
		song = player_get_song ();
		break;
	default:
		break;
	}

	if (!song) {
		song = source_model_get_current (priv->song_model);
	}
	
	if (!song) {
		song = source_model_first (priv->song_model);
	}
	
	return song;
}

static void
connect_proxy_button (MainWindow  *window,
		      GtkWidget   *button,
		      const char  *action_name)
{
	MainWindowPriv *priv;
	GtkAction      *action;

	priv = window->priv;
     
	action = gtk_action_group_get_action (priv->action_group, action_name);
	gtk_action_connect_proxy (action, button);
}

static gboolean
safe_gconf_client_get_int (GConfClient *client,
			   const char  *key,
			   int         *i)
{
	GConfValue *value;
	GError     *error = NULL;

	value = gconf_client_get_default_from_schema (client, key, NULL);
	if (!value) {
		return FALSE;
	}

	gconf_value_free (value);

	*i = gconf_client_get_int (client, key, &error);

	if (error) {
		g_error_free (error);
		return FALSE;
	}

	return TRUE;
}

static void
setup_gui (MainWindow *window)
{
	MainWindowPriv    *priv;
	GladeXML          *glade;
	GtkCellRenderer   *cell;
	GtkTreeViewColumn *column;
	GtkTreeSelection  *selection;
	GtkSizeGroup      *size_group;
	GtkWidget         *w;
	gboolean           random, repeat;
	GtkAction         *action;
	int                paned_position;
	int                width, height;
	int                x, y;
	GtkAccelGroup     *accel_group;      
	Player            *player;
	GtkWidget         *toolbar;
	GtkToolItem       *item;
	GtkWidget         *hbox;
	int                volume;

	priv = window->priv;

	/* Window icon. */
	gtk_window_set_default_icon_from_file (DATADIR "/pixmaps/jamboree.png",
					       NULL);

	g_signal_connect (window,
			  "delete_event", G_CALLBACK (delete_event_cb),
			  NULL);

	priv->tooltips = gtk_tooltips_new ();

	/* Setup UI manager. */
	priv->manager = gtk_ui_manager_new ();

	accel_group = gtk_ui_manager_get_accel_group (priv->manager);
	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

	priv->action_group = gtk_action_group_new ("MainWindow");
	gtk_action_group_set_translation_domain (priv->action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (priv->action_group,
				      actions,
				      G_N_ELEMENTS (actions),
				      window);
	gtk_action_group_add_toggle_actions (priv->action_group,
					     toggle_actions,
					     G_N_ELEMENTS (toggle_actions),
					     window);
  
	gtk_ui_manager_insert_action_group (priv->manager,
					    priv->action_group,
					    0);
  
	grab_keys (grab_key_cb, window);
  
	priv->genre_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
	priv->artist_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
	priv->album_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

	set_browse_mode (window, TRUE);

	/* Setup player. */
	player = player_get ();
  
	g_signal_connect (player,
			  "eos",
			  G_CALLBACK (player_eos_cb),
			  window);
  
	g_signal_connect (player,
			  "tick",
			  G_CALLBACK (player_tick_cb),
			  window);

	g_signal_connect (player,
			  "state_changed",
			  G_CALLBACK (player_state_changed_cb),
			  window);
  
	g_signal_connect (player,
			  "error", 
			  G_CALLBACK (player_error_cb),
			  window);

	/* Setup glade UI parts. */
	glade = glade_xml_new (DATADIR "/jamboree/jamboree.glade", "top_vbox", NULL);

	priv->top_vbox = glade_xml_get_widget (glade, "top_vbox");
	gtk_container_add (GTK_CONTAINER (window), priv->top_vbox);

	priv->ui_vbox = glade_xml_get_widget (glade, "ui_vbox");
  
	g_signal_connect (priv->manager,
			  "add_widget",
			  G_CALLBACK (manager_add_widget_cb),
			  window);

	gtk_ui_manager_add_ui_from_string (priv->manager, ui, -1, NULL);

	priv->playing_song_label = glade_xml_get_widget (glade, "playing_song_label");
	priv->playing_info_box = glade_xml_get_widget (glade, "playing_info_box");

	priv->search_entry = glade_xml_get_widget (glade, "search_entry");
	g_signal_connect (priv->search_entry,
			  "activate",
			  G_CALLBACK (search_entry_activate_cb),
			  window);
	g_signal_connect (priv->search_entry,
			  "changed",
			  G_CALLBACK (search_entry_changed_cb),
			  window);

	priv->sources_view = GTK_TREE_VIEW (glade_xml_get_widget (glade, "sources_view"));
	sources_view_setup (priv->sources_view, priv->database);

	g_signal_connect (gtk_tree_view_get_selection (priv->sources_view),
			  "changed",
			  G_CALLBACK (sources_view_selection_changed_cb),
			  window);
	
	priv->artist_tree = GTK_TREE_VIEW (glade_xml_get_widget (glade, "artist_tree"));
	priv->artist_store = gtk_list_store_new (ARTIST_NUM_COLS,
						 G_TYPE_STRING,
						 G_TYPE_STRING,
						 G_TYPE_BOOLEAN);
	gtk_tree_view_set_model (priv->artist_tree, GTK_TREE_MODEL (priv->artist_store));

	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (priv->artist_store),
					 ARTIST_COL_NAME,
					 artist_sort_func,
					 NULL,
					 NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (priv->artist_store),
					      ARTIST_COL_NAME,
					      GTK_SORT_ASCENDING);

	hack_tree_view_setup_selection (priv->artist_tree);
  
	selection = gtk_tree_view_get_selection (priv->artist_tree);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);
	g_signal_connect (selection, "changed",
			  G_CALLBACK (artist_selection_changed_cb),
			  window);
	gtk_tree_view_set_enable_search (priv->artist_tree, TRUE);
	g_signal_connect (priv->artist_tree, "row_activated",
			  G_CALLBACK (artist_row_activated_cb),
			  window);

 	cell = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Artist"), 
							   cell, 
							   "text", ARTIST_COL_NAME, 
							   NULL);


	gtk_tree_view_column_set_cell_data_func (column,
						 cell,
						 artist_data_func,
						 NULL,
						 NULL);
	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_append_column (priv->artist_tree, column);

	/* Genre */
	priv->genre_tree = GTK_TREE_VIEW (glade_xml_get_widget (glade, "genre_tree"));
	priv->genre_store = gtk_list_store_new (GENRE_NUM_COLS,
						 G_TYPE_STRING,
						 G_TYPE_STRING,
						 G_TYPE_BOOLEAN);
	gtk_tree_view_set_model (priv->genre_tree, GTK_TREE_MODEL (priv->genre_store));

	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (priv->genre_store),
					 GENRE_COL_NAME,
					 genre_sort_func,
					 NULL,
					 NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (priv->genre_store),
					      GENRE_COL_NAME,
					      GTK_SORT_ASCENDING);

	hack_tree_view_setup_selection (priv->genre_tree);
  
	selection = gtk_tree_view_get_selection (priv->genre_tree);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);
	g_signal_connect (selection, "changed",
			  G_CALLBACK (genre_selection_changed_cb),
			  window);
	gtk_tree_view_set_enable_search (priv->genre_tree, TRUE);
	g_signal_connect (priv->genre_tree, "row_activated",
			  G_CALLBACK (genre_row_activated_cb),
			  window);

 	cell = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Genre"), 
							   cell, 
							   "text", GENRE_COL_NAME, 
							   NULL);


	gtk_tree_view_column_set_cell_data_func (column,
						 cell,
						 genre_data_func,
						 NULL,
						 NULL);
	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_append_column (priv->genre_tree, column);

	/* Album */
	priv->album_tree = GTK_TREE_VIEW (glade_xml_get_widget (glade, "album_tree"));
	priv->album_store = gtk_list_store_new (ALBUM_NUM_COLS,
						G_TYPE_STRING,
						G_TYPE_STRING,
						G_TYPE_BOOLEAN);
	gtk_tree_view_set_model (priv->album_tree, GTK_TREE_MODEL (priv->album_store));

	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (priv->album_store),
					 ALBUM_COL_NAME,
					 album_sort_func,
					 NULL,
					 NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (priv->album_store),
					      ALBUM_COL_NAME,
					      GTK_SORT_ASCENDING);

	hack_tree_view_setup_selection (priv->album_tree);
		
	selection = gtk_tree_view_get_selection (priv->album_tree);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);
	g_signal_connect (selection, "changed",
			  G_CALLBACK (album_selection_changed_cb),
			  window);
	
	gtk_tree_view_set_enable_search (priv->album_tree, TRUE);
	
	g_signal_connect (priv->album_tree, "row_activated",
			  G_CALLBACK (album_row_activated_cb),
			  window);

	cell = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Album"), 
							   cell, 
							   "text", ARTIST_COL_NAME, 
							   NULL);
	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_append_column (priv->album_tree, column);

	w = glade_xml_get_widget (glade, "song_scrolledwindow");
	priv->song_tree = SOURCE_VIEW (source_view_new ());
	source_view_setup_columns (priv->song_tree);
  
	priv->song_model = SOURCE_MODEL (gtk_tree_view_get_model (GTK_TREE_VIEW (priv->song_tree)));

	gtk_container_add (GTK_CONTAINER (w), GTK_WIDGET (priv->song_tree));
	gtk_widget_show (GTK_WIDGET (priv->song_tree));
	
	g_signal_connect (priv->song_tree,
			  "row_activated",
			  G_CALLBACK (song_tree_row_activated_cb),
			  window);

	g_signal_connect (priv->song_tree,
			  "button_press_event",
			  G_CALLBACK (song_tree_row_button_press_event_cb),
			  window);

	/* Setup drag-n-drop. */
	gtk_drag_dest_set (GTK_WIDGET (priv->song_tree),
			   GTK_DEST_DEFAULT_ALL,
			   songs_target_types,
			   G_N_ELEMENTS (songs_target_types),
			   GDK_ACTION_COPY);

	g_signal_connect (priv->song_tree,
			  "drag_data_received",
			  G_CALLBACK (song_tree_drag_data_received_cb),
			  window);

	w = glade_xml_get_widget (glade, "random_button");
	connect_proxy_button (window, w, "Random");
	random = gconf_client_get_bool (gconf_client,
					 "/apps/jamboree/control/shuffle",
					 NULL);
	action = gtk_action_group_get_action (priv->action_group, "Random");
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), random);
	gtk_tooltips_set_tip (priv->tooltips,
			      w, _("Random"), NULL);
	
	repeat = gconf_client_get_bool (gconf_client,
					"/apps/jamboree/control/repeat",
					 NULL);
	w = glade_xml_get_widget (glade, "repeat_button");
	connect_proxy_button (window, w, "Repeat");
	action = gtk_action_group_get_action (priv->action_group, "Repeat");
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), repeat);
	gtk_tooltips_set_tip (priv->tooltips,
			      w, _("Repeat"), NULL);
	
	priv->browse_hbox = glade_xml_get_widget (glade, "browse_hbox");
	priv->browse_paned = glade_xml_get_widget (glade, "browse_paned");
	if (!safe_gconf_client_get_int (gconf_client,
					"/apps/jamboree/ui/browse_paned_position",
					&paned_position)) {
		paned_position = 200;
	}

	gtk_paned_set_position (GTK_PANED (priv->browse_paned), paned_position);

	priv->playlist_paned = glade_xml_get_widget (glade, "playlist_paned");
	if (!safe_gconf_client_get_int (gconf_client,
				      "/apps/jamboree/ui/playlist_paned_position",
				      &paned_position)) {
		paned_position = 140;
	}

	gtk_paned_set_position (GTK_PANED (priv->playlist_paned), paned_position);

	g_signal_connect (priv->playlist_paned,
			  "notify::position",
			  G_CALLBACK (playlist_paned_notify_position_cb),
			  window);
	
	size_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	gtk_size_group_add_widget (size_group, GTK_WIDGET (priv->artist_tree));
	gtk_size_group_add_widget (size_group, GTK_WIDGET (priv->album_tree));
	g_object_unref (size_group);

	priv->info_label = glade_xml_get_widget (glade, "info_label");

	priv->cover_eventbox = glade_xml_get_widget (glade, "cover_eventbox");
	priv->cover_frame = glade_xml_get_widget (glade, "cover_frame");
	priv->cover_image = glade_xml_get_widget (glade, "cover_image");

	g_signal_connect (priv->cover_eventbox,
			  "button-press-event",
			  G_CALLBACK (cover_button_press_event_cb),
			  window);
	g_signal_connect (priv->cover_eventbox,
			  "button-release-event",
			  G_CALLBACK (cover_button_release_event_cb),
			  window);
	
	width = gconf_client_get_int (gconf_client, "/apps/jamboree/ui/width", NULL);
	height = gconf_client_get_int (gconf_client, "/apps/jamboree/ui/height", NULL);

	x = gconf_client_get_int (gconf_client,
				  "/apps/jamboree/ui/x",
				  NULL);
	y = gconf_client_get_int (gconf_client,
				  "/apps/jamboree/ui/y",
				  NULL);

	x = MAX (0, x);
	y = MAX (0, y);
  
	gtk_window_move (GTK_WINDOW (window), x, y);

	if (width == 0) {
		width = 800;
	}
	if (height == 0) {
		height = 600;
	}
	
	gtk_window_set_default_size (GTK_WINDOW (window), width, height);
	gtk_window_resize (GTK_WINDOW (window), width, height);
	
	priv->search_box = glade_xml_get_widget (glade, "search_box");

	g_object_unref (glade);

	update_edit_smart_playlist_sensitivity (window);
	
	gtk_ui_manager_ensure_update (priv->manager);

	toolbar = gtk_ui_manager_get_widget (priv->manager, "ui/Toolbar");

	/* Separators. */
	item = gtk_separator_tool_item_new ();
	gtk_separator_tool_item_set_draw (GTK_SEPARATOR_TOOL_ITEM (item), FALSE);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);
	gtk_widget_show_all (GTK_WIDGET (item));
	item = gtk_separator_tool_item_new ();
	gtk_separator_tool_item_set_draw (GTK_SEPARATOR_TOOL_ITEM (item), FALSE);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);
	gtk_widget_show_all (GTK_WIDGET (item));

	/* Seek slider. */
	priv->tick_label = gtk_label_new ("0:00");
	gtk_label_set_width_chars (GTK_LABEL (priv->tick_label), 5);
	priv->length_label = gtk_label_new ("0:00");
	gtk_label_set_width_chars (GTK_LABEL (priv->length_label), 5);
	priv->seek_scale = gtk_hscale_new_with_range (0, 100, 5);
	gtk_scale_set_draw_value (GTK_SCALE (priv->seek_scale), FALSE);
	gtk_range_set_update_policy (GTK_RANGE (priv->seek_scale), GTK_UPDATE_DISCONTINUOUS);
	gtk_widget_set_size_request (priv->seek_scale, 150, -1);
	
	g_signal_connect (priv->seek_scale,
			  "value_changed",
			  G_CALLBACK (seek_scale_value_changed_cb),
			  window);
	g_signal_connect (priv->seek_scale,
			  "button_press_event",
			  G_CALLBACK (seek_scale_button_press_cb),
			  window);
	g_signal_connect (priv->seek_scale,
			  "button_release_event",
			  G_CALLBACK (seek_scale_button_release_cb),
			  window);
	g_signal_connect (priv->seek_scale,
			  "motion_notify_event",
			  G_CALLBACK (seek_scale_motion_notify_cb),
			  window);

	item = gtk_tool_item_new ();
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), priv->tick_label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), priv->seek_scale, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), priv->length_label, FALSE, FALSE, 6);
	gtk_container_add (GTK_CONTAINER (item), hbox);
	gtk_widget_show_all (GTK_WIDGET (item));
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);

	/* Separator. */
	item = gtk_separator_tool_item_new ();
	gtk_tool_item_set_expand (GTK_TOOL_ITEM (item), TRUE);
	gtk_separator_tool_item_set_draw (GTK_SEPARATOR_TOOL_ITEM (item), FALSE);
	gtk_widget_show_all (GTK_WIDGET (item));
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);
	
	/* Volume button. */
	priv->volume_button = volume_button_new ();

	volume = gconf_client_get_int (gconf_client, 	 
				       GCONF_PATH "/volume", 	 
				       NULL); 	 
  	 
	volume_button_set_volume (VOLUME_BUTTON (priv->volume_button), volume); 	 
 	
	g_signal_connect (priv->volume_button, 	 
			  "volume_changed", 	 
			  G_CALLBACK (volume_changed_cb), 	 
			  window); 	 
	item = gtk_tool_item_new ();
	gtk_container_add (GTK_CONTAINER (item), priv->volume_button);
	gtk_widget_show_all (GTK_WIDGET (item));
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);
}

static void
handle_generic_error (MainWindow *window, Song *song, GError *error)
{
	GtkWidget  *dialog;
	const char *title;
	char       *header;

	if (song) {
		title = song_get_title (song);
	} else {
		title = _("Unknown");
	}
	
	header = g_strdup_printf (_("Could not play the song \"%s\""), title);

	dialog = hig_dialog_new (GTK_WINDOW (window),
				 0,
				 GTK_MESSAGE_ERROR,
				 GTK_BUTTONS_OK,
				 header,
				 error->message);
  
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
  
	g_free (header);
}

static void
handle_resource_busy_error (MainWindow *window, Song *song, GError *error)
{
	GtkWidget  *dialog;
	GtkWidget  *button;
	const char *title;
	char       *header;
	int         response;

	if (song) {
		title = song_get_title (song);
	} else {
		title = _("Unknown");
	}
	
	header = g_strdup_printf (_("Could not play the song \"%s\""), title);

	dialog = hig_dialog_new (GTK_WINDOW (window),
				 0,
				 GTK_MESSAGE_ERROR,
				 GTK_BUTTONS_NONE,
				 header,
				 error->message);

	button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CANCEL);
	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_widget_show (button);
      
	button = gtk_button_new_with_mnemonic (_("_Try again"));
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_OK);
	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_widget_show (button);
	
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
  
	g_free (header);

	if (response == GTK_RESPONSE_OK) {
		main_window_handle_play (window, FALSE);
	}
}

static void
handle_play_error (MainWindow *window, Song *song, GError *error)
{
	if (error) {
		if (g_error_matches (error, PLAYER_ERROR, PLAYER_ERROR_RESOURCE_BUSY)) {
			handle_resource_busy_error (window, song, error);
		} else {			
			handle_generic_error (window, song, error);
		}
	}
}

static void
save_ui_state (MainWindow *window)
{
	MainWindowPriv *priv;
	int             width, height;
	int             x, y;

	priv = window->priv;
 
	gtk_window_get_size (GTK_WINDOW (window), &width, &height);

	gconf_client_set_int (gconf_client,
			      "/apps/jamboree/ui/width", width,
			      NULL);
	gconf_client_set_int (gconf_client,
			      "/apps/jamboree/ui/height", height,
			      NULL);

	gtk_window_get_position (GTK_WINDOW (window), &x, &y);

	if (x >= 0) {
		gconf_client_set_int (gconf_client,
				      "/apps/jamboree/ui/x",
				      x,
				      NULL);
	}
	if (y >= 0) {
		gconf_client_set_int (gconf_client,
				      "/apps/jamboree/ui/y",
				      y,
				      NULL);
	}

	gconf_client_set_int (gconf_client,
			      "/apps/jamboree/ui/browse_paned_position",
			      gtk_paned_get_position (GTK_PANED (priv->browse_paned)),
			      NULL);
  
	gconf_client_set_int (gconf_client,
			      "/apps/jamboree/ui/playlist_paned_position",
			      gtk_paned_get_position (GTK_PANED (priv->playlist_paned)),
			      NULL);

	source_view_store_columns_widths (priv->song_tree);
}

static Expr *
generate_browse_expr (MainWindow *window)
{
	MainWindowPriv   *priv;
	GtkTreeSelection *selection;
	GtkTreeModel     *model;
	GList            *paths;
	GtkTreeIter       iter;
	GList            *l;
	Expr             *e_tmp, *e_genres, *e_artists, *e_albums;
	Expr             *v, *c;
	char             *str;
	gboolean          all;
  
	priv = window->priv;

	e_genres = NULL;
	e_artists = NULL;
	e_albums = NULL;
	
	/* genres */
	selection = gtk_tree_view_get_selection (priv->genre_tree);
	paths = gtk_tree_selection_get_selected_rows (selection, &model);

	for (l = paths; l; l = l->next) {
		if (!gtk_tree_model_get_iter (model, &iter, l->data)) {
			continue;
		}
	  
		gtk_tree_model_get (model, &iter,
				    GENRE_COL_NAME, &str,
				    GENRE_COL_ALL, &all,
				    -1);
	  
		if (all) {
			g_free (str);
			continue;
		}
	  
		v = expr_new_variable (VARIABLE_GENRE);
		c = expr_new_constant (constant_string_new (str));
  
		e_tmp = expr_new_binary (EXPR_OP_EQ, v, c);

		if (e_genres) {
			e_genres = expr_new_binary (EXPR_OP_OR, e_genres, e_tmp);
		} else {
			e_genres = e_tmp;
		}
		
		g_free (str);
	}  
	
	g_list_foreach (paths, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (paths);

	/* artists */
	selection = gtk_tree_view_get_selection (priv->artist_tree);
	paths = gtk_tree_selection_get_selected_rows (selection, &model);

	for (l = paths; l; l = l->next) {
		if (!gtk_tree_model_get_iter (model, &iter, l->data)) {
			continue;
		}
	  
		gtk_tree_model_get (model, &iter,
				    ARTIST_COL_NAME, &str,
				    ARTIST_COL_ALL, &all,
				    -1);
	  
		if (all) {
			g_free (str);
			continue;
		}
	  
		v = expr_new_variable (VARIABLE_ARTIST);
		c = expr_new_constant (constant_string_new (str));
  
		e_tmp = expr_new_binary (EXPR_OP_EQ, v, c);

		if (e_artists) {
			e_artists = expr_new_binary (EXPR_OP_OR, e_artists, e_tmp);
		} else {
			e_artists = e_tmp;
		}
		
		g_free (str);
	}  
	
	g_list_foreach (paths, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (paths);

	/* albums */
	selection = gtk_tree_view_get_selection (priv->album_tree);
	paths = gtk_tree_selection_get_selected_rows (selection, &model);
	
	for (l = paths; l; l = l->next) {
		if (!gtk_tree_model_get_iter (model, &iter, l->data)) {
			continue;
		}
		
		gtk_tree_model_get (model, &iter,
				    ALBUM_COL_NAME, &str,
				    ALBUM_COL_ALL, &all,
				    -1);
		
		if (all) {
			g_free (str);
			continue;
		}

		v = expr_new_variable (VARIABLE_ALBUM);
		c = expr_new_constant (constant_string_new (str));
		
		e_tmp = expr_new_binary (EXPR_OP_EQ, v, c);
	  
		if (e_albums) {
			e_albums = expr_new_binary (EXPR_OP_OR, e_albums, e_tmp);
		} else {
			e_albums = e_tmp;
		}
		
		g_free (str);
	}
	
	g_list_foreach (paths, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (paths);

	if (e_artists && e_albums) {
		e_tmp = expr_new_binary (EXPR_OP_AND, e_artists, e_albums);
	}
	else if (e_artists) {
		e_tmp = e_artists;
	}
	else if (e_albums) {
		e_tmp = e_albums;
	} else {
		e_tmp = NULL;
	}

	if (e_genres) {
	  if (e_tmp) {
 	    e_tmp = expr_new_binary (EXPR_OP_AND, e_tmp, e_genres);
	  } else {
	    e_tmp = e_genres;
	  }
	}

	return e_tmp;
}

static void
update_songs (MainWindow *window,
	      gboolean update_artists,
	      gboolean update_albums,
	      gboolean update_genres)
{
	MainWindowPriv *priv;

	priv = window->priv;

	g_hash_table_foreach (priv->genre_hash, foreach_reset_count_cb, NULL);
	g_hash_table_foreach (priv->artist_hash, foreach_reset_count_cb, NULL);
	g_hash_table_foreach (priv->album_hash, foreach_reset_count_cb, NULL);

	if (update_genres) {
		reset_genre_view (window);
	}
	
	if (update_artists) {
		reset_artist_view (window);
	}

	if (update_albums) {
		reset_album_view (window);
	}

	perform_search (window, FALSE);
}

static void
fill_songs_last_burst (MainWindow *window)
{
	MainWindowPriv *priv;
    
	priv = window->priv;

	if (player_get_state () != PLAYING_STATE_STOPPED) {
		source_model_set_current (priv->song_model, NULL);
	} else {
		source_model_first (priv->song_model);
	}

	if (priv->browse_mode) {
		update_artist_album_count (window);
	}
	
	update_songs_info (window);
	update_playing_info (window);
}

static void
fill_songs_idle_destroy_notify (MainWindow *window)
{
	MainWindowPriv *priv;

	priv = window->priv;

	/* FIXME: unref songs */
	g_list_free (priv->fill_songs_list);
	priv->fill_songs_list = NULL;

	set_cursor_default (window);
}

static void
fill_songs_idle_cancel (MainWindow *window)
{
	MainWindowPriv *priv;

	priv = window->priv;

	if (priv->fill_songs_idle_id) {
		g_source_remove (priv->fill_songs_idle_id);
		priv->fill_songs_idle_id = 0;
	}
}

static gboolean
fill_songs_idle_cb (MainWindow *window)
{
	MainWindowPriv *priv;
	GList          *songs, *l;
	GList          *prev;
	int             i;

	priv = window->priv;

	songs = priv->fill_songs_list;

	for (l = songs, i = 0; l && i < BURST; l = l->next, i++) {
		add_song (window, l->data);
	}

	/* Free the part of the list that has been handled. */
	if (l && l->prev) {
		prev = l->prev;
		prev->next = NULL;
		l->prev = NULL;

		/* FIXME: unref inserted songs */
		g_list_free (songs);
	}
	
	priv->fill_songs_list = l;
	
	if (priv->fill_songs_list) {
		return TRUE;
	} else {
		fill_songs_last_burst (window);
		return FALSE;
	}
}

typedef struct {
	SongSortId   sort_id;
	GtkSortType  sort_type;	
} SortData;

static int
fill_compare_func (Song *song_a, Song *song_b, SortData *data)
{
	return song_compare_func (song_a, song_b, data->sort_id, data->sort_type);
}

static void
fill_songs (MainWindow *window, GList *songs)
{
	MainWindowPriv *priv;
	
	priv = window->priv;

	if (priv->fill_songs_idle_id) {
		fill_songs_idle_cancel (window);
	}

	priv->fill_songs_list = songs;

	if (!songs) {
		fill_songs_last_burst (window);
		return;
	}

	set_cursor_watch (window);
		
	/* Higher than drawing which is done in HIGH_IDLE + 20. */
	priv->fill_songs_idle_id = g_idle_add_full (G_PRIORITY_HIGH_IDLE + 15, 
						    (GSourceFunc) fill_songs_idle_cb,
						    window,
						    (GDestroyNotify) fill_songs_idle_destroy_notify);
}

static void
perform_search (MainWindow *window, gboolean play_first)
{
	MainWindowPriv *priv;
	Source         *list;
	const char     *keyword;
	GList          *songs, *l;
	int             i;
	SortData        data;
	
	priv = window->priv;

	list = sources_view_get_selected (priv->sources_view);
	if (!list) {
		return;
	}
	
	keyword = gtk_entry_get_text (GTK_ENTRY (priv->search_entry));
	if (keyword[0] != '\0') {
		source_set_search_expr (list, keyword);
	} else {
		source_set_search_expr (list, NULL);
	}		

	if (priv->browse_mode) {
		source_set_browse_expr (list, generate_browse_expr (window));
	} else {
		source_set_browse_expr (list, NULL);
	}
	
	priv->total_num = 0;
	priv->total_length = 0;
	priv->total_size = 0;
  
	songs = source_get_songs_filtered (list);

	source_model_get_sorting (priv->song_model, &data.sort_id, &data.sort_type);
	songs = g_list_sort_with_data (songs,
				       (GCompareDataFunc) fill_compare_func,
				       &data);
		
	/*source_model_remove_delta (priv->song_model, songs);*/
	source_model_clear (priv->song_model);

	if (songs && play_first) {
		main_window_handle_play_song (window, songs->data);
	}
	
	for (l = songs, i = 0; l && i < FIRST_BURST; l = l->next, i++) {
		add_song (window, l->data);
	}

	fill_songs (window, l);
}

static void
set_cursor_watch (MainWindow *window)
{
	MainWindowPriv *priv;
	GdkPixmap      *cursor, *mask;

	priv = window->priv;
	
	if (!GTK_WIDGET (window)->window) {
		return;
	}
	
	if (!priv->watch_arrow_cursor) {
		cursor = gdk_bitmap_create_from_data (NULL, cursor_bits, 32, 32);
		mask = gdk_bitmap_create_from_data (NULL, cursor_mask_bits, 32, 32);
		priv->watch_arrow_cursor =  gdk_cursor_new_from_pixmap (cursor, mask,
									&GTK_WIDGET (window)->style->black,
									&GTK_WIDGET (window)->style->white,
									2, 2);
		g_object_unref (cursor);
		g_object_unref (mask);
	}
	
	gdk_window_set_cursor (GTK_WIDGET (window)->window, priv->watch_arrow_cursor);
}

static void
set_cursor_default (MainWindow *window)
{
	if (!GTK_WIDGET (window)->window) {
		return;
	}

	gdk_window_set_cursor (GTK_WIDGET (window)->window, NULL);
}

static gboolean
window_get_is_on_current_workspace (GtkWindow *window)
{
	GdkWindow *gdk_window;

	gdk_window = GTK_WIDGET (window)->window;
	if (gdk_window) {
		return !(gdk_window_get_state (gdk_window) &
			 GDK_WINDOW_STATE_ICONIFIED);
	} else {
		return FALSE;
	}
}

/* Checks if the window is visible as in visible on the current workspace. */
gboolean
main_window_get_is_visible (MainWindow *window)
{
	gboolean visible;

	g_object_get (window,
		      "visible", &visible,
		      NULL);

	return visible && window_get_is_on_current_workspace (
		GTK_WINDOW (window));
}

/* Takes care of moving the window to the current workspace. */
void
main_window_present (MainWindow *window)
{
	gboolean visible;
	gboolean on_current;
	gboolean show;

	/* There are three cases: hidden, visible, visible on another
	 * workspace.
	 */

	g_object_get (window,
		      "visible", &visible,
		      NULL);

	on_current = window_get_is_on_current_workspace (
		GTK_WINDOW (window));

	if (!visible) {
		show = TRUE;
	}
	else if (on_current) {
		show = FALSE;
	} else {
		/* Hide it so present brings it to the current workspace. */
		gtk_widget_hide (GTK_WIDGET (window));
		show = TRUE;
	}

	if (show) {
		gtk_window_present (GTK_WINDOW (window));
	}
}

void
main_window_toggle_visibility (MainWindow *window)
{
	MainWindowPriv *priv;
	gboolean        visible;

	priv = window->priv;

	visible = main_window_get_is_visible (window);

	if (visible) {
		gtk_widget_hide (GTK_WIDGET (window));
	} else {
		main_window_present (window);
	}
}

void
main_window_select_all (MainWindow *window)
{
	select_all (window);
}

void
main_window_select_playing_artist (MainWindow *window)
{
	Song *song;

	song = main_window_get_current_song (window);
	if (song) {
		select_artist (window, song_get_artist_folded (song));
	}
}

void
main_window_select_playing_album (MainWindow *window)
{
	Song *song;

	song = main_window_get_current_song (window);
	if (song) {
		select_album (window, song_get_album_folded (song));
	}
}

Song *
main_window_get_prev_song (MainWindow *window, Song *song)
{
	MainWindowPriv *priv;
	GtkTreeIter     iter;

	priv = window->priv;
	
	if (!source_model_song_get_iter (priv->song_model, song, &iter)) {
		return NULL;
	}

	if (!source_model_iter_prev (GTK_TREE_MODEL (priv->song_model), &iter)) {
		return NULL;
	}
	
	return source_model_get_song (priv->song_model, &iter);
}

Song *
main_window_get_next_song (MainWindow *window, Song *song)
{
	MainWindowPriv *priv;
	GtkTreeIter     iter;

	priv = window->priv;
	
	if (!source_model_song_get_iter (priv->song_model, song, &iter)) {
		return NULL;
	}
	
	if (!gtk_tree_model_iter_next (GTK_TREE_MODEL (priv->song_model), &iter)) {
		return NULL;
	}
	
	return source_model_get_song (priv->song_model, &iter);
}

void
main_window_set_random (MainWindow *window, gboolean random)
{
        MainWindowPriv *priv;
        GtkAction      *action;

        priv = window->priv;
        
	action = gtk_action_group_get_action (priv->action_group, "Random");
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), random);
}

gboolean
main_window_get_random (MainWindow *window)
{
        MainWindowPriv *priv;
        GtkAction      *action;

        priv = window->priv;
        
	action = gtk_action_group_get_action (priv->action_group, "Random");
	return gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));
}

gboolean
main_window_get_repeat (MainWindow *window)
{
        MainWindowPriv *priv;
        GtkAction      *action;

        priv = window->priv;
        
	action = gtk_action_group_get_action (priv->action_group, "Repeat");
	return gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));
}

void
main_window_set_repeat (MainWindow *window, gboolean repeat)
{
        MainWindowPriv *priv;
        GtkAction      *action;

        priv = window->priv;
        
	action = gtk_action_group_get_action (priv->action_group, "Repeat");
	gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), repeat);
}

Source *
main_window_get_database (MainWindow *window)
{
	MainWindowPriv *priv;

        priv = window->priv;
        
	return priv->database;
}

void
main_window_set_selected_source (MainWindow *window,
				 Source     *source)
{
	MainWindowPriv *priv;

        priv = window->priv;
        
	sources_view_set_selected (GTK_TREE_VIEW (priv->sources_view),
				   priv->database);
}

