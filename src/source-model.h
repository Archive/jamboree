/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SOURCE_MODEL_H__
#define __SOURCE_MODEL_H__

#include <gtk/gtktreemodel.h>
#include "gsequence.h"
#include "song.h"
#include "types.h"


#define TYPE_SOURCE_MODEL		 (source_model_get_type ())
#define SOURCE_MODEL(obj)		 (GTK_CHECK_CAST ((obj), TYPE_SOURCE_MODEL, SourceModel))
#define SOURCE_MODEL_CLASS(klass)	 (GTK_CHECK_CLASS_CAST ((klass), TYPE_SOURCE_MODEL, SourceModelClass))
#define IS_SOURCE_MODEL(obj)	         (GTK_CHECK_TYPE ((obj), TYPE_SOURCE_MODEL))
#define IS_SOURCE_MODEL_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((obj), TYPE_SOURCE_MODEL))
#define SOURCE_MODEL_GET_CLASS(obj)   (GTK_CHECK_GET_CLASS ((obj), TYPE_SOURCE_MODEL, SourceModelClass))

typedef struct _SourceModel SourceModel;
typedef struct _SourceModelClass SourceModelClass;

struct _SourceModel {
	GObject      parent;
  
	int          stamp;

	SongSortId   sort_id;
	GtkSortType  sort_type;

	gboolean     random;

	int         *indices;
	int          current_index;
  
	GSequence   *songs;
	GHashTable  *reverse_map;

	int          total_duration;
	guint64      total_size;
};

struct _SourceModelClass {
	GObjectClass parent_class;
};


GType         source_model_get_type      (void) G_GNUC_CONST;
GtkTreeModel *source_model_new           (void);
gboolean      source_model_add           (SourceModel *model,
					  Song        *song);
gboolean      source_model_remove        (SourceModel *model,
					  Song        *song);
void          source_model_clear         (SourceModel *model);
void          source_model_set_sorting   (SourceModel *model,
					  SongSortId   id,
					  GtkSortType  type);
void          source_model_get_sorting   (SourceModel *model,
					  SongSortId  *id,
					  GtkSortType *type);
void          source_model_sort          (SourceModel *model);
gboolean      source_model_song_get_iter (SourceModel *model,
					  Song        *song,
					  GtkTreeIter *iter);
Song *        source_model_get_song      (SourceModel *model,
					  GtkTreeIter *iter);
GList *       source_model_get_songs     (SourceModel *model);
void          source_model_remove_delta  (SourceModel *model,
					  GList       *songs);
Song *        source_model_get_current   (SourceModel *model);
gboolean      source_model_set_current   (SourceModel *model,
					  Song        *song);
Song *        source_model_next          (SourceModel *model);
Song *        source_model_prev          (SourceModel *model);
Song *        source_model_first         (SourceModel *model);
void          source_model_set_random    (SourceModel *model,
					  gboolean     random);
void          source_model_reshuffle     (SourceModel *model);
gboolean      source_model_has_next      (SourceModel *model);
gboolean      source_model_has_prev      (SourceModel *model);
gboolean      source_model_iter_prev     (GtkTreeModel *tree_model,
					  GtkTreeIter  *iter);

#endif /* __SOURCE_MODEL_H__ */

