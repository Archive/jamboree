/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SOURCE_H__
#define __SOURCE_H__

#include <glib-object.h>
#include <gdk/gdkpixbuf.h>
#include "song.h"
#include "expr.h"

#define TYPE_SOURCE            (source_get_type ())
#define SOURCE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SOURCE, Source))
#define SOURCE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SOURCE, SourceClass))
#define IS_SOURCE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SOURCE))
#define IS_SOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SOURCE))
#define SOURCE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SOURCE, SourceClass))


typedef struct _Source      Source;
typedef struct _SourceClass SourceClass;
typedef struct _SourcePriv  SourcePriv;

struct _Source {
	GObject     parent;
	SourcePriv *priv;
};

struct _SourceClass {
	GObjectClass parent_class;

	/* Signals */
	void (*song_changed)             (Source     *source, Song *song, int tags);
	void (*song_added)               (Source     *source, Song *song);
	void (*song_removed)             (Source     *source, Song *song);

	void (*songs_changed)            (Source     *source);

	/* Vtable */ 
	Song *    (*lookup_song)         (Source     *source, const char *path);
	GList *   (*get_songs)           (Source     *source);
	GList *   (*get_songs_filtered)  (Source     *source);
	
	gboolean  (*add_song)            (Source     *source, Song *song);
	gboolean  (*remove_song)         (Source     *source, Song *song);
	gboolean  (*update_song)         (Source     *source, Song *song);

	const GdkPixbuf * (*get_pixbuf)  (Source     *source);
	const char *      (*get_name)    (Source     *source);
	const char *      (*set_name)    (Source     *source, const char *name);

	gboolean  (*get_is_editable)     (Source    *source);

	char *    (*to_xml)              (Source     *source);
};


GType            source_get_type           (void) G_GNUC_CONST;
Song *           source_lookup             (Source     *source,
					    const char *path);
gboolean         source_add_song           (Source     *source,
					    Song       *song);
gboolean         source_remove_song        (Source     *source,
					    Song       *song);
gboolean         source_update_song        (Source     *source,
					    Song       *song);
gboolean         source_song_set_title     (Source     *source,
					    Song       *song,
					    const char *str);
gboolean         source_song_set_album     (Source     *source,
					    Song       *song,
					    const char *str);
gboolean         source_song_set_artist    (Source     *source,
					    Song       *song,
					    const char *str);
gboolean         source_song_set_genre     (Source     *source,
					    Song       *song,
					    const char *str);
gboolean         source_song_set_duration  (Source     *source,
					    Song       *song,
					    guint32     duration);
gboolean         source_song_set_year      (Source     *list,
					    Song       *song,
					    int         year);
void             source_song_mark_played   (Source     *source,
					    Song       *song);
GList *          source_get_songs          (Source     *source);
GList *          source_get_songs_filtered (Source     *source);
void             source_song_changed       (Source     *source,
					    Song       *song,
					    int         tags);
void             source_song_added         (Source     *source,
					    Song       *song);
void             source_song_removed       (Source     *source,
					    Song       *song);
void             source_songs_changed      (Source     *source);
void             source_set_browse_expr    (Source     *source,
					    Expr       *expr);
void             source_set_search_expr    (Source     *source,
					    const char *keyword);
Expr *           source_get_browse_expr    (Source     *source);
Expr *           source_get_search_expr    (Source     *source);
GList *          source_get_filtered_list  (Source     *source);
const GdkPixbuf *source_get_pixbuf         (Source     *source);
const char *     source_get_name           (Source     *source);
gboolean         source_get_is_editable    (Source     *source);
void             source_set_name           (Source     *source,
					    const char *name);
char *           source_to_xml             (Source     *source);


#endif /* __SOURCE_H__ */
