/* -*- mode: C; c-file-style: "gnu" -*- */
/*
 * Copyright (C) 2003 Richard Hult <richard@imendio.com>
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>

#define GCONF_PATH "/apps/jamboree"

extern GConfClient *gconf_client;


typedef enum {
	XF86AUDIO_NONE = 0,
	
	XF86AUDIO_PLAY,
	XF86AUDIO_PAUSE,
	XF86AUDIO_STOP,
	XF86AUDIO_NEXT,
	XF86AUDIO_PREV,
	XF86AUDIO_MAX
} XF86Key;

typedef void (* KeyGrabFunction) (XF86Key  key,
				  gpointer user_data);


GtkWidget *hig_dialog_new                 (GtkWindow       *parent,
					   GtkDialogFlags   flags,
					   GtkMessageType   type,
					   GtkButtonsType   buttons,
					   const char      *header,
					   const char      *messagefmt,
					   ...);
void       grab_keys                      (KeyGrabFunction  function,
					   gpointer         user_data);
gchar *    format_file_size_for_display   (long long        size);
gchar *    format_time_for_display        (guint            time);
char *     format_length_for_display      (guint            time);

void       hack_tree_view_setup_selection (GtkTreeView     *tree);


#endif /* _UTILS_H__ */
