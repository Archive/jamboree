/*
 * Copyright (C) 2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include <glib/gi18n.h>
#include "jamboree-marshal.h"
#include "source.h"
#include "song-private.h"


enum {
	SONG_ADDED,
	SONG_REMOVED,
	SONG_CHANGED,
	SONGS_CHANGED,
	LAST_SIGNAL
};

struct _SourcePriv {
	char       *name;

	GdkPixbuf  *pixbuf;

	Expr       *browse_expr;
	Expr       *search_expr;
};

static void   source_finalize    (GObject *object);
static GList *get_songs_filtered (Source  *source);


static guint source_signals[LAST_SIGNAL] = { 0 };
static GdkPixbuf *default_pixbuf;

#define SOURCE_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_SOURCE, SourcePriv))
G_DEFINE_TYPE (Source, source, G_TYPE_OBJECT);


static void
source_class_init (SourceClass *klass)
{
        GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
        object_class->finalize = source_finalize;

	klass->get_songs_filtered = get_songs_filtered;
	
	source_signals[SONG_ADDED] =
		g_signal_new ("song_added",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (SourceClass, song_added),
			      NULL, NULL,
			      jamboree_marshal_VOID__BOXED,
			      G_TYPE_NONE, 1, TYPE_SONG);
	
	source_signals[SONG_REMOVED] =
		g_signal_new ("song_removed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (SourceClass, song_removed),
			      NULL, NULL,
			      jamboree_marshal_VOID__BOXED,
			      G_TYPE_NONE, 1, TYPE_SONG);

	source_signals[SONG_CHANGED] =
		g_signal_new ("song_changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (SourceClass, song_changed),
			      NULL, NULL,
			      jamboree_marshal_VOID__BOXED_INT,
			      G_TYPE_NONE, 2, TYPE_SONG, G_TYPE_INT);

	source_signals[SONGS_CHANGED] =
		g_signal_new ("songs_changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (SourceClass, songs_changed),
			      NULL, NULL,
			      jamboree_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	g_type_class_add_private (klass, sizeof (SourcePriv));

	default_pixbuf = gdk_pixbuf_new_from_file (
		DATADIR "/jamboree/jamboree-playlist-regular.png", NULL);
}

static void
source_init (Source *source)
{
	source->priv = SOURCE_GET_PRIVATE (source);
}

static void
source_finalize (GObject *object)
{
	Source *source;

	source = SOURCE (object);

	if (G_OBJECT_CLASS (source_parent_class)->finalize) {
		G_OBJECT_CLASS (source_parent_class)->finalize (object);
	}
}

void
source_song_changed (Source *source, Song *song, int tags)
{
	g_signal_emit (source, source_signals[SONG_CHANGED], 0, song, tags);
}

void
source_songs_changed (Source *source)
{
	g_signal_emit (source, source_signals[SONGS_CHANGED], 0);
}

void
source_song_added (Source *source, Song *song)
{
	g_signal_emit (source, source_signals[SONG_ADDED], 0, song);
}

void
source_song_removed (Source *source, Song *song)
{
	g_signal_emit (source, source_signals[SONG_REMOVED], 0, song);
}

Song *
source_lookup (Source *source, const char *path)
{
	g_return_val_if_fail (IS_SOURCE (source), NULL);
	g_return_val_if_fail (path != NULL, NULL);
	
	if (SOURCE_GET_CLASS (source)->lookup_song) {
		return SOURCE_GET_CLASS (source)->lookup_song (source, path);
	}
	
	return NULL;
}

gboolean
source_add_song (Source *source, Song *song)
{
	g_return_val_if_fail (IS_SOURCE (source), FALSE);
	g_return_val_if_fail (song != NULL, FALSE);
	
	if (SOURCE_GET_CLASS (source)->add_song) {
		if (SOURCE_GET_CLASS (source)->add_song (source, song)) {
			source_song_added (source, song);
			return TRUE;
		}
	}
	
	return FALSE;
}

gboolean
source_remove_song (Source *source, Song *song)
{
	g_return_val_if_fail (IS_SOURCE (source), FALSE);
	g_return_val_if_fail (song != NULL, FALSE);
	
	if (SOURCE_GET_CLASS (source)->remove_song) {
		if (SOURCE_GET_CLASS (source)->remove_song (source, song)) {
			source_song_removed (source, song);
			return TRUE;
		}
	}

	return FALSE;
}

gboolean
source_update_song (Source *list, Song *song)
{
	g_return_val_if_fail (IS_SOURCE (list), FALSE);
	g_return_val_if_fail (song != NULL, FALSE);
	
	if (SOURCE_GET_CLASS (list)->update_song) {
		if (SOURCE_GET_CLASS (list)->update_song (list, song)) {
			/* Hm, we don't want to emit changed here. */
			/*source_song_changed (list, song, SONG_TAG_ALL);*/
			return TRUE;
		}
	}
	
	return FALSE;
}

GList *
source_get_songs (Source *list)
{
	g_return_val_if_fail (IS_SOURCE (list), NULL);

	if (SOURCE_GET_CLASS (list)->get_songs) {
		return SOURCE_GET_CLASS (list)->get_songs (list);
	}
	
	return NULL;
}

GList *
source_get_songs_filtered (Source *source)
{
	g_return_val_if_fail (IS_SOURCE (source), NULL);

	if (SOURCE_GET_CLASS (source)->get_songs_filtered) {
		return SOURCE_GET_CLASS (source)->get_songs_filtered (source);
	}

	return NULL;
}

gboolean
source_song_set_title (Source     *list,
		       Song       *song,
		       const char *str)
{
	if (_song_set_title (song, g_strdup (str))) {
		source_song_changed (list, song, SONG_TAG_TITLE);
		return TRUE;
	}

	return FALSE;
}

gboolean
source_song_set_album (Source     *list,
		       Song       *song,
		       const char *str)
{
	if (_song_set_album_const (song, str)) {
		source_song_changed (list, song, SONG_TAG_ALBUM);
		return TRUE;
	}

	return FALSE;
}

gboolean
source_song_set_artist (Source     *list,
			Song       *song,
			const char *str)
{
	if (_song_set_artist_const (song, str)) {
		source_song_changed (list, song, SONG_TAG_ARTIST);
		return TRUE;
	}

	return FALSE;
}

gboolean
source_song_set_genre (Source     *list,
		       Song       *song,
		       const char *str)
{
	if (_song_set_genre_const (song, str)) {
		source_song_changed (list, song, SONG_TAG_GENRE);
		return TRUE;
	}

	return FALSE;
}

gboolean
source_song_set_duration (Source   *list,
			  Song     *song,
			  guint32   duration)
{
	if (_song_set_duration (song, duration)) {
		source_song_changed (list, song, SONG_TAG_DURATION);
		return TRUE;
	}

	return FALSE;
}

gboolean
source_song_set_year (Source   *list,
		      Song     *song,
		      int       year)
{
	if (_song_set_year (song, year)) {
		source_song_changed (list, song, SONG_TAG_YEAR);
		return TRUE;
	}

	return FALSE;
}

void
source_song_mark_played (Source *source, Song *song)
{
	_song_set_playcount (song, song_get_playcount (song) + 1);
	_song_set_time_played (song, time (NULL));

	/* FIXME: Should we have tags for those? */
	source_song_changed (source, song, SONG_TAG_ALL);
}
		  
#if 0
void
source_song_set (Source   *source,
		 Song     *song,
		 SongTag   first_tag,
		 ...)
{
	va_list  args;
	SongTag  tag;
	int      tags;
	char    *str;
	guint    ui32;
	
	va_start (args, first_tag);

	tag = first_tag;
	tags = 0;

	while (tag != SONG_TAG_INVALID) {
		tags |= tag;

		switch (tag) {
		case SONG_TAG_TITLE:
			str = va_arg (args, char *);
			_song_set_title (song, str);
			break;

		case SONG_TAG_ALBUM:
			str = va_arg (args, char *);
			_song_set_album (song, str);
			break;

		case SONG_TAG_ARTIST:
			str = va_arg (args, char *);
			_song_set_artist (song, str);
			break;

		case SONG_TAG_GENRE:
 			str = va_arg (args, char *);
			_song_set_genre (song, str);
			break;


		case SONG_TAG_DURATION:
 			ui32 = va_arg (args, guint32);
			_song_set_duration (song, ui32);
			break;
			
			/* FIXME: More here... */

		default:
			g_assert_not_reached ();
		}

		tag = va_arg (args, int);
	}
	
	va_end (args);
	
	source_song_changed (source, song, tags);
}
#endif

void
source_set_browse_expr (Source *source, Expr *expr)
{
	SourcePriv *priv;

	priv = source->priv;

	if (priv->browse_expr) {
		expr_free (priv->browse_expr);
	}
	
	priv->browse_expr = expr;
}

Expr *
source_get_browse_expr (Source *source)
{
	SourcePriv *priv;

	priv = source->priv;

	return priv->browse_expr;
}

void
source_set_search_expr (Source *source, const char *keyword)
{
	SourcePriv *priv;

	priv = source->priv;

	if (priv->search_expr) {
		expr_free (priv->search_expr);
	}

	if (keyword) {
		priv->search_expr = expr_new_simple (keyword, keyword, keyword);
	} else {
		priv->search_expr = NULL;
	}
}

Expr *
source_get_search_expr (Source *source)
{
	SourcePriv *priv;

	priv = source->priv;

	return priv->search_expr;
}

const GdkPixbuf *
source_get_pixbuf (Source *source)
{
	g_return_val_if_fail (IS_SOURCE (source), NULL);

	if (SOURCE_GET_CLASS (source)->get_pixbuf) {
		return SOURCE_GET_CLASS (source)->get_pixbuf (source);
	}
	
	return default_pixbuf;
}

const char *
source_get_name (Source *source)
{
	g_return_val_if_fail (IS_SOURCE (source), NULL);

	if (SOURCE_GET_CLASS (source)->get_name) {
		return SOURCE_GET_CLASS (source)->get_name (source);
	}
	
	return source->priv->name;
}

gboolean
source_get_is_editable (Source *source)
{
	g_return_val_if_fail (IS_SOURCE (source), FALSE);

	if (SOURCE_GET_CLASS (source)->get_is_editable) {
		return SOURCE_GET_CLASS (source)->get_is_editable (source);
	}

	return FALSE;
}

void
source_set_name (Source *source, const char *name)
{
	g_return_if_fail (IS_SOURCE (source));

	if (!source_get_is_editable (source)) {
		g_warning ("Trying to edit non-editable source.");
	}

	if (SOURCE_GET_CLASS (source)->set_name) {
		SOURCE_GET_CLASS (source)->set_name (source, name);
		return;
	}

	g_free (source->priv->name);
	source->priv->name = g_strdup (name);
}

char *
source_to_xml (Source *source)
{
	g_return_val_if_fail (IS_SOURCE (source), NULL);

	if (SOURCE_GET_CLASS (source)->to_xml) {
		return SOURCE_GET_CLASS (source)->to_xml (source);
	}

	return NULL;
}

static GList *
get_songs_filtered (Source *source)
{
	SourcePriv *priv;
	GList      *songs, *l;
	GList      *filtered;
	Song       *song;
	
	priv = source->priv;
	
	songs = source_get_songs (source);

	filtered = NULL;
	for (l = songs; l; l = l->next) {
		song = l->data;
		
		if ((!priv->browse_expr || expr_evaluate (priv->browse_expr, song)) &&
		    (!priv->search_expr || expr_evaluate (priv->search_expr, song))) {
			filtered = g_list_prepend (filtered, song);
		}
	}

	return g_list_reverse (filtered);
}


