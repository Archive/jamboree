/*
 * Copyright (C) 2004 Imendio HB
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SOURCE_PLAYLIST_H__
#define __SOURCE_PLAYLIST_H__

#include "source-proxy.h"

#define TYPE_SOURCE_PLAYLIST            (source_playlist_get_type ())
#define SOURCE_PLAYLIST(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SOURCE_PLAYLIST, SourcePlaylist))
#define SOURCE_PLAYLIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SOURCE_PLAYLIST, SourcePlaylistClass))
#define IS_SOURCE_PLAYLIST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SOURCE_PLAYLIST))
#define IS_SOURCE_PLAYLIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SOURCE_PLAYLIST))
#define SOURCE_PLAYLIST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SOURCE_PLAYLIST, SourcePlaylistClass))


typedef struct _SourcePlaylist      SourcePlaylist;
typedef struct _SourcePlaylistClass SourcePlaylistClass;
typedef struct _SourcePlaylistPriv  SourcePlaylistPriv;

struct _SourcePlaylist {
	SourceProxy         parent;
	SourcePlaylistPriv *priv;
};

struct _SourcePlaylistClass {
	SourceProxyClass parent_class;
};

GType       source_playlist_get_type   (void) G_GNUC_CONST;
Source *    source_playlist_new        (Source             *source,
					const char         *name,
					int                 id);
void        source_playlist_set_id     (SourcePlaylist     *playlist,
					int                 id);
int         source_playlist_get_id     (SourcePlaylist     *playlist);


#endif /* __SOURCE_PLAYLIST_H__ */
