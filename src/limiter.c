/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include "limiter.h"
#include "song.h"

struct _Limiter {
	LimiterVariable variable;
	LimiterUnit     unit;
	long            value;
};


Limiter *
limiter_new (LimiterVariable variable,
	     LimiterUnit     unit,
	     long            value)
{
	Limiter *limiter;
	
	limiter = g_new0 (Limiter, 1);

	limiter->variable = variable;
	limiter->unit = unit;
	limiter->value = value;
	
	return limiter;
}

void
limiter_free (Limiter *limiter)
{
	g_free (limiter);
}

static int
compare_int (int a, int b)
{
	if (a < b) {
		return -1;
	}
	else if (a > b) {
		return 1;
	} else {
		return 0;
	}
}

static int
limit_compare_func (Song    *song_a,
		    Song    *song_b,
		    Limiter *limiter)
{
	switch (limiter->variable) {
	case LIMITER_VARIABLE_DATE_ADDED:
		return -compare_int (song_get_time_added (song_a),
				     song_get_time_added (song_b));
      
	case LIMITER_VARIABLE_MOST_RECENTLY_PLAYED:
		return -compare_int (song_get_time_played (song_a),
				     song_get_time_played (song_b));
      
	case LIMITER_VARIABLE_LEAST_RECENTLY_PLAYED:
		return compare_int (song_get_time_played (song_a),
				    song_get_time_played (song_b));
      
	case LIMITER_VARIABLE_RATING:
		return compare_int (song_get_rating (song_a),
				    song_get_rating (song_b));

	case LIMITER_VARIABLE_RANDOM:
		return compare_int (song_get_random (song_a),
				    song_get_random (song_b));

	case LIMITER_VARIABLE_LAST_PLAYED:
		return compare_int (song_get_time_played (song_a),
				    song_get_time_played (song_b));

	case LIMITER_VARIABLE_PLAYCOUNT:
		return -compare_int (song_get_playcount (song_a),
				     song_get_playcount (song_b));

	case LIMITER_VARIABLE_QUALITY:
		/* FIXME: quality compare is not implemented */
		return 0;

	case LIMITER_VARIABLE_YEAR:
		return compare_int (song_get_year (song_a),
				    song_get_year (song_b));

	case LIMITER_VARIABLE_NONE:
		/*g_warning ("Must select on something.");*/
		return 0;
	}

	return 0;
}

GList *
limiter_apply_limits (Limiter *limiter, GList *songs)
{
	GList *l;
	Song  *song;
	long   value;
	
	g_return_val_if_fail (limiter != NULL, NULL);
	
	if (limiter->unit == LIMITER_UNIT_NONE) {
		return songs;
	}
	
	songs = g_list_sort_with_data (songs,
				       (GCompareDataFunc) limit_compare_func,
				       limiter);
	
	value = 0;
	for (l = songs; l; l = l->next) {
		song = l->data;
		
		switch (limiter->unit) {
		case LIMITER_UNIT_SONGS:
			value++;
			break;

		case LIMITER_UNIT_MINUTES:
		case LIMITER_UNIT_HOURS:
			value += song_get_duration (song);
			break;

		case LIMITER_UNIT_MB:
		case LIMITER_UNIT_GB:
			value += song_get_size (song);
			break;

		case LIMITER_UNIT_NONE:
			g_assert_not_reached ();
		}
		
		if (value > limiter->value) {
			GList *prev;

			/* Remove the rest of the list. */
			prev = l->prev;
			if (prev) {
				prev->next = NULL;
			}

			l->prev = NULL;
			g_list_free (l);
			break;
		}
	}

	return songs;
}

void
limiter_set_variable (Limiter         *limiter,
		      LimiterVariable  variable)
{
	limiter->variable = variable;
}

LimiterVariable
limiter_get_variable (Limiter *limiter)
{
	return limiter->variable;
}

void
limiter_set_unit (Limiter     *limiter,
		  LimiterUnit  unit)
{
	limiter->unit = unit;
}

LimiterUnit
limiter_get_unit (Limiter *limiter)
{
	return limiter->unit;
}

void
limiter_set_value (Limiter *limiter,
		   long     value)
{
	limiter->value = value;
}

long
limiter_get_value (Limiter *limiter)
{
	return limiter->value;
}

const char *
limiter_unit_to_string (LimiterUnit unit)
{
	switch (unit) {
	case LIMITER_UNIT_NONE:
		return "none";
	case LIMITER_UNIT_SONGS:
		return "songs";
	case LIMITER_UNIT_MB:
		return "mb";
	case LIMITER_UNIT_GB:
		return "gb";
	case LIMITER_UNIT_MINUTES:
		return "minutes";
	case LIMITER_UNIT_HOURS:
		return "hours";

	default:
		return "invalid-limit-type";
	}
}

LimiterUnit
limiter_unit_from_string (const char *str)
{
	LimiterUnit unit;
	
	if (strcmp (str, "songs") == 0) {
		unit = LIMITER_UNIT_SONGS;
	}
	else if (strcmp (str, "mb") == 0) {
		unit = LIMITER_UNIT_MB;
	}
	else if (strcmp (str, "gb") == 0) {
		unit = LIMITER_UNIT_GB;
	}
	else if (strcmp (str, "minutes") == 0) {
		unit = LIMITER_UNIT_MINUTES;
	}
	else if (strcmp (str, "hours") == 0) {
		unit = LIMITER_UNIT_HOURS;
	} else {
		unit = LIMITER_UNIT_NONE;
	}
	
	return unit;
}

const char *
limiter_variable_to_string (LimiterVariable variable)
{
	switch (variable) {
	case LIMITER_VARIABLE_DATE_ADDED:
		return "date-added";
	case LIMITER_VARIABLE_MOST_RECENTLY_PLAYED:
		return "most-recently-played";
	case LIMITER_VARIABLE_LEAST_RECENTLY_PLAYED:
		return "least-recently-played";
	case LIMITER_VARIABLE_RATING:
		return "rating";
	case LIMITER_VARIABLE_QUALITY:
		return "quality";
	case LIMITER_VARIABLE_RANDOM:
		return "random";
	case LIMITER_VARIABLE_YEAR:
		return "year";
	case LIMITER_VARIABLE_PLAYCOUNT:
		return "play-count";
    
	default:
		g_assert_not_reached ();
		return "invalid-limiter-type";
	}
}

LimiterVariable
limiter_variable_from_string (const char *str)
{
	LimiterVariable variable;
  
	if (strcmp (str, "date-added") == 0) {
		variable = LIMITER_VARIABLE_DATE_ADDED;
	}
	else if (strcmp (str, "most-recently-played") == 0) {
		variable = LIMITER_VARIABLE_MOST_RECENTLY_PLAYED;
	}
	else if (strcmp (str, "least-recently-played") == 0) {
		variable = LIMITER_VARIABLE_LEAST_RECENTLY_PLAYED;
	}
	else if (strcmp (str, "rating") == 0) {
		variable = LIMITER_VARIABLE_RATING;
	}
	else if (strcmp (str, "quality") == 0) {
		variable = LIMITER_VARIABLE_QUALITY;
	}
	else if (strcmp (str, "year") == 0) {
		variable = LIMITER_VARIABLE_YEAR;
	}
	else if (strcmp (str, "random") == 0){
		variable = LIMITER_VARIABLE_RANDOM;
	}
	else if (strcmp (str, "play-count") == 0) {
		variable = LIMITER_VARIABLE_PLAYCOUNT;
	} else {
		variable = LIMITER_VARIABLE_NONE;
	}
	
	return variable;
}

char *
limiter_to_xml (Limiter *limiter)
{
	return g_strdup_printf ("    <limit type=\"%s\" value=\"%ld\" selection=\"%s\"/>\n",
				limiter_unit_to_string (limiter->unit),
				limiter->value,
				limiter_variable_to_string (limiter->variable));
}

