/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include <math.h>
#include <glib/gi18n.h>
#include <gst/gst.h>
#include "jamboree-marshal.h"
#include "utils.h"
#include "types.h"
#include "player.h"

/* Only used for the esd hack below. */
#include <esd.h>

#define TICK_TIMEOUT 250
#define d(x) 

static void     player_class_init   (PlayerClass  *klass);
static void     player_init         (Player       *player);
static void     player_finalize     (GObject      *object);
static void     player_update_state (void);
static gboolean player_setup        (GError      **error);
static gboolean tick_timeout_cb     (void);
static void     start_tick          (void);
static void     stop_tick           (void);
static void     timer_start         (void);
static void     timer_pause         (void);
static void     timer_stop          (void);
static int      timer_get_time      (void);
static gboolean workaround_esd      (const char *sink_name);
static char    *get_sink_name       (void);

enum {
	EOS,
	TICK,
	STATE_CHANGED,
	ERROR,
	LAST_SIGNAL
};

struct _Player {
	GObject        parent;

	GstElement    *pipeline;
	GstElement    *source;
	GstElement    *decoder;
        GstElement    *audioconvert;
        GstElement    *audioscale;
	GstElement    *volume;
	GstElement    *sink;

	guint          bus_id;

	int            volume_level;

	gboolean       playing;
	Song          *current_song;

	GList         *songs;
	GList         *current;
	
	guint          tick_timeout_id;

	GTimer        *timer;
	int            timer_add;

	gboolean       has_error;
};

static guint signals[LAST_SIGNAL];
static Player *player = NULL;

G_DEFINE_TYPE (Player, player, G_TYPE_OBJECT);


static void
player_class_init (PlayerClass *klass)
{
	GObjectClass *object_class;

	object_class = (GObjectClass*) klass;

	object_class->finalize = player_finalize;

	signals[EOS] =
		g_signal_new ("eos",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      0,
			      NULL, NULL,
			      jamboree_marshal_VOID__BOXED,
			      G_TYPE_NONE, 1, TYPE_SONG);
	
	signals[TICK] =
		g_signal_new ("tick",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      0,
			      NULL, NULL,
			      jamboree_marshal_VOID__INT,
			      G_TYPE_NONE, 1, G_TYPE_INT);
	
	signals[STATE_CHANGED] =
		g_signal_new ("state_changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      0,
			      NULL, NULL,
			      jamboree_marshal_VOID__INT,
			      G_TYPE_NONE, 1, G_TYPE_INT);
	
	signals[ERROR] =
		g_signal_new ("error",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      0,
			      NULL, NULL,
			      jamboree_marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);
}

static void
player_init (Player *instance)
{
	player = instance;
	
	player->timer = g_timer_new ();
	timer_stop ();
	
	player->volume_level = 100;
}

static void
player_finalize (GObject *object)
{
	Player *instance;

	instance = PLAYER (object);

	gconf_client_set_int (gconf_client,
			      GCONF_PATH "/volume",
			      instance->volume_level,
			      NULL);

	stop_tick ();

	G_OBJECT_CLASS (player_parent_class)->finalize (object);
}

GQuark
player_error_quark (void)
{
	static GQuark quark = 0;
	if (!quark) {
		quark = g_quark_from_static_string ("player_error");
	}
	
	return quark;
}

PlayingState
player_get_state (void)
{
	if (!player->pipeline) {
		return PLAYING_STATE_STOPPED;
	}
	
	if (player->playing) {
		return PLAYING_STATE_PLAYING;
	}
	
	return PLAYING_STATE_PAUSED;
}

static void
message_received (GstBus * bus, GstMessage * message, gpointer unused)
{
        switch (message->type) {
        case GST_MESSAGE_EOS:
                if (!player->has_error) {
                        player_stop ();
                        g_signal_emit (player, signals[TICK], 0, (int) 0);
                        g_signal_emit (player, signals[EOS], 0, player->current_song, NULL);
                        ///g_signal_emit (player, signals[STATE_CHANGED], 0, PLAYING_STATE_STOPPED);
                }
                break;

        case GST_MESSAGE_ERROR:
                if (!player->has_error) {
                        GError *error, *new_error;
                        gchar *debug;
                        GstObject *src;
                        
                        gst_message_parse_error (message, &error, &debug);
                        src = GST_MESSAGE_SRC (message);

                        player->has_error = TRUE;
                        
                        if (g_getenv ("JAM_DEBUG_GST")) {
                                g_print ("error origin: %p, domain: %d, msg: %s\n",
                                         src, error->domain, error->message);
                        }	

                        if (src == (GstObject*)player->sink && error->domain == GST_RESOURCE_ERROR) {
                                new_error = g_error_new (PLAYER_ERROR,
                                                         PLAYER_ERROR_RESOURCE_BUSY,
                                                         _("The audio device is busy."));
                        } else {
                                new_error = g_error_copy (error);
                        }

                        player_stop ();
                        g_signal_emit (player, signals[TICK], 0, (int) 0);
                        g_signal_emit (player, signals[ERROR], 0, new_error);
                        g_signal_emit (player, signals[STATE_CHANGED], 0, PLAYING_STATE_STOPPED);
                }
                break;
                
        default:
                break;
        }
}

static gboolean 
tick_timeout_cb (void)
{
	if (!player->has_error && player->playing && player->sink) {
                GstStateChangeReturn res;
                GstState state, pending;
                
		res = gst_element_get_state (player->sink, &state, &pending, 50 * GST_USECOND);
                if (res == GST_STATE_CHANGE_SUCCESS && state == GST_STATE_PLAYING) {
			g_signal_emit (player, signals[TICK], 0, (int) timer_get_time ());
		}
	}
	
	return TRUE;
}
                                                                                
gboolean
player_set_song (Song *song, GError **error)
{
	gboolean was_playing;
	gboolean success;
  
	was_playing = player->playing;
  
	if (!song) {
		player_stop ();
		return TRUE;
	}

	player->current_song = song;
	success = player_setup (error);
  
	player->playing = was_playing && success;
	player_update_state ();

        if (player->playing) { 
                g_signal_emit (player, signals[STATE_CHANGED], 0, PLAYING_STATE_PLAYING);
        }
        
	return success;
}

gboolean
player_play (GError **error)
{
	if (!player->current_song) {
                g_set_error (error, 0, 0, "No song selected");
		return FALSE;
	}
  
	if (!player->pipeline) {
		return player_play_song (player->current_song, error);
	}

	if (!player->playing) {
		g_signal_emit (player, signals[STATE_CHANGED], 0, PLAYING_STATE_PLAYING);
	}
	
	player->playing = TRUE;

	player_update_state ();

	return TRUE;
}

gboolean
player_play_song (Song    *song,
		  GError **error)
{
	player->current_song = song;

	if (song) {
		player->playing = player_setup (error);
	} else {
		player->playing = FALSE;
	}

	if (player->playing) {
		g_signal_emit (player, signals[STATE_CHANGED], 0, PLAYING_STATE_PLAYING);
	}

	player_update_state ();

	if (!song && player->playing) {
		return FALSE;
	}

	return TRUE;
}

void
player_stop (void)
{
	gboolean emit;

	stop_tick ();
	timer_stop ();
  
	emit = player->playing;
	player->playing = FALSE;
	
	if (player->pipeline) {
		GstObject *parent;
                GstBus *bus;

		/* Reset the location so we close the file as soon as possible
		 * if something below fails. This seems to help against Jamboree
		 * keeping lots of played files open indefinitely.
		 */
		if (player->source) {
			g_object_set (player->source, "location", NULL, NULL);
		}
		
		gst_element_set_state (player->pipeline, GST_STATE_NULL);

                /* If these are not added, we need to free them ourselves. */
		parent = gst_element_get_parent (player->audioconvert);
		if (!parent) {
			gst_object_unref (player->audioconvert);
			gst_object_unref (player->audioscale);
			gst_object_unref (player->volume);
			gst_object_unref (player->sink);
		} else {
			gst_object_unref (parent);
		}

                bus = gst_pipeline_get_bus (GST_PIPELINE (player->pipeline));
                gst_bus_remove_signal_watch (bus);
		g_signal_handler_disconnect (bus, player->bus_id);
                gst_object_unref (bus);
                
		gst_object_unref (player->pipeline);
		
		player->pipeline = NULL;
		player->source = NULL;
		player->decoder = NULL;
		player->audioconvert = NULL;
		player->audioscale = NULL;
		player->sink = NULL;

		/* If we had a thread, we're either playing or paused. */
		emit = TRUE;
	}

	if (emit) {
		g_signal_emit (player, signals[STATE_CHANGED], 0, PLAYING_STATE_STOPPED);
	}
}

void
player_pause (void)
{
	if (player->playing) {
		g_signal_emit (player, signals[STATE_CHANGED], 0, PLAYING_STATE_PAUSED);
	}

	player->playing = FALSE;
	player_update_state ();
}

void
player_set_volume (int volume)
{
	double d;

	g_return_if_fail (volume >= 0 && volume <= 100);

	player->volume_level = volume;
   
	d = volume / 100.0;

	if (player->volume) {
		g_object_set (player->volume, "volume", d, NULL);
	}
}

int
player_get_volume (void)
{
	return player->volume_level;
}

/* t is in milliseconds. */
void
player_seek (int t)
{
	if (!player->pipeline || !player->sink) {
		return;
	}
  
	timer_stop ();

	gst_element_seek (player->pipeline, 1.0, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH,
                          GST_SEEK_TYPE_SET, t * GST_SECOND, GST_SEEK_TYPE_NONE, 0);

	if (player->playing) {
		timer_start ();
	}

	player->timer_add = t;
}

/* Return value is in seconds. */
int
player_tell (void)
{
	if (!player->has_error && player->playing && player->sink) {
		return timer_get_time ();
	}

	return 0;
}

gboolean
player_is_playing (Song *song)
{
	g_return_val_if_fail (song != NULL, FALSE);

	return player->playing && song == player->current_song;
}

Song *
player_get_song (void)
{
	return player->current_song;
}

static void
player_update_state (void)
{
	if (player->playing) {
		gst_element_set_state (player->pipeline, GST_STATE_PLAYING);
		timer_start ();
		start_tick ();
	} else {
		stop_tick ();
      
		if (player->pipeline) {
			gst_element_set_state (player->pipeline, GST_STATE_PAUSED);

			timer_pause ();
		} else {
			timer_stop ();
		}
	}
}

static void
player_new_decoded_pad_cb (GstElement *decodebin,
			   GstPad     *pad,
			   gboolean    last,
			   Player     *player)
{
	GstCaps      *caps;
	GstStructure *str;
	GstPad       *audio_pad;

	audio_pad = gst_element_get_pad (player->audioconvert, "sink");
	
	/* Only link once. */
	if (gst_pad_is_linked (audio_pad)) {
 		return;
	}

	/* Only link audio. */
	caps = gst_pad_get_caps (pad);
	str = gst_caps_get_structure (caps, 0);
	if (!g_strrstr (gst_structure_get_name (str), "audio")) {
		gst_caps_unref (caps);
		return;
	}

	gst_caps_unref (caps);
	
	gst_pad_link (pad, audio_pad);
}

static GstElement *
create_pipeline (const char *sink_name, GError **error)
{
	GstElement *pipeline;
	GstElement *source;
	GstElement *decodebin;
	GstElement *audioconvert;
	GstElement *audioscale;
	GstElement *volume;
	GstElement *sink;

	/* src -> decodebin -> audioconvert -> audioscale -> volume -> sink */
	
	pipeline = gst_element_factory_make ("pipeline", "pipeline");
	source = gst_element_factory_make ("filesrc", "source");
	decodebin = gst_element_factory_make ("decodebin", "decoder");
	audioconvert = gst_element_factory_make ("audioconvert", "audioconvert");
	audioscale = gst_element_factory_make ("identity", "audioscale");
	volume = gst_element_factory_make ("volume", "volume");
	sink = gst_element_factory_make (sink_name, "sink");

	if (g_getenv ("JAM_DEBUG_GST")) {
		g_print ("pipeline:       %s\n", pipeline ? "OK" : "failed");
		g_print ("source:       %s\n", source ? "OK" : "failed");
		g_print ("decodebin:    %s\n", decodebin ? "OK" : "failed");
		g_print ("audioconvert: %s\n", audioconvert ? "OK" : "failed");
		g_print ("audioscale:   %s\n", audioscale ? "OK" : "failed");
		g_print ("volume:       %s\n", volume ? "OK" : "failed");
		g_print ("sink:         %s (%s)\n", sink ? "OK" : "failed", sink_name);
	}
	
	if (!pipeline || !source || !decodebin || !audioconvert || !audioscale || !volume || !sink) {
		goto fail;
	}
  
	gst_bin_add_many (GST_BIN (pipeline),
                          source,
                          decodebin,
                          audioconvert,
                          audioscale,
                          volume,
                          sink,
                          NULL);

	if (!gst_element_link_many (source, decodebin, NULL)) {
		goto fail;
	}
        
        if (!gst_element_link_many (audioconvert, audioscale, volume, sink, NULL)) {
		goto fail;
	}

        player->source = source;
        player->decoder = decodebin;
        player->audioconvert = audioconvert;
        player->audioscale = audioscale;
        player->volume = volume;
        player->sink = sink;
        
	return pipeline;
  
 fail:
	/* This is pretty much fatal so don't bother too much with cleaning
	 * up.
	 */

	if (pipeline) {
		g_object_unref (pipeline);
	}
	
	g_set_error (error,
		     PLAYER_ERROR,
		     PLAYER_ERROR_INTERNAL,
		     _("Internal error, please check your GStreamer installation."));
  
	return NULL;
}

static gboolean
player_setup (GError **error)
{
	char             *sink_name;
        GstBus		 *bus;

	player->has_error = FALSE;

	if (player->pipeline) {
		player_stop ();
	}
	
	if (!player->current_song) {
		return FALSE;
	}

	sink_name = get_sink_name ();
	if (!workaround_esd (sink_name)) {
		g_set_error (error, PLAYER_ERROR,
			     PLAYER_ERROR_RESOURCE_BUSY,
			     _("The audio device is busy."));

		return FALSE;
	}

	player->pipeline = create_pipeline (sink_name, error);

	g_free (sink_name);
      
	if (!player->pipeline) {
		goto bail;
	}
 
	g_object_set (player->source, "location", song_get_path (player->current_song), NULL);

        bus = gst_element_get_bus (player->pipeline);
        gst_bus_add_signal_watch (bus);
        player->bus_id = g_signal_connect (bus, "message", G_CALLBACK (message_received), NULL);
        gst_object_unref (bus);

	g_signal_connect (player->decoder,
			  "new_decoded_pad",
			  G_CALLBACK (player_new_decoded_pad_cb),
			  player);
        
	player_set_volume (player->volume_level);
  
	return TRUE;

 bail:

	player_stop ();

	return FALSE;
}

static void
start_tick (void)
{
	if (player->tick_timeout_id) {
		return;
	}
  
	player->tick_timeout_id = g_timeout_add (TICK_TIMEOUT,
					       (GSourceFunc) tick_timeout_cb,
					       player);
}

static void
stop_tick (void)
{
	if (!player->tick_timeout_id) {
		return;
	}
  
	g_source_remove (player->tick_timeout_id);
	player->tick_timeout_id = 0;
}

static void
timer_start (void)
{
	g_timer_reset (player->timer);
	g_timer_start (player->timer);
}

static void
timer_pause (void)
{
	player->timer_add = player->timer_add + floor (0.5 + g_timer_elapsed (player->timer, NULL));
  
	g_timer_stop (player->timer);
	g_timer_reset (player->timer);
}

static void
timer_stop (void)
{
	g_timer_stop (player->timer);
	g_timer_reset (player->timer);

	player->timer_add = 0;
}

static int
timer_get_time (void)
{
	return player->timer_add + floor (0.5 + g_timer_elapsed (player->timer, NULL));
}

Player *
player_get (void)
{
	if (!player) {
		g_object_new (TYPE_PLAYER, NULL);
	}   
	
	return player;
}

void
player_shutdown (void)
{
	if (player) {
		g_object_unref (player);
		player = NULL;
	}
}

/* Hacky workaround for esd/esdsink suckyness. */
static gboolean
workaround_esd (const char *sink_name)
{
	static int esd_fd;

	if (strcmp (sink_name, "esdsink") == 0) {
		if (!esd_fd) {
			d(g_print ("hack: opening esd connection\n"));
			esd_fd = esd_open_sound (NULL);

			if (esd_fd == -1) {
				esd_fd = 0;
				return FALSE;
			}
		}
	}
	else if (esd_fd) {
		d(g_print ("hack: closing esd connection\n"));
		esd_close (esd_fd);
		esd_fd = 0;
	}
	
	return TRUE;
}

static char *
get_sink_name (void)
{
	char *sink_name;

	sink_name = g_strdup ("alsasink");

	return sink_name;
}

