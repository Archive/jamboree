/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __EXPR_H__
#define __EXPR_H__

#include <glib.h>
#include <time.h>
#include "song.h"

typedef struct _Expr       Expr;
typedef struct _ExprUnary  ExprUnary;
typedef struct _ExprBinary ExprBinary;
typedef struct _Constant   Constant;

typedef enum {
	EXPR_OP_NONE = 0,

	/* Only used as placeholder for unary ops until we get some real. */
	EXPR_OP_FAKE,

	EXPR_OP_CONTAINS,
	EXPR_OP_DOES_NOT_CONTAIN,
	EXPR_OP_STARTS_WITH,
	EXPR_OP_ENDS_WITH,
	EXPR_OP_EQ,
	EXPR_OP_NE,
	EXPR_OP_GE,
	EXPR_OP_GT,
	EXPR_OP_LE,
	EXPR_OP_LT,
	EXPR_OP_AND,
	EXPR_OP_OR,
	EXPR_OP_RANGE,
  
	EXPR_OP_CONSTANT,
	EXPR_OP_VARIABLE,

	/* Internal only. */
	EXPR_OP_HAS_PLAYLIST
} ExprOp;

#define EXPR_BINARY_OPS EXPR_OP_CONTAINS: case EXPR_OP_DOES_NOT_CONTAIN: case EXPR_OP_STARTS_WITH: \
	case EXPR_OP_ENDS_WITH: case EXPR_OP_EQ: case EXPR_OP_NE: case EXPR_OP_GE: case EXPR_OP_GT: \
	case EXPR_OP_LE: case EXPR_OP_LT: case EXPR_OP_AND: case EXPR_OP_OR: case EXPR_OP_HAS_PLAYLIST: \
        case EXPR_OP_RANGE

#define EXPR_UNARY_OPS EXPR_OP_FAKE

typedef enum {
	CONSTANT_TYPE_NONE = 0,

	CONSTANT_TYPE_INT,
	CONSTANT_TYPE_STRING,
	CONSTANT_TYPE_DATE,
	CONSTANT_TYPE_SIZE,
	CONSTANT_TYPE_RANGE,
	/* ... more? ... */
} ConstantType;

typedef enum {
	VARIABLE_NONE = 0,

	VARIABLE_TITLE,
	VARIABLE_ARTIST,
	VARIABLE_ALBUM,
	VARIABLE_YEAR,
	VARIABLE_GENRE,
	VARIABLE_RATING,
	VARIABLE_TRACK,
	VARIABLE_PLAYCOUNT,
	VARIABLE_LAST_PLAYED,
	VARIABLE_LENGTH,
	VARIABLE_BIT_RATE,
	VARIABLE_SIZE,
	VARIABLE_SAMPLE_RATE,
	VARIABLE_COMMENT,
	VARIABLE_DATE_MODIFIED,
	VARIABLE_DATE_ADDED,
	VARIABLE_KIND, 
	/* ... more ... */

	/* Internal only. */
	VARIABLE_PLAYLISTS
} Variable;

typedef enum {
	UNIT_MINUTES,
	UNIT_HOURS,
	UNIT_DAYS,
	UNIT_WEEKS,
	UNIT_MONTHS,
	UNIT_YEARS,
} Unit;

struct _ExprUnary {
	Expr *expr;
};

struct _ExprBinary {
	Expr *expr_1;
	Expr *expr_2;
};

struct _Constant {
	ConstantType type;
	Unit         unit;
  
	union {
		char *str;
		int i;
		time_t date;
		struct {
			int start;
			int end;
		} range;
	} v;
};

struct _Expr {
	ExprOp op;
  
	union {
		ExprUnary   unary;
		ExprBinary  binary;
		Constant   *constant;
		Variable    variable;
	} v;
};


Expr *       expr_new                  (ExprOp        op);
Expr *       expr_new_simple           (const char *title,
					const char *artist,
					const char *album);
Expr *       expr_copy                 (Expr         *expr);
Expr *       expr_new_unary            (ExprOp        op,
					Expr         *expr);
Expr *       expr_new_binary           (ExprOp        op,
					Expr         *expr_1,
					Expr         *expr_2);
Expr *       expr_new_constant         (Constant     *constant);
Expr *       expr_new_variable         (Variable      variable);
void         expr_set_binary_op        (Expr         *expr,
					ExprOp        op,
					Expr         *expr_1,
					Expr         *expr_2);
void         expr_set_unary_op         (Expr         *expr,
					ExprOp        op,
					Expr         *expr_1);
void         expr_set_constant_op      (Expr         *expr,
					Constant     *constant);
void         expr_set_variable_op      (Expr         *expr,
					Variable      variable);
void         expr_free                 (Expr         *expr);
gboolean     expr_evaluate             (Expr         *expr,
					Song         *song);
Constant *   constant_string_new       (const char   *str);
Constant *   constant_int_new          (int           i);
Constant *   constant_date_new         (time_t        date,
					Unit          unit);
Constant *   constant_range_new        (int           start,
					int           end);
void         constant_free             (Constant     *constant);
int          constant_get_int_value    (Constant     *constant);
time_t       constant_get_date_value   (Constant     *constant);
char *       constant_get_string_value (Constant     *constant);
void         constant_get_range_values (Constant     *constant,
					int          *start,
					int          *end);
void         constant_set_unit         (Constant     *constant,
					Unit          unit);
Unit         constant_get_unit         (Constant     *constant);

const char * unit_to_string            (Unit          unit);
Unit         unit_from_string          (const char *  str);
const char * constant_type_to_string   (ConstantType  type);
ConstantType constant_type_from_string (const char   *str);
const char * operator_to_string        (ExprOp        op);
ExprOp       operator_from_string      (const char   *str);
const char * variable_to_string        (Variable      variable);
Variable     variable_from_string      (const char   *str);
gboolean     expr_equal                (Expr         *e1,
					Expr         *e2);
char *       expr_to_xml               (Expr         *expr);


#endif /* __EXPR_H__ */
