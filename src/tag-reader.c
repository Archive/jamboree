/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004, 2005 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <glib/gi18n.h>
#include <gst/gst.h>
#include <gst/gsttagsetter.h>
#include "tag-reader.h"
#include "song-private.h"

typedef struct _TagReader TagReader;
struct _TagReader {
	GstElement *pipeline;
	GstElement *source;
	GstElement *decoder;
	GstElement *sink;	

	gboolean    got_eos;
	gboolean    got_error;
	gboolean    got_handoff;
	
	char       *title;
	char       *artist;
	char       *album;
	char       *genre;
	int         duration;
	int         track;
	int         year;
};

#define IS_TAG(tag,x) (strcmp(tag,x)==0)

static void reader_free (TagReader *reader);

static char *
get_title_from_path (const char *path)
{
	char     *utf8;
	char     *tmp;
	char     *p;
	gunichar  c;
  
	utf8 = g_filename_to_utf8 (path, -1, NULL, NULL, NULL);
	if (utf8) {
		tmp = g_path_get_basename (utf8);
		g_free (utf8);

		p = strrchr (tmp, '.'); 
		if (p) {
			*p = 0;
		}
		
		p = tmp;
		while (*p) {
			c = g_utf8_get_char (p);
	  
			if (c == '_') {
				*p = ' ';
			}
			
			p = g_utf8_next_char (p);
		}

		/* Extra check to catch bugs in this function. */
		if (g_utf8_validate (tmp, -1, NULL)) {
			return tmp;
		}

		g_free (tmp);
		return g_strdup (_("Invalid Unicode"));
	}

	return NULL;
}

static void
reader_load_tag (const GstTagList *list,
		 const char       *tag,
		 TagReader        *reader)
{
	int           count;
	const GValue *val;

	count = gst_tag_list_get_tag_size (list, tag);
	if (count < 1) {
		return;
	}

	val = gst_tag_list_get_value_index (list, tag, 0);

	if (IS_TAG (tag, GST_TAG_TITLE)) {
		reader->title = g_value_dup_string (val);
		/*g_print ("%s\n", reader->reader->title);*/
	}
	else if (IS_TAG (tag, GST_TAG_ALBUM)) {
		reader->album = g_value_dup_string (val);
	}
	else if (IS_TAG (tag, GST_TAG_ARTIST)) {
		reader->artist = g_value_dup_string (val);
	}
	else if (IS_TAG (tag, GST_TAG_GENRE)) {
		reader->genre = g_value_dup_string (val);
	}
	else if (IS_TAG (tag, GST_TAG_DURATION)) {
		reader->duration = g_value_get_uint64 (val) / GST_SECOND;
	}
	else if (IS_TAG (tag, GST_TAG_TRACK_NUMBER)) {
		reader->track = g_value_get_uint (val);
	}
	else if (IS_TAG (tag, GST_TAG_DATE)) {
		const GDate	*date;
		GDateYear  year;

		date = gst_value_get_date (val);
		if (date) {
			year = g_date_get_year (gst_value_get_date (val));
			
			reader->year = year;
		}
	}
	/* FIXME: more... */

}

static void
reader_handoff_cb (GstElement *sink,
		   GstBuffer  *buf,
		   GstPad     *pad,
		   TagReader  *reader)
{
	reader->got_handoff = TRUE;
	gst_element_post_message
		(sink, gst_message_new_application (NULL, gst_structure_new ("foo", NULL)));
}

static void
reader_new_decoded_pad_cb (GstElement *decodebin,
			   GstPad     *pad,
			   gboolean    last,
			   TagReader  *reader)
{
	GstCaps      *caps;
	GstStructure *str;
	GstPad       *audio_pad;

	audio_pad = gst_element_get_pad (reader->sink, "sink");
	
	/* Only link once. */
	if (GST_PAD_IS_LINKED (audio_pad)) {
		goto exit;
	}

	/* Only link audio. */
	caps = gst_pad_get_caps (pad);
	if (!(gst_caps_get_size(caps)))
		goto exit;

	str = gst_caps_get_structure (caps, 0);
	if (!g_strrstr (gst_structure_get_name (str), "audio")) {
		gst_caps_unref (caps);
		goto exit;
	}

	gst_caps_unref (caps);
	
	gst_pad_link (pad, audio_pad);

exit:
	gst_object_unref (audio_pad);
}

static TagReader *
reader_create_pipeline (const char *path)
{
	TagReader *reader;

	reader = g_new0 (TagReader, 1);

	reader->pipeline = gst_element_factory_make ("pipeline", "pipeline");
	if (!reader->pipeline) {
		goto fail;
	}

	reader->source = gst_element_factory_make ("filesrc", "source");
	if (!reader->source) {
		goto fail;
	}
	gst_bin_add (GST_BIN (reader->pipeline), reader->source);
	g_object_set (reader->source, "location", path, NULL);

	reader->decoder = gst_element_factory_make ("decodebin", "decoder");
	if (!reader->decoder) {
		goto fail;
	}
	gst_bin_add (GST_BIN (reader->pipeline), reader->decoder);

	reader->sink = gst_element_factory_make ("fakesink", "fakesink");
	if (!reader->sink) {
		goto fail;
	}
	gst_bin_add (GST_BIN (reader->pipeline), reader->sink);
	
	if (!gst_element_link (reader->source, reader->decoder)) {
		goto fail;
	}

	g_signal_connect (reader->decoder,
			  "new-decoded-pad",
			  G_CALLBACK (reader_new_decoded_pad_cb),
			  reader);
	g_object_set (reader->sink, "signal-handoffs", (gboolean)TRUE, NULL);
	g_signal_connect (reader->sink,
			  "handoff",
			  G_CALLBACK (reader_handoff_cb),
			  reader);

	return reader;

 fail:
	reader_free (reader);
	return NULL;
}

static void
reader_free (TagReader *reader)
{
	if (reader->pipeline) {
		gst_element_set_state (reader->pipeline, GST_STATE_NULL);
		gst_object_unref (reader->pipeline);
	}
	
	g_free (reader);
}

static void
reader_fixup_tags (TagReader *reader, Song *song)
{
	if (!reader->title) {
		_song_set_title (song, get_title_from_path (song_get_path (song)));
	}

	if (!reader->artist) {
		_song_set_artist_const (song, _("Unknown artist"));
	}

	if (!reader->album) {
		_song_set_album_const (song, _("Unknown album"));
	}

	if (!reader->genre) {
		_song_set_genre (song, g_strdup (""));
	}
}

static gboolean 
reader_loop (TagReader *reader)
{
	GstBus *bus;
	GstMessageType revent;
	GstMessage *message = NULL;

	bus = gst_element_get_bus (reader->pipeline);

	while (TRUE) {
		message = gst_bus_poll (bus, GST_MESSAGE_ANY, GST_SECOND * 2);

		if (message) {
			revent = GST_MESSAGE_TYPE (message);
		}
		else {
			gchar *location;

			/* poll timed out */

			g_object_get (reader->source, "location", &location, NULL);
			g_print ("\n\nPlease let #gstreamer know about this file: %s\n\n", location);
			return FALSE;
		}

		switch (revent) {
		case GST_MESSAGE_EOS:
			gst_message_unref (message);
			return FALSE;
		  
		case GST_MESSAGE_TAG: {
			GstTagList *tags;

			gst_message_parse_tag (message, &tags);
			gst_tag_list_foreach (tags, (GstTagForeachFunc)reader_load_tag,
					      reader);
			gst_tag_list_free (tags);
			gst_message_unref (message);
			break;
		}

		case GST_MESSAGE_WARNING: {
			GError *gerror = NULL;
			gchar *debug = NULL;
		  
			/* How about doing something with this error message ??*/
			gst_message_parse_warning (message, &gerror, &debug);
			if (gerror)
				g_error_free (gerror);
			if (debug)
				g_free (debug);
			gst_message_unref (message);
			break;
			
		}
		case GST_MESSAGE_ERROR: {
			GError *gerror = NULL;
			gchar *debug = NULL;
		  
			/* How about doing something with this error message ??*/
			gst_message_parse_error (message, &gerror, &debug);
			if (gerror)
				g_error_free (gerror);
			if (debug)
				g_free (debug);
			gst_message_unref (message);
			return FALSE;
		}

		case GST_MESSAGE_APPLICATION:
			gst_message_unref (message);
			return TRUE;

		default:
			gst_message_unref (message);
			break;
		}
	}

	g_assert_not_reached ();
}

static Song *
reader_read_file (const char *path)
{
	TagReader   *reader;
	Song        *song = NULL;
	time_t       now;
	struct stat  buf;

	reader = reader_create_pipeline (path);
	if (!reader) {
		return NULL;
	}
	
	gst_element_set_state (reader->pipeline, GST_STATE_PLAYING);

	if (!reader_loop (reader))
		goto exit;

	gst_element_set_state (reader->pipeline, GST_STATE_PAUSED);

	if (reader->duration == 0 && reader->got_handoff) {
		GstPad *pad, *peer;
		GstFormat format = GST_FORMAT_TIME;
		gint64 total;

		pad = gst_element_get_pad (reader->sink, "sink");
		peer = gst_pad_get_peer (pad);
		gst_object_unref (pad);
		
		if (gst_pad_query_duration (peer, &format, &total)) {
			reader->duration = total / GST_SECOND;
		}
		gst_object_unref (peer);
	}

	song = song_new (path);

	/* These move the ownership of the strings to the song. */
	_song_set_title (song, reader->title);
	_song_set_album (song, reader->album);
	_song_set_artist (song, reader->artist);
	_song_set_duration (song, reader->duration);
	_song_set_genre (song, reader->genre);
	_song_set_track (song, reader->track);
	_song_set_year (song, reader->year);

	now = time (NULL);
	_song_set_time_added (song, now);

	if (stat (path, &buf) == 0) {
		_song_set_size (song, buf.st_size);
		_song_set_time_modified (song, buf.st_mtime);
	}
	
	reader_fixup_tags (reader, song);

 exit:

	reader_free (reader);

	return song;
}

static gint
reader_count_files (const char *path)
{
	GDir       *dir;
	gint        files;
	char       *full;
	const char *name;

	dir = g_dir_open (path, 0, NULL);
	if (!dir) {
		return 0;
	}

	files = 0;
	
	while ((name = g_dir_read_name (dir))) {
		full = g_build_filename (path, name, NULL);
		if (g_file_test (full, G_FILE_TEST_IS_DIR)) {
			files += reader_count_files (full);
		} else {
			files++;
		}

		g_free (full);
	}

	g_dir_close (dir);
	
	return files;
}

static gboolean
reader_add_dir (const char             *path,
		gint                    files,
		gint                   *completed,
		TagReaderSongFoundFunc  song_found_func,
		TagReaderSongReadFunc   song_read_func,
		TagReaderProgressFunc   progress_func,
		gpointer                user_data)
{
	GDir       *dir;
	const char *name;
	char       *full;
	Song       *song;
	gboolean    ret;

	dir = g_dir_open (path, 0, NULL);
	if (!dir) {
		return FALSE;
	}

	ret = FALSE;
	
	while ((name = g_dir_read_name (dir))) {
		full = g_build_filename (path, name, NULL);

		if (g_file_test (full, G_FILE_TEST_IS_DIR)){
			if (!progress_func (full, files, *completed, user_data)) {
				g_free (full);
				g_dir_close (dir);
				
				return FALSE;
			}
			
			ret |= reader_add_dir (full,
					       files,
					       completed,
					       song_found_func,
					       song_read_func,
					       progress_func,
					       user_data);
		} else {
			if (!progress_func (full, files, *completed, user_data)) {
				g_free (full);
				g_dir_close (dir);
				
				return FALSE;
			}

			if (!song_found_func || song_found_func (full, user_data)) {
				song = reader_read_file (full);
				if (song) {
					ret = TRUE;
					
					if (song_read_func) {
						song_read_func (song, user_data);
						(*completed)++;
					}
					song_unref (song);
				}
			}
		}
		
		g_free (full);
	}
	
	g_dir_close (dir);

	return ret;
}

gboolean
tag_reader_read_path (const char             *path,
		      TagReaderSongFoundFunc  song_found_func,
		      TagReaderSongReadFunc   song_read_func,
		      TagReaderProgressFunc   progress_func,
		      gpointer                user_data)
{
	Song *song;
	gint  files;
	gint  completed;

	files = reader_count_files (path);
	completed = 0;
	
	if (g_file_test (path, G_FILE_TEST_IS_DIR)){
		return reader_add_dir (path,
				       files,
				       &completed,
				       song_found_func,
				       song_read_func,
				       progress_func,
				       user_data);
	} else {
		if (!song_found_func || song_found_func (path, user_data)) {
			song = reader_read_file (path);

			if (song) {
				if (song_read_func) {
					song_read_func (song, user_data);
				}
				song_unref (song);

				return TRUE;
			}
		}
	}

	return FALSE;
}
