/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SMART_PLAYLIST_DIALOG_H__
#define __SMART_PLAYLIST_DIALOG_H__

#include <gtk/gtkdialog.h>
#include "source-smart-playlist.h"

#define TYPE_SMART_PLAYLIST_DIALOG            (smart_playlist_dialog_get_type ())
#define SMART_PLAYLIST_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SMART_PLAYLIST_DIALOG, SmartPlaylistDialog))
#define SMART_PLAYLIST_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SMART_PLAYLIST_DIALOG, SmartPlaylistDialogClass))
#define IS_SMART_PLAYLIST_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SMART_PLAYLIST_DIALOG))
#define IS_SMART_PLAYLIST_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SMART_PLAYLIST_DIALOG))
#define SMART_PLAYLIST_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SMART_PLAYLIST_DIALOG, SmartPlaylistDialogClass))

typedef struct _SmartPlaylistDialog      SmartPlaylistDialog;
typedef struct _SmartPlaylistDialogClass SmartPlaylistDialogClass;
typedef struct _SmartPlaylistDialogPriv  SmartPlaylistDialogPriv;

struct _SmartPlaylistDialog {
	GtkDialog parent;

	SmartPlaylistDialogPriv *priv;
};

struct _SmartPlaylistDialogClass {
	GtkDialogClass parent_class;
};

GType       smart_playlist_dialog_get_type  (void);
GtkWidget * smart_playlist_dialog_new       (void);
gboolean    smart_playlist_dialog_run       (GtkWindow           *parent,
					     SourceSmartPlaylist *playlist);


#endif /* __SMART_PLAYLIST_DIALOG_H__ */
