/*
 * Copyright (C) 2003-2005 Imendio AB
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <stdio.h>
#include <string.h>
#include <glib/gi18n.h>
#include <glade/glade.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtktreeviewcolumn.h>
#include <gtk/gtktreeselection.h>
#include "source-view.h"
#include "eggtreemultidnd.h"
#include "utils.h"
#include "string-utils.h"
#include "player.h"

static gboolean           drag_drop_cb           (GtkWidget           *widget,
						  GdkDragContext      *drag_context,
						  int                  x,
						  int                  y,
						  guint                time,
						  gpointer             user_data);
static GtkTreeViewColumn *setup_column           (SourceView        *song_view,
						  const char          *title,
						  GtkTreeCellDataFunc  func,
						  int                  sort_id,
						  gboolean             pack_start,
						  gboolean             expand,
						  gboolean             visible,
						  float                xalign,
						  int                  default_width);
static void               song_title_func        (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_playing_func      (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_rating_func       (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_year_func         (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_time_func         (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_artist_func       (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_album_func        (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_genre_func        (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_playcount_func    (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_last_played_func  (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_track_func        (GtkTreeViewColumn   *tree_column,
						  GtkCellRenderer     *cell,
						  GtkTreeModel        *tree_model,
						  GtkTreeIter         *iter,
						  gpointer             data);
static void               song_column_clicked_cb (GtkTreeViewColumn   *column,
						  SourceView        *view);
static void               drag_data_get          (GtkWidget           *widget,
						  GdkDragContext      *context,
						  GtkSelectionData    *selection_data,
						  guint                info,
						  guint                time);
const char *              song_sort_id_to_string (SongSortId           sort_id);
static gboolean           search_equal_func 	 (GtkTreeModel 	      *model,
                                             	  gint 		       column,
                                             	  const gchar 	      *key,
						  GtkTreeIter 	      *iter,
						  gpointer 	       search_data);
	      


#define SOURCE_VIEW_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_SOURCE_VIEW, SourceViewPriv))
G_DEFINE_TYPE (SourceView, source_view, GTK_TYPE_TREE_VIEW);


struct _SourceViewPriv {
	int dummy;
};

enum {
	TARGET_INTERNAL,
	TARGET_UTF8_STRING
};

static const GtkTargetEntry drag_types[] = {
	{ "x-special/jamboree-song-list", GTK_TARGET_SAME_APP, TARGET_INTERNAL },
	{ "UTF8_STRING", 0, TARGET_UTF8_STRING }
};


static void
source_view_class_init (SourceViewClass *klass)
{
	GtkWidgetClass *widget_class = (GtkWidgetClass *) klass;
  
	widget_class->drag_data_get = drag_data_get;
	/* Do we need to implement drag_data_delete? */

	g_type_class_add_private (klass, sizeof (SourceViewPriv));
}

static void
source_view_init (SourceView *view)
{
	SourceViewPriv    *priv;
	GtkTreeModel      *model;
	GtkTreeSelection  *selection;
	GtkTreeViewColumn *column;
	GtkCellRenderer   *cell;
	PangoContext      *context;
	PangoFontMetrics  *metrics;
	int                f;

	priv = SOURCE_VIEW_GET_PRIVATE (view);
	view->priv = priv;

	g_object_set (view, "fixed-height-mode", TRUE, NULL);
  
	model = source_model_new ();

	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (view), TRUE);
	gtk_tree_view_set_model (GTK_TREE_VIEW (view), model);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);

	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (view), TRUE);
	egg_tree_multi_drag_add_drag_support (GTK_TREE_VIEW (view));

	gtk_tree_view_enable_model_drag_source (GTK_TREE_VIEW (view),
						GDK_BUTTON1_MASK,
						drag_types,
						G_N_ELEMENTS (drag_types),
						GDK_ACTION_COPY);

	g_signal_connect (view,
			  "drag_drop",
			  G_CALLBACK (drag_drop_cb),
			  view);

	g_signal_connect (view,
			  "drag_data_get",
			  G_CALLBACK (drag_data_get),
			  NULL);

	context = gtk_widget_get_pango_context (GTK_WIDGET (view));
	metrics = pango_context_get_metrics (context, GTK_WIDGET (view)->style->font_desc, NULL);
       
	f = PANGO_PIXELS (pango_font_metrics_get_approximate_char_width (metrics));
	pango_font_metrics_unref (metrics);
 
	/* i18n: the # sign is used as a number sign here. */
	setup_column (view, _("#"), song_track_func, SONG_SORT_ID_TRACK, TRUE, FALSE, TRUE, 1.0, f*4);
	column = setup_column (view, _("Title"), song_title_func, SONG_SORT_ID_TITLE, FALSE, TRUE, TRUE, 0.0, f*40);
	
	setup_column (view, _("Album"), song_album_func, SONG_SORT_ID_ALBUM, TRUE, TRUE, TRUE, 0.0, f*25);
	setup_column (view, _("Artist"), song_artist_func, SONG_SORT_ID_ARTIST, TRUE, TRUE, TRUE, 0.0, f*25);
	setup_column (view, _("Genre"), song_genre_func, SONG_SORT_ID_GENRE, TRUE, TRUE, FALSE, 0.0, f*10);
	setup_column (view, _("Duration"), song_time_func, SONG_SORT_ID_TIME, TRUE, FALSE, TRUE, 1.0, f*10);
	setup_column (view, _("Year"), song_year_func, SONG_SORT_ID_YEAR, TRUE, FALSE, FALSE, 1.0, f*6);
	setup_column (view, _("Rating"), song_rating_func, SONG_SORT_ID_RATING, TRUE, FALSE, FALSE, 1.0, f*4);
	setup_column (view, _("Play Count"), song_playcount_func, SONG_SORT_ID_PLAYCOUNT, TRUE, FALSE, FALSE, 1.0, f*4);
	setup_column (view, _("Last Played"), song_last_played_func, SONG_SORT_ID_LAST_PLAYED, TRUE, FALSE, FALSE, 0.0, f*10);

	cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, cell, FALSE);

	gtk_tree_view_column_set_cell_data_func (column,
						 cell,
						 song_playing_func,
						 view,
						 NULL);

	gtk_tree_view_set_search_column (GTK_TREE_VIEW (view),
					 SONG_SORT_ID_TITLE);
	gtk_tree_view_set_enable_search (GTK_TREE_VIEW (view), TRUE);
	gtk_tree_view_set_search_equal_func (GTK_TREE_VIEW (view),
					     search_equal_func, NULL, NULL);

	source_model_set_sorting (SOURCE_MODEL (model),
				  SONG_SORT_ID_DEFAULT,
				  GTK_SORT_ASCENDING);  
}

static gboolean 
search_equal_func (GtkTreeModel *model,
		   gint          column,
		   const char   *key,
		   GtkTreeIter  *iter,
		   gpointer      data)
{
	Song     *song;
	gboolean  retval;
	gchar    *folded_key, *folded_song;
	int       len;


	song = source_model_get_song (SOURCE_MODEL (model), iter);
	folded_key = g_utf8_casefold (key, -1);

	len = g_utf8_strlen (folded_key, -1);
	
	folded_song = g_utf8_casefold (song_get_title (song), len);
	
	retval = g_utf8_collate (folded_key, folded_song);

	g_free (folded_key);
	g_free (folded_song);
	
	return retval;
}

static GtkTreeViewColumn *
setup_column (SourceView          *song_view,
	      const char          *title,
	      GtkTreeCellDataFunc  func,
	      int                  sort_id,
	      gboolean             pack_start,
	      gboolean             expand,
	      gboolean             visible,
	      float                xalign,
	      int                  default_width)
{
	GtkTreeView       *view;
	GtkCellRenderer   *cell;
	GtkTreeViewColumn *column;

	view = GTK_TREE_VIEW (song_view);
  
	cell = gtk_cell_renderer_text_new ();

	g_object_set (cell,
		      "xalign", xalign,
		      "ellipsize", PANGO_ELLIPSIZE_END,
		      NULL);
  
	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, title);

	if (pack_start) {
		gtk_tree_view_column_pack_start (column, cell, TRUE);
	} else {
		gtk_tree_view_column_pack_end (column, cell, TRUE);
	}
	
	gtk_tree_view_column_set_cell_data_func (column,
						 cell,
						 func,
						 song_view,
						 NULL);
	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_resizable (column, TRUE);

	/*gtk_tree_view_column_set_expand (column, expand);*/
	
	gtk_tree_view_column_set_clickable (column, TRUE);
	g_object_set_data (G_OBJECT (column), "sort_id", GINT_TO_POINTER (sort_id));
	g_signal_connect (column,
			  "clicked",
			  G_CALLBACK (song_column_clicked_cb),
			  view);
  
	gtk_tree_view_column_set_fixed_width (column, default_width);

	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);

	gtk_tree_view_column_set_visible (column, visible);

	return column;
}

static gboolean
drag_drop_cb (GtkWidget      *widget,
	      GdkDragContext *drag_context,
	      int             x,
	      int             y,
	      guint           time,
	      gpointer        user_data)
{
	g_signal_stop_emission_by_name (widget, "drag_drop");

	return TRUE;
}

static void
song_title_func (GtkTreeViewColumn *tree_column,
		 GtkCellRenderer   *cell,
		 GtkTreeModel      *tree_model,
		 GtkTreeIter       *iter,
		 gpointer           data)
{
	Song       *song;
	SourceView *view;

	song = source_model_get_song ((SourceModel *) tree_model, iter);

	view = data;

	g_object_set (cell,
		      "attributes", NULL,
		      "text", song_get_title (song),
		      NULL);
}

static void
song_playing_func (GtkTreeViewColumn *tree_column,
		   GtkCellRenderer   *cell,
		   GtkTreeModel      *tree_model,
		   GtkTreeIter       *iter,
		   gpointer           data)
{
	SourceView       *view;
	Song             *song, *playing_song;
	GdkPixbuf        *pixbuf;
	static GdkPixbuf *playing_pixbuf = NULL;
	static GdkPixbuf *paused_pixbuf = NULL;
	static GdkPixbuf *blank_pixbuf = NULL;

	view = data;

	song = source_model_get_song ((SourceModel *) tree_model, iter);

	if (!playing_pixbuf) {
		playing_pixbuf = gdk_pixbuf_new_from_file (DATADIR "/jamboree/jamboree-note.png", NULL);
		paused_pixbuf = gdk_pixbuf_new_from_file (DATADIR "/jamboree/jamboree-small-pause.png", NULL);
		blank_pixbuf = gdk_pixbuf_new_from_file (DATADIR "/jamboree/jamboree-small-blank.png", NULL);
	}

	pixbuf = NULL;
	
	playing_song = player_get_song ();
	if (playing_song == song) {
		switch (player_get_state ()) {
		case PLAYING_STATE_PLAYING:
			pixbuf = playing_pixbuf;
			break;
		case PLAYING_STATE_PAUSED:
			pixbuf = paused_pixbuf;
			break;
		default:
			break;
		}
	}

	if (!pixbuf) {
		pixbuf = blank_pixbuf;
	}
		
	g_object_set (cell, "pixbuf", pixbuf, NULL);
}

static void
song_artist_func (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer   *cell,
		  GtkTreeModel      *tree_model,
		  GtkTreeIter       *iter,
		  gpointer           data)
{
	Song *song;

	song = source_model_get_song ((SourceModel *) tree_model, iter);
	
	g_object_set (cell, "text", song_get_artist (song), NULL);
}

static void
song_album_func (GtkTreeViewColumn *tree_column,
		 GtkCellRenderer   *cell,
		 GtkTreeModel      *tree_model,
		 GtkTreeIter       *iter,
		 gpointer           data)
{
	Song *song;

	song = source_model_get_song ((SourceModel *) tree_model, iter);
	
	g_object_set (cell, "text", song_get_album (song), NULL);
}

static void
song_genre_func (GtkTreeViewColumn *tree_column,
		 GtkCellRenderer   *cell,
		 GtkTreeModel      *tree_model,
		 GtkTreeIter       *iter,
		 gpointer           data)
{
	Song *song;

	song = source_model_get_song ((SourceModel *) tree_model, iter);
	
	g_object_set (cell, "text", song_get_genre (song), NULL);
}

static void
song_year_func (GtkTreeViewColumn *tree_column,
		GtkCellRenderer   *cell,
		GtkTreeModel      *tree_model,
		GtkTreeIter       *iter,
		gpointer           data)
{
	Song *song;
	char  buf[32];
	
	song = source_model_get_song ((SourceModel *) tree_model, iter);

	if (song_get_year (song) > 0) {
		snprintf (buf, 31, "%d", (int) song_get_year (song));
		g_object_set (cell, "text", buf, NULL);
	} else {
		g_object_set (cell, "text", NULL, NULL);
	}
}

static void
song_time_func (GtkTreeViewColumn *tree_column,
		GtkCellRenderer   *cell,
		GtkTreeModel      *tree_model,
		GtkTreeIter       *iter,
		gpointer           data)
{
	Song *song;
	char *buf;

	song = source_model_get_song ((SourceModel *) tree_model, iter);

	if (song_get_duration (song) > 0) {
		buf = string_utils_format_duration (song_get_duration (song));
		g_object_set (cell, "text", buf, NULL);
		g_free (buf);
	} else {
		g_object_set (cell, "text", NULL, NULL);
	}
}

static void
song_rating_func (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer   *cell,
		  GtkTreeModel      *tree_model,
		  GtkTreeIter       *iter,
		  gpointer           data)
{
	Song *song;

	song = source_model_get_song ((SourceModel *) tree_model, iter);

	/* Doesn't do anything yet. */
  
	g_object_set (cell, "text", "", NULL);
}

static void
song_playcount_func (GtkTreeViewColumn *tree_column,
		     GtkCellRenderer   *cell,
		     GtkTreeModel      *tree_model,
		     GtkTreeIter       *iter,
		     gpointer           data)
{
	Song *song;
	char buf[32];
	int  count;
  
	song = source_model_get_song ((SourceModel *) tree_model, iter);
	count = song_get_playcount (song);

	if (count > 0) {
		snprintf (buf, 31, "%d", count);
		g_object_set (cell,
			      "text", buf,
			      NULL);
	} else {
		g_object_set (cell,
			      "text", NULL,
			      NULL);
	}
}

static void
song_last_played_func (GtkTreeViewColumn *tree_column,
		       GtkCellRenderer   *cell,
		       GtkTreeModel      *tree_model,
		       GtkTreeIter       *iter,
		       gpointer           data)
{
	Song      *song;
	time_t     t;
	struct tm *tm;
	char       buf[64];
  
	song = source_model_get_song ((SourceModel *) tree_model, iter);
	t = song_get_time_played (song);
	
	if (t > 0) {
		tm = localtime (&t);
		strftime (buf, sizeof (buf), _("%Y-%m-%d"), tm);
		g_object_set (cell,
			      "text", buf,
			      NULL);
	} else {
		g_object_set (cell,
			      "text", NULL,
			      NULL);
	}
}

static void
song_track_func (GtkTreeViewColumn *tree_column,
		 GtkCellRenderer   *cell,
		 GtkTreeModel      *tree_model,
		 GtkTreeIter       *iter,
		 gpointer           data)
{
	Song *song;
	int   track;
	char  buf[16];

	song = source_model_get_song ((SourceModel *) tree_model, iter);
	track = song_get_track (song);
	
	if (track > 0) {
		snprintf (buf, 4, "% 2d", track);
		g_object_set (cell, "text", buf, NULL);
	} else {
		g_object_set (cell, "text", NULL, NULL);
	}
}

static void
song_column_clicked_cb (GtkTreeViewColumn *column,
			SourceView      *view)
{
	GtkTreeModel *model;
	GtkSortType   order;
	SongSortId    id;
	gboolean      sorted;
	GList        *columns, *l;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));

	sorted = gtk_tree_view_column_get_sort_indicator (column);

	columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (view));
	for (l = columns; l; l = l->next) {
		gtk_tree_view_column_set_sort_indicator (l->data, FALSE); 
	}
	
	id = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (column), "sort_id"));

	if (sorted) {
		order = gtk_tree_view_column_get_sort_order (column);
		if (order == GTK_SORT_ASCENDING) {
			order = GTK_SORT_DESCENDING;
		} else {
			order = -1;
		}
	} else {
		order = GTK_SORT_ASCENDING;
	}
	
	if (order != -1) {
		gtk_tree_view_column_set_sort_indicator (column, TRUE);
		gtk_tree_view_column_set_sort_order (column, order);
  
		source_model_set_sorting (SOURCE_MODEL (model), id, order);
	} else {
		source_model_set_sorting (SOURCE_MODEL (model),
					     SONG_SORT_ID_DEFAULT,
					     GTK_SORT_ASCENDING);
	}
	
	g_list_free (columns);
}

GtkWidget *
source_view_new (void)
{
	return g_object_new (TYPE_SOURCE_VIEW, NULL);
}

const char *
song_sort_id_to_string (SongSortId sort_id)
{
	switch (sort_id) {
	case SONG_SORT_ID_TITLE:
		return "title";
	case SONG_SORT_ID_ARTIST:
		return "artist";
	case SONG_SORT_ID_TIME:
		return "time";
	case SONG_SORT_ID_ALBUM:
		return "album";
	case SONG_SORT_ID_GENRE:
		return "genre";
	case SONG_SORT_ID_YEAR:
		return "year";
	case SONG_SORT_ID_RATING:
		return "rating";
	case SONG_SORT_ID_TRACK:
		return "track";
	case SONG_SORT_ID_LAST_PLAYED:
		return "last_played";
	case SONG_SORT_ID_PLAYCOUNT:
		return "playcount";
      
	default:
		g_assert_not_reached();
		break;
	}
  
	return NULL;
}

static GtkTreeViewColumn *
get_column_by_sort_id (SourceView *view, SongSortId  sort_id)
{
	GList             *columns, *l;
	GtkTreeViewColumn *column = NULL;
	gpointer           data;
  
	columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (view));

	for (l = columns; l; l = l->next) {
		column = l->data;

		data = g_object_get_data (G_OBJECT (column), "sort_id");
		if (GPOINTER_TO_INT (data) == sort_id) {
 			break;
		}

		column = NULL;
	}

	g_list_free (columns);
  
	return column;
}

typedef struct {
	GtkTreeViewColumn *column;
	gboolean           state;
} ColumnToggledData;

static gboolean
column_toggled_idle_cb (ColumnToggledData *data)
{
	gtk_tree_view_column_set_visible (data->column, data->state);
	g_free (data);

	return FALSE;
}

static void
column_toggled_cb (GtkToggleButton   *button,
		   GtkTreeViewColumn *column)
{
	SongSortId         sort_id;
	const char        *sort_key;
	char              *key;
	ColumnToggledData *data;
 
	data = g_new0 (ColumnToggledData, 1);
	
	data->state = gtk_toggle_button_get_active (button);
	data->column = column;

	sort_id = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (column), "sort_id"));
	sort_key = song_sort_id_to_string (sort_id);

	key = g_strdup_printf ("/apps/jamboree/columns/%s_visible", sort_key);
	gconf_client_set_bool (gconf_client, key, data->state, NULL);
	g_free (key);

	g_idle_add ((GSourceFunc) column_toggled_idle_cb, data);
}

static void
hookup_column (SourceView      *view,
	       GtkTreeViewColumn *column,
	       GladeXML          *glade,
	       const char        *name)
{
	GtkWidget *w;

	w = glade_xml_get_widget (glade, name);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), gtk_tree_view_column_get_visible (column));
  
	g_signal_connect (w,
			  "toggled",
			  G_CALLBACK (column_toggled_cb),
			  column);
}

void
source_view_setup_columns (SourceView *view)
{
	int                id;
	GtkTreeViewColumn *column;
	char              *key;
	GError            *error = NULL;
	gboolean           visible;
	int                width;
	gboolean           broken;

	broken = FALSE;
	for (id = SONG_SORT_ID_DEFAULT + 1; id < SONG_SORT_ID_NO_COLUMNS; id++) {
		/* Title column should always be visible. */
		if (id == SONG_SORT_ID_TITLE) {
			visible = TRUE;
		} else {
			key = g_strdup_printf ("/apps/jamboree/columns/%s_visible", song_sort_id_to_string (id));
			visible = gconf_client_get_bool (gconf_client, key, &error);
			g_free (key);
	  
			if (error) {
				broken = TRUE;

				/* Make broken installations look a bit better. */
				if (id == SONG_SORT_ID_ARTIST || id == SONG_SORT_ID_ALBUM ||
				    id == SONG_SORT_ID_TIME) {
					visible = TRUE;
				} else {
					visible = FALSE;
				}
				
				g_error_free (error);

			}
		}
		
		column = get_column_by_sort_id (view, id);
		gtk_tree_view_column_set_visible (column, visible);
		
		key = g_strdup_printf ("/apps/jamboree/columns/%s_width",
				       song_sort_id_to_string (id));
		width = gconf_client_get_int (gconf_client, key, &error);
		g_free (key);
		
		if (error){
			g_error_free (error);
		}
		else if (width > 0) {
			gtk_tree_view_column_set_fixed_width (column, width);
		}
	}

	if (broken) {
		g_warning ("Schema installation broken? Can't get visible columns.");
	}
}

void
source_view_store_columns_widths (SourceView *view)
{
	GList *columns, *l;
	int    id;
	char  *key;
	int    width;
  
	columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (view));
  
	for (l = columns; l; l = l->next) {
		id = GPOINTER_TO_INT (g_object_get_data (l->data, "sort_id"));
      
		key = g_strdup_printf ("/apps/jamboree/columns/%s_width", song_sort_id_to_string (id));
		width = gtk_tree_view_column_get_width (l->data);
		gconf_client_set_int (gconf_client, key, width, NULL);
		g_free (key);
	}

	g_list_free (columns);
}

void
source_view_column_chooser (SourceView *view,
			       GtkWindow    *parent)
{
	static GtkWidget *window = NULL;
	GladeXML *glade;
	GList *columns, *l;
  
	if (window) {
		if (parent) {
			gtk_window_set_transient_for (GTK_WINDOW (window), parent);
		}
		
		gtk_window_present (GTK_WINDOW (window));
		return;
	}
  
	glade = glade_xml_new (DATADIR "/jamboree/jamboree.glade", NULL, NULL);

	window = glade_xml_get_widget (glade, "column_chooser_dialog");
	if (parent) {
		gtk_window_set_transient_for (GTK_WINDOW (window), parent);
	}
	
	g_signal_connect_swapped (glade_xml_get_widget (glade, "close_button"),
				  "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  window);
  
	g_object_add_weak_pointer (G_OBJECT (window), (gpointer) &window);

	columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (view));

	l = columns;
	/*hookup_column (view, l->data, glade, "playing_checkbutton");*/

	hookup_column (view, l->data, glade, "track_number_checkbutton");

	l = l->next;
	/*hookup_column (view, l->data, glade, "title_checkbutton");*/

	l = l->next;
	hookup_column (view, l->data, glade, "album_checkbutton");

	l = l->next;
	hookup_column (view, l->data, glade, "artist_checkbutton");

	l = l->next;
	hookup_column (view, l->data, glade, "genre_checkbutton");

	l = l->next;
	hookup_column (view, l->data, glade, "duration_checkbutton");

	l = l->next;
	hookup_column (view, l->data, glade, "year_checkbutton");
  
	l = l->next;
	hookup_column (view, l->data, glade, "rating_checkbutton");

	l = l->next;
	hookup_column (view, l->data, glade, "playcount_checkbutton");

	l = l->next;
	hookup_column (view, l->data, glade, "last_played_checkbutton");

	gtk_widget_show (window);
  
	g_object_unref (glade);

	g_list_free (columns);
}

static void
drag_data_get (GtkWidget        *widget,
	       GdkDragContext   *context,
	       GtkSelectionData *data,
	       guint             info,
	       guint             time)
{
	if (info == TARGET_UTF8_STRING) {
		GtkTreeSelection *selection;
		GList            *list, *l;
		GtkTreeModel     *model;
		GtkTreeIter       iter;
		Song             *song;
		GString          *string = NULL;
		gboolean          first = TRUE;
      
		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (widget));

		list = gtk_tree_selection_get_selected_rows (selection, &model);

		if (list) {
			string = g_string_new (NULL);
		}
		
		for (l = list; l; l = l->next) {
			gtk_tree_model_get_iter (model, &iter, l->data);
	  
			song = source_model_get_song (SOURCE_MODEL (model), &iter);
	  
			if (first) {
				first = FALSE;
			} else {
				g_string_append (string, ", ");
			}
			
			g_string_append_printf (string, "%s - %s",
						song_get_title (song),
						song_get_artist (song));
		}

		g_list_foreach (list, (GFunc) gtk_tree_path_free, NULL);
		g_list_free (list);
      
		if (string) {
			gtk_selection_data_set_text (data, string->str, -1);
			g_signal_stop_emission_by_name (widget, "drag_data_get");
			g_string_free (string, TRUE);
	  
			return;
		}
	}

	if (GTK_WIDGET_CLASS (source_view_parent_class)->drag_data_get) {
		GTK_WIDGET_CLASS (source_view_parent_class)->drag_data_get (widget, context, data, info, time);
	}
}

