/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtkmain.h>
#include "main-window.h"
#include "player.h"
#include "dbus.h"

static void              server_unregistered_func (DBusConnection *connection,
						   gpointer        user_data);
static DBusHandlerResult server_message_func      (DBusConnection *connection,
						   DBusMessage    *message,
						   gpointer        user_data);
static gboolean          message_append_song      (DBusMessage    *message,
						   Song           *song);


static DBusConnection *bus_conn;

static DBusObjectPathVTable server_vtable = {
	server_unregistered_func,
	server_message_func,
	NULL,
};


gboolean
jamboree_dbus_init_service (void)
{
	DBusError       error;
	static gboolean inited = FALSE;
  
	if (inited) {
		return TRUE;
	}
	
	inited = TRUE;
  
	dbus_error_init (&error);
	bus_conn = dbus_bus_get (DBUS_BUS_SESSION, &error);
  
	if (!bus_conn) {
		g_warning ("Failed to connect to the D-BUS daemon: %s", error.message);
		dbus_error_free (&error);
		return FALSE;
	}
  
	dbus_bus_acquire_service (bus_conn, JAMBOREE_DBUS_SERVICE, 0, &error);
	if (dbus_error_is_set (&error)) {
		g_warning ("Failed to acquire player service");
		dbus_error_free (&error);
		return FALSE;
	}

	if (!dbus_connection_register_object_path (bus_conn,
						   JAMBOREE_PLAYER_OBJECT,
						   &server_vtable,
						   NULL)) {
		g_warning ("Failed to register server object with the D-BUS bus daemon");
		return FALSE;
	}
  
	dbus_connection_setup_with_g_main (bus_conn, NULL);

	return TRUE;
}

static gboolean
init_client (void)
{
	DBusError error;
	static gboolean inited = FALSE;

	if (inited) {
		return TRUE;
	}
	
	inited = TRUE;
  
	dbus_error_init (&error);
	bus_conn = dbus_bus_get (DBUS_BUS_SESSION, &error);
  
	if (!bus_conn) {
		g_warning ("Failed to connect to the D-BUS daemon: %s", error.message);
		dbus_error_free (&error);
		return FALSE;
	}
  
	dbus_connection_setup_with_g_main (bus_conn, NULL);

	return TRUE;
}

static void
server_unregistered_func (DBusConnection *connection, void *user_data)
{
}

static void
send_ok_reply (DBusConnection *connection,
	       DBusMessage    *message)
{
	DBusMessage *reply;

	reply = dbus_message_new_method_return (message);
                                                                                
	dbus_connection_send (connection, reply, NULL);
	dbus_message_unref (reply);
}

static void
handle_play (DBusConnection *connection,
	     DBusMessage    *message,
	     MainWindow     *window)
{
	main_window_handle_play (window, FALSE);
	send_ok_reply (connection, message);
}

static void
handle_play_song (DBusConnection *connection,
		  DBusMessage    *message,
		  MainWindow     *window)
{
	Source *source;
	gchar  *str;
	GList  *songs, *l;
	Song   *song;

	source = main_window_get_database (window);

	dbus_message_get_args (message, NULL,
			       DBUS_TYPE_STRING, &str,
			       DBUS_TYPE_INVALID);

	songs = source_get_songs (source);

	main_window_set_selected_source (window, source);
	
	song = NULL;
	for (l = songs; l; l = l->next) {
		song = l->data;

		if (strcmp (song_get_title (song), str) == 0) {
			break;
		}

		song = NULL;
	}

	dbus_free (str);

	if (song) {
		main_window_handle_play_song (window, song);
	}
	
	send_ok_reply (connection, message);
}

static void
handle_pause (DBusConnection *connection,
	      DBusMessage    *message,
	      MainWindow     *window)
{
	main_window_handle_pause (window);
	send_ok_reply (connection, message);
}

static void
handle_stop (DBusConnection *connection,
	     DBusMessage    *message,
	     MainWindow     *window)
{
	main_window_handle_stop (window);
	send_ok_reply (connection, message);
}


static void
handle_prev (DBusConnection *connection,
	     DBusMessage    *message,
	     MainWindow     *window)
{
	main_window_handle_prev (window);
	send_ok_reply (connection, message);
}

static void
handle_next (DBusConnection *connection,
	     DBusMessage    *message,
	     MainWindow     *window)
{
	main_window_handle_next (window);
	send_ok_reply (connection, message);
}

static void
handle_quit (DBusConnection *connection,
	     DBusMessage    *message,
	     MainWindow     *window)
{
	send_ok_reply (connection, message);

	dbus_connection_flush (connection);
  
	gtk_main_quit ();
}

static void
handle_get_current_song (DBusConnection *connection,
			 DBusMessage    *message,
			 MainWindow     *window)
{
	DBusMessage *reply;
	Song        *song;
	
	song = main_window_get_current_song (window);
	
	reply = dbus_message_new_method_return (message);
	
	message_append_song (reply, song);
	
	dbus_connection_send (connection, reply, NULL);
	dbus_message_unref (reply);
}

static void
handle_get_all_songs (DBusConnection *connection,
		      DBusMessage    *message,
		      MainWindow     *window)
{
	DBusMessage *reply;
	Source      *source;
	GList       *songs, *l;
	Song        *song;

	source = main_window_get_database (window);

	songs = source_get_songs (source);

	reply = dbus_message_new_method_return (message);

	for (l = songs; l; l = l->next) {
		song = l->data;
		
		message_append_song (reply, song);
	}
	
	dbus_connection_send (connection, reply, NULL);
	dbus_message_unref (reply);
}

static void
handle_set_window_visiblity (DBusConnection *connection,
			     DBusMessage    *message,
			     MainWindow     *window)
{
	dbus_bool_t visible;

	if (dbus_message_get_args (message, NULL,
				   DBUS_TYPE_BOOLEAN, &visible,
				   DBUS_TYPE_INVALID)) {
		if (visible) {
			gtk_window_present (GTK_WINDOW (window));
		} else {  
			gtk_widget_hide (GTK_WIDGET (window));
		}
	
		send_ok_reply (connection, message);
	}

	/* FIXME: Should send error back if the arguments were incorrect. */
}

static void
handle_get_window_visiblity (DBusConnection *connection,
			     DBusMessage    *message,
			     MainWindow     *window)
{
	DBusMessage *reply;
	
	reply = dbus_message_new_method_return (message);
	
	dbus_message_append_args (reply,
				  DBUS_TYPE_BOOLEAN, main_window_get_is_visible (window),
				  DBUS_TYPE_INVALID);
	
	dbus_connection_send (connection, reply, NULL);
	dbus_message_unref (reply);
}

static void
handle_get_random (DBusConnection *connection,
                   DBusMessage    *message,
                   MainWindow     *window)
{
	DBusMessage *reply;
        gboolean     random;
	
	reply = dbus_message_new_method_return (message);

        random = main_window_get_random (window);
        
	dbus_message_append_args (reply,
                                  DBUS_TYPE_BOOLEAN, random,
                                  DBUS_TYPE_INVALID);
	
	dbus_connection_send (connection, reply, NULL);
	dbus_message_unref (reply);
}

static void
handle_set_random (DBusConnection *connection,
                   DBusMessage    *message,
                   MainWindow     *window)
{
	dbus_bool_t random;

	if (dbus_message_get_args (message, NULL,
				   DBUS_TYPE_BOOLEAN, &random,
				   DBUS_TYPE_INVALID)) {
		main_window_set_random (window, random);
		send_ok_reply (connection, message);
	}

        /* FIXME: error message. */
}

static void
handle_get_repeat (DBusConnection *connection,
                   DBusMessage    *message,
                   MainWindow     *window)
{
	DBusMessage *reply;
        gboolean     repeat;
	
	reply = dbus_message_new_method_return (message);

        repeat = main_window_get_random (window);
        
	dbus_message_append_args (reply,
                                  DBUS_TYPE_BOOLEAN, repeat,
                                  DBUS_TYPE_INVALID);
	
	dbus_connection_send (connection, reply, NULL);
	dbus_message_unref (reply);
}

static void
handle_set_repeat (DBusConnection *connection,
                   DBusMessage    *message,
                   MainWindow     *window)
{
	dbus_bool_t repeat;

	if (dbus_message_get_args (message, NULL,
				   DBUS_TYPE_BOOLEAN, &repeat,
				   DBUS_TYPE_INVALID)) {
		main_window_set_repeat (window, repeat);
		send_ok_reply (connection, message);
	}

        /* FIXME: error message. */
}

static DBusHandlerResult
server_message_func (DBusConnection *connection,
		     DBusMessage    *message,
		     gpointer        user_data)
{
	MainWindow *window;
  
	window = main_window_get ();

	if (dbus_message_is_method_call (message,
					 JAMBOREE_PLAYER_INTERFACE,
					 JAMBOREE_PLAYER_PLAY)) {
		handle_play (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_PLAY_SONG)) {
		handle_play_song (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_STOP)) {
	        handle_stop (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_PAUSE)) {
		handle_pause (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_PREV)) {
		handle_prev (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_NEXT)) {
		handle_next (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_QUIT)) {
		handle_quit (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_SET_WINDOW_VISIBILITY)) {
		handle_set_window_visiblity (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_GET_WINDOW_VISIBILITY)) {
		handle_get_window_visiblity (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_GET_CURRENT_SONG)) {
		handle_get_current_song (connection, message, window);
        }
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_GET_ALL_SONGS)) {
		handle_get_all_songs (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_GET_RANDOM)) {
		handle_get_random (connection, message, window);
        }
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_SET_RANDOM)) {
		handle_set_random (connection, message, window);
	}
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_GET_REPEAT)) {
		handle_get_repeat (connection, message, window);
        }
	else if (dbus_message_is_method_call (message,
					      JAMBOREE_PLAYER_INTERFACE,
					      JAMBOREE_PLAYER_SET_REPEAT)) {
		handle_set_repeat (connection, message, window);
	} else {
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	}

	return DBUS_HANDLER_RESULT_HANDLED;
}

gboolean
jamboree_dbus_send_remote_cmd (const char *arg, gboolean *invalid_arg)
{
	DBusMessage *message;
	DBusMessage *reply;
	DBusError    error;
	const char  *msg = NULL;

	/* This is ugly, but will do for testing. Clean up when the API is more
	 * fleshed out.
	 */

	if (invalid_arg) {
		*invalid_arg = FALSE;
	}
	
	if (!init_client ()) {
		return FALSE;
	}

	if (strcmp (arg, "play") == 0) {
		msg = JAMBOREE_PLAYER_PLAY;
	}
	else if (strcmp (arg, "stop") == 0) {
		msg = JAMBOREE_PLAYER_STOP;
	}
	else if (strcmp (arg, "prev") == 0) {
		msg = JAMBOREE_PLAYER_PREV;
	}
	else if (strcmp (arg, "next") == 0) {
		msg = JAMBOREE_PLAYER_NEXT;
	}
	else if (strcmp (arg, "quit") == 0) {
		msg = JAMBOREE_PLAYER_QUIT;
	}
	else if (strcmp (arg, "show-window") == 0) {
		msg = JAMBOREE_PLAYER_SET_WINDOW_VISIBILITY;
	}
	else if (strcmp (arg, "hide-window") == 0) {
		msg = JAMBOREE_PLAYER_SET_WINDOW_VISIBILITY;
	} else {
		if (invalid_arg) {
			*invalid_arg = TRUE;
		}
		return FALSE;
	}
  
	g_assert (msg != NULL);
  
	message = dbus_message_new_method_call (JAMBOREE_DBUS_SERVICE,
						JAMBOREE_PLAYER_OBJECT,
						JAMBOREE_PLAYER_INTERFACE,
						msg);

	if (strcmp (arg, "show-window") == 0) {
		dbus_message_append_args (message,
					  DBUS_TYPE_BOOLEAN, TRUE,
					  DBUS_TYPE_INVALID);
	}
	else if (strcmp (arg, "hide-window") == 0) {
		dbus_message_append_args (message,
					  DBUS_TYPE_BOOLEAN, FALSE,
					  DBUS_TYPE_INVALID);
	}

	dbus_error_init (&error);
  
	reply = dbus_connection_send_with_reply_and_block (bus_conn,
							   message, -1,
							   &error);
  
	dbus_message_unref (message);

	if (dbus_error_is_set (&error)) {
		/*g_warning ("Failed to send message \"%s\": %s", msg, error.message);*/
		dbus_error_free (&error);
		return FALSE;
	}
  
	dbus_message_unref (reply);
  
	return TRUE;
}

DBusMessage *
jamboree_dbus_send_remote_cmd_with_reply (const char *msg)
{
	DBusMessage *message;
	DBusMessage *reply;
	DBusError    error;

	if (!init_client ()) {
 		return NULL;
	}
  
	message = dbus_message_new_method_call (JAMBOREE_DBUS_SERVICE,
						JAMBOREE_PLAYER_OBJECT,
						JAMBOREE_PLAYER_INTERFACE,
						msg);
      
	dbus_error_init (&error);
  
	reply = dbus_connection_send_with_reply_and_block (bus_conn,
							   message, 1000,
							   &error);
  
	dbus_message_unref (message);

	if (dbus_error_is_set (&error)) {
		g_warning ("Failed to send message '%s': %s", msg, error.message);
		dbus_error_free (&error);
		return NULL;
	}
  
	return reply;
}

#if 0
Song *
jamboree_dbus_message_to_song (DBusMessage *message)
{
	DBusMessageIter iter, dict;
	Song *song;
	char *title = NULL, *artist = NULL, *album = NULL;
	unsigned char *filename = NULL;
	char *key;
	int len;
    
	dbus_message_iter_init (message, &iter);
	dbus_message_iter_init_dict_iterator (&iter, &dict);

	song = g_new0 (Song, 1);

	while ((key = dbus_message_iter_get_dict_key (&dict))) {
		if (!title && strcmp (key, "title") == 0)
			title = dbus_message_iter_get_string (&dict);
		else if (!filename && strcmp (key, "filename") == 0)
			dbus_message_iter_get_byte_array (&dict, &filename, &len);
		else if (!artist && strcmp (key, "artist") == 0)
			artist = dbus_message_iter_get_string (&dict);
		else if (!album && strcmp (key, "album") == 0)
			album = dbus_message_iter_get_string (&dict);
      
		dbus_free (key);

		if (!dbus_message_iter_next (&dict))
			break;
	}      

	_song_set_title (song, g_strdup (title));
	_song_set_filename (song, g_strndup (filename, len)); /* leak */
	_song_set_artist (song, g_strdup (artist));

	dbus_free (title);
	dbus_free (filename);
	dbus_free (artist);
  
	return song;
}
#endif

static gboolean
message_append_song (DBusMessage *message, Song *song)
{
	DBusMessageIter iter, dict;
  
	dbus_message_append_iter_init (message, &iter);

	dbus_message_iter_append_dict (&iter, &dict);  

	dbus_message_iter_append_dict_key (&dict, "title");
	dbus_message_iter_append_string (&dict, song_get_title (song));

	dbus_message_iter_append_dict_key (&dict, "artist");
	dbus_message_iter_append_string (&dict, song_get_artist (song));

	dbus_message_iter_append_dict_key (&dict, "album");
	dbus_message_iter_append_string (&dict, song_get_album (song));

	/*  dbus_message_iter_append_dict_key (&dict, "filename");
	    dbus_message_iter_append_byte_array (&dict, (unsigned const char*)song->filename,
	    strlen (song->filename));
	*/
	dbus_message_iter_append_dict_key (&dict, "length");
	dbus_message_iter_append_int32 (&dict, song_get_duration (song));
  
	return TRUE;
}

void
jamboree_dbus_emit_playing_song (Song *song)
{
	DBusMessage *message;

	if (bus_conn == NULL) {
		return;
	}

	message = dbus_message_new_signal (JAMBOREE_PLAYER_OBJECT,
					   JAMBOREE_PLAYER_INTERFACE,
					   JAMBOREE_PLAYER_PLAYING);

	message_append_song (message, song);
	dbus_connection_send (bus_conn, message, NULL);
	dbus_message_unref (message);
}

void
jamboree_dbus_emit_state (PlayingState state)
{
	DBusMessage *message;
        const char  *state_str;

	if (bus_conn == NULL) {
		return;
	}

	message = dbus_message_new_signal (JAMBOREE_PLAYER_OBJECT,
					   JAMBOREE_PLAYER_INTERFACE,
					   JAMBOREE_PLAYER_STATE);

        switch (state) {
        case PLAYING_STATE_PLAYING:
                state_str = "playing";
                break;
        case PLAYING_STATE_STOPPED:
                state_str = "stopped";
                break;
        case PLAYING_STATE_PAUSED:
                state_str = "paused";
                break;
        default:
                return;
        }
        
        dbus_message_append_args (message, 
                                  DBUS_TYPE_STRING, state_str,
                                  DBUS_TYPE_INVALID);

	dbus_connection_send (bus_conn, message, NULL);
	dbus_message_unref (message);
}

