/*
 * Copyright (C) 2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __SOURCE_SMART_PLAYLIST_H__
#define __SOURCE_SMART_PLAYLIST_H__

#include "source-proxy.h"
#include "expr.h"
#include "limiter.h"

#define TYPE_SOURCE_SMART_PLAYLIST            (source_smart_playlist_get_type ())
#define SOURCE_SMART_PLAYLIST(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SOURCE_SMART_PLAYLIST, SourceSmartPlaylist))
#define SOURCE_SMART_PLAYLIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SOURCE_SMART_PLAYLIST, SourceSmartPlaylistClass))
#define IS_SOURCE_SMART_PLAYLIST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SOURCE_SMART_PLAYLIST))
#define IS_SOURCE_SMART_PLAYLIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SOURCE_SMART_PLAYLIST))
#define SOURCE_SMART_PLAYLIST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SOURCE_SMART_PLAYLIST, SourceSmartPlaylistClass))


typedef struct _SourceSmartPlaylist      SourceSmartPlaylist;
typedef struct _SourceSmartPlaylistClass SourceSmartPlaylistClass;
typedef struct _SourceSmartPlaylistPriv  SourceSmartPlaylistPriv;

struct _SourceSmartPlaylist {
	SourceProxy              parent;
	SourceSmartPlaylistPriv *priv;
};

struct _SourceSmartPlaylistClass {
	SourceProxyClass parent_class;
};

GType    source_smart_playlist_get_type       (void) G_GNUC_CONST;
Source * source_smart_playlist_new            (Source              *source,
					       const char          *name);
void     source_smart_playlist_set_exprs      (SourceSmartPlaylist *playlist,
					       GList               *exprs);
GList *  source_smart_playlist_get_exprs      (SourceSmartPlaylist *playlist);
void     source_smart_playlist_set_limiter    (SourceSmartPlaylist *playlist,
					       Limiter             *limiter);
Limiter *source_smart_playlist_get_limiter    (SourceSmartPlaylist *playlist);
void     source_smart_playlist_set_op         (SourceSmartPlaylist *playlist,
					       ExprOp               op);
ExprOp   source_smart_playlist_get_op         (SourceSmartPlaylist *playlist);
gboolean source_smart_playlist_song_evaluates (SourceSmartPlaylist *playlist,
					       Song                *song);


#endif /* __SOURCE_SMART_PLAYLIST_H__ */
