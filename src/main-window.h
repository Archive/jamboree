/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __MAIN_WINDOW_H__
#define __MAIN_WINDOW_H__

#include <gtk/gtkwindow.h>
#include "source.h"

#define TYPE_MAIN_WINDOW            (main_window_get_type ())
#define MAIN_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MAIN_WINDOW, MainWindow))
#define MAIN_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MAIN_WINDOW, MainWindowClass))
#define IS_MAIN_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MAIN_WINDOW))
#define IS_MAIN_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MAIN_WINDOW))
#define MAIN_WINDOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MAIN_WINDOW, MainWindowClass))

typedef struct _MainWindow      MainWindow;
typedef struct _MainWindowClass MainWindowClass;
typedef struct _MainWindowPriv  MainWindowPriv;

struct _MainWindow {
	GtkWindow       parent;
	MainWindowPriv *priv;
};

struct _MainWindowClass {
	GtkWindowClass parent_class;
};

GType       main_window_get_type              (void) G_GNUC_CONST;
GtkWidget * main_window_new                   (Source     *database);
MainWindow *main_window_get                   (void);
void        main_window_handle_play           (MainWindow *window,
					       gboolean    toggle);
void        main_window_handle_play_song      (MainWindow *window,
					       Song       *song);
void        main_window_handle_pause          (MainWindow *window);
void        main_window_handle_next           (MainWindow *window);
void        main_window_handle_prev           (MainWindow *window);
void        main_window_handle_stop           (MainWindow *window);
Song *      main_window_get_current_song      (MainWindow *window);
void        main_window_toggle_visibility     (MainWindow *window);
gboolean    main_window_get_is_visible        (MainWindow *window);
void        main_window_present               (MainWindow *window);
void        main_window_select_all            (MainWindow *window);
void        main_window_select_playing_artist (MainWindow *window);
void        main_window_select_playing_album  (MainWindow *window);
Song *      main_window_get_prev_song         (MainWindow *window,
					       Song       *song);
Song *      main_window_get_next_song         (MainWindow *window,
					       Song       *song);
void        main_window_set_random            (MainWindow *window,
					       gboolean    random);
gboolean    main_window_get_random            (MainWindow *window);
void        main_window_set_repeat            (MainWindow *window,
					       gboolean    random);
gboolean    main_window_get_repeat            (MainWindow *window);
Source *    main_window_get_database          (MainWindow *window);
void        main_window_set_selected_source   (MainWindow *window,
					       Source     *source);

#endif /* __MAIN_WINDOW_H__ */
