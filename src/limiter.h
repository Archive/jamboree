/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __LIMITER_H__
#define __LIMITER_H__

#include <glib.h>

typedef struct _Limiter Limiter;

typedef enum  {
	LIMITER_VARIABLE_NONE = 0,
	
	LIMITER_VARIABLE_DATE_ADDED,
	LIMITER_VARIABLE_MOST_RECENTLY_PLAYED,
	LIMITER_VARIABLE_LEAST_RECENTLY_PLAYED,
	LIMITER_VARIABLE_LAST_PLAYED,
	LIMITER_VARIABLE_RATING,
	LIMITER_VARIABLE_PLAYCOUNT,
	LIMITER_VARIABLE_QUALITY,
	LIMITER_VARIABLE_RANDOM,
	LIMITER_VARIABLE_YEAR
} LimiterVariable;

/* Only for the UI. */
typedef enum {
	LIMITER_UNIT_NONE = 0,
	
	LIMITER_UNIT_SONGS,
	LIMITER_UNIT_MINUTES,
	LIMITER_UNIT_HOURS,
	LIMITER_UNIT_MB,
	LIMITER_UNIT_GB
} LimiterUnit;


Limiter *       limiter_new                  (LimiterVariable  variable,
					      LimiterUnit      unit,
					      long             value);
void            limiter_free                 (Limiter         *limiter);
void            limiter_set_variable         (Limiter         *limiter,
					      LimiterVariable  variable);
LimiterVariable limiter_get_variable         (Limiter         *limiter);
void            limiter_set_unit             (Limiter         *limiter,
					      LimiterUnit      unit);
LimiterUnit     limiter_get_unit             (Limiter         *limiter);
void            limiter_set_value            (Limiter         *limiter,
					      long             value);
long            limiter_get_value            (Limiter         *limiter);
GList *         limiter_apply_limits         (Limiter         *limiter,
					      GList           *songs);
const char *    limiter_unit_to_string       (LimiterUnit      unit);
LimiterUnit     limiter_unit_from_string     (const char      *str);
const char *    limiter_variable_to_string   (LimiterVariable  variable);
LimiterVariable limiter_variable_from_string (const char      *str);
char *          limiter_to_xml               (Limiter         *limiter);


#endif /* __LIMITER_H__ */
