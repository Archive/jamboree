/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SOURCE_LIST_VIEW_H__
#define __SOURCE_LIST_VIEW_H__

#include <gtk/gtktreeview.h>
#include "source.h"

void     sources_view_setup               (GtkTreeView *tree,
					   Source      *source);
Source * sources_view_get_selected        (GtkTreeView *tree);
void     sources_view_set_selected        (GtkTreeView *tree,
					   Source      *source);
void     sources_view_append              (GtkTreeView *tree,
					   Source      *list);
void     sources_view_append_and_edit     (GtkTreeView *tree,
					   Source      *list);
void     sources_view_remove              (GtkTreeView *tree,
					   Source      *list);


#endif /* __SOURCE_LIST_VIEW_H__ */
