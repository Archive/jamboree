/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <glib/gi18n.h>
#include "string-utils.h"

#define MINUTE_FACTOR (60.0)
#define HOUR_FACTOR   (60.0 * 60.0)
#define DAY_FACTOR    (60.0 * 60.0 * 24.0)
#define WEEK_FACTOR   (60.0 * 60.0 * 24.0 * 7.0) 

static GHashTable *strings = NULL;

struct _SharedString {
	int   refcount;
	char *str;
	char *collated;
	char *folded;
};

void
shared_string_init (void)
{
	if (strings == NULL) {
		strings = g_hash_table_new (g_str_hash, g_str_equal);
	}
}

static void
dump_foreach (gpointer key, gpointer value, gpointer user_data)
{
	SharedString *string;

	string = value;

	g_print ("%s\n", string->str);
}

/* For memleak finding purposes. */
void
shared_string_shutdown (void)
{
	int n_entries;
  
	if (strings) {
		n_entries = g_hash_table_size (strings);
		if (n_entries != 0) {
			g_warning ("%d string entries left.", n_entries);
			if (0) {
				g_hash_table_foreach (strings, dump_foreach, NULL);
			}
		}
		
		g_hash_table_destroy (strings);
		
		strings = NULL;
	}
}

static void
shared_string_destroy (SharedString *string)
{
	g_assert (string->refcount == 0);

	g_hash_table_remove (strings, string->str);
  
	g_free (string->str);
	g_free (string->folded);
	g_free (string->collated);
	g_free (string);
}

SharedString *
shared_string_ref (SharedString *string)
{
	string->refcount++;

	return string;
}

void
shared_string_unref (SharedString *string)
{
	string->refcount--;

	if (string->refcount == 0) {
		shared_string_destroy (string);
	}
}

/* Note: The shared string owns the indata string after this call. It might be
 * freed, so don't touch it.
 */
SharedString *
shared_string_add (char *str)
{
	SharedString *string;
  
	string = g_hash_table_lookup (strings, str ? str : "");
	if (string) {
		g_free (str);
		return shared_string_ref (string);
	}
  
	string = g_new0 (SharedString, 1);
	string->str = str ? str : g_strdup ("");
	string->refcount = 1;

	g_hash_table_insert (strings, string->str, string);

	return string;
}

SharedString *
shared_string_add_const (const char *str)
{
	SharedString *string;
  
	string = g_hash_table_lookup (strings, str ? str : "");
	if (string) {
		return shared_string_ref (string);
	}
  
	string = g_new0 (SharedString, 1);
	string->str = str ? g_strdup (str) : g_strdup ("");
	string->refcount = 1;

	g_hash_table_insert (strings, string->str, string);

	return string;
}

const char *
shared_string_get_str (SharedString *string)
{
	return string->str;
}

const char *
shared_string_get_collated (SharedString *string)
{
	if (G_UNLIKELY (string->collated == NULL)) {
		string->collated = string_utils_create_collate_key (string->str);
	}
	
	return string->collated;
}

const char *
shared_string_get_folded (SharedString *string)
{
	if (G_UNLIKELY (string->folded == NULL)) {
		string->folded = g_utf8_casefold (string->str, -1);
	}
	
	return string->folded;
}

char *
string_utils_create_collate_key (const char *p)
{
	GString  *string;
	gunichar  c;
	gboolean  is_space;
	gboolean  last_was_space;
	char     *ret;

	g_return_val_if_fail (p != NULL, NULL);
	
	string = g_string_new (NULL);

	if (g_ascii_strncasecmp (p, "the ", 4) == 0) {
		p += 4;
	}

	is_space = FALSE;
	last_was_space = FALSE;
	
	while (*p) {
		c = g_utf8_get_char (p);
		if (g_unichar_isspace (c)) {
			if (last_was_space) {
				p = g_utf8_next_char (p);
				continue;
			}
	  
			is_space = TRUE;
		}
      
		if (c == '(' || c == ')' || c == '"' || c == '\'' || c == '-' ||
		    c == '.' || c == '[' || c == ']' || c == '{' || c == '}') {
			if (!last_was_space) {
				g_string_append_c (string, ' ');
			}

			last_was_space = TRUE;
		} else {
			g_string_append_unichar (string, c);
			last_was_space = is_space;
		}
		
		p = g_utf8_next_char (p);
	}
	
	ret = g_utf8_collate_key (string->str, -1);
	g_string_free (string, TRUE);
	
	return ret;
}

char *
string_utils_format_duration (int seconds)
{
	if (seconds <= 0) {
		return g_strdup ("0:00");
	}
	else if (seconds > 3600) {
		return g_strdup_printf ("%d:%02d:%02d",
					seconds / 3600,
					(seconds % 3600) / 60,
					(seconds % 3600) % 60);
	} else {
		return g_strdup_printf ("%d:%02d", seconds / 60, seconds % 60);
	}
}

char *
string_utils_format_duration_long (int seconds)
{
	int    i;
	double d;
	
	if (seconds < MINUTE_FACTOR) {
		return g_strdup_printf (ngettext ("%d second", "%d seconds", seconds), seconds);
	}
	else if (seconds < HOUR_FACTOR) {
		i = floor (seconds / MINUTE_FACTOR + 0.5);
		
		return g_strdup_printf (ngettext ("%d minute", "%d minutes", i), i);
	}
	else if (seconds < DAY_FACTOR) {
		d = seconds / HOUR_FACTOR;
		
		return g_strdup_printf (ngettext ("%.1f hour", "%.1f hours", d), d);
	}
	else if (seconds < WEEK_FACTOR) {
		d = seconds / DAY_FACTOR;
		
		return g_strdup_printf (ngettext ("%.1f day", "%.1f days", d), d);
	} else {
		d = seconds / WEEK_FACTOR;
	  
		return g_strdup_printf (ngettext ("%.1f week", "%.1f weeks", d), d);
	}
}

