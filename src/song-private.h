/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SONG_PRIVATE_H__
#define __SONG_PRIVATE_H__

#include <glib.h>
#include "song.h"

gboolean _song_set_path          (Song       *song,
				  const char *str);
gboolean _song_set_title         (Song       *song,
				  char       *str);
gboolean _song_set_album         (Song       *song,
				  char       *str);
gboolean _song_set_album_const   (Song       *song,
				  const char *str);
gboolean _song_set_artist        (Song       *song,
				  char       *str);
gboolean _song_set_artist_const  (Song       *song,
				  const char *str);
gboolean _song_set_genre         (Song       *song,
				  char       *str);
gboolean _song_set_genre_const   (Song       *song,
				  const char *str);
gboolean _song_set_duration      (Song       *song,
				  int         duration);
gboolean _song_set_playcount     (Song       *song,
				  int         playcount);
gboolean _song_set_time_played   (Song       *song,
				  time_t      t);
gboolean _song_set_bitrate       (Song       *song,
				  int         bitrate);
gboolean _song_set_samplerate    (Song       *song,
				  int         samplerate);
gboolean _song_set_track         (Song       *song,
				  int         track);
gboolean _song_set_time_added    (Song       *song,
				  time_t      t);
gboolean _song_set_time_modified (Song       *song,
				  time_t      t);
gboolean _song_set_rating        (Song       *song,
				  short       rating);
gboolean _song_set_size          (Song       *song,
				  size_t      size);
gboolean _song_set_year          (Song       *song,
				  int         year);
gboolean _song_set_playlists     (Song       *song,
				  GList      *playlists);
gboolean _song_remove_playlist   (Song       *song,
				  int         id);
gboolean _song_add_playlist      (Song       *song,
				  int         id);

#endif /*__SONG_PRIVATE_H__ */
