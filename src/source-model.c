/*
 * Copyright (C) 2003-2004 Imendio HB
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <string.h>
#include "source-model.h"
#include "song.h"
#include "eggtreemultidnd.h"

static void source_model_tree_model_init        (GtkTreeModelIface           *iface);
static void source_model_multi_drag_source_init (EggTreeMultiDragSourceIface *iface);
static void source_model_class_init             (SourceModelClass            *klass);
static void source_model_finalize               (GObject                     *object);
static void source_model_remove_iter            (SourceModel                 *model,
						 GtkTreeIter                 *iter);
static void clear_indices                       (SourceModel                 *model);
static void ensure_indices                      (SourceModel                 *model);


enum {
	TARGET_SOURCE
};

static const GtkTargetEntry drag_types[] = {
	{  "x-special/jamboree-song-list", 0, TARGET_SOURCE },
};


G_DEFINE_TYPE_WITH_CODE (SourceModel, source_model, G_TYPE_OBJECT,
			 G_IMPLEMENT_INTERFACE (GTK_TYPE_TREE_MODEL,
						source_model_tree_model_init);
			 G_IMPLEMENT_INTERFACE (EGG_TYPE_TREE_MULTI_DRAG_SOURCE,
						source_model_multi_drag_source_init);
	);


static GtkTreeModelFlags
source_model_get_flags (GtkTreeModel *tree_model)
{
	return GTK_TREE_MODEL_ITERS_PERSIST | GTK_TREE_MODEL_LIST_ONLY;
}

static int
source_model_get_n_columns (GtkTreeModel *tree_model)
{
	return 1;
}

static GType
source_model_get_column_type (GtkTreeModel *tree_model, int index)
{
	switch (index) {
	case 0:
		return G_TYPE_POINTER;
	default:
		return G_TYPE_INVALID;
	}
}

static gboolean
source_model_get_iter (GtkTreeModel *tree_model,
		       GtkTreeIter  *iter,
		       GtkTreePath  *path)
{
	SourceModel  *model;
	GSequencePtr  ptr;
	int           i;

	model = (SourceModel *) tree_model;
  
	i = gtk_tree_path_get_indices (path)[0];

	if (i >= g_sequence_get_length (model->songs)) {
		return FALSE;
	}

	ptr = g_sequence_get_ptr_at_pos (model->songs, i);

	iter->stamp = model->stamp;
	iter->user_data = ptr;

	return TRUE;
}

static GtkTreePath *
source_model_get_path (GtkTreeModel *tree_model,
		       GtkTreeIter  *iter)
{
	SourceModel *model;
	GtkTreePath *path;
  
	model = (SourceModel *) tree_model;

	g_return_val_if_fail (model->stamp == iter->stamp, NULL);

	if (g_sequence_ptr_is_end (iter->user_data)) {
		return NULL;
	}
  
	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, g_sequence_ptr_get_position (iter->user_data));

	return path;
}

static void
source_model_get_value (GtkTreeModel *tree_model,
			GtkTreeIter  *iter,
			int           column,
			GValue       *value)
{
	SourceModel *model;
	Song *song;
   
	model = (SourceModel *) tree_model;

	g_return_if_fail (model->stamp == iter->stamp);

	song = g_sequence_ptr_get_data (iter->user_data);
  
	switch (column) {
	case 0:
		g_value_init (value, G_TYPE_POINTER);
		g_value_set_pointer (value, song);
		break;

	default:
		g_assert_not_reached ();
		break;
	}
}

static gboolean
source_model_iter_nth_child (GtkTreeModel *tree_model,
			     GtkTreeIter  *iter,
			     GtkTreeIter  *parent,
			     int           n)
{
	SourceModel *model;
	GSequencePtr child;

	if (parent) {
		return FALSE;
	}
	
	model = (SourceModel *) tree_model;

	child = g_sequence_get_ptr_at_pos (model->songs, n);

	if (g_sequence_ptr_is_end (child)) {
		return FALSE;
	}
  
	iter->stamp = model->stamp;
	iter->user_data = child;

	return TRUE;
}

static gboolean
source_model_iter_next (GtkTreeModel *tree_model,
			GtkTreeIter  *iter)
{
	SourceModel *model;
  
	model = (SourceModel *) tree_model;

	g_return_val_if_fail (model->stamp == iter->stamp, FALSE);

	iter->user_data = g_sequence_ptr_next (iter->user_data);

	return !g_sequence_ptr_is_end (iter->user_data);
}

/* Expose this since it's not in the treemodel api... */
gboolean
source_model_iter_prev (GtkTreeModel *tree_model,
			GtkTreeIter  *iter)
{
	SourceModel *model;
  
	model = (SourceModel *) tree_model;

	g_return_val_if_fail (model->stamp == iter->stamp, FALSE);

	iter->user_data = g_sequence_ptr_prev (iter->user_data);

	return iter->user_data != NULL;
}

static gboolean
source_model_iter_children (GtkTreeModel *tree_model,
			    GtkTreeIter  *iter,
			    GtkTreeIter  *parent)
{
	SourceModel *model;
  
	if (parent) {
		return FALSE;
	}
	
	model = (SourceModel *) tree_model;
  
	if (g_sequence_get_length (model->songs) == 0) {
		return FALSE;
	}

	iter->stamp = model->stamp;
	iter->user_data = g_sequence_get_begin_ptr (model->songs);
  
	return TRUE;
}

static int
source_model_iter_n_children (GtkTreeModel *tree_model,
			      GtkTreeIter  *iter)
{
	SourceModel *model;
  
	model = (SourceModel *) tree_model;
  
	if (iter == NULL) {
		return g_sequence_get_length (model->songs);
	}
	
	g_return_val_if_fail (model->stamp == iter->stamp, -1);

	return 0;
}

static gboolean
source_model_iter_parent (GtkTreeModel *tree_model,
			  GtkTreeIter  *iter,
			  GtkTreeIter  *child)
{
	return FALSE;
}

static gboolean
source_model_iter_has_child (GtkTreeModel *tree_model,
			     GtkTreeIter  *iter)
{
	return FALSE;
}

static gboolean
source_model_multi_row_draggable (EggTreeMultiDragSource *drag_source,
				  GList                  *path_list)
{
	return TRUE;
}

static GtkTargetList *drag_target_list = NULL;

typedef struct {
	SourceModel *model;
	GList       *path_list;
} DragData;

static gboolean
source_model_multi_drag_data_get (EggTreeMultiDragSource *drag_source, 
				  GList                  *path_list, 
				  GtkSelectionData       *selection_data)
{
	SourceModel *model;
	guint        target_info;

	model = (SourceModel *) drag_source;
	
	if (!drag_target_list) {
		drag_target_list = gtk_target_list_new (drag_types, G_N_ELEMENTS (drag_types));
	}
	
	if (gtk_target_list_find (drag_target_list, selection_data->target, &target_info)) {
		GList *l;
		Song  *song;
                int    len, i;

                gpointer *data;
                
		g_assert (target_info == TARGET_SOURCE);

                len = g_list_length (path_list);
                
                data = g_new (gpointer, len);
                
		for (l = path_list, i = 0; l; l = l->next, i++) {
			GtkTreeIter  iter;
			GtkTreePath *path;

			path = gtk_tree_row_reference_get_path (l->data);
			gtk_tree_model_get_iter (GTK_TREE_MODEL (model), &iter, path);

			song = g_sequence_ptr_get_data (iter.user_data);

			data[i] = song;
		}

		gtk_selection_data_set (selection_data,
					selection_data->target,
					8,
					(guchar *) data,
					len * sizeof (gpointer));
      
		return TRUE;
	} else {
		return FALSE;
	}
}

static gboolean
source_model_multi_drag_data_delete (EggTreeMultiDragSource *drag_source, GList *path_list)
{
	return TRUE;
}

static void
source_model_tree_model_init (GtkTreeModelIface *iface)
{
	iface->get_flags = source_model_get_flags;
	iface->get_n_columns = source_model_get_n_columns;
	iface->get_column_type = source_model_get_column_type;
	iface->get_iter = source_model_get_iter;
	iface->get_path = source_model_get_path;
	iface->get_value = source_model_get_value;
	iface->iter_nth_child = source_model_iter_nth_child;
	iface->iter_next = source_model_iter_next;
	iface->iter_has_child = source_model_iter_has_child;
	iface->iter_n_children = source_model_iter_n_children;
	iface->iter_children = source_model_iter_children;
	iface->iter_parent = source_model_iter_parent;
}

static void
source_model_multi_drag_source_init (EggTreeMultiDragSourceIface *iface)
{
	iface->row_draggable = source_model_multi_row_draggable;
	iface->drag_data_get = source_model_multi_drag_data_get;
	iface->drag_data_delete = source_model_multi_drag_data_delete;
}

static void
source_model_class_init (SourceModelClass *klass)
{
	GObjectClass *object_class;

	object_class = (GObjectClass*) klass;

	object_class->finalize = source_model_finalize;
}

static void
source_model_init (SourceModel *model)
{
	model->songs = g_sequence_new (NULL);
	model->reverse_map = g_hash_table_new (NULL, NULL);

	model->stamp = g_random_int ();

	model->sort_id = SONG_SORT_ID_DEFAULT;
	model->sort_type = GTK_SORT_ASCENDING;

	model->current_index = -1;
}

static void
source_model_finalize (GObject *object)
{
	SourceModel *model = SOURCE_MODEL (object);
  
	g_sequence_free (model->songs);
	g_hash_table_destroy (model->reverse_map);

	G_OBJECT_CLASS (source_model_parent_class)->finalize (object);
}

GtkTreeModel *
source_model_new (void)
{
	return g_object_new (TYPE_SOURCE_MODEL, NULL);
}

static int
compare_func (Song *song_a, Song *song_b, SourceModel *model)
{
	return song_compare_func (song_a, song_b, model->sort_id, model->sort_type);
}

gboolean
source_model_add (SourceModel *model, Song *song)
{
	GtkTreeIter   iter;
	GtkTreePath  *path;
	GSequencePtr  new_ptr;

	if (g_hash_table_lookup (model->reverse_map, song)) {
		return FALSE;
	}
	
	clear_indices (model);
  
	new_ptr = g_sequence_insert_sorted (model->songs, song,
					    (GCompareDataFunc) compare_func,
					    model);
  
	g_hash_table_insert (model->reverse_map, song, new_ptr);
	
	iter.stamp = model->stamp;
	iter.user_data = new_ptr;
  
	path = gtk_tree_model_get_path (GTK_TREE_MODEL (model), &iter);
	gtk_tree_model_row_inserted (GTK_TREE_MODEL (model), path, &iter);
	gtk_tree_path_free (path);

	return TRUE;
}

void
source_model_remove_iter (SourceModel *model, GtkTreeIter *iter)
{
	GSequencePtr ptr;
	GtkTreePath *path;

	clear_indices (model);

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (model), iter);
	ptr = iter->user_data;

	g_hash_table_remove (model->reverse_map, g_sequence_ptr_get_data (ptr));
	g_sequence_remove (ptr);
  
	model->stamp++;

	gtk_tree_model_row_deleted (GTK_TREE_MODEL (model), path);
	gtk_tree_path_free (path);
}

gboolean
source_model_remove (SourceModel *model, Song *song)
{
	GtkTreeIter iter;
  
	if (source_model_song_get_iter (model, song, &iter)) {
		source_model_remove_iter (model, &iter);
	} else {
		return FALSE;
	}
	
	return TRUE;
}

void
source_model_clear (SourceModel *model)
{
	GtkTreeIter iter;
  
	g_return_if_fail (model != NULL);

	clear_indices (model);
  
	while (g_sequence_get_length (model->songs) > 0) {
		iter.stamp = model->stamp;
		iter.user_data = g_sequence_get_begin_ptr (model->songs);
		source_model_remove_iter (model, &iter);
	}
}

void
source_model_sort (SourceModel *model)
{
	GSequence *songs;
	GSequencePtr *old_order;
	GtkTreePath *path;
	int *new_order;
	int length;
	int i;

	songs = model->songs;
	length = g_sequence_get_length (songs);

	if (length <= 1) {
		return;
	}
  
	/* Generate old order of GSequencePtrs. */
	old_order = g_new (GSequencePtr, length);
	for (i = 0; i < length; ++i) {
		old_order[i] = g_sequence_get_ptr_at_pos (songs, i);
	}
	
	g_sequence_sort (songs, (GCompareDataFunc) compare_func, model);
  
	/* Generate new order. */
	new_order = g_new (int, length);
	for (i = 0; i < length; ++i) {
		new_order[i] = g_sequence_ptr_get_position (old_order[i]);
	}
	
	path = gtk_tree_path_new ();
  
	gtk_tree_model_rows_reordered (GTK_TREE_MODEL (model), path, NULL, new_order);
  
	gtk_tree_path_free (path);
	g_free (old_order);
	g_free (new_order);
}

void
source_model_set_sorting (SourceModel *model,
			  SongSortId     id,
			  GtkSortType    type)
{
	if (id == model->sort_id && type == model->sort_type) {
		return;
	}

	model->sort_id = id;
	model->sort_type = type;

	source_model_sort (model);
}

void
source_model_get_sorting (SourceModel *model,
			  SongSortId  *id,
			  GtkSortType *type)
{
	*id = model->sort_id;
	*type = model->sort_type;
}

gboolean
source_model_song_get_iter (SourceModel *model,
			    Song          *song,
			    GtkTreeIter   *iter)
{
	GSequencePtr ptr;

	ptr = g_hash_table_lookup (model->reverse_map, song);
	if (!ptr) {
		return FALSE;
	}
	
	if (iter != NULL) {
		iter->stamp = model->stamp;
		iter->user_data = ptr;
	}
  
	return TRUE;
}

Song *
source_model_get_song (SourceModel *model, GtkTreeIter *iter)
{
	g_return_val_if_fail (model->stamp == iter->stamp, NULL);
  
	return g_sequence_ptr_get_data (iter->user_data);
}

GList *
source_model_get_songs (SourceModel *model)
{
	GList        *list;
	GSequencePtr  ptr;

	list = NULL;
	ptr = g_sequence_get_begin_ptr (model->songs);
	while (!g_sequence_ptr_is_end (ptr)) {
		list = g_list_prepend (list, g_sequence_ptr_get_data (ptr));
		ptr = g_sequence_ptr_next (ptr);
	}

	return g_list_reverse (list);
}

static void
remove_ptr (SourceModel *model, GSequencePtr ptr)
{
	GtkTreePath *path;
  
	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, g_sequence_ptr_get_position (ptr));
  
	g_hash_table_remove (model->reverse_map, g_sequence_ptr_get_data (ptr));
  
	g_sequence_remove (ptr);

	model->stamp++;
  
	gtk_tree_model_row_deleted (GTK_TREE_MODEL (model), path);
	gtk_tree_path_free (path);
}

void
source_model_remove_delta (SourceModel *model, GList *songs)
{
	GHashTable   *hash;
	GList        *l, *remove = NULL;
	Song         *song;
	GSequencePtr  ptr;
  
	if (g_sequence_get_length (model->songs) == 0) {
		return;
	}

	if (!songs) {
		source_model_clear (model);
		return;
	}

	hash = g_hash_table_new (NULL, NULL);
 
	for (l = songs; l; l = l->next) {
		g_hash_table_insert (hash, l->data, GINT_TO_POINTER (TRUE));
	}
	
	ptr = g_sequence_get_begin_ptr (model->songs);
	while (!g_sequence_ptr_is_end (ptr)) {
		song = g_sequence_ptr_get_data (ptr);
		if (!g_hash_table_lookup (hash, song)){
			remove = g_list_prepend (remove, ptr);
		}
		
		ptr = g_sequence_ptr_next (ptr);
	}

	for (l = remove; l; l = l->next) {
		remove_ptr (model, l->data);
	}
	
	g_list_free (remove);
	g_hash_table_destroy (hash);

	clear_indices (model);
}

Song *
source_model_get_current (SourceModel *model)
{
	int          pos;
	GSequencePtr ptr;
  
	g_return_val_if_fail (IS_SOURCE_MODEL (model), NULL);

	if (g_sequence_get_length (model->songs) == 0) {
		return NULL;
	}
	
	ensure_indices (model);

	if (model->current_index == -1) {
		return NULL;
	}
	
	pos = model->indices[model->current_index];

	ptr = g_sequence_get_ptr_at_pos (model->songs, pos);
  
	if (!ptr) {
		return NULL;
	}
	
	return g_sequence_ptr_get_data (ptr);
}

gboolean
source_model_set_current (SourceModel *model, Song *song)
{
	GSequencePtr ptr;
	int          i, len, pos;
  
	g_return_val_if_fail (IS_SOURCE_MODEL (model), FALSE);

	if (!song) {
		model->current_index = -1;
		return TRUE;
	}
  
	len = g_sequence_get_length (model->songs);
  
	if (len == 0) {
		return FALSE;
	}
	
	ensure_indices (model);

	ptr = g_hash_table_lookup (model->reverse_map, song);
	if (!ptr) {
		return FALSE;
	}
	
	pos = g_sequence_ptr_get_position (ptr);

	if (!model->random) {
		model->current_index = pos;
		return TRUE;
	}

	for (i = 0; i < len; i++) {
		if (model->indices[i] == pos) {
			model->current_index = i;
			return TRUE;
		}
	}
	
	return FALSE;
}

Song *
source_model_next (SourceModel *model)
{
	GSequencePtr  ptr;
	Song         *song = NULL;
  
	g_return_val_if_fail (IS_SOURCE_MODEL (model), NULL);

	if (!source_model_has_next (model)) {
		return NULL;
	}

	ptr = g_sequence_get_ptr_at_pos (model->songs, model->indices[model->current_index + 1]);
	if (ptr) {
		song = g_sequence_ptr_get_data (ptr);
		model->current_index++;
	}

	return song;
}

Song *
source_model_prev (SourceModel *model)
{
	GSequencePtr  ptr;
	Song         *song = NULL;
  
	g_return_val_if_fail (IS_SOURCE_MODEL (model), NULL);

	if (!source_model_has_prev (model)) {
		return NULL;
	}

	ptr = g_sequence_get_ptr_at_pos (model->songs, model->indices[model->current_index - 1]);
	if (ptr) {
		song = g_sequence_ptr_get_data (ptr);
		model->current_index--;
	}

	return song;
}

Song *
source_model_first (SourceModel *model)
{
	GSequencePtr ptr;
	Song *song = NULL;
  
	g_return_val_if_fail (IS_SOURCE_MODEL (model), NULL);

	if (g_sequence_get_length (model->songs) == 0) {
		return NULL;
	}
  
	ensure_indices (model);

	ptr = g_sequence_get_ptr_at_pos (model->songs, model->indices[0]);
	if (ptr) {
		song = g_sequence_ptr_get_data (ptr);
		model->current_index = 0;
	}

	return song;
}

typedef struct
{
	int random;
	int index;
} RandomData;

static int
compare_random (gconstpointer ptr_a, gconstpointer ptr_b)
{
	RandomData *a = (RandomData *) ptr_a;
	RandomData *b = (RandomData *) ptr_b;

	if (a->random < b->random) {
		return -1;
	}
	else if (a->random > b->random) {
		return 1;
	} else {
		return 0;
	}
}

static void
clear_indices (SourceModel *model)
{
	model->current_index = -1;
	g_free (model->indices);
	model->indices = NULL;
}

static void
ensure_indices (SourceModel *model)
{
	int         len, i;
	RandomData  data;
	GArray     *array;

	if (model->indices) {
		return;
	}
  
	len = g_sequence_get_length (model->songs);
	model->indices = g_new (int, len);

	if (model->random) {
		array = g_array_sized_new (FALSE, FALSE, sizeof (RandomData), len);

		for (i = 0; i < len; i++) {
			data.random = g_random_int_range (0, len);
			data.index = i;
	  
			g_array_append_val (array, data);
		}
      
		g_array_sort (array, compare_random);

		for (i = 0; i < len; i++) {
			model->indices[i] = g_array_index (array, RandomData, i).index;
		}
		
		g_array_free (array, TRUE);
	} else {
		for (i = 0; i < len; i++){
			model->indices[i] = i;
		}
	}
}

void
source_model_set_random (SourceModel *model, gboolean random)
{
	Song *song;
  
	g_return_if_fail (IS_SOURCE_MODEL (model));
  
	model->random = random;
  
	song = source_model_get_current (model);
  
	clear_indices (model);

	/* Keep the same current song when going from shuffle to normal. */
	if (!random && song) {
		source_model_set_current (model, song);
	}
}

void
source_model_reshuffle (SourceModel *model)
{
	g_return_if_fail (IS_SOURCE_MODEL (model));

	if (!model->random) {
		return;
	}
  
	clear_indices (model);
}

gboolean
source_model_has_prev (SourceModel *model)
{
	int          len;
	GSequencePtr ptr;

	g_return_val_if_fail (IS_SOURCE_MODEL (model), FALSE);

	len = g_sequence_get_length (model->songs);
	if (len == 0) {
		return FALSE;
	}
	
	ensure_indices (model);

	/* Caller must decide if we should wrap. */
	if (model->current_index <= 0) {
		return FALSE;
	}

	ptr = g_sequence_get_ptr_at_pos (model->songs, model->indices[model->current_index - 1]);
  
	return ptr != NULL;
}

gboolean
source_model_has_next (SourceModel *model)
{
	int          len;
	GSequencePtr ptr;

	g_return_val_if_fail (IS_SOURCE_MODEL (model), FALSE);

	len = g_sequence_get_length (model->songs);
	if (len == 0) {
		return FALSE;
	}
	
	ensure_indices (model);

	/* Caller must decide if we should wrap. */
	if (model->current_index + 1 >= len) {
		return FALSE;
	}

	ptr = g_sequence_get_ptr_at_pos (model->songs, model->indices[model->current_index + 1]);
  
	return ptr != NULL;
}

