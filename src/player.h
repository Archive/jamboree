/*
 * Copyright (C) 2003-2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <glib-object.h>
#include "song.h"

#define TYPE_PLAYER            (player_get_type ())
#define PLAYER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PLAYER, Player))
#define PLAYER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_PLAYER, PlayerClass))
#define IS_PLAYER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PLAYER))
#define IS_PLAYER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_PLAYER))
#define PLAYER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_PLAYER, PlayerClass))

typedef struct _Player      Player;
typedef struct _PlayerClass PlayerClass;

struct _PlayerClass {
	GObjectClass parent_class;
};

#define PLAYER_ERROR player_error_quark ()

enum {
	PLAYER_ERROR_FORMAT,
	PLAYER_ERROR_RESOURCE_BUSY,
	PLAYER_ERROR_INTERNAL
};

GType        player_get_type    (void) G_GNUC_CONST;
Player      *player_get         (void);
void         player_shutdown    (void);
GQuark       player_error_quark (void);
gboolean     player_set_song    (Song    *song,
				 GError **error);
Song *       player_get_song    (void);
gboolean     player_play        (GError **error);
gboolean     player_play_song   (Song    *song,
				 GError **error);
void         player_stop        (void);
void         player_pause       (void);
void         player_set_volume  (int      v);
int          player_get_volume  (void);
PlayingState player_get_state   (void);
void         player_seek        (int      t);
int          player_tell        (void);
gboolean     player_is_playing  (Song    *song);

#endif /* __PLAYER_H__ */
