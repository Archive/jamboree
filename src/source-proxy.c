/*
 * Copyright (C) 2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <glib/gi18n.h>
#include "source-proxy.h"


static void             source_proxy_class_init    (SourceProxyClass *klass);
static void             source_proxy_init          (SourceProxy      *db);
static void             proxy_finalize             (GObject          *object);
static gboolean         proxy_update_song          (Source           *source,
						    Song             *song);
static GList *          proxy_get_songs            (Source           *list);


#define SOURCE_PROXY_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_SOURCE_PROXY, SourceProxyPriv))
G_DEFINE_TYPE (SourceProxy, source_proxy, TYPE_SOURCE);

struct _SourceProxyPriv {
	Source          *source;
};

static void
source_proxy_class_init (SourceProxyClass *klass)
{
	GObjectClass *object_class;
	SourceClass  *source_class;

	object_class = (GObjectClass*) klass;
	object_class->finalize = proxy_finalize;

	source_class = (SourceClass*) klass;

	source_class->update_song = proxy_update_song;
	source_class->get_songs = proxy_get_songs;

	g_type_class_add_private (klass, sizeof (SourceProxyPriv));
}

static void
source_proxy_init (SourceProxy *proxy)
{
	SourceProxyPriv *priv;

	priv = SOURCE_PROXY_GET_PRIVATE (proxy);
	proxy->priv = priv;
}

static void
proxy_finalize (GObject *object)
{
	SourceProxy     *proxy;
	SourceProxyPriv *priv;
	
	proxy = SOURCE_PROXY (object);
	priv = proxy->priv;

	g_object_unref (priv->source);

	if (G_OBJECT_CLASS (source_proxy_parent_class)->finalize) {
		G_OBJECT_CLASS (source_proxy_parent_class)->finalize (object);
	}
}

static void
song_added_cb (Source      *list,
	       Song        *song,
	       SourceProxy *proxy)
{
	/* FIXME: check if the proxy evaluates */
	source_song_added (SOURCE (proxy), song);
}


static void
song_removed_cb (Source      *list,
		 Song        *song,
		 SourceProxy *proxy)
{
	/* FIXME: check if the proxy evaluates */
	source_song_removed (SOURCE (proxy), song);
}

static void
song_changed_cb (Source      *list,
		 Song        *song,
		 int          tags,
		 SourceProxy *proxy)
{
	/* FIXME: check if the proxy evaluates */
	source_song_changed (SOURCE (proxy), song, tags);
}

static void
songs_changed_cb (Source      *list,
		  SourceProxy *proxy)
{
	source_songs_changed (SOURCE (proxy));
}

static gboolean
proxy_update_song (Source *source, Song *song)
{
	SourceProxy     *proxy;
	SourceProxyPriv *priv;

	proxy = SOURCE_PROXY (source);
	priv = proxy->priv;

	return source_update_song (priv->source, song);	
}

static GList *
proxy_get_songs (Source *source)
{
	SourceProxy     *proxy;
	SourceProxyPriv *priv;

	proxy = SOURCE_PROXY (source);
	priv = proxy->priv;

	return source_get_songs (priv->source);	
}

void
source_proxy_set_source (SourceProxy *proxy, Source *source)
{
	SourceProxyPriv *priv;

	priv = proxy->priv;

	/* Can't bother supporting re-setting this for now... */
	g_assert (priv->source == NULL);
	
	priv->source = g_object_ref (source);

	g_signal_connect (source,
			  "song_added",
			  G_CALLBACK (song_added_cb),
			  proxy);

	g_signal_connect (source,
			  "song_removed",
			  G_CALLBACK (song_removed_cb),
			  proxy);

	g_signal_connect (source,
			  "song_changed",
			  G_CALLBACK (song_changed_cb),
			  proxy);

	g_signal_connect (source,
			  "songs_changed",
			  G_CALLBACK (songs_changed_cb),
			  proxy);
}

