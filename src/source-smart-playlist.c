/*
 * Copyright (C) 2004 Imendio HB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include "source-smart-playlist.h"
#include "limiter.h"
#include "expr.h"

static void             source_smart_playlist_class_init    (SourceSmartPlaylistClass *klass);
static void             source_smart_playlist_init          (SourceSmartPlaylist      *source);
static void             smart_playlist_finalize             (GObject                  *object);
static void             smart_playlist_free_exprs           (GList                    *exprs);
static gboolean         smart_playlist_get_is_editable      (Source                   *source);
static const GdkPixbuf *smart_playlist_get_pixbuf           (Source                   *source);
static GList *          smart_playlist_get_songs_filtered   (Source                   *source);
static char *           smart_playlist_to_xml               (Source                   *source);


#define SOURCE_SMART_PLAYLIST_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_SOURCE_SMART_PLAYLIST, SourceSmartPlaylistPriv))
G_DEFINE_TYPE (SourceSmartPlaylist, source_smart_playlist, TYPE_SOURCE_PROXY);

struct _SourceSmartPlaylistPriv {
	GdkPixbuf  *pixbuf;

	GList      *exprs;
	ExprOp      exprs_op;
	
	Limiter    *limiter;
};

static void
source_smart_playlist_class_init (SourceSmartPlaylistClass *klass)
{
	GObjectClass *object_class;
	SourceClass  *source_class;

	object_class = (GObjectClass*) klass;
	object_class->finalize = smart_playlist_finalize;

	source_class = (SourceClass*) klass;

	source_class->get_is_editable = smart_playlist_get_is_editable;
	source_class->get_pixbuf = smart_playlist_get_pixbuf;
	source_class->get_songs_filtered = smart_playlist_get_songs_filtered;
	source_class->to_xml = smart_playlist_to_xml;

	g_type_class_add_private (klass, sizeof (SourceSmartPlaylistPriv));
}

static void
source_smart_playlist_init (SourceSmartPlaylist *playlist)
{
	SourceSmartPlaylistPriv *priv;

	priv = SOURCE_SMART_PLAYLIST_GET_PRIVATE (playlist);
	playlist->priv = priv;

	priv->pixbuf = gdk_pixbuf_new_from_file (DATADIR "/jamboree/jamboree-playlist-smart.png",
						 NULL);
}

static void
smart_playlist_finalize (GObject *object)
{
	SourceSmartPlaylist     *playlist;
	SourceSmartPlaylistPriv *priv;
	
	playlist = SOURCE_SMART_PLAYLIST (object);
	priv = playlist->priv;

	g_object_unref (priv->pixbuf);

	smart_playlist_free_exprs (priv->exprs);

	if (priv->limiter) {
		limiter_free (priv->limiter);
	}
	
	G_OBJECT_CLASS (source_smart_playlist_parent_class)->finalize (object);
}

Source *
source_smart_playlist_new (Source     *source,
			   const char *name)
{
	SourceSmartPlaylist     *playlist;
	SourceSmartPlaylistPriv *priv;

	playlist = g_object_new (TYPE_SOURCE_SMART_PLAYLIST, NULL);

	priv = playlist->priv;

	source_set_name (SOURCE (playlist), name);

	source_proxy_set_source (SOURCE_PROXY (playlist), source);

	return SOURCE (playlist);
}

static void
smart_playlist_free_exprs (GList *exprs)
{
	GList *l;

	for (l = exprs; l; l = l->next) {
		expr_free (l->data);
	}
	
	g_list_free (exprs);
}

void
source_smart_playlist_set_exprs (SourceSmartPlaylist *playlist,
				 GList               *exprs)
{
	SourceSmartPlaylistPriv *priv;

	priv = playlist->priv;

	smart_playlist_free_exprs (priv->exprs);
	priv->exprs = exprs;
}

GList *
source_smart_playlist_get_exprs (SourceSmartPlaylist *playlist)
{
	SourceSmartPlaylistPriv *priv;

	priv = playlist->priv;

	return priv->exprs;
}

void
source_smart_playlist_set_limiter (SourceSmartPlaylist *playlist,
				   Limiter             *limiter)
{
	SourceSmartPlaylistPriv *priv;

	priv = playlist->priv;

	if (priv->limiter) {
		limiter_free (priv->limiter);
	}
	priv->limiter = limiter;
}

Limiter *
source_smart_playlist_get_limiter (SourceSmartPlaylist *playlist)
{
	return playlist->priv->limiter;
}

void
source_smart_playlist_set_op (SourceSmartPlaylist *playlist,
			      ExprOp               op)
{
	g_return_if_fail (op == EXPR_OP_OR || op == EXPR_OP_AND);
	
	playlist->priv->exprs_op = op;
}

ExprOp
source_smart_playlist_get_op (SourceSmartPlaylist *playlist)
{
	return playlist->priv->exprs_op;
}

static gboolean
smart_playlist_get_is_editable (Source *source)
{
	return TRUE;
}

static const GdkPixbuf *
smart_playlist_get_pixbuf (Source *source)
{
	SourceSmartPlaylist *playlist;

	playlist = SOURCE_SMART_PLAYLIST (source);

	return playlist->priv->pixbuf;
}

static GList *
smart_playlist_get_songs_filtered (Source *source)
{
	SourceSmartPlaylist     *playlist;
	SourceSmartPlaylistPriv *priv;
	GList                   *songs, *l;
	GList                   *filtered;
	Song                    *song;
	GList                   *e;
	gboolean                 base_evalulates;
	Expr                    *browse_expr;
	Expr                    *search_expr;
	LimiterVariable          variable;

	playlist = SOURCE_SMART_PLAYLIST (source);
	
	priv = playlist->priv;
	
	songs = source_get_songs (source);

	browse_expr = source_get_browse_expr (source);
	search_expr = source_get_search_expr (source);
	
	if (priv->limiter) {
		variable = limiter_get_variable (priv->limiter);
	} else {
		variable = LIMITER_VARIABLE_NONE;
	}
	
	filtered = NULL;
	for (l = songs; l; l = l->next) {
		song = l->data;

		base_evalulates = FALSE;
		
		if (priv->exprs_op == EXPR_OP_OR) {
			for (e = priv->exprs; e; e = e->next) {
				if (expr_evaluate (e->data, song)) {
					base_evalulates = TRUE;
					break;
				}
			}
		} else {
			base_evalulates = TRUE;
			for (e = priv->exprs; e; e = e->next) {
				if (!expr_evaluate (e->data, song)) {
					base_evalulates = FALSE;
					break;
				}
			}
		}
		
		if ((!priv->exprs || base_evalulates) &&
		    (!browse_expr || expr_evaluate (browse_expr, song)) &&
		    (!search_expr || expr_evaluate (search_expr, song))) {
			
			if (variable == LIMITER_VARIABLE_RANDOM) {
				_song_update_random (song);
			}
			
			filtered = g_list_prepend (filtered, song);
		}
	}

	if (priv->limiter) {
		return limiter_apply_limits (priv->limiter, filtered);
	} else {
		return filtered;
	}
}

static char *
expr_list_to_xml (GList *exprs)
{
	GString *string;
	GList   *l;
	Expr    *expr;
	char    *str;
	
	string = g_string_new (NULL);
	
	for (l = exprs; l; l = l->next) {
		expr = l->data;
		
		str = expr_to_xml (expr);
		g_string_append (string, str);
		g_free (str);
	}

	return g_string_free (string, FALSE);
}

static char *
smart_playlist_to_xml (Source *source)
{
	SourceSmartPlaylist     *playlist;
	SourceSmartPlaylistPriv *priv;
	char                    *expr_str, *limit_str;
	char                    *str;
	const char              *op_string;
	char                    *tmp;

	playlist = SOURCE_SMART_PLAYLIST (source);
	priv = playlist->priv;

	if (!priv->exprs && !priv->limiter) {
		return NULL;
	}
	
	if (priv->exprs) {
		expr_str = expr_list_to_xml (priv->exprs);
	} else {
		expr_str = NULL;
	}

	if (priv->limiter) {
		limit_str = limiter_to_xml (priv->limiter);
	} else {
		limit_str = NULL;
	}

	if (priv->exprs_op == EXPR_OP_AND) {
		op_string = "all";
	} else {
		op_string = "any";
	}

	tmp = g_markup_escape_text (source_get_name (source), -1);

	str = g_strdup_printf ("  <smart-playlist name='%s' match='%s'>\n"
			       "%s%s"
			       "  </smart-playlist>\n",
			       tmp,
			       op_string,
			       expr_str ? expr_str : "",
			       limit_str ? limit_str : "");

	g_free (tmp);
	g_free (expr_str);
	g_free (limit_str);

	return str;
}

/* Might need to make this a vfunc. Should share code with get_filtered list. */
gboolean
source_smart_playlist_song_evaluates (SourceSmartPlaylist *playlist, Song *song)
{
	SourceSmartPlaylistPriv *priv;
	GList                   *e;
	gboolean                 base_evalulates;
	Expr                    *browse_expr;
	Expr                    *search_expr;
	
	priv = playlist->priv;
	
	browse_expr = source_get_browse_expr (SOURCE (playlist));
	search_expr = source_get_search_expr (SOURCE (playlist));
	
	base_evalulates = FALSE;
		
	if (priv->exprs_op == EXPR_OP_OR) {
		for (e = priv->exprs; e; e = e->next) {
			if (expr_evaluate (e->data, song)) {
				base_evalulates = TRUE;
				break;
			}
		}
	} else {
		base_evalulates = TRUE;
		for (e = priv->exprs; e; e = e->next) {
			if (!expr_evaluate (e->data, song)) {
				base_evalulates = FALSE;
				break;
			}
		}
	}
	
	if ((!priv->exprs || base_evalulates) &&
	    (!browse_expr || expr_evaluate (browse_expr, song)) &&
	    (!search_expr || expr_evaluate (search_expr, song))) {
		
		return TRUE;
	}

	return FALSE;
}
