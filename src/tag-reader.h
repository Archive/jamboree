/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004, 2005 Imendio HB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __TAG_READER_H__
#define __TAG_READER_H__

#include "song.h"

typedef gboolean (*TagReaderSongFoundFunc)  (const char *path,
					     gpointer    user_data);
typedef void     (*TagReaderSongReadFunc)   (Song       *song,
					     gpointer    user_data);
typedef gboolean (*TagReaderProgressFunc)   (const char *path,
					     gint        files,
					     gint        completed,
					     gpointer    user_data);

gboolean   tag_reader_read_path (const char             *path,
				 TagReaderSongFoundFunc  song_found_func,
				 TagReaderSongReadFunc   song_read_func,
				 TagReaderProgressFunc   progress_func,
				 gpointer                user_data);

#endif /*__TAG_READER_H__ */
