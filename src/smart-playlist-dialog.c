/*
 * Copyright (C) 2003-2004 Imendio HB
 * Copyright (C) 2003 Johan Dahlin <johan@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <config.h>
#include <stdlib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "smart-playlist-dialog.h"
#include "expr.h"
#include "limiter.h"

typedef struct {
	GtkWidget *hbox;

	GtkWidget *variable;
	GtkWidget *operator;
	GtkWidget *value1;
	GtkWidget *value2;
	GtkWidget *label;
	GtkWidget *unit;  

	GtkWidget *remove_button;
} ConditionRow;

typedef struct {
	ConditionRow *row;
	SmartPlaylistDialog *dialog;
} ConditionData;

typedef struct {
	const char *label;
	int         value;
	int         type;
} OptionMenuEntry;


static void       smart_playlist_dialog_class_init (SmartPlaylistDialogClass *klass);
static void       smart_playlist_dialog_init       (SmartPlaylistDialog      *window);
static void       name_entry_changed_cb            (GtkWidget                *entry,
						    SmartPlaylistDialog      *dialog);
static void       add_row_at_pos                   (SmartPlaylistDialog      *dialog,
						    int                       pos);
static void       check_match_toggled_cb           (GtkButton                *check,
						    SmartPlaylistDialog      *dialog);
static void       check_limit_toggled_cb           (GtkWidget                *button,
						    SmartPlaylistDialog      *dialog);
static void       option_menu_changed_cb           (GtkWidget                *option,
						    ConditionData            *row);
static void       remove_button_clicked_cb         (GtkWidget                *button,
						    SmartPlaylistDialog      *dialog);
static void       add_button_clicked_cb            (GtkWidget                *button,
						    SmartPlaylistDialog      *dialog);
static GtkWidget *create_row                       (SmartPlaylistDialog      *dialog);
static void       update_option_menu               (SmartPlaylistDialog      *dialog,
						    GtkWidget                *option,
						    ConditionRow             *row);
static GtkWidget* create_option_menu               (const OptionMenuEntry    *options,
						    int                       length,
						    gboolean                  activate_first);
static void       setup_option_menu                (GtkWidget                *option_menu,
						    const OptionMenuEntry    *options,
						    int                       length,
						    gboolean                  activate_first);
static void   smart_playlist_dialog_update_playlist (SmartPlaylistDialog *dialog);


#define PADDING 6

typedef enum {
	SIZE_KB,
	SIZE_MB,
	SIZE_GB,
} SizeOptions;

struct _SmartPlaylistDialogPriv {
	SourceSmartPlaylist *playlist;
	
	GtkSizeGroup *size_group;
	GtkSizeGroup *size_group_sub;
	GtkSizeGroup *size_group_button;

	GtkWidget    *name_entry;
	GtkWidget    *check_limit;
	GtkWidget    *check_match;
  
	GtkWidget    *vbox;
	GtkWidget    *option_match;
	GtkWidget    *label_match;
	GtkWidget    *option_limit;
	GtkWidget    *entry_limit;
	GtkWidget    *label_selected;
	GtkWidget    *option_selected;

	GtkWidget    *active_child;

	GtkWidget    *ok_button;
  
	GList        *rows;
};

const OptionMenuEntry variable_values[] = {
	{ N_("Album"),         VARIABLE_ALBUM,         CONSTANT_TYPE_STRING },
	{ N_("Artist"),        VARIABLE_ARTIST,        CONSTANT_TYPE_STRING },
	/*{ N_("Bit rate"),      VARIABLE_BIT_RATE,      CONSTANT_TYPE_INT, },*/
	/*{ N_("Comment"),       VARIABLE_COMMENT,       CONSTANT_TYPE_STRING },*/
	{ N_("Date Added"),    VARIABLE_DATE_ADDED,    CONSTANT_TYPE_DATE },
	{ N_("Date Modified"), VARIABLE_DATE_MODIFIED, CONSTANT_TYPE_DATE },
	{ N_("Genre"),         VARIABLE_GENRE,         CONSTANT_TYPE_STRING },
	/*{ N_("Kind"),          VARIABLE_KIND,          CONSTANT_TYPE_STRING },*/
	{ N_("Last Played"),   VARIABLE_LAST_PLAYED,   CONSTANT_TYPE_DATE },
	/*{ N_("My Rating"),     VARIABLE_RATING,        CONSTANT_TYPE_INT },*/
	{ N_("Play Count"),    VARIABLE_PLAYCOUNT,     CONSTANT_TYPE_INT },
	/*{ N_("Sample Rate"),   VARIABLE_SAMPLE_RATE,   CONSTANT_TYPE_INT },*/
	/*{ N_("Size"),          VARIABLE_SIZE,          CONSTANT_TYPE_SIZE },*/
	{ N_("Song Name"),     VARIABLE_TITLE,         CONSTANT_TYPE_STRING },
	/*{ N_("Time"),          VARIABLE_TIME,          CONSTANT_TYPE_TIME },*/
	{ N_("Track Number"),  VARIABLE_TRACK,         CONSTANT_TYPE_INT },
	{ N_("Year"),          VARIABLE_YEAR,          CONSTANT_TYPE_INT }
};

const OptionMenuEntry int_values[] = {
	{ N_("is"),              EXPR_OP_EQ },
	{ N_("is not"),          EXPR_OP_NE },
	{ N_("is greater than"), EXPR_OP_GT },
	{ N_("is less than"),    EXPR_OP_LT },
	{ N_("is in the range"), EXPR_OP_RANGE }
};

const OptionMenuEntry string_values[] = {
	{ N_("is"),               EXPR_OP_EQ },
	{ N_("is not"),           EXPR_OP_NE },
	{ N_("contains"),         EXPR_OP_CONTAINS },
	{ N_("does not contain"), EXPR_OP_DOES_NOT_CONTAIN }
};

const OptionMenuEntry date_values[] = {
	{ N_("is in the last"),     EXPR_OP_GT },
	{ N_("is not in the last"), EXPR_OP_LT }
};

const OptionMenuEntry date_sub_values[] = {
	{ N_("minutes"), UNIT_MINUTES },
	{ N_("hours"),   UNIT_HOURS },
	{ N_("days"),    UNIT_DAYS },
	{ N_("weeks"),   UNIT_WEEKS },
	{ N_("months"),  UNIT_MONTHS },
};

#if 0
const OptionMenuEntry time_values[] = {
	{ N_("seconds"), UNIT_SECONDS },  
	{ N_("minutes"), UNIT_MINUTES },
	{ N_("hours"),   UNIT_HOURS }
};
#endif

const OptionMenuEntry size_values[] = {
	{ N_("kb"), SIZE_KB },
	{ N_("Mb"), SIZE_MB },
	{ N_("Gb"), SIZE_GB }    
};

const OptionMenuEntry limit_values[] = {
	{ N_("songs"),   LIMITER_UNIT_SONGS },
	{ N_("minutes"), LIMITER_UNIT_MINUTES }, /* FIXME: i18n, need to separate those from the ones above */
	{ N_("hours"),   LIMITER_UNIT_HOURS },
	{ N_("MB"),      LIMITER_UNIT_MB },
	{ N_("GB"),      LIMITER_UNIT_GB }
};

const OptionMenuEntry match_values[] = {
	{ N_("any"), EXPR_OP_OR },
	{ N_("all"), EXPR_OP_AND }
};

const OptionMenuEntry selection_values[] = {
	{ N_("date added"),            LIMITER_VARIABLE_DATE_ADDED },
	{ N_("most recently played"),  LIMITER_VARIABLE_MOST_RECENTLY_PLAYED },
	{ N_("least recently played"), LIMITER_VARIABLE_LEAST_RECENTLY_PLAYED },
	{ N_("last played"),           LIMITER_VARIABLE_LAST_PLAYED },
	{ N_("my rating"),             LIMITER_VARIABLE_RATING },
	{ N_("play count"),            LIMITER_VARIABLE_PLAYCOUNT },
	{ N_("quality"),               LIMITER_VARIABLE_QUALITY },
	{ N_("random"),                LIMITER_VARIABLE_RANDOM },
	{ N_("year"),                  LIMITER_VARIABLE_YEAR }
};

#define SMART_PLAYLIST_DIALOG_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_SMART_PLAYLIST_DIALOG, SmartPlaylistDialogPriv))

G_DEFINE_TYPE (SmartPlaylistDialog, smart_playlist_dialog, GTK_TYPE_DIALOG);

static void
smart_playlist_dialog_class_init (SmartPlaylistDialogClass *klass)
{
	g_type_class_add_private (klass, sizeof (SmartPlaylistDialogPriv));
}

static void
smart_playlist_dialog_init (SmartPlaylistDialog *dialog)
{
	SmartPlaylistDialogPriv *priv;
	GladeXML                *gui;
	GtkWidget               *glade_dialog, *vbox1, *button;
	GtkWidget               *hbox;
  
	priv = SMART_PLAYLIST_DIALOG_GET_PRIVATE (dialog);
	dialog->priv = priv;

	priv->size_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	priv->size_group_sub = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	priv->size_group_button = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	
	gui = glade_xml_new (DATADIR "/jamboree/smart-playlist-dialog.glade",
			     NULL, NULL);
  
	glade_dialog = glade_xml_get_widget (gui, "smart_playlist_dialog");
	g_signal_connect (glade_dialog,
			  "delete_event",
			  G_CALLBACK (gtk_main_quit),
			  NULL);

	priv->name_entry = glade_xml_get_widget (gui, "name_entry");
	gtk_editable_select_region (GTK_EDITABLE (priv->name_entry), 0, -1);

	g_signal_connect (priv->name_entry,
			  "changed",
			  G_CALLBACK (name_entry_changed_cb),
			  dialog);

	vbox1 = glade_xml_get_widget (gui, "vbox1");
	gtk_widget_reparent (vbox1, GTK_DIALOG (dialog)->vbox);
	priv->vbox = glade_xml_get_widget (gui, "vbox2");

	priv->check_match = glade_xml_get_widget (gui, "check_match");
	g_signal_connect (priv->check_match,
			  "toggled",
			  G_CALLBACK (check_match_toggled_cb),
			  dialog);
	priv->option_match = glade_xml_get_widget (gui, "option_match");
	setup_option_menu (priv->option_match,
			   match_values,
			   G_N_ELEMENTS (match_values),
			   FALSE);

	priv->label_match = glade_xml_get_widget (gui, "label_match");

	priv->check_limit = glade_xml_get_widget (gui, "check_limit");
	g_signal_connect (priv->check_limit,
			  "toggled",
			  G_CALLBACK (check_limit_toggled_cb),
			  dialog);
  
	priv->option_limit = glade_xml_get_widget (gui, "option_limit");
	setup_option_menu (priv->option_limit,
			   limit_values,
			   G_N_ELEMENTS (limit_values),
			   FALSE);
  
	priv->entry_limit = glade_xml_get_widget (gui, "entry_limit");
	priv->label_selected = glade_xml_get_widget (gui, "label_selected");

	priv->option_selected = glade_xml_get_widget (gui, "option_selected");
	setup_option_menu (priv->option_selected,
			   selection_values,
			   G_N_ELEMENTS (selection_values),
			   FALSE);
  
	/* Normal buttons */
	button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      button,
				      GTK_RESPONSE_CANCEL);
	gtk_widget_show (button);
  
	button = gtk_button_new_from_stock (GTK_STOCK_OK);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      button,
				      GTK_RESPONSE_OK);
	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT | GTK_HAS_DEFAULT);
  
	gtk_widget_show (button);
	priv->ok_button = button;
  
	add_row_at_pos (dialog, 0);

	/* Add button */
	hbox = gtk_hbox_new (FALSE, 0);
	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (add_button_clicked_cb),
			  dialog);
	gtk_widget_show (button);
	gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (priv->vbox), hbox, FALSE, FALSE, 2);
	gtk_widget_show (hbox);

	gtk_size_group_add_widget (priv->size_group_button, button);

	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
}

GtkWidget *
smart_playlist_dialog_new (void)
{
	return g_object_new (TYPE_SMART_PLAYLIST_DIALOG, NULL);
}

static void
name_entry_changed_cb (GtkWidget           *entry,
		       SmartPlaylistDialog *dialog)
{
	SmartPlaylistDialogPriv *priv;
	const char              *str;

	priv = dialog->priv;
  
	str = gtk_entry_get_text (GTK_ENTRY (entry));
	gtk_widget_set_sensitive (priv->ok_button, str && str[0]);
}

static void
update_match (SmartPlaylistDialog *dialog,
	      gboolean             state)
{
	SmartPlaylistDialogPriv *priv;
	int                      i;
	GList                   *list;
	ConditionRow            *row;

	priv = dialog->priv;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv->check_match)) != state) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->check_match), state);
	}
	
	gtk_widget_set_sensitive (priv->label_match, state);
	gtk_widget_set_sensitive (priv->option_match, state);

	for (i = 0, list = priv->rows; list; list = list->next, i++) {
		row = list->data;

		gtk_widget_set_sensitive (row->variable, state);
		gtk_widget_set_sensitive (row->operator, state);
		gtk_widget_set_sensitive (row->value1, state);

		if (row->value2) {
			gtk_widget_set_sensitive (row->value2, state);
		}
		
		if (row->unit) {
			gtk_widget_set_sensitive (row->unit, state);
		}
		
		if (row->label) {
			gtk_widget_set_sensitive (row->label, state);
		}
		
		gtk_widget_set_sensitive (row->remove_button, state);
	}
}

static void
check_match_toggled_cb (GtkButton           *check,
			SmartPlaylistDialog *dialog)
{
	gboolean state;
  
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (check));
	update_match (dialog, state);
}

static void
update_limit (SmartPlaylistDialog *dialog,
	      gboolean             state)
{
	SmartPlaylistDialogPriv *priv;
    
	priv = dialog->priv;
  
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv->check_limit)) != state) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->check_limit), state);
	}
	
	gtk_widget_set_sensitive (priv->option_limit, state);
	gtk_widget_set_sensitive (priv->entry_limit, state);
	gtk_widget_set_sensitive (priv->label_selected, state);
	gtk_widget_set_sensitive (priv->option_selected, state);
}

static void
check_limit_toggled_cb (GtkWidget           *button,
			SmartPlaylistDialog *dialog)
{
	gboolean state;
  
	state = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button));
	update_limit (dialog, state);
}

static GtkWidget *
create_row (SmartPlaylistDialog *dialog)
{
	SmartPlaylistDialogPriv *priv;
	GtkWidget *hbox, *sub_hbox;
	ConditionRow *row;
	ConditionData *condition_data;
  
	hbox = gtk_hbox_new (FALSE, 6);
  
	/* Dynamically filled box. */
	sub_hbox = gtk_hbox_new (FALSE, 6);

	row = g_new0 (ConditionRow, 1);
	priv = dialog->priv;

	priv->rows = g_list_append (priv->rows, row);
	row->hbox = sub_hbox;
        
	/* This is the main (leftmost) GtkOptionMenu, for types. */
	row->variable = create_option_menu (variable_values,
					    G_N_ELEMENTS (variable_values),
					    FALSE);
	condition_data = g_new0 (ConditionData, 1);
	condition_data->row = row;
	condition_data->dialog = dialog;

	g_signal_connect (row->variable,
			  "changed",
			  G_CALLBACK (option_menu_changed_cb),
			  condition_data);
  
	gtk_option_menu_set_history (GTK_OPTION_MENU (row->variable), 0);
	gtk_widget_show (row->variable);
  
	gtk_box_pack_start (GTK_BOX (hbox), row->variable, FALSE, FALSE, 0);
	gtk_size_group_add_widget (priv->size_group, row->variable);
	update_option_menu (dialog, row->variable, row);

	gtk_box_pack_start (GTK_BOX (hbox), sub_hbox, FALSE, FALSE, 0);
	gtk_widget_show (sub_hbox);

	row->remove_button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (row->remove_button,
			  "clicked",
			  G_CALLBACK (remove_button_clicked_cb),
			  dialog);
	gtk_widget_show (row->remove_button);
	gtk_box_pack_end (GTK_BOX (hbox), row->remove_button, FALSE, FALSE, 0);

	gtk_size_group_add_widget (priv->size_group_button, row->remove_button);

	return hbox;
}

static void
setup_option_menu (GtkWidget             *option_menu,
		   const OptionMenuEntry *options,
		   int                    length,
		   gboolean               activate_first)
{
	GtkWidget *menu;
	GtkWidget *menu_item;
	int        i;

	menu = gtk_menu_new ();
	gtk_widget_show (menu);
    
	for (i = 0; i < length; i++) {
		menu_item = gtk_menu_item_new_with_label (_(options[i].label));
		g_object_set_data (G_OBJECT (menu_item),
				   "value",
				   GINT_TO_POINTER (options[i].value));
		g_object_set_data (G_OBJECT (menu_item),
				   "type",
				   GINT_TO_POINTER (options[i].type));
		gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
		gtk_widget_show (menu_item);
	}

	if (activate_first) {
		gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), 0);
	}
	
	gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
}

static GtkWidget*
create_option_menu (const OptionMenuEntry *options,
		    int                    length,
		    gboolean               activate_first)
{
	GtkWidget *menu;
	GtkWidget *option_menu;
  
	menu = gtk_menu_new ();
	gtk_widget_show (menu);
    
	option_menu = gtk_option_menu_new ();

	setup_option_menu (option_menu, options, length, activate_first);
  
	return option_menu;
}

static GtkWidget*
option_menu_get_active_child (GtkWidget *option_menu)
{
	GtkWidget *menu;
	GList     *children;
	int        pos;
	GtkWidget *child;
  
	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (option_menu));
	children = gtk_container_get_children (GTK_CONTAINER (menu));

	pos = gtk_option_menu_get_history (GTK_OPTION_MENU (option_menu));
	child = g_list_nth (children, pos)->data;
	g_assert (GTK_IS_WIDGET (child));
  
	return child;
}

static int
get_child_pos (GtkWidget *parent,
	       GtkWidget *child)
{
	GList *children, *pchild;
	int    i;

	g_assert (GTK_IS_CONTAINER (parent));
	g_assert (GTK_IS_WIDGET (child));
  
	children = gtk_container_get_children (GTK_CONTAINER (parent));

	g_assert (children->data != NULL);

	for (i = 0, pchild = children; pchild; pchild = pchild->next, ++i) {
		if (pchild->data == child) {
			return i;
		}
	}
  
	g_assert_not_reached ();
	return -1;
}

static int
option_menu_get_value (GtkWidget *widget)
{
	GtkWidget *menu_item;
  
	menu_item = option_menu_get_active_child (widget);

	return GPOINTER_TO_INT (g_object_get_data (G_OBJECT (menu_item), "value"));
}

static void
option_menu_set_value (GtkWidget *widget, int value)
{
	GtkWidget *menu_item;
	GtkWidget *menu;
	gpointer   data;
	int        item_value;
	GList     *list, *children;
	int        i;

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (widget));
	children = gtk_container_get_children (GTK_CONTAINER (menu));
  
	for (list = children, i = 0; list; list = list->next, i++) {
		menu_item = list->data;
		data = g_object_get_data (G_OBJECT (menu_item), "value");
		item_value = GPOINTER_TO_INT (data);
		if (value == item_value) {
			gtk_option_menu_set_history (GTK_OPTION_MENU (widget), i);
			break;
		}
	}
}

static int
row_get_constant_type (ConditionRow *row)
{
	GtkWidget *widget;

	widget = option_menu_get_active_child (row->variable);

	return GPOINTER_TO_INT (g_object_get_data (G_OBJECT (widget), "type"));
}

static void
add_row_at_pos (SmartPlaylistDialog *dialog,
		int                  pos)
{
	SmartPlaylistDialogPriv *priv;
	GtkWidget               *hbox;

	priv = dialog->priv;

	hbox = GTK_WIDGET (create_row (dialog));
	gtk_widget_show (hbox);

	gtk_box_pack_start (GTK_BOX (priv->vbox), hbox, FALSE, FALSE, 2);
	gtk_box_reorder_child (GTK_BOX (priv->vbox), hbox, pos);
}

static void
remove_row_from_pos (SmartPlaylistDialog *dialog,
		     int                  pos)
{
	SmartPlaylistDialogPriv *priv;
	GList                   *children;
  
	priv = dialog->priv;

	children = gtk_container_get_children (GTK_CONTAINER (priv->vbox));
	gtk_container_remove (GTK_CONTAINER (priv->vbox), g_list_nth (children, pos)->data);
  
	priv->rows = g_list_delete_link (priv->rows, g_list_nth (priv->rows, pos));  
}

static void
add_button_clicked_cb (GtkWidget           *button,
		       SmartPlaylistDialog *dialog)
{
	GtkWidget *hbox;
	int        pos;

	hbox = button->parent;
  
	pos = get_child_pos (hbox->parent, hbox);
	add_row_at_pos (dialog, pos);
}

static void
remove_button_clicked_cb (GtkWidget           *button,
			  SmartPlaylistDialog *dialog)
{
	GtkWidget *hbox;
	int        pos;

	hbox = button->parent;
  
	pos = get_child_pos (hbox->parent, hbox);
	remove_row_from_pos (dialog, pos);
}

static void
option_menu_changed_cb (GtkWidget     *option_menu,
			ConditionData *data)
{
	SmartPlaylistDialogPriv *priv;
	GtkWidget               *menu_item;
  
	priv = data->dialog->priv;
  
	menu_item = option_menu_get_active_child (option_menu);

	/* Only call update_option_menu if the child actually changed. */
	if (menu_item != priv->active_child) {
		update_option_menu (data->dialog, option_menu, data->row);
	}
}

static void
int_option_changed_cb (GtkWidget    *option_menu,
		       ConditionRow *row)
{
	int value;
  
	value = option_menu_get_value (option_menu);
  
	if (value == EXPR_OP_RANGE) {
		row->label = GTK_WIDGET (gtk_label_new (_("to")));
		gtk_widget_show (row->label);
    
		gtk_box_pack_start (GTK_BOX (row->hbox), row->label,
				    FALSE, FALSE, PADDING);
            
		row->value2 = GTK_WIDGET (gtk_entry_new ());
		gtk_entry_set_width_chars (GTK_ENTRY (row->value2), 6);
		gtk_widget_show (row->value2);
		gtk_box_pack_start (GTK_BOX (row->hbox), row->value2, FALSE, FALSE, 0);
            
		gtk_entry_set_width_chars (GTK_ENTRY (row->value1), 6);
	} else {
		int        i;
		GList     *children;
		GtkWidget *child;

		children = gtk_container_get_children (GTK_CONTAINER (row->hbox));
		
		for (i = 2; i < g_list_length (children); i++) {
			child = g_list_nth (children, i)->data;
			if (!GTK_IS_OPTION_MENU (child)) {
				gtk_container_remove (GTK_CONTAINER (row->hbox), child);
			}
		}
      
		gtk_entry_set_width_chars (GTK_ENTRY (row->value1), 10);
	}
}

static void
update_option_menu (SmartPlaylistDialog *dialog,
		    GtkWidget           *option_menu,
		    ConditionRow        *row)
{
	SmartPlaylistDialogPriv *priv;
	GList                   *children, *l;
	ConstantType             constant_type;
	const OptionMenuEntry   *options;
	int                      length;

	priv = dialog->priv;

	priv->active_child = option_menu_get_active_child (option_menu);
  
	children = gtk_container_get_children (GTK_CONTAINER (row->hbox));
  
	for (l = children; l; l = l->next) {
		gtk_container_remove (GTK_CONTAINER (row->hbox), l->data);
	}
						   
	constant_type = row_get_constant_type (row);
	if (constant_type == CONSTANT_TYPE_INT || constant_type == CONSTANT_TYPE_SIZE) {
		options = int_values;
		length = G_N_ELEMENTS (int_values);
	}
	else if (constant_type == CONSTANT_TYPE_STRING) {
		options = string_values;
		length = G_N_ELEMENTS (string_values);      
	}
	else if (constant_type == CONSTANT_TYPE_DATE) {
		options = date_values;
		length = G_N_ELEMENTS (date_values);
	} else {
		options = NULL;
		length = 0;
		g_assert_not_reached ();
	}
  
	row->operator = GTK_WIDGET (create_option_menu (options, length, TRUE));

	gtk_size_group_add_widget (priv->size_group_sub, row->operator);
	gtk_widget_show (row->operator);
	
	gtk_box_pack_start (GTK_BOX (row->hbox),
			    row->operator, FALSE, FALSE, PADDING);
	
	row->value1 = GTK_WIDGET (gtk_entry_new ());
	gtk_box_pack_start (GTK_BOX (row->hbox), row->value1, FALSE, FALSE, 0);
	gtk_widget_show (row->value1);

	if (constant_type == CONSTANT_TYPE_INT || constant_type == CONSTANT_TYPE_SIZE) {
		g_signal_connect (row->operator,
				  "changed",
				  G_CALLBACK (int_option_changed_cb),
				  row);
	} else {
		row->value2 = NULL;
		row->label = NULL;
	}
	
	if (constant_type == CONSTANT_TYPE_DATE) {
		row->unit = create_option_menu (date_sub_values,
						G_N_ELEMENTS (date_sub_values),
						FALSE);
		option_menu_set_value (row->unit, UNIT_DAYS);
		
		row->unit = GTK_WIDGET (row->unit);
		gtk_box_pack_start (GTK_BOX (row->hbox), row->unit, FALSE, FALSE, PADDING);
		gtk_widget_show (row->unit);
		gtk_entry_set_width_chars (GTK_ENTRY (row->value1), 5);
		
		return;
	}
	else if (constant_type == CONSTANT_TYPE_SIZE) {
		row->unit = create_option_menu (size_values,
						G_N_ELEMENTS (size_values),
						TRUE);
		row->unit = GTK_WIDGET (row->unit);      
		gtk_box_pack_start (GTK_BOX (row->hbox), row->unit, FALSE, FALSE, 0);
		gtk_widget_show (row->unit);
		gtk_entry_set_width_chars (GTK_ENTRY (row->value1), 5);
		
		return;
	} else {
		row->unit = NULL;
	}
	
	gtk_box_set_child_packing (GTK_BOX (row->hbox), row->value1, TRUE, TRUE, 0, GTK_PACK_START);
	gtk_entry_set_width_chars (GTK_ENTRY (row->value1), 10);
}

static void
smart_playlist_dialog_update_playlist (SmartPlaylistDialog *dialog)
{
	SmartPlaylistDialogPriv *priv;
	GList                   *list;
	ConditionRow            *row;
	ExprOp                   operator;
	Variable                 variable;
	ConstantType             constant_type;
	const char              *value1, *value2;
	ExprOp                   match_type;
	Expr                    *v, *c;
	GList                   *exprs;

	priv = dialog->priv;

	exprs = NULL;
	match_type = option_menu_get_value (priv->option_match);

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv->check_match))) {
		for (list = priv->rows; list; list = list->next) {
			row = list->data;
        
			variable = option_menu_get_value (row->variable);
			v = expr_new_variable (variable);

			value1 = gtk_entry_get_text (GTK_ENTRY (row->value1));
			if (row->value2) {
				value2 = gtk_entry_get_text (GTK_ENTRY (row->value2));
			} else {
				value2 = NULL;
			}
			
			operator = option_menu_get_value (row->operator);
    
			constant_type = row_get_constant_type (row);
			if (constant_type == CONSTANT_TYPE_STRING) {
				c = expr_new_constant (constant_string_new (value1));
			}
			else if (constant_type == CONSTANT_TYPE_INT) {
				if (operator == EXPR_OP_RANGE) {
					c = expr_new_constant (constant_range_new (atoi (value1),
										   atoi (value2)));
				} else {
					c = expr_new_constant (constant_int_new (atoi (value1)));
				}
			}
			else if (constant_type == CONSTANT_TYPE_SIZE) {
				int unit = option_menu_get_value (row->unit);
				int value = atoi (value1);
	
				if (unit == SIZE_KB) {
					value *= 1024;
				}
				else if (unit == SIZE_MB) {
					value *= 1048576;
				}
				else if (unit == SIZE_GB) {
					value *= 1073741824;
				}
				
				c = expr_new_constant (constant_int_new (value));
			}
			else if (constant_type == CONSTANT_TYPE_DATE) {
				int unit = option_menu_get_value (row->unit);
				c = expr_new_constant (constant_date_new (atol (value1), unit));
			} else {
				g_warning ("Constant type: %s is currently unsupported",
					   constant_type_to_string (constant_type));
				continue;
			}

			exprs = g_list_append (exprs, expr_new_binary (operator, v, c));
		}
	}

	source_smart_playlist_set_exprs (priv->playlist, exprs);
	source_smart_playlist_set_op (priv->playlist, match_type);
	
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv->check_limit))) {
		Limiter *limiter;
		
		limiter = limiter_new (option_menu_get_value (priv->option_selected),
				       option_menu_get_value (priv->option_limit),
				       atoi (gtk_entry_get_text (GTK_ENTRY (priv->entry_limit))));

		source_smart_playlist_set_limiter (priv->playlist, limiter);
	} else {
		source_smart_playlist_set_limiter (priv->playlist, NULL);
	}		

	source_set_name (SOURCE (priv->playlist),
			 gtk_entry_get_text (GTK_ENTRY (priv->name_entry)));
}

static void
smart_playlist_dialog_set_playlist (SmartPlaylistDialog *dialog,
				    SourceSmartPlaylist *playlist)
{
	SmartPlaylistDialogPriv *priv;
	Limiter                 *limiter;
	LimiterUnit              limit_unit;
	LimiterVariable          limit_variable;
	char                    *tmp;
	GList                   *l, *exprs, *rows;
	int                      pos;
  
	priv = dialog->priv;

	priv->playlist = playlist;
	
	gtk_entry_set_text (GTK_ENTRY (priv->name_entry), source_get_name (SOURCE (playlist)));

	option_menu_set_value (priv->option_match, source_smart_playlist_get_op (playlist));
	
	exprs = g_list_copy (source_smart_playlist_get_exprs (playlist));
	update_match (dialog, exprs != NULL);

	limiter = source_smart_playlist_get_limiter (playlist);
	if (limiter) {
		limit_unit = limiter_get_unit (limiter);
		if (limit_unit != LIMITER_UNIT_NONE) {
			update_limit (dialog, TRUE);

			option_menu_set_value (priv->option_limit, limit_unit);
			
			tmp = g_strdup_printf ("%ld", limiter_get_value (limiter));
			gtk_entry_set_text (GTK_ENTRY (priv->entry_limit), tmp);
			g_free (tmp);
		}

		limit_variable = limiter_get_variable (limiter);
		if (limit_variable != LIMITER_VARIABLE_NONE) {
			option_menu_set_value (priv->option_selected, limit_variable);
		}
	}
	
	rows = priv->rows;
	for (l = exprs, pos = 0; l; l = l->next, pos++) {
		Expr         *e, *e1,*e2;
		Constant     *constant;
		Variable      variable;
		ConditionRow *row;

		e =  l->data;
		
		e1 = e->v.binary.expr_1;
		e2 = e->v.binary.expr_2;
      
		if (e1->op == EXPR_OP_CONSTANT && e2->op == EXPR_OP_VARIABLE) {
			constant = e1->v.constant;
			variable = e2->v.variable;
		}
		else if (e1->op == EXPR_OP_VARIABLE && e2->op == EXPR_OP_CONSTANT) {
			variable = e1->v.variable;
			constant = e2->v.constant;
		} else {
			variable = VARIABLE_NONE;
			constant = NULL;
			g_assert_not_reached ();
		}
      
		if (rows) {
			row = rows->data;
			rows = rows->next;
		} else {
			add_row_at_pos (dialog, pos);
			row = g_list_last (priv->rows)->data;
		}
	
		option_menu_set_value (row->variable, variable);
		option_menu_set_value (row->operator, e->op);
      
		if (constant->type == CONSTANT_TYPE_DATE) {
			int   value;
			char *tmp;

			value = constant_get_int_value (constant);
			
			option_menu_set_value (row->unit, constant_get_unit (constant));

			tmp = g_strdup_printf ("%d", value);
			gtk_entry_set_text (GTK_ENTRY (row->value1), tmp);
			g_free (tmp);
		}
		else if (constant->type == CONSTANT_TYPE_RANGE) {
			int   start, end;
			char *tmp;
	  
			constant_get_range_values (constant, &start, &end);
	  
			tmp = g_strdup_printf ("%d", start);
			gtk_entry_set_text (GTK_ENTRY (row->value1), tmp);
			g_free (tmp);
	  
			tmp = g_strdup_printf ("%d", end);
			gtk_entry_set_text (GTK_ENTRY (row->value2), tmp);
			g_free (tmp);
		} else {
			char *str;

			str = constant_get_string_value (constant);
			gtk_entry_set_text (GTK_ENTRY (row->value1), str);
			g_free (str);
		}
	}

	g_list_free (exprs);
}

gboolean
smart_playlist_dialog_run (GtkWindow           *parent,
			   SourceSmartPlaylist *playlist)
{
	GtkWidget *dialog;
	int        response;

	dialog = smart_playlist_dialog_new ();
	if (parent) {
		gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	}

	gtk_window_set_title (GTK_WINDOW (dialog), _("Smart Playlist"));
	
	if (playlist) {
		smart_playlist_dialog_set_playlist (SMART_PLAYLIST_DIALOG (dialog), playlist);
	}
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	
	if (response == GTK_RESPONSE_OK) {
		smart_playlist_dialog_update_playlist (SMART_PLAYLIST_DIALOG (dialog));

		gtk_widget_destroy (dialog);
		return TRUE;
	} else {
		gtk_widget_destroy (dialog);
		return FALSE;
	}
}
