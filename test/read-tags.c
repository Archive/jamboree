/*
 * Copyright (C) 2004-2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA
 *
 */

#include <gst/gst.h>

#include <config.h>
#include "../src/string-utils.h"
#include "../src/tag-reader.h"


static void
usage (gchar *prog)
{
        g_print ("usage: %s MEDIA-FILE\n", prog);
        exit (1);
}

static void
song_read (Song       *song,
           gpointer    user_data)
{
        _song_print (song);
}

static gboolean
song_found (const char *path,
            gpointer    user_data)
{
        /* g_print ("Adding song %s...\n", path); */
	return TRUE;
}

static gboolean
progress (const char *path,
          gpointer    user_data)
{
	return TRUE;
}

int
main (int argc, char **argv)
{
        gst_init (&argc, &argv);
        shared_string_init ();

	int i;

        if (argc < 2) {
                usage (argv[0]);
        }

	gst_alloc_trace_set_flags_all (GST_ALLOC_TRACE_LIVE);
	if (!gst_alloc_trace_available ()) {
		g_warning ("Trace not available (recompile with trace enabled).");
	}
	gst_alloc_trace_print_live ();
	g_print ("\n");

	for (i = 1; i < argc; ++i) {
		if (!tag_reader_read_path (argv[i],
					   song_found,
					   song_read,
					   progress,
					   NULL)) {
			g_print ("Error reading %s\n", argv[i]);
			return 1;
		}
					   
		g_print ("\n");
		g_print ("gst_alloc_trace_print_live ():\n");
		g_print ("Successfully read %s\n", argv[i]);
        }
	gst_alloc_trace_print_live ();

	return 0;
}
